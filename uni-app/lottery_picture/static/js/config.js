// #ifdef H5
const API = location.hostname == 'localhost' ? '/api/' : location.protocol + '//' + location.hostname + '/';
// #endif
// #ifdef APP-PLUS
const API = 'https://x00100.xb774.com';
// #endif
module.exports = {
	API,
};