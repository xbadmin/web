import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
import uView from '@/uni_modules/uview-ui'
import common from './static/js/common'
import httpRequest from './static/js/httpRequest'
import config from './static/js/config'

Vue.use(uView)

Vue.config.productionTip = false

Vue.prototype.$common = common;
Vue.prototype.$config = config;
Vue.prototype.$Request = httpRequest;


App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif