import common from './common.js'
import config from './config.js'

const apiUrl = config.API

module.exports = {
	post:function(url,data,loading,header = 'application/x-www-form-urlencoded'){
		url = apiUrl + url
		if(loading)common.loading(loading)
		return new Promise((success,error) => {
			uni.request({
				url: url,
				data:data,
				method:'POST',
				header:{
					"content-type":header,
					"authorization":uni.getStorageSync('token') || '',
				},
				success:function(result){
					if(loading)common.loading(false)
					if(result.data.msg == '登陆超时,请重新登陆系统'){
						uni.setStorageSync('token', '');
						uni.navigateTo({
							url: '/pages/login/login'
						})
					} else {
						success.call(self, result.data)
					}
				},
				fail:function(e){
					if(loading)common.loading(false)
					console.log(e)
				}
			})
		})
	},
	get:function(url,data,loading,header = 'application/x-www-form-urlencoded'){
		url = apiUrl + url
		if(loading)common.loading(loading)
		return new Promise((success,error) => {
			uni.request({
				url: url,
				data:data,
				method:'GET',
				header:{
					"content-type":header,
					"authorization":uni.getStorageSync('token') || '',
				},
				success:function(result){
					if(loading)common.loading(false)
					if(result.data.msg == '登陆超时,请重新登陆系统'){
						uni.setStorageSync('token', '');
						uni.navigateTo({
							url: '/pages/login/login'
						})
					} else {
						success.call(self, result.data)
					}
				},
				fail:function(e){
					if(loading)common.loading(false)
					console.log(e)
				}
			})
		})
	},
}