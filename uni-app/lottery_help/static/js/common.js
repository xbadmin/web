// 验证返回结果
module.exports = {
	loading: function(onOff) {
		if (onOff) {
			uni.showLoading();
		} else {
			uni.hideLoading()
		}
	},
	toPath: function(path) {
		uni.navigateTo({
			url: path
		})
	},
	back() {
		const pages = getCurrentPages();
		if (pages.length === 2) {
			uni.navigateBack({
				delta: 1
			});
		} else if (pages.length === 1) {
			uni.reLaunch({
				url: '/pages/tabs/index',
			})
		} else {
			uni.navigateBack({
				delta: 1
			});
		}
	},
	// 解析毫秒
	getMyDate: function(str, status = 1) {
		let oDate = new Date(str),
			oYear = oDate.getFullYear(),
			oMonth = oDate.getMonth() + 1,
			oDay = oDate.getDate(),
			oHour = oDate.getHours(),
			oMin = oDate.getMinutes(),
			oSen = oDate.getSeconds(),
			oTime = oYear + '-' + this.getzf(oMonth) + '-' + this.getzf(oDay) + ' ' + this.getzf(oHour) + ':' +
			this.getzf(oMin) + ':' + this.getzf(oSen); //最后拼接时间
		if (status === 2) {
			oTime = oYear + '-' + this.getzf(oMonth) + '-' + this.getzf(oDay)
		}
		return oTime;
	},
	getzf: function(num) {
		if (parseInt(num) < 10) {
			num = '0' + num;
		}
		return num;
	},

	validateCodex: function(data) {
		uni.hideToast();
		// 用于跳出循环
		let circleIs = true
		let xss = /xss/i,
			script = /script/i,
			blank = /\s/
		if (xss.test(data) || script.test(data)) {
			uni.showToast({
				title: '包含敏感字符，请重新输入!',
				icon: 'none',
				duration: 2000
			});
			validateIs = false
			circleIs = false
		} else if (blank.test(data)) {
			uni.showToast({
				title: '请勿包含空格等!',
				icon: 'none',
				duration: 2000
			});
			validateIs = false
			circleIs = false
		} else if (!data) {
			uni.showToast({
				title: '请输入内容!',
				icon: 'none',
				duration: 2000
			});
			validateIs = false
			circleIs = false
		} else {
			validateIs = true
		}
		return circleIs
	},
	// 前端非空,xss,特殊字符验证
	dataSafeValidate: function(type, data) {
		let that = this
		switch (type) {
			case 1:
				//字符串类型
				if (!that.validateCodex(data)) return false
				break;
			case 2:
				// 数组类型
				$.each(data, function(index, item) {
					if (!that.validateCodex(item)) return false
				});
				break;
			default:
				console.log('暂无可执行语句')
		}
		return validateIs
	},
	splitNum:function(num, i){
		if (!num) {
			num = 0
		}
		if (!i) {
			i = 2
		}
		num = parseFloat(num).toFixed(10) // 补0
		let str = num.toString()
		let len = 0,
			num1 = 0,
			num2 = 0;
		len = str.indexOf('.')
		num1 = str.substring(0, len)
		num2 = str.substring(len, len + (i + 1))
		let returnNum = parseFloat(num1 + num2)
		return returnNum
	},
}