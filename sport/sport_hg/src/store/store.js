import Vue from 'vue'//引入vue
import Vuex from 'vuex'//引入vuex
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        apiUrl: location.hostname == 'localhost' ? '/api/' : location.protocol + '//' + location.hostname + '/',
        isLogin: false,
        userInfo: '',
        itemList: [],
        itemList2: [],
        gameList: [],
        rType: 1,
    },
    plugins: [createPersistedState({storage: window.sessionStorage})],
    mutations: {
        pushIsLogin(state,e){
            state.isLogin = e
        },
        pushUserInfo(state,e){
            state.isLogin = e
        },
        pushItemList(state,e){
            state.itemList = e
        },
        pushItemList2(state,e){
            state.itemList2 = e
        },
        pushGameList(state,e){
            state.gameList = e
        },
        pushRType(state,e){
            state.rType = e
        },
    }
});


export default store
