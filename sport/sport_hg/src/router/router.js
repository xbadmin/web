import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/index")
const sportDetailT = ()=>import("@/components/sport/football/sportDetailT")
const sportDetailR = ()=>import("@/components/sport/football/sportDetailR")
const sportDetailZ = ()=>import("@/components/sport/football/sportDetailZ")
const sportDetailC = ()=>import("@/components/sport/football/sportDetailC")
const ballDetailT = ()=>import("@/components/sport/basketball/ballDetailT")
const ballDetailR = ()=>import("@/components/sport/basketball/ballDetailR")
const ballDetailZ = ()=>import("@/components/sport/basketball/ballDetailZ")
const ballDetailC = ()=>import("@/components/sport/basketball/ballDetailC")

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: true,requireAuth:false},}, // 首页
        {path: '/sportDetailR', name:'sportDetailR', component:sportDetailR, meta: {keepAlive: false,requireAuth:false},}, // 皇冠体育-足球-滚球
        {path: '/sportDetailT', name:'sportDetailT', component:sportDetailT, meta: {keepAlive: false,requireAuth:false},}, // 皇冠体育-足球-今日
        {path: '/sportDetailZ', name:'sportDetailZ', component:sportDetailZ, meta: {keepAlive: false,requireAuth:false},}, // 皇冠体育-足球-早盘
        {path: '/sportDetailC', name:'sportDetailC', component:sportDetailC, meta: {keepAlive: false,requireAuth:false},}, // 皇冠体育-足球-综合过关
        {path: '/ballDetailT', name:'ballDetailT', component:ballDetailT, meta: {keepAlive: false,requireAuth:false},}, // 皇冠体育-篮球-今日
        {path: '/ballDetailR', name:'ballDetailR', component:ballDetailR, meta: {keepAlive: false,requireAuth:false},}, // 皇冠体育-篮球-今日
        {path: '/ballDetailZ', name:'ballDetailZ', component:ballDetailZ, meta: {keepAlive: false,requireAuth:false},}, // 皇冠体育-篮球-早盘
        {path: '/ballDetailC', name:'ballDetailC', component:ballDetailC, meta: {keepAlive: false,requireAuth:false},}, // 皇冠体育-篮球-综合过关
    ]
})

