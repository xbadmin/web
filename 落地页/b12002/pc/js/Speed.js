/*! 网址测速组件 Rui */
var Speed={
    ping:1,
    list:[],
    init:function(urlList,cb){
        var me=this;
        me.cb=cb;
        me.ping=1;
        me.list=[];
        me.urlList=urlList;
        me.t=setInterval(function(){me.ping++},1);
		// 使用JQ方法删除div
		$('.mspics').remove();
        for(var i=0;i<urlList.length;i++){
            var div=document.createElement("div");
            div.width=1;
            div.height=1;
            div.style.display='none';
			div.setAttribute("class", "mspics");
            document.body.appendChild(div)
            div.innerHTML="<img src="+urlList[i]+""+Math.random()+"/ width='1' height='1' onerror=Speed.autotest("+i+") style='display:none'>";
        }
        return this;
    },
    autotest:function(i){ 
        this.list[i]=this.ping;
        this.cb(i,this.ping);
        if(this.list.length==this.urlList.length&&this.isarr(this.list)){
            this.isgo=false;
            clearInterval(this.t);
        }
    },
    isarr(arr){
        for(var i=0;i<arr.length;i++){
            if(!arr[i]){
                return false;
            }
        }
        return true;
    }
}