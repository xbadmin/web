import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import ElementUI from 'element-ui';
import VueResource from 'vue-resource'
import 'element-ui/lib/theme-chalk/index.css';
import common from './assets/js/common.js'
import store from './store/store.js'
import VueClipboard from 'vue-clipboard2'
import { CountDown ,Toast } from 'vant';
import VueCookies from 'vue-cookies'


Vue.config.productionTip = false
VueClipboard.config.autoSetContainer = true

Vue.use(VueCookies)
Vue.use(ElementUI);
Vue.use(VueResource);
Vue.use(VueClipboard)
Vue.use(CountDown,Toast)

router.beforeEach((to, from, next) => {
  // console.log(store.state.stationInfo)
  // if ('content' in store.state.stationInfo){
  //   switch (store.state.stationInfo.content.station.floder){
  //     case 'x00286':
  //       common.addFavicon('https://tpxb.me/x00286/6.ico')
  //       break;
  //   }
  // }

  if(to.meta.requireAuth){
    if(store.state.isLogin){
      next()
    } else {
      next({path: '/login', query:{ redirect: to.fullPath}})
    }
  } else {
    next()
  }
})

// 焦点事件
Vue.directive('focus', {
  // 当被绑定的元素插入到 DOM 中时……
  inserted: function (el) {
    // 聚焦元素
    el.focus()
  }
})

new Vue({
  data: function(){
    return {
      //  全局变量  刷新后初始化
      common: common, // common方法
      url:'https://gosick.xb336.com/'
    }
  },
  render: h => h(App),
  router,
  store,
}).$mount('#app')
