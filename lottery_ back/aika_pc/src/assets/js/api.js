const native = 'native/'
const lotteryUrl = {
    getLotGamePlays: native + 'getLotGamePlays.do', // 获取彩种小类详情
    getCountDown: native + 'getCountDown.do', // 获取彩种小类详情
    openResultDetail: native + 'open_result_detail.do', // 获取彩种小类详情
    getLoctterys: native + 'getLoctterys.do', // 获取彩种小类详情
    getLotteryOrder: native + 'get_lottery_order.do', // 获取彩种小类详情
    dolotBet: native + 'dolotBet.do', // 彩票投注接口
    getZodiacsToHaoMa: native + 'getZodiacsToHaoMa.do', // 六合彩生肖
}
module.exports = lotteryUrl