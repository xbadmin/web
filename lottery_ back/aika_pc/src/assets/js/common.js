import $ from  'jquery'
import store from '../../store/store.js'
import router from '../../router/router'
import { Message } from 'element-ui';

// 验证返回结果
let validateIs = true
export default{
    loca_url:function(url,openType){
        if(openType){
            location.href = url
        } else {
            window.open('openType','_blank')
        }
    },
    loading:function (onOff,dom) {
        let html  = '<div class="loading"><img src="data:image/gif;base64,R0lGODlhQgAGAPUAANs2Z+FZgdgmWuyWr/O8zfXI1fbN2eNlie2dtOh+nOJfhd5JdfTD0dozY9ovYfO3x+BUfuuQqt9PeNgqXeqLptksX+mFovfS3NksXtgpXeuRq+qKpt5KdeNkitkvYeiFot9PeeiFoffT3d9OefK3x/fS3eBVfeh+nfTC0eJehO2dtdoyZOqKp9DQ0P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUP8/eHBhY2tldCBiZWdpbj0i72lkOjU0NEYyQzc4QuHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEBQoALgAsAAAAAEIABgAABo/Ai6HAIDxcSBdiEKFYEknXQRGASBZRQMNRmQiirbAYLA6Ty0KiMbpsPqPT6jW77X6TZTM+fxani0dJbU5QSXFWWElaXF59ekh5LY4tf2uCTIRwVIh0jHeQfHtlkyVDgGxMGx+FSIcgiUiLGBmfLpGTkyJDKAQkUSoDGiwhJ1EdKQEmIxxZKx6ztbeiY9NhQQAh+QQFCgAuACwAAAAALgAGAAAGfMCWcOgqui6GAoPwMLoQgwjFknAeFAGIZOEENByViWBINCKVTCdUSrVitVyjFywmC53nZdO4nlaNV1lbXV9hY3Z4SXpqUX5ugnFFc4Z2LU4liml8URsff0WBECCRLnMYGYdkTiJJKAQkTioDGiwhJ04dKQEmIxxdKx6nAkEAIfkEBQoALgAsCgAAAC4ABgAABnzAlnDoKrouhgKD8DC6EIMIxZJwHhQBiGThBDQclYlgSDQilUwnVEq1YrVcoxcsJgud52XTuJ5WjVdZW11fYWN2eEl6alF+boJxRXOGdi1OJYppfFEbH39FgRAgkS5zGBmHZE4iSSgEJE4qAxosISdOHSkBJiMcXSsepwJBACH5BAUKAC4ALBQAAAAuAAYAAAZ8wJZw6Cq6LoYCg/AwuhCDCMWScB4UAYhk4QQ0HJWJYEg0IpVMJ1RKtWK1XKMXLCYLnedl07ieVo1XWVtdX2FjdnhJempRfm6CcUVzhnYtTiWKaXxRGx9/RYEQIJEucxgZh2ROIkkoBCROKgMaLCEnTh0pASYjHF0rHqcCQQAh+QQFCgAuACwAAAAAQgAGAAAGh0BAw1GZCFzIpHLJTLaeUOXFUGAQHkrEIEKxJJQHRQAiWQiJxqZ6DY0mp9Vrdtv9JsPj8rl4XPuVbU9SVFZYSVpcXmBiZGZDfH+RLoEtg3GGSIh1i3mODhgZfZJsgUolhHKHWxsfdkh4ECBmKx6goqNNlEoiVCgEJEoqAxosISdKHSkBJiMcQQAh+QQFCgAuACwAAAAAQgAGAAAGhsCDIgCRLFxIF6DhqEwEyah0Om1Zr9GLocAgPKKIQYRiSQiJxuiy+aS631dsUsv1gsVk87B4TK6dUG+CUXFWWVtdX0lhY2VnfGpMgIOULoUth3WKSIx5jyB9SGsYGYGVcIVRJYh2i2IbHwkdKQEmIxxqKx6kpqdUl1EiWygEJFEqAxosISdBACH5BAUKAC4ALAAAAABCAAYAAAaGQMQgQrEkXEjXQRGASBZJF6DhqEwE0ax2i2x5v9GLocAgPIREY3TZfEan1St3vv2Ck2KyGV08JtlOUElwVlh0h112LWFjZWdDfWtMgW9UhYiIiot4jXtDGx9+SIAggkhwGBmGmHOaUSWdDyoDGiwhJ1EdKQEmIxxvKx6pq6x1ilEiYygEJEEAOy8qICB8eEd2MDB8Yzg0ZjBiMGEwYTgwYWIyYTg0NTkwMDg2NjY0ZTY3MDAgKi8="></div>'
        if(onOff){
            $(''+dom+'').css('position','relative').append(html)
        } else {
            $(''+dom+'').find('.loading').remove()
        }
    },
    // 开奖号码颜色分类
    resultNumberColor:function (number) {
        switch (number) {
            case '01':
            case '13':
            case '25':
            case '37':
            case '49':
                return '#e6de00';
            case '00':
            case '02':
            case '14':
            case '26':
            case '38':
                return '#0092e0';
            case '03':
            case '15':
            case '27':
            case '39':
                return '#4b4b4b';
            case '04':
            case '16':
            case '28':
            case '40':
                return '#ff7501';
            case '05':
            case '17':
            case '29':
            case '41':
                return '#17e1e1';
            case '06':
            case '18':
            case '30':
            case '42':
                return '#5333ff';
            case '07':
            case '19':
            case '31':
            case '43':
                return '#bfbfbf';
            case '08':
            case '20':
            case '32':
            case '44':
                return '#ff2600';
            case '09':
            case '21':
            case '33':
            case '45':
                return '#790a01';
            case '10':
            case '22':
            case '34':
            case '46':
                return '#07be01';
            case '11':
            case '23':
            case '35':
            case '47':
                return '#f8a968';
            case '12':
            case '24':
            case '36':
            case '48':
                return '#455467';
            default:
                return  '#bfbfbf'
        }
    },
    // 快三筛子分类
    resultKsColor:function (number) {
        switch (number) {
            case '1':
                return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzVGMzYwRkU4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzVGMzYwRkY4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozNUYzNjBGQzhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozNUYzNjBGRDhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PmfiecUAAARtSURBVHja7FlLaxRBEK6enZmdbLIhD9cginhQowYNSEARRAheRHz8BBE86EFESbx4EM3Jgwe9+Qa96E3Rm2fBByJeNCqoIPiIRCJssjs7M23VzM6+nOnp3kQ3ge2ll52e7q6vq+urrq5lnHNYakWDJVjaoNugBUWPapycnAQk6OpUKjVmmuYBTdP6F1ZsNPnRJ3iu6761bfsaPt7AWhocHJQDjYDXGIb+MJfLDWUyGUDQ8nASvFHwmse2OY4zMjMzg/X3RpzrhLSmEeR4b2/PUEdHh//seR5+Mx88YwxrHBiIfJekddf1fBm0YJKRzWahVCodz+dnH2KHx1I2rev6PsMwK8+plE5tFdC0gMYaLKaZqvlzkzxNS1V2yjTT+JrtlSYidu4ngDSeADN19SkXkmEYhi/P83gos1fFe3BaMRLxvwCuLQFwLdS4pwA6WCmBbkUh4CQ/jtRaHMPJzlrmh3UD7ZupHy5EkFaVkKSKoFsf+Ync/aI9xkWHlCZ7ai362GM+ZerZK/j15j0w9PN9WzZC//CmBY1PhKBVLzSebcPTsQmYvHMX8tNf6YyEzoGVMHTkEIycG1M+30XyF0TT3HXh+ZkL8PLSRUiBBSZ0+e1z37/D04kJMDo7Yfj0sWYCEzUiVonA/d9xlcrM+4/w4dY9BNuBkAk086uFzyZ+3l65DbPfpirzJtV5XwJkzCT/+QvMTn1C/aYhi2C7Ud9Uu8hEEHr+4wco/Pi5YN5DbzYurpvEQrAsA1muIVCM2MrtNoYOeXDB0DEmN/RE0lVFMnXQKttFJ1f3hrWwZvNW4K9fYFjWg/oObLeIoKcR9vIdo5BZtcKHFj8frwHr/ds7IgXv1kAO1p8/Ccu7ctAPBViGwpfhuz78PdC3CtadPQFGd5buUgJblt9doXnIEoO8R8/+3ZC5fx3g4k0wnrwGjn5aHx2B7lOHwdg2jLcTV9qPJsnVRXZWP5ALicldB4zR7cB3jgAvFIMQM2MBw/CWO04FTGPISV6wcc6gjf9bTYdr4rwUSMyky3OgbdqukOBR0yeFEcITUUyaCKGRwqoEqw035W7tSqB5+QbOawbzOpdUf/PmDULq+zLGa4CK+lY9kjIRw0noghnjM8rvpfyLZL9ahTVBxJAscQPD9igSNd+XV3bQS1hlrKaD5Ikn9hgJ2yy2UR7ZFhJYBDxG05T1ccr5B5UwlUvEK0mL475Pb+oYL5VKlFdrSCMEHkB+MVGLYH8RuBEgySbgcYTUo49mzopFG6/yBUinrYqQ6O1kDYdGrbdQ1zaBLRQKPvDYtF1MPDFdLBZXhslBSp7MN1sqaxpzc3OAsn3QKPeXivd4YNv2UcdxwbbxeDaCBGRSPqJWuChD1LgI6kfEQ5m+SVLVNI0GP1JJ9V7AVe7Cbdpk28UKWMr6UBInCbgs2LAvAQ5TvWFazDTNy1FpXhERPyHwPZZljeNkB6vZSy7lSZJ3oz7HHZIdx3lY3+HzVQj+CYge3f4fsQ26Dbr15Y8AAwCaaa2V2cir4gAAAABJRU5ErkJggg==';
            case '2':
                return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAAACXBIWXMAAAsTAAALEwEAmpwYAAAFwWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDIgNzkuMTYwOTI0LCAyMDE3LzA3LzEzLTAxOjA2OjM5ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE5LTEwLTE1VDIxOjE2OjM5KzA4OjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxOS0xMC0xNVQyMToxNzo0MSswODowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxOS0xMC0xNVQyMToxNzo0MSswODowMCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo5NmZiMDUxMS04NDIxLWE5NDItYjM5YS1jZDQ5MjRiODJhYzEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzVGMzYxMDM4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDozNUYzNjEwMzhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozNUYzNjEwMDhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozNUYzNjEwMThEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIvPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDo5NmZiMDUxMS04NDIxLWE5NDItYjM5YS1jZDQ5MjRiODJhYzEiIHN0RXZ0OndoZW49IjIwMTktMTAtMTVUMjE6MTc6NDErMDg6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE4IChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5dGoALAAAF6ElEQVRYhe2ZTWwcRRaAv6r+mx8ntmOsxFLicEiGyJNIOeAbp+TCISciJP5u3Dh4V4qEZrVc4AJ7WnlPBA5whIAESCABUmLgFIRPYBCJOBAiZ9F4xdiOnZnp6q7aQ097etrd43ZkDJF4ktXVVeXXX1W99+pVjTDG8KCJ/KMB7kf+gt4veSCh7azKmzdvApzQWjeMMReklIeklEYIkasoaspu7//b9vZYZ+8pjNG/KRV+rHX4GvDTqVOnikEbYx7WWn+ilKq5rovnubiui+O4WJaFlBIhBFICSIQAIaK6JES/PAidfNdaE4YBWmuEEGitDysVPN9qtR5TSj0O/FwIWmvd6HQ6NceJgCuVCp5XwrZtpBRbgFIKQGwBpqGLi4fWIWEYYgyUy2Db1iPNZvNF4IV070yb1lpfMMbgOA6lUplSqYRlWaQ54hAfx3pjzLY/KLYPSGlh2w5SSkBQqVRwHPdCZt8cHeNCCBzHxnGcAcjd7kW76S+EwLKsCExKpJQTWf0yzSNWkATOQaLvXFE5tu/7lcishptWDrTpOVvvzaQB+/VJk4k/+MXCAteuXWN1dY0zZ07zxMWLTExkTlqmRCaSv0Q50WP7iHdyLCktlFJcfv113n/vfZrNJpubG3z04QdcuXKF+fn/MFOfKQQthGBYTpS7jmlIrdNK4pk3W1FjYWGBL778kpEDI0w8NMH4oUOUSiW++/Y7Xnn5ZdrtdkHo4e2FjS+pKGsWfN9ncXGRgwcPUq1W8TwP27KxLItKpcLi4iJfX79e9HN7AW22RYGkPQshCMOQVquF67rYjt0LkZGJWZbF2toaKysr+wmdv15RGIwc98CBAyilCIMQrXXPlQxaa6rVKmNjY3uAXBA63yn69Z7ncfbsWdbX12l3OvhdnzBUaG1ot9vUT59mdnZ2L5h3hjbGpJzSJNr6fQDOnz9PvV7nf80VWq0WG3fvsrm5yZEjR2g0GoyNjxeC2mlDyt1c0uAxsDEi07uDIKBardJoNKjPzPD5Z5+ztrbK6TNnePbZ5zhx8kQh4CIispZ+aWlpE6iMjY3heR6u6/a21cix4iwvK1HqO+D9izEGrUPu3PnvvWPHjlXT7bkz3U94IDaJ9A6YjNWxsxqje6tRfGParQy16Z0+ll6kPWbLlaE23U85ISv36PeLgONnlDv8flLIEdMSRRSIFmpwMFJatFotbv/yC13f5/Dhwxw9enRPB1IYOm3PxtA7bvUrbdtm8ZtveOPyZa5f/5rV1RbT09NcfPJJ5ubmtnLlfYNOSjoPEUJg2zY3btxgfn6eW7duUa5WMBiWl5f516uv0u10afyjsSfQBdZse0hMOmAc7oIg4OrVq4RaMzk5SblUxrYdSqUSruvx7rvv8P3S0v5A9wHj0DbQutWmlOL27duMjIzgui6u42BLiRASz/P49c6v8dVEIRm2Kw6BNgPlODpkKc4KdckjrTEmGvOuYmL+oTgXOgk4CGtS/aJNyHEcjh8/zsbGBr7vR9me1hij8X2fqakpHqnVdgGdLzuYh9lWzlo2Ywy2bXPu3Dksy2Kl2aR97x5hoGi3O3S7XZ565mlm6vXfFzqaweR7Zq+tUhAE1Go1/jY3x8xMnU6nw/r6OtPTx/jnSy9x6dKlXWANT/NyQ57WeiCP3p6iDsZuYwxhGPLo7CwnazWWl5cJlOKhyUmmpqZ2lX+EoR7qiEOhtdY5rdlbegw+OjrK6OhoYci0Dq3DoYPMNA8hhDbG4Pt+zqll+KzFAx7MFItJEATJnCdz1vKgf4wVtNttwjDsKdnumFlyv7/jKKUIw4Do24owDH7I6pdpHpZlveE4zqNKKbrdLkIIyuUytm3jODZak7g1jSR5Mo+fRezYGEMQqB6w3tK5vn4X31dvFoYG3iqVSid93/+7UsqJRh5g23bmdW6c8SXBsw4B8cX74AW8SUUqQ7frq3a7/W/g7d1AB8CLrut+DDwOjAdBYJRSOd33TATQAj4Fvspbqcwz4p9dHsgfiv6C3i95IKH/D8k/qS3QFJvKAAAAAElFTkSuQmCC';
            case '3':
                return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzVGRTRFMjM4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzVGRTRFMjQ4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozNUYzNjEwNDhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozNUYzNjEwNThEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PmGyIWAAAAZGSURBVHja7FlLTFRXGP7uvfOAYZhheKgLoTRQNVpDUi0UU1w0tETdKLqxNWgXNm1jVzUak9bWYuxOYjc0dcECEqtLbGlMfKysgBasMdVGQFsDUqE28hru3Jm5/f9zuTMDzuPOOIaYcJKTYc6cc8/3f//7Ium6jpdtyHgJxxLoJdBJhi3e4t27d/ljk91uP0izTpIkV3avje/84TDUUEjr1rTgKQoQF3htzZo11kDTaHC5cs8WFxd7nU4nCLR1OCmikfGzHneNPl2aFtjy5Ml/70xPT39Ii2csM03sHvf5fF6HwyFA8JQkGbIsxRUgFmca8kWEDIVCxLIugNtsNng8HieBbw4EtKu0+Lcl0AT2DUVRxANlmcEqSdlOF+j8s5K4g+/SNI0ECApyHA5nBYGusgxaUeiYZLCqKNEtIyMj6Ghvx/XrN0gbNtTV1eGDPXuYmee2cr6LNauqrNmgSZLbsnmY6o4FfOvWLXz68ScYGhoSzPBDr1y+gq6uX9Da2oqVpSuz4qJkmggEVNM39DRCnh4BxuPp06f49sQJDAwMwO12w+VyITc3V/zd29ODlpaTCJJqsxKD6V4GnlGcttmiB/+kEPgbmYTX6xEq5IhiTIdYu3zpMh6PjWUtIDLoZH6S0DxYYnNMTk7CP+tHScky2B12KOSYPILkNCzEBP0+OzubvYwnK0lDp83KQ0qWLccrZeXkLUC+J18wYXr75MQkSoqXIT8/P7vpR3/ONL5q1Wuof+9dOHOc8BYUwFdYKKbH64WDTGR743YUFRUtbho3nJanYVg5OTk48NkByIqMB389iPg0O2JtbS2a9u6dZ06LBPrZQSkdR44cQV9fHx4+fChAVlRUoKqq6gVB09N1RFEHPOPBbMs1NTViJhrXrl3DT+fPY2xsHOXl5di5aydWr16dVZu2ZYuXMJVoLSdb8D0lGip2IiXAj2fO4OtvjqGxsfHF1tMsZTTk6JGiKdHkceniRXx36pQAz7bO0SQvL08kpuPNzRi4dy9Gi9aemXH0sNr3njt7TgjIScdBsZzjN39y9hx/PIbOzs6slbdZM49Ho49Eaqc6HAplUyrUuKhHMKjB75/B6OhoUoczcUaryQySi1VVmReVl7+KkeEREceZbbZnrpP9fr9IQqWlpSlYNEJs9N6M47T1sYuixJ07f1AtUjDHtkJFVBATExPwFfiwZetWCyTo8whL2xG5k7DqNLy3bvNm7N23j2prSvNkz4pCdk2ML1+xQiSmyspKwXw2HNGWTOr5h/WEjqnrIdFtNDU1YcOGDbh58yampqZEaq+urkZZWRnZdjASBrkrih3agrI2Q9B6WjbNIxTSRbhbu3Yt1q1bFxO/Q1TUB8w2TlSMXT934fbt23DS95rat1BfX0/3hEmwUCTmZ5ARDfOQZd2Sx88/p8UhwAA8ODiIg58fRD+VAuygMjXLbW1t2NG4A0e/Okpx3R3RyHNUebGMhwUDxjTCWaJ9C/eySTDDzceO4derV82OG+58tzjT0d6B0z+cjmg14+RiXKgnCEFhMaPA5u9buJdrlv7+fvT09gqwRgJyiMkVZF6eSySfMep+2Df4mRk1AansObWtR+Muj3/HxzFFbHO1yK0cdfxCOI4o3Nn/Q8lnZmaG68nMHNEIY+E5VVsvI5/dG1U3M1xcVAIX1SOc2u1kIrwe0AKYnpqGz1coNGJqLyOm2ZPZ9kxH4uyaWoD4DqSqKkWU17Gx+k3cvz+EwqJCOOZaNv4tRM7X0NCAQuqGOPyxM6Zt0yyppqlzr6vCc8xDMM/ATC1EbTqebUedkkMeA93/0X7ROIhOnmyZJyei2k2bsPv93SJ+852833gVJ1lnmg+qakAwbbc7hDNZMRNTpfG0wiFu/fr1OHT4MLq7uzE8PCyez0JwU8HlLHf0zLRgPyRitpQO6DBdIjN7XLnZ5uwvXbNYOJjBAiqotm3bFmHRfAHJTmgWWAya76Z7p9JxxD6SeiPbNUvODsKqM+6RLDGejg+YwFkotmeedOcgbfjdMmgC+AVJek5VZz00YbyMNF71pu66U7MeqzUTcGxiIZsPEMtfxntjmix6XCBJtxD4QwT+bfqea90MpJSvfhc6mFlA0bpKs5e+tzCGhOeX/o+4BHoJ9OKP/wUYACXLDFbHoFtpAAAAAElFTkSuQmCC';
            case '4':
                return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzVGRTRFMjc4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzVGRTRFMjg4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozNUZFNEUyNThEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozNUZFNEUyNjhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlUs24QAAAfqSURBVHja7Fl/bBvVHf+cz+ezz0mcpCFt2lCgKKsrUUAMlqVF5ce2AvtjKpU2EKMMkNhA2/rHNO2vLqs6pGloghaQtpVpqJomNoluqkBFbFWBKlOrEsoKIQlrlS5duyaltZO2dnw+37193p2d2unZPocJCSkverr67n3f+7zv+/74fF8VIQQ+by2Ez2FbAL0AukYL+70cHR2Vj15N037MfoeiKPH/77L+zu84yNu2ddiyCjsYIPbKd8lkMhhotrsNI/ZqR0dHm67rIOjgcOpEI++z8H3Hp2FZ+fWpVPquTCbzBF/uCqxpavfpNrZIJOKCkF1RQgiFFN8NlONsYH+zm3QcG7YtXODhcBiJREIrFKxtppk/wJcnAoGORLTbVFV1JwyFJFi1prYbBVopq7hrqKqAZVkEX3CVo2mR5QT9RT/Qvo7IScJyMtlVNdyQeXwa8PJkpYIcR5TWbGo4ekhtf9aNplmuJBEYdMkspB1Xa3Y+j0J2BqJgBwZk50zYM7naMZjralq4aoSpFT3oEJrv+9wn5/HB9pcw9uc9yJ1NIbFyBVY98TC+8NgDCGn+06U+GMH7z7yAM/v+ASdfQOfaW3Djj57C0rvWVNU2DaYx0DIa+NmxmZrCvm9+F+Pv/I2CMU6rYmLwXZwZPIT0yDH0Pbf1Cpmzh97HmxsewYXJccoYLpSx11/Dqf0H8JU//AbXbbzPR9tqTU03lBGHf70LJwk4ihYC0NhD0PkXRgQfPf87nN4/UDG+QFN4r/+XuDj5b8o0U0LlWJUScVjZKRz56TPI/nfS1ylrhfuGQJ/cvZeL6+wKlw1Rb16PEY5wLuL0G29VjJ85M4mz7xxkCGh2z8Uok2niDOnhIaSHRgNnzDo2XUVo+iIXVLmg1JbiHrV0QxOOTMLApWzlLBZjbj5HeBGCpoMV7bQgN+RKZOCY+ZrJqiFH9Gvda27DubHjaCtaJ1MOLG4wQxhharPj5hsqxuuL2rCsh9zh2DG0Eni0eLB5ykxTMt52LZqv7W6In9QIef6De37wGBYbHejgsp0U7STUTgJfhByWr74ZVz/4jUrQHe1Y+b1N/C4oY+MqbrOTvYM6XsQ5er69EYlVPY1zmBK3KO/j4+PCJhnwa5m//F2kk/eIGawQeSwTGXWVSN25SZgffuw7XjC9TW19Xkwv6RUmrmG/WlxsvUWkn+wXTmamiogjJIaRkZHv+OEL12dr4rLm6dXG/V9FrPdGiIEjUKYvQV3WCePuXiCq+2pHRoLEz34IZ8N6KEc/phPYCCdXINR3U1WN1mOKdW26Ql7uVD6X0ii+da8HqmR9VRYqAVBuWgnIHkBmXkVAAJuaB+8XNR2u/PO8ND3Xhvz4wdxlaYh1OUVFlSLnrTxG9wyqrTnPkCdNg2GOi2dOT2Bi4F3kL1xAfFkXuu7sg2bEaK52Mf1f1p4i+TjfnfvnMM4fHXIJVisjxuI1t7o+4ti2j9ZrAw8HiSolJ5SAx3a/gcEtv0BqdJjROQtdbUfX7V9G3wtPY9HqJAqFwmxhIFmiBHXk5zswvPNlXJr4D2E5iLcuwfUPbMSXftUPLW7MAV4yD9F4Rpy7Wwl44uAg3n7k+zCzKUbbGLNiG4RtkUC9CfOhNL5+YDe0RItboYqiww09uxOHt21jVJdsxauPzakpHP3ti1BYYPRt3wohd1i2lrc2Gk8ujJbF+s3xuiRML75MopMm3CYCVpnjmEAIJcZsmBr6EGOv7HE3J2UVFhCZyU/wr5f+yBEas2GM4xW36y5/iePEK3/FOdJWlXVhPT8KRJg84KLC2lKHjrjkSKbwZoq2ELh8xvlUySamjn7khTPhBTbzfBqZ48f4PcptKu74yzI6zPQpZMZPXWGOTh2nrmrTnuDlY5JuGG+RS4a4qOqyNtY2tGtBuNQsN6PH414kEfJkhKvtpoiBWN52N6YXdeTxFdvlh2pE8+hWEWiQejRUK0bLAnNWA4R13Yb70M7FFnOvXQS5lH0J+1Uc2xlqRdfX1sEq2qNDh4x2deKa29fQ8nPuuHIZyT26k6uRuGGlG3XKlWXbzvyihxQuF7QJovuphxHZdxDRgQFqznC1blHXuiRATz6K9nvWoZDLeSaSt6C2NGPFls2YHjqO5rNnaCSGS2hz/NOMNhj9m6Ev72bdOOM5rlKK05/CPDxnLL5gIau2t2Lxn3ZAbN+FyKv7ED5HgtnTDe3x+xF6fCPrv/xs5PCqgBkYd9wKfc9OqM/+HpG3yVcsG2rvWuibN0FdvxZ2NutzwrU1rfh9PHFiTCQSrYhGo7N27UYleR+he8RIyWRd8iNkEdpkkNkzPlsF/+hKMgVJ9ll+KVKTnANGFILV+dzYJufOc/MXmLj4fDSZTO4KpGlpy1LQu4NAMb0WM53p1SmKzhJLFCt2eS3gq5lihqPG3Ykk+FKd7V4liCuilOuoluVqu5pT+oKWjmESnLx/0LSIWxhdgcmuweiUepyqevKQgHP0i6JzKo2AtimoShCxWMy9GqtdbIpAxK9e0pDazdLGpcLkv8Ph8KVGWN5hgu4rkNyUzETeZpZMJQi4+tpGhXlIzcq1JHeRnWue5ID3AoNWVXULJ9ptmrlWbhqly0jvBjUUCEjQMSXA5elb13WLSur3uzGtFfL2U+heAvwJj2kdfxvBASkBrn4rx5QuOqmYPPsgfz/Hn3sbCnkL/1G0AHoB9Gfb/ifAAJWtRmJ+XSXgAAAAAElFTkSuQmCC';
            case '5':
                return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzVGRTRFMkI4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzVGRTRFMkM4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozNUZFNEUyOThEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozNUZFNEUyQThEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgMpSoAAAAfrSURBVHja7FltTFNnFD63LeWrfClCiwIaoKBxCIoO4pIZiYKOzKCITrdE3Za4ZIEx5zL/bPulbsvUxC1zyTBu0R9+THEBF50m8hEZJOIfFyWBTUSjQyrKR6Et7d055/Ytpesn+kfDm7y5be+9533u857znHNuJVmW4UUbKngBxzToadAvG2iNtx87OzvpoMW5XavVvqfRqHMApOf4gL4Vy253mGw223m73f4Nfr2bk5MTHGiSQUmSvkpIiP8oPj4eNBpN8HCCkFBv1yhrMugos9n8ocn0uHh8fLwMT/0drHsU63TRVbGxsaBWq8HhcOCUgdaSkHCV6v+TflfOqQNOtVozaSJBbBsB8+KRkZEQFxc7H+1+GrR74FgbHh6uUowRA2onMN/s+TsXaCjgCbQdxsdtTFJYWBjusLosaNAIdgYxQr4nmBSj9do1OHnyJPT09IA+WQ/l68th9eoSvCl4kE1NTXD61Gm4d68XUlJSYENFBaxcuZJ31ekioHiQNNOnf3nO27dvHzWZ+mWLZQy/OmQxjvxwRM6YO09O0Rvk2YYUnqlzUuUvP/9CxuCRgxkHDxyU56XPddmgY3pqmrx/3z4ZGeZrbDar/PjxY7m7u3vEGz6Vv0ChJxcUdly/DocOHQSH7IDo6GiIioriqcVtrK2thT8uXQrIcGtrK3z/3WH2YWGDjhToR478CI2Njcr2a8hdJJ9BrfLtoxIoLqKM+vp6GB4agoiICAgP1+IMdx61fO25c+cCgv7t/HkYG7O43Tthw46+XOeyISHwsNB0WtwouUXXo/5+0GDAREUhM8iuyhmkFDgoTYASFRA0bjlotWHMsLsNG9qwWm2TbBBhvpgOWoCNWUaIj09AKYqDiMgI3kKKdgsyR4FjNGYFtJGZmQntbe1A2h8eEe6yMTY2xsdso3HqGdHbWLduHTRevQpWmxVIv0mSxnGh4eFhUGvUUFm5KaCN8vL10NLcwk4ZoyMbGrYx5HS7DRsrPLJmiD7teUNaehrs2v0JHtNBiwuQNhHw5ORkqKqqgvzF+YGZzsqEml0fw5w5qaBFPxY2DCh7NTU1MH/+fDcxAPCVXDWhJIGioiLIysqCGzdusH/GxMTAokWLYPbs2UHbWLFiBSxYsIBtPHnyhN0tLy8P9Hr9sxVM/mqIxMREWLVqlU+DVqsVfj1zBuXtT3Dg1i8pWAIbKytBp9O5rklKSoKSkpIpF1UaeI6jr68Pqquq0W+bObDowevq6lAO6+Aw6nM6ulbwhdcU6umJm5SA8JaZxKSBGREOfHsArly+zPpLzJL7UIC1t7XB/r17YdRs9pmFPe09UxPgLyDcx4MHD+DSxYtUnXGywDqcJz0AgW9uaoaurq7nUuI+N/cYGRnBwBrgwNJotCiDKi4AxlHDCbzJZIKnT5/69V+BUwpQMvoNxGC3i2sJzJSpqWnsJroYHQMVbjM8NMxuEodJxTeLArgUcF3VVKLX2wMaUgywZu1a1t/4hASYMXMmT/pM2a+4uBgyMjL8+LJgWnyWQ0/jgW70BE1JYse7O2B01Az/3LnD32XnLixdtgx2frCTOxJSlcDDMVWf9gSsMOCtX6SCiSbp72d79kAbqgXWwnw/yVxhYSEHI7mKKD09/RZraHDvJKYEWmHZMSk4aLEeZLGh4QLcv38PEmfN4iRBGY5AEyhimLIeTfeHoqQjOdN2V1c3/H6hARXnIZYBSVBaugayc7L5fgHW7w57869bt24dffjwoYwLuSaN+voG+dWly7jjmDUzUTYk6+XcV3Ll2p9queug6ywWi8ccc32mceb0ablg8RLuWISN/Lx8+cSJE3weKz6+ljqXzs5Or52L5O2JqN1KSEjYTiWkYJg09q3Nm6Hv3z5WAtH0EjsarNaO/XwMli9/jRkVriTqchpU0f118yZs3bIFBgeHWF3cbURiufvL8eOQn5/Pfj8yMgz9/Saz0WiMDiEjTmwTNbZ1mIof9T3i9ogWpG1WCvpIsFqscOrkKReIyTw4eFLrdu7sWdZqbtNcNrQcoCMjZjiLNQsR5HDY/SY0v4FIrbwImJ6eOwheUroONExdB8GxOzuX3t5eL74os+4K+aRr6F4COWEDux8bxgPu0N27dycpV8ig6eUMgSagNAwGAypALDYAMai7Ea6XOOh/3LkYDCnOlzoO70kDZ3KyHnTRMRDLqZ5sqNxs2CFZb2Cw3u0EqR5kiF7UkBytRpVoaWnh7aRiSLRK5tFRvras7A38Pu7hz+4lqwVKSkuhvb0doqKpCycbasV/sZAiF1yLyYliggD703OVr7QsJIwYJ0NU7G/btg1ZimOmtdRJ45Hcherl11HilAU9o11hjfy+YGkBbH3nbYjBdo2yJHVAZIPiZBMGeVFRIbMu1g6VaYluooaTGBDb+Cb2iZnYuXR0dMDAwAAzTl1Hbm7upC0VLxM9GafzFRUV3FaRjcHBQU46ZGPhwoXcL4pGl8CjHVUooAfEkxJT5H/EviTZuKM2unXNzCIy7J6I/NczErds2dnZbmoju9yCjgSajkiYKRTQDfjE1bhNKtoqesFC8qS8hAzlTaMc8EGEShBg4RaKWzooH9SHAvoKgjyMW1RNdbLCsuT2Wld6RrAT5wXTBNL1rg7XQMC38Ph1qE3Abuw6bqO0vU/vakTQBve/oxTg1a/v80jIAIKtw0n/BPR4vWb6z89p0C8Z6P8EGAAC/gl28OLcYAAAAABJRU5ErkJggg==';
            case '6':
                return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzYwQTQxQjg4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzYwQTQxQjk4RDg2MTFFN0EzQzE4M0FCQUREQjdDMzgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozNUZFNEUyRDhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozNjBBNDFCNzhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pt0h30wAAAgYSURBVHja7FlbTFxlEJ69srBcdrnTQtmlpYVexeKDQKtoG0ykaRQKTayJiamNb63xUvVBGjUxvvhgrS+WQIKaCL60PtRehAR46GNNm5bSFtom5Q5pZdld9vI7M2fP7h7OObCLNcaEP5kc9j/fOTP//DPzzX8wCCHg/zaM8D8ca0avGb3MMGtNDg0NyffeslqtR81m01YAw1NcoH7yh8Ph2UAgcD4YDH2NP0crKioSM5oqisFg+MLhyPrI4XCCxWJO3JwEqpEWRtIJEAqF07xe77vT0zP7gsHgq3hrONHw2GO329/LysoC9DKtnoV0GdDhRiOJSSXSPZOmmEzmqJjNFhb5tyGyiWgwX222VEDd5ajnZMLhgeOVlBSrBb0d8QApNpD3owCvzwt/XPkDRkdGID09HWpqa6G8vFzXuz6fD/FXYCSCf76mBjZv3sz3TCYTCzkmEFjEa4h312w2N+pu1VK5efPm9zMz08LrXRChUFAsHcO3h0XTa6+LMpdb5GbniKKCQvFsVZU4+8NZoTXuDGvgn6kS7WfVeNLn8XjE3NycuHv3bkDLPqN+2IloKMSPx48fw4cfvA8D/QP82+FwQFpaGkxNTsGptjb4/cIFBf6vJ08Q/wH09/cr8VOEP6XCUyhZrRY5WcNJlzzaMoxixVxfbx/8ee1PyMjMgJSUFNxGCyqxAuYAJ1JX149KfF8fXLt2DTIzM1V4Mqyrq0uVmBTvS52VSExz/EpGK8e9e3cBsxvyCvLBjElEsU4DSxQbfefOHY5NWeno/fsSPi8fTGZMPAXeABhqmIAhil+FHlqcXmlcppYpE08e2dnZkIlVhZLJZrOx4eQp/6Kf8fl5eYrnHFkOrAQOSM/QxhcUFGh6leb0yqc5WVqoravDrC8Hn38R4zOLPUQv9/n8EFgMQGPjAYXRNbU1XFV8fr82/kCjptFkr17JT5rlNm3aBEePHQNnjhNsqamQhrGZiollxhJV/1I9tBxuUeA3btwIR985Cs5sJ6Qswb+872VoaW19OjS+EtU2NjZCaWkpDA4Owvj4OG/7rl274IUXX4Q0NEyFP3AA3GVlXEFk/M6dO6G+vh5SNfD/wOjlx44dO1gSHdu2bWN5Gv2JebU9xIMHD6Cnuxtu3RrCRMuEhoYG2Ld/vy7+4cMH0P1LD+JvYSJL+P3L4CX1ApJhxDPT01NCb2CtFs/trhbri9aJwvwCZjjXhg3i5EcnkUW9Knxvb68KX1qyQXxy8mNNPI2FhQVx+/ZtfzKMGJe5gkX2/KNHj6Ct7TO+ErNlZGRw+SMm6+zogF97ehQ7NT42Bqc+a1PhiQM6EN/T3aNy4Eo7vWL1WFp6+np7YQQJxp5uj7IbCbGdzZYC3RgykdY2yogj9+4xAy7FY1PGIUYN0r+aiOPjExAMhCAnxx5hOCNvRjAUZHacwPvxDDc1NYn3QpCNRlOZU+EnxpkdrdYYmUgLXmUixm+X/LL169dBTm4ubnEG1tvUKFn4kWyIRd1ud5TNJMYrwgXmclgo8X7Gu1xuDhVJj+A5Se9TqtP0MqrF58+fh5mZWXA6Heg9C897F7zosQA0HWpmo6UtN8LeF/bCuXPnED+jwofQw82HDrHRtDsxvYbVxbQUy8qspUaosLAQjp84AaXuUkihXgI9R7GaZk+D1sOHmXikU45gwyX8cXC5XSp8y+FWpnEZH5NwnOeT9LRytSJyJApCdXU1FBcXw9WrV5nhiNW2b98OVVVV0cXJA895CvzExAQzIhEN4aV3hnTCc1UxHdZ5UPLgwYMHVXfIyNiiY3NaeNKBJ28d/WHQaDJXMlq7y5L77L6+XvgRG35iROreGhpegTeOvMEnk3hDCE8hgQQDP//0E9y6GWPEI0fehCx8dnFxUdUGL9fl6TLi2NiYwPZRYJZHxMdCo729XbhdLlG8bj0z24biElGQly9aD7UI3H6BnmWm83ol/Fk8C2riW1oFhhfjiQFloWcx0cXQ0FCyjBhLCDm+LBYr3Lh+Hb779lsIBYLMcBKp2LikEZF0dnRGvUZnvRs3rsOZ06exUmjg0fudnR3Rbx7xoUGyKkZcukIypn9gACYnJ5gR45mQruk4d+niRf5cIJ8+BgcGYXJikhlRC3/54iXGywm8Uo1eNqblDzTEWvEvmX/yF6/VzgxnQYYzcdIFAhLDLSx4FNXA41kAg1EPb4T5eQ8nqsFgUZBZfAVKqnoQvVosJkXdLsOTSGFREaSjEVRrFYw4B1BZuTXCiNKi6bBAlcOuwvsZvxVLn/yhRqk7mJzRFAb0UDC4iC+0RVmKtnHP3jq4fHk3jI6OYuY7wIreC6OS+fl5hIWZEckwqgh0Cq+rq4XLu3XwOJqam5hsJFqP6dYrhfxpo62tTTWJlEufo6opOcgLdKXV07YTkWzZUsG0TC8Wke8jdEpvbm7Gc+JLrJQ8Td/mqN/YUoH42ZnIvGC80+mE15ua+MhF8zEWDbNzaMFUeXJzcz9XOVWL47HUfIkPf0LZL7WQNsWRnjyDpYlPIXNzc5xgZWVuZL0S9LBflUiEJyMIPzs7y+90uVxQUlLCOxJvA/0mo8nzqHOysrKyIFGj96AXr6DhFjJI7hfIcLmc0d80J3do5HV1HMYqgR4+vumnOVnI47i4djT67UQTsR8VfIOr/dDj8bChJNInXqPmRxw9Y1e6H9+MRckDdWAtH8YFfpXsIeBTXOkwevkYvqhCrumJ/QvPAMuvS/8+OmQOjf4NhQwe1cSs/R9xzeg1o//78bcAAwDM6a+jh2egLwAAAABJRU5ErkJggg==';
            default:
                return  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAAACXBIWXMAAAsTAAALEwEAmpwYAAAHJWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDIgNzkuMTYwOTI0LCAyMDE3LzA3LzEzLTAxOjA2OjM5ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE4LTExLTI1VDE3OjQ4OjQ1KzA4OjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxOC0xMS0yNVQxODoxNDowMyswODowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxOC0xMS0yNVQxODoxNDowMyswODowMCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpkYWU5ZjNiNS1jNjQwLTY0NDMtODVkNi1kOWNhMjljMGZmMDkiIHhtcE1NOkRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDoxMzU0MjUwNC00MTViLWRjNDAtOTQxNC03YjA2NTg2MzA2MzIiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDozNUYzNjBGRjhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozNUYzNjBGQzhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozNUYzNjBGRDhEODYxMUU3QTNDMTgzQUJBRERCN0MzOCIvPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpiMmI5ODUxZi03MmMzLTg1NDgtOTY4Yy00ODUyMmM3NDBkMjQiIHN0RXZ0OndoZW49IjIwMTgtMTEtMjVUMTg6MTQ6MDMrMDg6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE4IChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6ZGFlOWYzYjUtYzY0MC02NDQzLTg1ZDYtZDljYTI5YzBmZjA5IiBzdEV2dDp3aGVuPSIyMDE4LTExLTI1VDE4OjE0OjAzKzA4OjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOCAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDxwaG90b3Nob3A6VGV4dExheWVycz4gPHJkZjpCYWc+IDxyZGY6bGkgcGhvdG9zaG9wOkxheWVyTmFtZT0i77yfIiBwaG90b3Nob3A6TGF5ZXJUZXh0PSLvvJ8iLz4gPC9yZGY6QmFnPiA8L3Bob3Rvc2hvcDpUZXh0TGF5ZXJzPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pu5uMOYAAAU4SURBVFiF7VldaB1FFP5mdufemxsSjDUPiVoCgcQ0+PNQkaptsQ+ClqLgQwXti6UPKogJtlJ/akGpUB9KCIJUKYiIL1awtD5UqWBVFMVGqaSVBEofAmlq5abe2/2bOT5sd3P37szevTeSEuiBgd2dM+d8c+acMzNnGRFhtRG/0QDaoZugV4pWJWhb9/H8+fMgorWWZe0uFApPcM7X/L9q9cFPBCWlPOd53kcAjgDwh4eHU3xa0EQ0IIR9vLe3d7RcLoPz/AvSLBuF3WT8FgTB+kqlsr5SWRwhopd1MrSgOed7enpuGe3o6AAAKKUAMHDOwRgDYyYw0PZlE0FKBaUUiAicc3R1dcH3/Zeq1dpxAN+k8GlnYtvbhCjE75Zlw7btGDSQbuFk2mkctm1DiAI4t+KVKhSKjDG2VYdPC5oxtoZzDqIQMGvdfC0TYwxCCFiWDaUo0tmj4zU5KxERLMtaEcD1FALnkcWVjkfr00A4U8uyWlYqFy5DXVqAd+Z3WH19EPeMwuq9rSUZQggwxoxBbcgegG0b5pMB9uob78A7fDTxnQ32o7x/DJ3PPp1blm0LcG5eYWMuY6yFNFer4cqGrTFg+6ktKB0YC/tm51DdsRt/b9qG4MLFXPKiIG0RdGsnv6sTH4Bm58AG+9Fz9iRu/fxjdO8dR8/Zk2CD/QAAeXoKi6+8mVtmVrpf9jYeXLgI57VDAIDy/jGI0ZG4T4yOoGN81xLv0VO5rZ21SWlB63YtE7nf/xg/1/YfAtVqif7S448mZVerueRm0bItbfX1xc80OwdVTYKW85cS76yzM6fkFi0NZPtUPRU33B8HXfn9t1LprfreZPxsbbwP9sDaXHIz9RNRqs3MzFQdx6Hl0r+ffEbzuCNuzk+/5B47Pz9P09PTR3T4jMl4KRBIO+tmO6X786+o7tgdv5cOjKH4wPoMPfllN/Xpdu69VKth8ZkX4/fSgTF07x1vTUaG4hyWbp1qXxwDzc4BAIqv7swBmK7rjN7NWzhgvgQkmo6yltA5/GnIM9iP7n17suHWueESWO05KRv0ckmengIAFHduByuXM3mTRsm3upnukWXpPO5TfGRjjutXuj9LL5AZiI0DKf5GRJBSGht/+N5Q+LphSCmhlNK2UH46paV1J6ltS0c8Oup493V4X34FKpVASu+fUUzoLQ20vSOaAVNsqcYmFy4j+G0KrLsL/p/TWh4iSlhb34yYzTeX8AZeP7j+ObrMpvuq23dB/fAHAMDbN4nOc9/CHrgzITsJKA2wrc0lEqKUaeYKQL1/hjzeqe9iwBG5BycafLmZRanO3/WktXR0P2uWORhL7phZmaCRVyMV4QpGdRYzGS0dzjZsuqgP/bLeYgrW5ofAHrw7IUu88JyGlzRy6/1cZQI3WJogZXC9/tDK+YNQnHgbwYmvQZVFWFs2gd81lEqdWfKidNrWNu77PoIgaCgjhKWFrMmw4SGI4aE6gKqBLx3AjQB934eU0hiQWtBKEXNdD7btoFgsxUp0QROdF5bqFAyMNR6A0uNMfVJKOI4D3/cBU9lOD1pdcV339qg4KIQwaV+CkcuHmrvGtWvX4LoufN+HEOKf3KAZY8c8z3s+CCQ8L4AQYQGyWT2iXnlWhahxEtFm43kegiBAEATgnBOAE7lBc84PCiE2O46zzvPcGCznYZWzGfC8YCPe5FkkLIsVCoVJaMq8RtBEdIFz/lipVNqjlHoScfUye3kjar4ayRp3FOyMMcUY+8uyrA8R/gnQj17ODeVG0ar8UXQT9ErRqgT9H1mjDw/U3z1bAAAAAElFTkSuQmCC'
        }
    },
    // 登录状态管理
    isLoginSession:function (is) {
        if(is){
            store.commit('loginOn')
        } else {
            store.commit('loginOff')
            router.push({path:'/login',query:{fullPath:router.history.current.fullPath}})
        }

    },
    validateCodex:function(data){
        Message.closeAll()
        // 用于跳出循环
        let circleIs = true
        let xss = /xss/i , script = /script/i , blank = /\s/
        if(xss.test(data) || script.test(data)){
            Message.error('包含敏感字符，请重新输入!');
            validateIs = false
            circleIs = false
        } else if(blank.test(data)){
            Message.warning('请勿包含空格等!');
            validateIs = false
            circleIs = false
        } else if(!data){
            Message.warning('请输入内容!');
            validateIs = false
            circleIs = false
        } else {
            validateIs = true
        }
        return circleIs
    },
    // 前端非空,xss,特殊字符验证
    dataSafeValidate:function (type,data) {
        let that = this
        switch (type) {
            case 1:
                //字符串类型
                if(!that.validateCodex(data)) return false
                break;
            case 2:
                // 数组类型
                $.each(data,function (index,item) {
                    if(!that.validateCodex(item)) return false
                });
                break;
            default:
                console.log('暂无可执行语句')
        }
        return validateIs
    },
    // 查询日期
    dataSearch:function (num) {
        let year = new Date().getYear()+1900, month = new Date().getMonth() + 1, day = new Date().getDate() , lastMonthDay = new Date(year, month -1, 0).getDate();
        let result = {
            startTime:'',
            endTime:''
        }
        let lastDay = day - parseInt(num)
        if(lastDay < 1){
            if(month - 1 != 0){
                result.startTime = year + '-' + (month - 1) +'-' + (lastMonthDay + lastDay) + " 00:00:00"
                result.endTime = year + '-' + month +'-' + day + " 23:59:59"
            } else {
                result.startTime = (year - 1) + '-' + 12 +'-' + (lastMonthDay + lastDay) + " 00:00:00"
                result.endTime = year + '-' + month +'-' + day + " 23:59:59"
            }
        } else {
            result.startTime = year + '-' + month +'-' + lastDay+ " 00:00:00"
            result.endTime = year + '-' + month +'-' + day+ " 23:59:59"
        }
        return result
    },
    // 解析毫秒
    getMyDate:function (str) {
        let oDate = new Date(str),
            oYear = oDate.getFullYear(),
            oMonth = oDate.getMonth()+1,
            oDay = oDate.getDate(),
            oHour = oDate.getHours(),
            oMin = oDate.getMinutes(),
            oSen = oDate.getSeconds(),
            oTime = oYear +'-'+ this.getzf(oMonth) +'-'+ this.getzf(oDay) +' '+ this.getzf(oHour) +':'+ this.getzf(oMin) +':'+this.getzf(oSen);//最后拼接时间
        return oTime ;
    },
    getzf:function (num) {
        if(parseInt(num) < 10){
            num = '0'+num;
        }
        return num;
    },
    addFavicon:function (floder){
        let favicon = ''
        switch (floder) {
            case 'x00286':
                favicon = 'https://tpxb.me/x00286/6.ico'
                break;
            case 'x00292':
                favicon = 'https://tpxb.me/x002792/02312.png'
                break;
            case 'x002100':
                favicon = 'https://tpxb.me/x002100/886.png'
                break;
            case 'x00247':
                favicon = 'https://tpxb.me/x00247/16.jpg'
                break;
            case 'x002104':
                favicon = 'https://tpxb.me/x002104/1111.png'
                break;
            case 'x002103':
                favicon = 'https://tpxb.me/x002104/1111.png'
                break;
            case 'x002105':
                favicon = 'https://tpxb.me/x002104/1111.png'
                break;
            case 'x002115':
                favicon ='https://tpxb.me/x002115/2.ico'
                break;
            case 'x002117':
                favicon ='https://tpxb.me/x002117/50.png'
                break;
        }
        var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
        link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = favicon;
        document.getElementsByTagName('head')[0].appendChild(link);
}
}
