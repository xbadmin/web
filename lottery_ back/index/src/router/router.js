import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/index")

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: false,requireAuth:false},}, // 首页
    ]
})

