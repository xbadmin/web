import Vue from 'vue'//引入vue
import Vuex from 'vuex'//引入vuex
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        apiUrl: location.hostname == 'localhost' ? '/api/' : location.protocol + '//' + location.hostname + '/',
        isLogin: false,
        userInfo: '',
        stationInfo:'', //  网站基础信息
        stationSwitch:'', //  网站开关
        switch:'', // 开关列表
        beforeLotteryData:[], // 投注页面彩种切换
        homeData:[], // 首页列表
        scoreSwitch:[], // 个人中心列表
        accountInfo:[], // 个人信息
    },
    plugins: [createPersistedState({storage: window.sessionStorage})],
    mutations: {
        pushIsLogin(state,e){
            state.isLogin = e
        },
        pushStationInfo(state,e){
            state.stationInfo = e
        },
        pushStationSwitch(state,e){
            state.stationSwitch = e
        },
        pushServiceUrl(state,e){
            state.serviceUrl = e
        },
        pushSwitch(state,e){
            state.switch = e
        },
        pushBeforeLotteryData(state,e){
            state.beforeLotteryData.push(e)
        },
        pushHomeData(state,e){
            state.homeData = e
        },
        pushScoreSwitch(state,e){
            state.scoreSwitch = e
        },
        pushAccountInfo(state,e){
            state.accountInfo = e
        },
    }
});


export default store
