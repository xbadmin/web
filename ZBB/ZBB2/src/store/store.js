import Vue from 'vue'//引入vue
import Vuex from 'vuex'//引入vuex
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        apiUrl: location.hostname == 'localhost' ? '/zbb/api/' : location.protocol + '//' + location.hostname + '/api/',
        isLogin: false,
        userInfo: '',
        token: '',
        money: 0,
        money2: 0,
        type: 1, //1正式版  2 隐藏版
    },
    plugins: [createPersistedState({storage: window.sessionStorage})],
    mutations: {
        pushIsLogin(state,e){
            state.isLogin = e
        },
        pushUserInfo(state,e){
            state.userInfo = e
        },
        pushToken(state,e){
            state.token = e
        },
        pushMoney(state,e){
            state.money = e
        },
        pushMoney2(state,e){
            state.money2 = e
        },
    }
});


export default store
