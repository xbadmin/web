import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n)

// 引入语言配置文件
import zh from './config/zh';
import en from './config/en';
import ja from './config/ja';
import po from './config/po';
import ind from './config/ind';
import ko from './config/ko';
import je from './config/je';
import pe from './config/pe';

// 创建实例
const i18n = new VueI18n({
  // 设置默认语言
  locale: localStorage.getItem('locale') || 'en',
  // 添加多语言
  messages:{
    zh: zh,
    en: en,
    ja: ja,
    po: po,
    ind: ind,
    ko: ko,
    je: je,
    pe: pe,
  }
})
// 暴露i18n
export default  i18n;
