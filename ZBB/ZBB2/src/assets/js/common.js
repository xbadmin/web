import $ from  'jquery'
import { Toast } from 'vant';
import router from '../../router/router'

// 验证返回结果
let validateIs = true
export default{
    loading:function (onOff) {
        if(onOff){
            Toast.loading({
                message: '加载中...',
                forbidClick: true,
            });
        } else {
            Toast.clear()
        }
    },
    // 后退
    backPage:function () {
        router.go(-1);
    },
    // 前端非空,xss,特殊字符验证
    dataSafeValidate:function (type,data) {
        let that = this
        switch (type) {
            case 1:
                //字符串类型
                if(!that.validateCodex(data)) return false
                break;
            case 2:
                // 数组类型
                $.each(data,function (index,item) {
                    if(!that.validateCodex(item)) return false
                });
                break;
            default:
                console.log('暂无可执行语句')
        }
        return validateIs
    },
    validateCodex:function(data){
        Toast.clear()
        // 用于跳出循环
        let circleIs = true
        let xss = /xss/i , script = /script/i , blank = /\s/
        if(xss.test(data) || script.test(data)){
            Toast('包含敏感字符，请重新输入!');
            validateIs = false
            circleIs = false
        } else if(blank.test(data)){
            Toast('请勿包含空格等!');
            validateIs = false
            circleIs = false
        } else if(!data){
            Toast('请输入内容!');
            validateIs = false
            circleIs = false
        } else {
            validateIs = true
        }
        return circleIs
    },
    //解析毫秒
    milliDate:function (e){
        let date = new Date(e)
        return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDay()
    },
    //拼接时间
    getMyDate:function (str) {
        let oDate = str,
            oYear = oDate.getFullYear(),
            oMonth = oDate.getMonth()+1,
            oDay = oDate.getDate(),
            oTime = oYear +'-'+ this.getzf(oMonth) +'-'+ this.getzf(oDay);//最后拼接时间
        return oTime ;
    },
    getzf:function (num) {
        if(parseInt(num) < 10){
            num = '0'+num;
        }
        return num;
    },
}
