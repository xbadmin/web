import Vue from 'vue'
import Router from 'vue-router'

const login = ()=>import("@/components/login")
const register = ()=>import("@/components/register")

const index = ()=>import("@/components/tabs/index")
const tradin = ()=>import("@/components/tabs/tradin")
const invest = ()=>import("@/components/tabs/invest")
const share = ()=>import("@/components/tabs/share")
const personal = ()=>import("@/components/tabs/personal")

const notice = ()=>import("@/components/notice")
const noticeDetail = ()=>import("@/components/noticeDetail")
const recharge = ()=>import("@/components/recharge")
const rechargeCode = ()=>import("@/components/rechargeCode")
const withdrawal = ()=>import("@/components/withdrawal")
const team = ()=>import("@/components/team")
const tradelist = ()=>import("@/components/tradelist")
const investApply = ()=>import("@/components/investApply")
const profit = ()=>import("@/components/profit")
const transfer = ()=>import("@/components/transfer")
const changePassword = ()=>import("@/components/changePassword")
const changeSecurityPassword = ()=>import("@/components/changeSecurityPassword")
const drwerAddress = ()=>import("@/components/drwerAddress")

Vue.use(Router)

export default new Router({
    routes: [
        // 登录注册
        {path: '/login', name:'login', component:login, meta: {keepAlive: false,requireAuth:false},}, // 登录页
        {path: '/register', name:'register', component:register, meta: {keepAlive: false,requireAuth:false},}, // 登录页

        //主页面 keepAlive 页面
        // 缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: false,requireAuth:false},}, // 首页
        {path: '/tradin', name:'tradin', component:tradin, meta: {keepAlive: false,requireAuth:false},}, // 贸易
        {path: '/invest', name:'invest', component:invest, meta: {keepAlive: false,requireAuth:false},}, // 投资
        {path: '/share', name:'share', component:share, meta: {keepAlive: false,requireAuth:false},}, // 分享
        {path: '/personal', name:'personal', component:personal, meta: {keepAlive: false,requireAuth:false},}, // 我的

        // 其他页面
        {path: '/notice', name:'notice', component:notice, meta: {keepAlive: false,requireAuth:false},}, // 通知
        {path: '/noticeDetail', name:'noticeDetail', component:noticeDetail, meta: {keepAlive: false,requireAuth:false},}, // 通知详情
        {path: '/recharge', name:'recharge', component:recharge, meta: {keepAlive: false,requireAuth:false},}, // 充值
        {path: '/rechargeCode', name:'rechargeCode', component:rechargeCode, meta: {keepAlive: false,requireAuth:false},}, // 充值地址
        {path: '/withdrawal', name:'withdrawal', component:withdrawal, meta: {keepAlive: false,requireAuth:false},}, // 提款
        {path: '/team', name:'team', component:team, meta: {keepAlive: false,requireAuth:false},}, // 团队
        {path: '/tradelist', name:'tradelist', component:tradelist, meta: {keepAlive: false,requireAuth:false},}, // 交易清单
        {path: '/investApply', name:'investApply', component:investApply, meta: {keepAlive: false,requireAuth:false},}, // 投资下单
        {path: '/profit', name:'profit', component:profit, meta: {keepAlive: false,requireAuth:false},}, // 财务记录
        {path: '/transfer', name:'transfer', component:transfer, meta: {keepAlive: false,requireAuth:false},}, // 转账
        {path: '/changePassword', name:'changePassword', component:changePassword, meta: {keepAlive: false,requireAuth:false},}, // 修改密码
        {path: '/changeSecurityPassword', name:'changeSecurityPassword', component:changeSecurityPassword, meta: {keepAlive: false,requireAuth:false},}, // 修改安全密码
        {path: '/drwerAddress', name:'drwerAddress', component:drwerAddress, meta: {keepAlive: false,requireAuth:false},}, // 提现地址

    ]
})

