import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store/store.js'
import i18n from './assets/i18n/'
import 'vant/lib/index.css';
import VueCookies from 'vue-cookies'
import {post,fetch,patch,put,file} from '@/assets/js/http'
import { Swipe, SwipeItem } from 'vant';
import { Tabbar, TabbarItem } from 'vant';
import { Grid, GridItem } from 'vant';
import { Tab, Tabs } from 'vant';
import { Icon } from 'vant';
import { DropdownMenu, DropdownItem } from 'vant';
import { Checkbox, CheckboxGroup } from 'vant';
import { Button } from 'vant';
import { RadioGroup, Radio } from 'vant';
import { Field } from 'vant';
import { Calendar } from 'vant';
import { Uploader } from 'vant';
import { Popup } from 'vant';
import { Step, Steps } from 'vant';
import { Cell, CellGroup } from 'vant';
import { DatetimePicker } from 'vant';
import { Picker } from 'vant';
import echarts from 'echarts'

Vue.use(Picker);
Vue.use(DatetimePicker);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(Step);
Vue.use(Steps);
Vue.use(Popup);
Vue.use(Uploader);
Vue.use(Calendar);
Vue.use(Field);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(Button);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Icon);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(Grid);
Vue.use(GridItem);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(VueCookies)

Vue.config.productionTip = false
Vue.prototype.$post = post;
Vue.prototype.$fetch = fetch;
Vue.prototype.$patch = patch;
Vue.prototype.$put = put;
Vue.prototype.$file = file;
Vue.prototype.$echarts = echarts

router.beforeEach((to, from, next) => {
  if(to.meta.requireAuth){
    if(store.state.isLogin){
      next()
    } else {
      next({path: '/login', query:{ redirect: to.fullPath}})
    }
  } else {
    next()
  }
})

new Vue({
  render: h => h(App),
  router,
  store,
  i18n,
}).$mount('#app')
