import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/index")
const language = ()=>import("@/components/language")
const line = ()=>import("@/components/line")
const mytask = ()=>import("@/components/mytask")
const vip = ()=>import("@/components/vip")
const profit = ()=>import("@/components/profit")
const login = ()=>import("@/components/login")
const personal = ()=>import("@/components/personal/personal")
const register = ()=>import("@/components/register")
const service = ()=>import("@/components/service")
const help = ()=>import("@/components/help")
const task = ()=>import("@/components/task")
const video = ()=>import("@/components/video")
const promote = ()=>import("@/components/promote")
const postTask = ()=>import("@/components/postTask")
const wallet = ()=>import("@/components/personal/wallet")
const recharge = ()=>import("@/components/personal/recharge")
const auditRecord = ()=>import("@/components/personal/auditRecord")
const postRecord = ()=>import("@/components/personal/postRecord")
const userInfo = ()=>import("@/components/personal/userInfo")
const bankCard = ()=>import("@/components/personal/bankCard")
const realName = ()=>import("@/components/personal/realName")
const updatePass = ()=>import("@/components/personal/updatePass")
const updateMoneyPass = ()=>import("@/components/personal/updateMoneyPass")
const withdraw = ()=>import("@/components/personal/withdraw")
const dayReport = ()=>import("@/components/personal/dayReport")
const fundRecord = ()=>import("@/components/personal/fundRecord")
const teamReport = ()=>import("@/components/personal/teamReport")
const auditInfo = ()=>import("@/components/personal/auditInfo")
const credit = ()=>import("@/components/personal/credit")
const balance = ()=>import("@/components/personal/balance")
const balanceList = ()=>import("@/components/personal/balanceList")
const shop = ()=>import("@/components/personal/shop")
const assistShop = ()=>import("@/components/personal/assistShop")
const progressTasks = ()=>import("@/components/personal/progressTasks")
const taskList = ()=>import("@/components/taskList")
const taskShow = ()=>import("@/components/taskShow")
const taskInfo = ()=>import("@/components/taskInfo")

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: false,requireAuth:false},}, // 首页
        {path: '/language', name:'language', component:language, meta: {keepAlive: false,requireAuth:false},}, // 切换语言
        {path: '/line', name:'line', component:line, meta: {keepAlive: false,requireAuth:false},}, // 线路选择
        {path: '/mytask', name:'mytask', component:mytask, meta: {keepAlive: false,requireAuth:true},}, // 我的任务
        {path: '/vip', name:'vip', component:vip, meta: {keepAlive: false,requireAuth:false},}, // VIP
        {path: '/profit', name:profit, component:profit, meta: {keepAlive: false,requireAuth:false},}, // 收益
        {path: '/login', name:login, component:login, meta: {keepAlive: false,requireAuth:false},}, // 登录页
        {path: '/register', name:register, component:register, meta: {keepAlive: false,requireAuth:false},}, // 注册
        {path: '/personal', name:personal, component:personal, meta: {keepAlive: false,requireAuth:true},}, // 个人中心
        {path: '/service', name:service, component:service, meta: {keepAlive: false,requireAuth:false},}, // 在线客服
        {path: '/help', name:help, component:help, meta: {keepAlive: false,requireAuth:false},}, // 在线客服详情页面
        {path: '/task', name:task, component:task, meta: {keepAlive: false,requireAuth:false},}, // 每日任务
        {path: '/video', name:video, component:video, meta: {keepAlive: false,requireAuth:false},}, // 视频教程
        {path: '/promote', name:promote, component:promote, meta: {keepAlive: false,requireAuth:false},}, // 推广奖励
        {path: '/postTask', name:postTask, component:postTask, meta: {keepAlive: false,requireAuth:true},}, // 发布任务
        {path: '/wallet', name:wallet, component:wallet, meta: {keepAlive: false,requireAuth:true},}, // 我的钱包
        {path: '/recharge', name:recharge, component:recharge, meta: {keepAlive: false,requireAuth:true},}, // 充值记录
        {path: '/auditRecord', name:auditRecord, component:auditRecord, meta: {keepAlive: false,requireAuth:true},}, // 审核任务
        {path: '/auditInfo', name:auditInfo, component:auditInfo, meta: {keepAlive: false,requireAuth:true},}, // 审核任务详情
        {path: '/postRecord', name:postRecord, component:postRecord, meta: {keepAlive: false,requireAuth:true},}, // 发布管理
        {path: '/userInfo', name:userInfo, component:userInfo, meta: {keepAlive: false,requireAuth:true},}, // 用户信息
        {path: '/bankCard', name:bankCard, component:bankCard, meta: {keepAlive: false,requireAuth:true},}, // 银行卡绑定
        {path: '/realName', name:realName, component:realName, meta: {keepAlive: false,requireAuth:true},}, // 真实姓名
        {path: '/updatePass', name:updatePass, component:updatePass, meta: {keepAlive: false,requireAuth:true},}, // 修改密码
        {path: '/updateMoneyPass', name:updateMoneyPass, component:updateMoneyPass, meta: {keepAlive: false,requireAuth:true},}, // 修改资金密码
        {path: '/withdraw', name:withdraw, component:withdraw, meta: {keepAlive: false,requireAuth:true},}, // 提现
        {path: '/dayReport', name:dayReport, component:dayReport, meta: {keepAlive: false,requireAuth:true},}, // 日结报表
        {path: '/fundRecord', name:fundRecord, component:fundRecord, meta: {keepAlive: false,requireAuth:true},}, // 充值记录
        {path: '/teamReport', name:teamReport, component:teamReport, meta: {keepAlive: false,requireAuth:true},}, // 团队报表
        {path: '/credit', name:credit, component:credit, meta: {keepAlive: false,requireAuth:true},}, // 信用中心
        {path: '/balance', name:balance, component:balance, meta: {keepAlive: false,requireAuth:true},}, // 余额宝
        {path: '/balanceList', name:balanceList, component:balanceList, meta: {keepAlive: false,requireAuth:true},}, // 余额宝支出列表
        {path: '/shop', name:shop, component:shop, meta: {keepAlive: false,requireAuth:true},}, // 商店
        {path: '/assistShop', name:assistShop, component:assistShop, meta: {keepAlive: false,requireAuth:true},}, // 任务分发商店
        {path: '/progressTasks', name:progressTasks, component:progressTasks, meta: {keepAlive: false,requireAuth:true},}, // 任务进度
        {path: '/taskList', name:taskList, component:taskList, meta: {keepAlive: false,requireAuth:true},}, // 任务列表
        {path: '/taskShow', name:taskShow, component:taskShow, meta: {keepAlive: false,requireAuth:true},}, // 任务详情
        {path: '/taskInfo', name:taskInfo, component:taskInfo, meta: {keepAlive: false,requireAuth:true},}, // 已接任务信息
    ]
})

