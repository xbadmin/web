import Vue from 'vue'//引入vue
import Vuex from 'vuex'//引入vuex
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        apiUrl: location.hostname == 'localhost' ? '/api/' : location.protocol + '//' + location.hostname + '/',
        // apiUrl: 'http://953848.com/',
        isLogin: false,
        userInfo:'',
        token:'',
        backData:'',
        lang:localStorage.getItem('locale') || 'cn',
    },
    plugins: [createPersistedState({storage: window.sessionStorage})],
    mutations: {
        pushIsLogin(state,e){
            state.isLogin = e
        },
        pushUserInfo(state,e){
            state.userInfo = e
        },
        pushBackData(state,e){
            state.backData = e
        },
        pushToken(state,e){
            state.token = e
        },
    }
});


export default store
