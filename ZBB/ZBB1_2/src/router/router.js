import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/tabs/index")
const index2 = ()=>import("@/components/tabs/index2")
const personal = ()=>import("@/components/tabs/personal")
const licai = ()=>import("@/components/tabs/licai")
const assignment = ()=>import("@/components/tabs/assignment")
const invest = ()=>import("@/components/tabs/invest")

const login = ()=>import("@/components/login")
const register = ()=>import("@/components/register")
const noPass = ()=>import("@/components/noPass")
const investDetailed = ()=>import("@/components/investDetailed")
const investApply = ()=>import("@/components/investApply")
const resetPayPass = ()=>import("@/components/resetPayPass")
const zsfb = ()=>import("@/components/zsfb")
const alipay = ()=>import("@/components/alipay")
const share = ()=>import("@/components/share")
const sportEvent = ()=>import("@/components/sportEvent")
const baoku = ()=>import("@/components/baoku")
const integral = ()=>import("@/components/center/integral")

//personal
const recharge = ()=>import("@/components/center/recharge")
const cash = ()=>import("@/components/center/cash")
const bank = ()=>import("@/components/center/bank/bank")
const addBank = ()=>import("@/components/center/bank/addBank")
const invests = ()=>import("@/components/center/invests")
const cost = ()=>import("@/components/center/cost")
const founds = ()=>import("@/components/center/founds")
const touzi = ()=>import("@/components/center/touzi")
const touziDetailed = ()=>import("@/components/center/touziDetailed")
const contract = ()=>import("@/components/center/contract")
const address = ()=>import("@/components/center/address")
const editAddress = ()=>import("@/components/center/editAddress")
const addAddress = ()=>import("@/components/center/addAddress")
const order = ()=>import("@/components/center/order")
const volume = ()=>import("@/components/center/volume")
const notice = ()=>import("@/components/center/notice")
const account = ()=>import("@/components/center/account")
const about = ()=>import("@/components/center/about")
const auth = ()=>import("@/components/center/auth")
const setpass = ()=>import("@/components/center/setpass")
const art = ()=>import("@/components/center/art")
const server = ()=>import("@/components/center/server")
const rechargeMoney = ()=>import("@/components/center/rechargeMoney")
const showType = ()=>import("@/components/showType")

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: false,requireAuth:false},}, // 首页
        {path: '/index2', name:'index2', component:index2, meta: {keepAlive: false,requireAuth:false},}, // 首页
        {path: '/personal', name:'register', component:personal, meta: {keepAlive: false,requireAuth:false},}, // 个人中心
        {path: '/licai', name:'licai', component:licai, meta: {keepAlive: false,requireAuth:false},}, // 防疫
        {path: '/assignment', name:'assignment', component:assignment, meta: {keepAlive: false,requireAuth:false},}, // 任务
        {path: '/invest', name:'invest', component:invest, meta: {keepAlive: false,requireAuth:false},}, // 投资

        {path: '/login', name:'login', component:login, meta: {keepAlive: false,requireAuth:false},}, // 登录页
        {path: '/register', name:'register', component:register, meta: {keepAlive: false,requireAuth:false},}, // 注册页
        {path: '/noPass', name:'noPass', component:noPass, meta: {keepAlive: false,requireAuth:false},}, // 忘记密码
        {path: '/investDetailed', name:'investDetailed', component:investDetailed, meta: {keepAlive: false,requireAuth:false},}, // 投资详情
        {path: '/investApply', name:'investApply', component:investApply, meta: {keepAlive: false,requireAuth:false},}, // 投资下单
        {path: '/resetPayPass', name:'resetPayPass', component:resetPayPass, meta: {keepAlive: false,requireAuth:false},}, // 忘记支付密码
        {path: '/zsfb', name:'zsfb', component:zsfb, meta: {keepAlive: false,requireAuth:false},}, // 知识风暴
        {path: '/alipay', name:'alipay', component:alipay, meta: {keepAlive: false,requireAuth:false},}, // 支付宝设置
        {path: '/share', name:'share', component:share, meta: {keepAlive: false,requireAuth:false},}, // 邀请好友
        {path: '/sportEvent', name:'sportEvent', component:sportEvent, meta: {keepAlive: false,requireAuth:false},}, // 运动加油站
        {path: '/baoku', name:'baoku', component:baoku, meta: {keepAlive: false,requireAuth:false},}, // 宝库
        {path: '/integral', name:'integral', component:integral, meta: {keepAlive: false,requireAuth:false},}, // 积分
        {path: '/showType', name:'showType', component:showType, meta: {keepAlive: false,requireAuth:false},}, //充值中心

        //personal
        {path: '/recharge', name:'recharge', component:recharge, meta: {keepAlive: false,requireAuth:false},}, // 充值
        {path: '/cash', name:'cash', component:cash, meta: {keepAlive: false,requireAuth:false},}, // 提款
        {path: '/bank', name:'bank', component:bank, meta: {keepAlive: false,requireAuth:false},}, // 银行卡
        {path: '/addBank', name:'addBank', component:addBank, meta: {keepAlive: false,requireAuth:false},}, // 添加银行卡
        {path: '/invests', name:'invests', component:invests, meta: {keepAlive: false,requireAuth:false},}, // 充值记录
        {path: '/cost', name:'cost', component:cost, meta: {keepAlive: false,requireAuth:false},}, // 提款记录
        {path: '/touzi', name:'touzi', component:touzi, meta: {keepAlive: false,requireAuth:false},}, // 投资
        {path: '/touzi/detailed', name:'touziDetailed', component:touziDetailed, meta: {keepAlive: false,requireAuth:false},}, // 投资详细
        {path: '/founds', name:'founds', component:founds, meta: {keepAlive: false,requireAuth:false},}, // 资金
        {path: '/contract', name:'contract', component:contract, meta: {keepAlive: false,requireAuth:false},}, // 合同
        {path: '/address', name:'address', component:address, meta: {keepAlive: false,requireAuth:false},}, // 地址
        {path: '/editAddress', name:'editAddress', component:editAddress, meta: {keepAlive: false,requireAuth:false},}, // 编辑地址
        {path: '/addAddress', name:'addAddress', component:addAddress, meta: {keepAlive: false,requireAuth:false},}, // 编辑地址
        {path: '/order', name:'order', component:order, meta: {keepAlive: false,requireAuth:false},}, //我的订单
        {path: '/volume', name:'volume', component:volume, meta: {keepAlive: false,requireAuth:false},}, //卷包
        {path: '/notice', name:'notice', component:notice, meta: {keepAlive: false,requireAuth:false},}, //消息
        {path: '/account', name:'account', component:account, meta: {keepAlive: false,requireAuth:false},}, //用户
        {path: '/about', name:'about', component:about, meta: {keepAlive: false,requireAuth:false},}, //关于
        {path: '/auth', name:'auth', component:auth, meta: {keepAlive: false,requireAuth:false},}, //实名
        {path: '/setpass', name:'setpass', component:setpass, meta: {keepAlive: false,requireAuth:false},}, //设置密码，支付
        {path: '/art', name:'art', component:art, meta: {keepAlive: false,requireAuth:false},}, //aboutdetailed
        {path: '/server', name:'server', component:server, meta: {keepAlive: false,requireAuth:false},}, //客服
        {path: '/rechargeMoney', name:'rechargeMoney', component:rechargeMoney, meta: {keepAlive: false,requireAuth:false},}, //充值中心

    ]
})

