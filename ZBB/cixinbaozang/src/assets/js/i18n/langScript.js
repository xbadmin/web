/** 语言包同步到多语言js文件

 1.全局安装依赖：npm install node-xlsx --save
 2.运行脚本命令：node langeScript.js **/

// 1.读取
var xlsx = require("node-xlsx");

// 解析得到文档中的所有 sheet
var sheets = xlsx.parse("./语言包.xlsx");
// var helpSheet = xlsx.parse("../h5/lang/语言包-帮助中心.xlsx");

// 遍历 sheet
let zhData = "",
    enData = "",
    tnData = "",
    vnData = "";

doData(sheets);
// doData(helpSheet);
function getTran(item, v) {
    if (item, v) {
        return v
    } else {
        return item
    }
}

function doData(sheetData) {
    sheetData.forEach(function (sheet) {
        // console.log('名字',sheet['name']);
        // 读取每行内容
        for (var rowId in sheet["data"]) {
            if (rowId != 0) {
                // console.log('id',rowId);
                var row = sheet["data"][rowId];
                if (row[0] && row[0].trim() && aleadyHasKey(row[0], zhData)) {
                    //有key 且不重复
                    zhData += "\n\"" + row[0] + "\":" + "\"" + getTran(row[0], row[1]) + "\","; // 中文对象
                    enData += "\n\"" + row[0] + "\":" + "\"" + getTran(row[0], row[2]) + "\","; // 英文对象
                    tnData += "\n\"" + row[0] + "\":" + "\"" + getTran(row[0], row[3]) + "\","; // 英文对象
                    vnData += "\n\"" + row[0] + "\":" + "\"" + getTran(row[0], row[4]) + "\","; // 英文对象
                    // console.log(22, row[5])
                }
            }
        }
    });
}

function aleadyHasKey(str, data) {
    let string = `\n${str}:`;
    let reg = new RegExp(string, "g");
    if (data.search(reg) === -1) {
        return true;
    } else {
        console.log("注意：key重复==>" + str);
    }
}

// 2.删
// 引入文件模块

// const langZh = require("../h5/lang/zh");
// const langEn = require("../h5/lang/en");
let fs = require("fs");

fs.writeFile("./config/zh.js", "", function () {
    // console.log('done')
});
fs.writeFile("./config/en.js", "", function () {
    // console.log('done')
});
fs.writeFile("./config/tn.js", "", function () {
    // console.log('done')
});
fs.writeFile("./config/vn.js", "", function () {
    // console.log('done')
});
// 3.写入
let params = "export default{";
fs.appendFile("./config/zh.js", params + zhData + "\n}", function (err) {
    if (!err) {
        console.log("中文包同步完成");
    }
});
fs.appendFile("./config/en.js", params + enData + "\n}", function (err) {
    if (!err) {
        console.log("英文包同步完成");
    }
});
fs.appendFile("./config/tn.js", params + tnData + "\n}", function (err) {
    if (!err) {
        console.log("泰国语同步完成");
    }
});
fs.appendFile("./config/vn.js", params + vnData + "\n}", function (err) {
    if (!err) {
        console.log("越南语同步完成");
    }
});
/* 批量制造key

let aa = `Deposit and withdrawal
Anticorruption rules
Order process
registration process
Overvnew
Rules and terms`

let bb = aa.replace(/^\s*|\s*$/g,"")
let c = bb.replace(/\n/g,"换行识别字符串")
let d = c.replace(/\s/g,"_")
let e = d.replace(/换行识别字符串/g,"\n")

console.log(e)

 */
