import $ from  'jquery'
import router from '../../router/router'
import { Toast } from 'vant';

// 验证返回结果
let validateIs = true
export default{
    loading:function (onOff) {
        if(onOff){
            Toast.loading({message: '',forbidClick: true,});
        } else {
            Toast.clear()
        }
    },
    languageIndex:function (){
        let str = localStorage.getItem('locale')
        switch (str){
            case 'zh':
                return 1
            case 'en':
                return 2
            case 'tn':
                return 3
            case 'vn':
                return 4
        }
    },
    toPath:function (path,query){
        if(location.hash == ('#' + path)){
            return false
        }
        console.log(query)
        router.push({path:path,query:query})
    },
    getMyDate:function (str,status=1) {
        let oDate = new Date(str),
            oYear = oDate.getFullYear(),
            oMonth = oDate.getMonth()+1,
            oDay = oDate.getDate(),
            oHour = oDate.getHours(),
            oMin = oDate.getMinutes(),
            oSen = oDate.getSeconds(),
            oTime = oYear +'-'+ this.getzf(oMonth) +'-'+ this.getzf(oDay) +' '+ this.getzf(oHour) +':'+ this.getzf(oMin) +':'+this.getzf(oSen);//最后拼接时间
        if(status === 2){
            oTime = oYear +'-'+ this.getzf(oMonth) +'-'+ this.getzf(oDay)
        }
        return oTime ;
    },
    getzf:function (num) {
        if(parseInt(num) < 10){
            num = '0'+num;
        }
        return num;
    },
    //解析毫秒1
    milliDate:function (e){
        let date = new Date(e)
        return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDay()
    },
    splitNum: (num,i) => {
        if(!num){num = 0}
        if(!i){i = 2}
        num = parseFloat(num).toFixed(10) // 补0
        let str = num.toString()
        let len = 0, num1 = 0 ,num2 = 0;
        len = str.indexOf('.')
        num1 = str.substring(0,len)
        num2 = str.substring(len,len + (i+1))
        return num1 + num2
    },
    validateCodex:function(data){
        Toast.clear()
        // 用于跳出循环
        let circleIs = true
        let xss = /xss/i , script = /script/i , blank = /\s/
        if(xss.test(data) || script.test(data)){
            Toast('包含敏感字符，请重新输入!');
            validateIs = false
            circleIs = false
        } else if(blank.test(data)){
            Toast('请勿包含空格等!');
            validateIs = false
            circleIs = false
        } else if(!data){
            Toast('请输入内容!');
            validateIs = false
            circleIs = false
        } else {
            validateIs = true
        }
        return circleIs
    },
    // 前端非空,xss,特殊字符验证
    dataSafeValidate:function (type,data) {
        let that = this
        switch (type) {
            case 1:
                //字符串类型
                if(!that.validateCodex(data)) return false
                break;
            case 2:
                // 数组类型
                $.each(data,function (index,item) {
                    if(!that.validateCodex(item)) return false
                });
                break;
            default:
                console.log('暂无可执行语句')
        }
        return validateIs
    },
}
