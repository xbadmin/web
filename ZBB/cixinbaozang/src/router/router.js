import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/tabs/index")
const personal = ()=>import("@/components/tabs/personal")
const order = ()=>import("@/components/tabs/order")
const help = ()=>import("@/components/tabs/help")

const login = ()=>import("@/components/login")
const register = ()=>import("@/components/register")

const orderDetail = ()=>import("@/components/orderDetail")
const helpDetail = ()=>import("@/components/helpDetail")
const invite = ()=>import("@/components/invite")
const sign = ()=>import("@/components/sign")
const newwalfare = ()=>import("@/components/newwalfare")
const financial = ()=>import("@/components/financial")
const productDetail = ()=>import("@/components/productDetail")
const confirm = ()=>import("@/components/confirm")
const newtask = ()=>import("@/components/newtask")
const safe = ()=>import("@/components/safe")
const bankList = ()=>import("@/components/bankList")
const addBank = ()=>import("@/components/addBank")
const setpass = ()=>import("@/components/setpass")
const address = ()=>import("@/components/address")
const addAddress = ()=>import("@/components/addAddress")
const editAddress = ()=>import("@/components/editAddress")
const notice = ()=>import("@/components/notice")
const myOrder = ()=>import("@/components/myOrder")
const moneybag = ()=>import("@/components/moneybag")
const recharge = ()=>import("@/components/recharge")
const cash = ()=>import("@/components/cash")
const auth = ()=>import("@/components/auth")
const rechargeMoney = ()=>import("@/components/rechargeMoney")
const language = ()=>import("@/components/language")
const service = ()=>import("@/components/service")
const app = ()=>import("@/components/app")

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: false,requireAuth:true},}, // 首页
        {path: '/personal', name:'personal', component:personal, meta: {keepAlive: false,requireAuth:true},}, // 个人中心
        {path: '/order', name:'order', component:order, meta: {keepAlive: false,requireAuth:true},}, // 订单
        {path: '/help', name:'help', component:help, meta: {keepAlive: false,requireAuth:true},}, // 帮助

        // 登录注册页
        {path: '/login', name:'login', component:login, meta: {keepAlive: false,requireAuth:false},}, // 登录
        {path: '/register', name:'register', component:register, meta: {keepAlive: false,requireAuth:false},}, // 注册

        // 二级页面
        {path: '/orderDetail', name:'orderDetail', component:orderDetail, meta: {keepAlive: false,requireAuth:true},}, // 订单详情
        {path: '/helpDetail', name:'helpDetail', component:helpDetail, meta: {keepAlive: false,requireAuth:true},}, // 帮助详情
        {path: '/invite', name:'invite', component:invite, meta: {keepAlive: false,requireAuth:true},}, // 邀请
        {path: '/sign', name:'sign', component:sign, meta: {keepAlive: false,requireAuth:true},}, // 签到
        {path: '/newwalfare', name:'newwalfare', component:newwalfare, meta: {keepAlive: false,requireAuth:true},}, // 新人福利
        {path: '/financial', name:'financial', component:financial, meta: {keepAlive: false,requireAuth:true},}, // 理财
        {path: '/productDetail', name:'productDetail', component:productDetail, meta: {keepAlive: false,requireAuth:true},}, // 投资详情页
        {path: '/confirm', name:'confirm', component:confirm, meta: {keepAlive: false,requireAuth:true},}, // 确认订单
        {path: '/newtask', name:'newtask', component:newtask, meta: {keepAlive: false,requireAuth:true},}, // 新人任务
        {path: '/safe', name:'safe', component:safe, meta: {keepAlive: false,requireAuth:true},}, // 设置
        {path: '/bankList', name:'bankList', component:bankList, meta: {keepAlive: false,requireAuth:true},}, // 银行卡列表
        {path: '/addBank', name:'addBank', component:addBank, meta: {keepAlive: false,requireAuth:true},}, // 添加银行卡
        {path: '/setpass', name:'setpass', component:setpass, meta: {keepAlive: false,requireAuth:true},}, //设置密码
        {path: '/address', name:'address', component:address, meta: {keepAlive: false,requireAuth:true},}, //收货地址
        {path: '/addAddress', name:'addAddress', component:addAddress, meta: {keepAlive: false,requireAuth:true},}, //添加收货地址
        {path: '/editAddress', name:'editAddress', component:editAddress, meta: {keepAlive: false,requireAuth:true},}, //编辑收货地址
        {path: '/notice', name:'notice', component:notice, meta: {keepAlive: false,requireAuth:true},}, //消息
        {path: '/myOrder', name:'myOrder', component:myOrder, meta: {keepAlive: false,requireAuth:true},}, //我的订单
        {path: '/moneybag', name:'moneybag', component:moneybag, meta: {keepAlive: false,requireAuth:true},}, //我的钱包
        {path: '/recharge', name:'recharge', component:recharge, meta: {keepAlive: false,requireAuth:true},}, //充值
        {path: '/cash', name:'cash', component:cash, meta: {keepAlive: false,requireAuth:true},}, //提款
        {path: '/auth', name:'auth', component:auth, meta: {keepAlive: false,requireAuth:true},}, //实名认证
        {path: '/rechargeMoney', name:'rechargeMoney', component:rechargeMoney, meta: {keepAlive: false,requireAuth:true},}, //充值详情
        {path: '/language', name:'language', component:language, meta: {keepAlive: false,requireAuth:true},}, //选择语言
        {path: '/service', name:'service', component:service, meta: {keepAlive: false,requireAuth:false},}, //在线客服
        {path: '/app', name:'app', component:app, meta: {keepAlive: false,requireAuth:true},}, //APP下载
    ]
})

