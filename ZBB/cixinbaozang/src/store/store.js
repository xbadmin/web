import Vue from 'vue'//引入vue
import Vuex from 'vuex'//引入vuex
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        apiUrl: location.hostname == 'localhost' ? '/api/' : 'http://45.207.38.223:86/',
        isLogin: false,
        userInfo: '',
        token: '',
        serviceUrl:'',
        appUrl:'',
        moneyUnit:{zh:'￥', en:'$',vn:'₫', tn:'฿',},
    },
    plugins: [createPersistedState({storage: window.sessionStorage})],
    mutations: {
        pushIsLogin(state,e){
            state.isLogin = e
        },
        pushUserInfo(state,e){
            state.userInfo = e
        },
        pushToken(state,e){
            state.token = e
        },
        pushServiceUrl(state,e){
            state.serviceUrl = e
        },
        pushAppUrl(state,e){
            state.appUrl = e
        },
    }
});


export default store
