import Vue from 'vue'//引入vue
import Vuex from 'vuex'//引入vuex
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        apiUrl: location.hostname == 'localhost' ? '/zbb/api/' : location.protocol + '//' + location.hostname + '/api/',
        apiUrl2: location.hostname == 'localhost' ? '/zbb/' : location.protocol + '//' + location.hostname + '/',
        isLogin: false,
        userInfo: '',
        token: '',
        money: 0,
        dialogBoxStatus: true,
        type: 2, //1正式版  2 隐藏版
    },
    plugins: [createPersistedState({storage: window.sessionStorage})],
    mutations: {
        pushIsLogin(state,e){
            state.isLogin = e
        },
        pushUserInfo(state,e){
            state.userInfo = e
        },
        pushToken(state,e){
            state.token = e
        },
        pushMoney(state,e){
            state.money = e
        },
        pushDialogBoxStatus(state,e){
            state.dialogBoxStatus = e
        },
    }
});


export default store
