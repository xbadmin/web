import Vue from 'vue'
import { Message } from 'iview'
let vue = new Vue()

//  GET请求,将url和请求参数传入，通过Promise，将成功和失败的数据派发出去
export function httpGet (url, params) {
  return new Promise((resolve, reject) => {
    vue
      .$http({
        url: global.url + url,
        params: params,
        credentials: true,
        method: 'GET'
      })
      .then(response => {
        if (response.body.content || response.body.code === 1) {
          resolve(response)
        } else if (response.body.code === 2002) {
          // global.goOut()
        } else if (response.body.code === 1000) {
          Message.info(response.body.data)
        } else {
          Message.info(response.body.msg)
        }
      }, response => {
        reject(response)
      })
  })
}

//  POST请求，将url,请求参数和body传入，通过Promise，将成功和失败的数据派发出去
export function httpPost (url, params) {
  return new Promise((resolve, reject) => {
    vue
      .$http({
        url: global.url + url,
        params: params,
        credentials: true,
        method: 'POST'
      })
      .then(response => {
        if (response.body.content || response.body.code === 1) {
          resolve(response)
        } else if (response.body.code === 2002) {
          // global.goOut()
        } else if (response.body.code === 1000) {
          Message.info(response.body.data)
        } else {
          Message.info(response.body.msg)
        }
      }, response => {
        reject(response.bodyText)
      })
  })
}

export function httpPost1 (url, params) {
  return new Promise((resolve, reject) => {
    vue
      .$http({
        url: global.url + url,
        params: params,
        credentials: true,
        method: 'POST'
      })
      .then(response => {
        if (response.body.content || response.body.code === 1) {
          resolve(response)
        } else if (response.body.code === 2002) {
          // global.goOut()
        } else if (response.body.code === 1000) {
          Message.info(response.body.data)
        } else {
          Message.info(response.body.msg)
        }
      }, response => {
        reject(response.bodyText)
      })
  })
}
// 获取管理员信息
export function getAdmin () {
  return new Promise((resolve, reject) => {
    vue
      .$http({
        url: global.url + '/admin/agent/getCurrentAdmin',
        params: {},
        credentials: true,
        method: 'GET'
      })
      .then(response => {
        resolve(response.body)
      }, response => {
        reject(response)
      })
  })
}

// 获取管理员信息
export function getadminList () {
  return new Promise((resolve, reject) => {
    vue
      .$http({
        url: global.url + '/user/getAdminList',
        params: {
          token: global.token
        },
        credentials: true,
        method: 'GET'
      })
      .then(response => {
        resolve(response.body)
      }, response => {
        reject(response)
      })
  })
}
