// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import '../static/css/index.css'
import animated from 'animate.css'
import VueResource from 'vue-resource'
import VueClipboard from 'vue-clipboard2'
import {httpGet, httpPost, httpPost1, getAdmin} from '../static/js/index'

Vue.config.productionTip = false
Vue.use(iView)
Vue.use(animated)
Vue.use(VueResource)
Vue.use(VueClipboard)

//  判断是否处于登陆状态，如果不处于登陆状态加载登陆页面
//  在非登陆页面刷新加载websocket
//  其他页面保存私聊数组
Vue.prototype.otherPriveList = []
global.httpPost1 = httpPost1
getAdmin().then(res => {
  if (res.code === 1 && location.hash !== '#/?login=false' && location.hash !== '#/' && location.hash.indexOf('admin') === -1) {
    //  判断是否从登陆界面进入
    global.loginWebsocket = false
    //  创建全局变量
    res.data.chatShow = true
    Vue.prototype.adminData = res.data
    getToken(res.data)
  } else {
    router.push({ name: 'login' })
    /* eslint-disable no-new */
    new Vue({
      el: '#app',
      router,
      components: { App },
      template: '<App/>'
    })
  }
})
//  登陆后调用此方法加载websocket
global.getCurrentAdmin = function (res) {
  iView.Message.loading('正在加载聊天室相关功能...')
  //  判断是否从登陆界面进入
  global.loginWebsocket = true
  //  创建全局变量
  res.data.chatShow = true
  Vue.prototype.adminData = res.data
  getToken(res.data)
}
//  获取token
function getToken (res) {
  let params = {
    token: res.token,
    platformId: res.platformId,
    level: res.level,
    name: res.name
  }
  httpGet('/getToken', params).then(res => {
    global.token = res.body.data
    loadingUser()
  }, reason => {})
}
// 聊天室认证接口
function loadingUser () {
  let params = {
    token: global.token
  }
  httpPost('/getAuthentication', params).then(res => {
    if (res.body.code === 1) {
      global.user = res.body.data
      webSocket()
    }
  }, reason => {})
}
//  连接websocket
function webSocket () {
  if ('WebSocket' in window) {
    let webSocketIp = (location.host.indexOf('localhost') !== -1 || location.host.indexOf('xinboc.com') !== -1 || location.host.indexOf('xbchat.com') !== -1) ? 'localhost:8080/chat' : window.location.host
    // let webSocketIp = 'wss://chat.xbxb555.com'
    let ws = (location.protocol === 'https:') ? 'wss' : 'ws'
    global.websocket = new WebSocket(ws + '://' + webSocketIp + '/websocket?token=' + global.token)
    // global.websocket = new WebSocket(webSocketIp + '/websocket?token=' + global.token)
  } else {
  }
  const sendHeart = function () {
    global.websocket.send(JSON.stringify({
      chat: {
        content: '重连',
        form: 'headt',
        to: '',
        time: '00:00'
      },
      type: 'heart'
    }))
  }
  // 连接成功
  global.websocket.onopen = function (event) {
    iView.Message.destroy()
    iView.Message.success('连接成功')
    setInterval(sendHeart, 200000)
    if (global.loginWebsocket) {
      router.push({ name: 'index' })
    } else {
      /* eslint-disable no-new */
      new Vue({
        el: '#app',
        router,
        components: { App },
        template: '<App/>'
      })
    }
  }
  // 连接出错
  global.websocket.onerror = function (event) {
    iView.Message.destroy()
    iView.Message.error({
      content: 'websocket连接失败,将无法启用聊天系统相关功能,请刷新页面或联系技术人员',
      duration: 10,
      closable: true
    })
    // 隐藏聊天室
    Vue.prototype.adminData.chatShow = false
    if (global.loginWebsocket) {
      router.push({ name: 'index' })
    } else {
      /* eslint-disable no-new */
      new Vue({
        el: '#app',
        router,
        components: { App },
        template: '<App/>'
      })
    }
  }
  //  连接关闭
  global.websocket.onbeforeunload = function (event) { }
  //  收到消息
  global.websocket.onmessage = function (event) {
    // 解析收到的消息
    let message = JSON.parse(event.data)
    if (message.list !== null && message.list !== undefined) { // 在线列表
      Vue.prototype.userChatList = message.list
    }
    if (message.type === 'message') {
      // @消息
      if (message.chat.content.indexOf('@' + global.user.nickname) !== -1) {
        iView.Message.info({
          content: message.chat.form + '@了您',
          duration: 5000,
          closable: true
        })
      }
      // 私聊消息
      if (message.chat.to === global.user.name) {
        // 判断数组是否存在此用户
        let params = {
          name: message.chat.name,
          nickname: message.chat.form,
          headImg: message.chat.headImg
        }
        let str = JSON.stringify(Vue.prototype.otherPriveList)
        if (str.indexOf(JSON.stringify(params)) === -1) {
          Vue.prototype.otherPriveList.push(params)
        }
        iView.Notice.destroy()
        iView.Notice.open({
          title: '新消息',
          render: h => {
            return h('span', [
              h('a', {
                on: {
                  click: () => {
                    router.push({ name: 'chat' })
                  }
                }
              }, '您有' + Vue.prototype.otherPriveList.length + '条未读消息,请前往聊天室查看')
            ])
          }
        })
      }
      // 系统精灵
      if (message.chat.form === '系统精灵') {
        iView.Message.info({
          content: '用户' + message.chat.content,
          duration: 100000,
          closable: true
        })
      }
    }
  }
  // websocket关闭
  global.websocket.onclose = function (event) {
    console.log('聊天室重连')
    // 重连
    webSocket()
  }
}
