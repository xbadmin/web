import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import index from '@/components/index'
import robot from '@/components/robot'
import lottery from '@/components/lottery'
import manager from '@/components/manager'
import send from '@/components/send'
import other from '@/components/other'
import chat from '@/components/chat'
import { Message } from 'iview'

Vue.use(Router)

let vue = new Vue()

Vue.component('side-click', {
  data: function () {
    return {
      count: 0,
      theme3: 'dark',
      url: global.url + '/user/uploadHeadImg?id=',
      // url: 'https://chat.xbxb555.com/user/uploadHeadImg',
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      spinShowUp: false,
      imagePop: false,
      namePop: false,
      userNameModel: false,
      userName: '',
      verifyClose: false
    }
  },
  methods: {
    handleError: function (res, file) {
      Message.error('图片上传失败，请刷新页面重新上传')
    },
    formatError: function (file) {
      Message.info('请上传正确的图片,jpg | jpeg | png | gif')
    },
    formatProgress: function () {
      this.spinShowUp = true
    },
    formatSuccess: function (res) {
      this.spinShowUp = false
      Message.success('头像更换成功')
      if (res.code === 1) {
        global.user.headImg = res.data.headImg
        this.data.headImg = res.data.headImg
      }
    },
    upName: function () {
      if (!this.userName) {
        this.$Message.info('请正确填写内容')
        return false
      }
      this.spinShowUp = true
      let params = {
        id: this.data.id,
        nickname: this.userName
      }
      vue.$http({
        url: global.url + '/user/updateUser',
        // url: 'https://chat.xbxb555.com/user/updateUser',
        params: params,
        credentials: true,
        method: 'POST'
      }).then(response => {
        if (response.body.code === 1) {
          global.user.nickName = response.body.nickname
          this.data.nickName = response.body.data.nickname
          this.spinShowUp = false
          Message.success('修改成功')
        }
      }, reason => { })
    },
    cancel: function (res) { }
  },
  props: ['data'],
  template: '<Menu :theme="theme3" width="12%" :active-name="$route.name" class="h-100 f-l"><div class="h-17 w-100 flex-c demo-spin-article p-r">' +
    '<Upload :on-progress="formatProgress"  :action="url + data.id " :with-credentials="true" :on-success="formatSuccess" :on-format-error="formatError" :headers="headers" :format="[\'jpg\',\'jpeg\',\'png\',\'gif\']" :on-error="handleError">' +
    '<div class="w-8 h-8 p-r"><img :src="data.headImg"  @mouseover="imagePop = true" alt="" class="w-8 b-r-50 p-a">' +
    '<div @mouseleave="imagePop = false" v-show="imagePop" class="w-8 h-8 b-r-50 p-a bg-004 flex-c c-w"><Button icon="md-images"  shape="circle"></Button></div></div>' +
    '</Upload><span class="t-c m-55-t p-a m-55-b c-f0">{{data.platformId}}</span>' +
    '<span class="c-w t-c m-70-t p-a m-55-b" v-show="!namePop" @mouseover="namePop = true">{{data.nickName}}</span><span class="c-w t-c m-70-t p-a m-55-b cur-p" v-show="namePop" @click="userNameModel = true"  @mouseleave="namePop = false">修改昵称</span>' +
    ' <Modal v-model="userNameModel"  title="修改昵称"  @on-ok="upName"  @on-cancel="cancel" class="t-c"><Row class="m-5-t m-5-b l-h-25 "> <Col span="8">用户昵称：</Col> <Col span="8"><Input v-model="userName" placeholder="请输入姓名" class="w-15" /></Col> </Row></Modal>' +
    '<Spin size="large" fix v-if="spinShowUp"></Spin></div>' +
    '<MenuItem name="index"  to="/index"><Icon type="ios-people" />用户列表</MenuItem>' +
    '<MenuItem name="robot" to="/robot"><Icon type="md-list"  />机器人</MenuItem>' +
    '<MenuItem name="lottery" to="/lottery"><Icon type="md-bookmarks" />计划管理</MenuItem>' +
    '<MenuItem name="send" to="/send"><Icon type="md-send" />消息推送</MenuItem>' +
    '<MenuItem v-show="data.chatShow" name="chat" to="/chat"><Icon type="ios-chatboxes" />聊天室</MenuItem>' +
    '<MenuItem name="manager" to="/manager"><Icon type="md-person"  />管理员账号</MenuItem>' +
    '<MenuItem name="other" to="/other"><Icon type="md-settings" />系统设置</MenuItem>' +
    '<MenuItem name="login" :to="{path:\'/\',query: {login: \'false\'}}"replace><Icon type="ios-exit" />退出登录</MenuItem>' +
    '</Menu>'
})
export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },
    {
      path: '/index',
      name: 'index',
      component: index
    },
    {
      path: '/robot',
      name: 'robot',
      component: robot
    },
    {
      path: '/lottery',
      name: 'lottery',
      component: lottery
    },
    {
      path: '/manager',
      name: 'manager',
      component: manager
    },
    {
      path: '/send',
      name: 'send',
      component: send
    },
    {
      path: '/other',
      name: 'other',
      component: other
    },
    {
      path: '/chat',
      name: 'chat',
      component: chat
    }
  ]
})
// 接口域名
global.url = (location.host.indexOf('localhost') !== -1 || location.host.indexOf('xinboc.com') !== -1 || location.host.indexOf('xbchat.com') !== -1) ? (location.protocol + '//' + location.hostname + ':8080/chat') : location.protocol + '//' + location.host
// global.url = 'https://chat.xbxb555.com'
//  图片验证
global.verifyPicBoole = false
global.verifyPic = function (e) {
  let format = ['bmp', 'jpg', 'gif', 'png']
  let headVerify = e.substring(e.lastIndexOf('.') + 1)
  //  判断头像后缀名
  if (format.indexOf(headVerify) === -1) {
    global.verifyPicBoole = true
  }
}
//  判断登录状态
global.goOut = function (e) {
  window.location.reload()
}
