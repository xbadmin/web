import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/tabs/index")
const login = ()=>import("@/components/login")
const register = ()=>import("@/components/register")

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: true,requireAuth:false},}, // 首页
        {path: '/login', name:'login', component:login, meta: {keepAlive: false,requireAuth:false},}, // 登录页面
        {path: '/register', name:'register', component:register, meta: {keepAlive: false,requireAuth:false},}, // 注册页面
    ]
})

