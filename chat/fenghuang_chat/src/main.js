import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store/store.js'
import common from './assets/js/common.js'
import {post,fetch,patch,put} from '@/assets/js/http'
import VueCookies from 'vue-cookies'

Vue.config.productionTip = false
Vue.prototype.$post = post;
Vue.prototype.$fetch = fetch;
Vue.prototype.$patch = patch;
Vue.prototype.$put = put;

// vant html组件
import { Button } from 'vant'

Vue.use(Button)
Vue.use(VueCookies)

router.beforeEach((to, from, next) => {
  if(to.meta.requireAuth){
    if(store.state.isLogin){
      next()
    } else {
      next({path: '/login', query:{ redirect: to.fullPath}})
    }
  } else {
    next()
  }
})

new Vue({
  data: function(){
    return {
      //  全局变量  刷新后初始化
      common: common, // common方法
    }
  },
  render: h => h(App),
  router,
  store,
}).$mount('#app')
