/**
 * @fileoverview gRPC-Web generated client stub for pb
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var connect_ext_pb = require('./connect.ext_pb.js')
const proto = {};
proto.pb = require('./connect.int_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pb.ConnectIntClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pb.ConnectIntPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.DeliverMessageReq,
 *   !proto.pb.DeliverMessageResp>}
 */
const methodDescriptor_ConnectInt_DeliverMessage = new grpc.web.MethodDescriptor(
  '/pb.ConnectInt/DeliverMessage',
  grpc.web.MethodType.UNARY,
  proto.pb.DeliverMessageReq,
  proto.pb.DeliverMessageResp,
  /**
   * @param {!proto.pb.DeliverMessageReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.DeliverMessageResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.DeliverMessageReq,
 *   !proto.pb.DeliverMessageResp>}
 */
const methodInfo_ConnectInt_DeliverMessage = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.DeliverMessageResp,
  /**
   * @param {!proto.pb.DeliverMessageReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.DeliverMessageResp.deserializeBinary
);


/**
 * @param {!proto.pb.DeliverMessageReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.DeliverMessageResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.DeliverMessageResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.ConnectIntClient.prototype.deliverMessage =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.ConnectInt/DeliverMessage',
      request,
      metadata || {},
      methodDescriptor_ConnectInt_DeliverMessage,
      callback);
};


/**
 * @param {!proto.pb.DeliverMessageReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.DeliverMessageResp>}
 *     Promise that resolves to the response
 */
proto.pb.ConnectIntPromiseClient.prototype.deliverMessage =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.ConnectInt/DeliverMessage',
      request,
      metadata || {},
      methodDescriptor_ConnectInt_DeliverMessage);
};


module.exports = proto.pb;

