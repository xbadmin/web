/**
 * @fileoverview gRPC-Web generated client stub for pb
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var connect_ext_pb = require('./connect.ext_pb.js')
const proto = {};
proto.pb = require('./logic.ext_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pb.LogicExtClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pb.LogicExtPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.RegisterReq,
 *   !proto.pb.RegisterResp>}
 */
const methodDescriptor_LogicExt_Register = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/Register',
  grpc.web.MethodType.UNARY,
  proto.pb.RegisterReq,
  proto.pb.RegisterResp,
  /**
   * @param {!proto.pb.RegisterReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.RegisterResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.RegisterReq,
 *   !proto.pb.RegisterResp>}
 */
const methodInfo_LogicExt_Register = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.RegisterResp,
  /**
   * @param {!proto.pb.RegisterReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.RegisterResp.deserializeBinary
);


/**
 * @param {!proto.pb.RegisterReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.RegisterResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.RegisterResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.register =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/Register',
      request,
      metadata || {},
      methodDescriptor_LogicExt_Register,
      callback);
};


/**
 * @param {!proto.pb.RegisterReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.RegisterResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.register =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/Register',
      request,
      metadata || {},
      methodDescriptor_LogicExt_Register);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.SendMessageReq,
 *   !proto.pb.SendMessageResp>}
 */
const methodDescriptor_LogicExt_SendMessage = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/SendMessage',
  grpc.web.MethodType.UNARY,
  proto.pb.SendMessageReq,
  proto.pb.SendMessageResp,
  /**
   * @param {!proto.pb.SendMessageReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SendMessageResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.SendMessageReq,
 *   !proto.pb.SendMessageResp>}
 */
const methodInfo_LogicExt_SendMessage = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.SendMessageResp,
  /**
   * @param {!proto.pb.SendMessageReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SendMessageResp.deserializeBinary
);


/**
 * @param {!proto.pb.SendMessageReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.SendMessageResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.SendMessageResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.sendMessage =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/SendMessage',
      request,
      metadata || {},
      methodDescriptor_LogicExt_SendMessage,
      callback);
};


/**
 * @param {!proto.pb.SendMessageReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.SendMessageResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.sendMessage =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/SendMessage',
      request,
      metadata || {},
      methodDescriptor_LogicExt_SendMessage);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.PushRoomReq,
 *   !proto.pb.PushRoomResp>}
 */
const methodDescriptor_LogicExt_PushRoom = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/PushRoom',
  grpc.web.MethodType.UNARY,
  proto.pb.PushRoomReq,
  proto.pb.PushRoomResp,
  /**
   * @param {!proto.pb.PushRoomReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.PushRoomResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.PushRoomReq,
 *   !proto.pb.PushRoomResp>}
 */
const methodInfo_LogicExt_PushRoom = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.PushRoomResp,
  /**
   * @param {!proto.pb.PushRoomReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.PushRoomResp.deserializeBinary
);


/**
 * @param {!proto.pb.PushRoomReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.PushRoomResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.PushRoomResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.pushRoom =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/PushRoom',
      request,
      metadata || {},
      methodDescriptor_LogicExt_PushRoom,
      callback);
};


/**
 * @param {!proto.pb.PushRoomReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.PushRoomResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.pushRoom =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/PushRoom',
      request,
      metadata || {},
      methodDescriptor_LogicExt_PushRoom);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.AddFriendReq,
 *   !proto.pb.AddFriendResp>}
 */
const methodDescriptor_LogicExt_AddFriend = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/AddFriend',
  grpc.web.MethodType.UNARY,
  proto.pb.AddFriendReq,
  proto.pb.AddFriendResp,
  /**
   * @param {!proto.pb.AddFriendReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.AddFriendResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.AddFriendReq,
 *   !proto.pb.AddFriendResp>}
 */
const methodInfo_LogicExt_AddFriend = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.AddFriendResp,
  /**
   * @param {!proto.pb.AddFriendReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.AddFriendResp.deserializeBinary
);


/**
 * @param {!proto.pb.AddFriendReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.AddFriendResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.AddFriendResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.addFriend =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/AddFriend',
      request,
      metadata || {},
      methodDescriptor_LogicExt_AddFriend,
      callback);
};


/**
 * @param {!proto.pb.AddFriendReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.AddFriendResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.addFriend =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/AddFriend',
      request,
      metadata || {},
      methodDescriptor_LogicExt_AddFriend);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.AgreeAddFriendReq,
 *   !proto.pb.AgreeAddFriendResp>}
 */
const methodDescriptor_LogicExt_AgreeAddFriend = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/AgreeAddFriend',
  grpc.web.MethodType.UNARY,
  proto.pb.AgreeAddFriendReq,
  proto.pb.AgreeAddFriendResp,
  /**
   * @param {!proto.pb.AgreeAddFriendReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.AgreeAddFriendResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.AgreeAddFriendReq,
 *   !proto.pb.AgreeAddFriendResp>}
 */
const methodInfo_LogicExt_AgreeAddFriend = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.AgreeAddFriendResp,
  /**
   * @param {!proto.pb.AgreeAddFriendReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.AgreeAddFriendResp.deserializeBinary
);


/**
 * @param {!proto.pb.AgreeAddFriendReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.AgreeAddFriendResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.AgreeAddFriendResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.agreeAddFriend =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/AgreeAddFriend',
      request,
      metadata || {},
      methodDescriptor_LogicExt_AgreeAddFriend,
      callback);
};


/**
 * @param {!proto.pb.AgreeAddFriendReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.AgreeAddFriendResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.agreeAddFriend =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/AgreeAddFriend',
      request,
      metadata || {},
      methodDescriptor_LogicExt_AgreeAddFriend);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.SetFriendReq,
 *   !proto.pb.SetFriendResp>}
 */
const methodDescriptor_LogicExt_SetFriend = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/SetFriend',
  grpc.web.MethodType.UNARY,
  proto.pb.SetFriendReq,
  proto.pb.SetFriendResp,
  /**
   * @param {!proto.pb.SetFriendReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SetFriendResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.SetFriendReq,
 *   !proto.pb.SetFriendResp>}
 */
const methodInfo_LogicExt_SetFriend = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.SetFriendResp,
  /**
   * @param {!proto.pb.SetFriendReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SetFriendResp.deserializeBinary
);


/**
 * @param {!proto.pb.SetFriendReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.SetFriendResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.SetFriendResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.setFriend =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/SetFriend',
      request,
      metadata || {},
      methodDescriptor_LogicExt_SetFriend,
      callback);
};


/**
 * @param {!proto.pb.SetFriendReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.SetFriendResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.setFriend =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/SetFriend',
      request,
      metadata || {},
      methodDescriptor_LogicExt_SetFriend);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.GetFriendsReq,
 *   !proto.pb.GetFriendsResp>}
 */
const methodDescriptor_LogicExt_GetFriends = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/GetFriends',
  grpc.web.MethodType.UNARY,
  proto.pb.GetFriendsReq,
  proto.pb.GetFriendsResp,
  /**
   * @param {!proto.pb.GetFriendsReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetFriendsResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.GetFriendsReq,
 *   !proto.pb.GetFriendsResp>}
 */
const methodInfo_LogicExt_GetFriends = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GetFriendsResp,
  /**
   * @param {!proto.pb.GetFriendsReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetFriendsResp.deserializeBinary
);


/**
 * @param {!proto.pb.GetFriendsReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GetFriendsResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GetFriendsResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.getFriends =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/GetFriends',
      request,
      metadata || {},
      methodDescriptor_LogicExt_GetFriends,
      callback);
};


/**
 * @param {!proto.pb.GetFriendsReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GetFriendsResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.getFriends =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/GetFriends',
      request,
      metadata || {},
      methodDescriptor_LogicExt_GetFriends);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.CreateGroupReq,
 *   !proto.pb.CreateGroupResp>}
 */
const methodDescriptor_LogicExt_CreateGroup = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/CreateGroup',
  grpc.web.MethodType.UNARY,
  proto.pb.CreateGroupReq,
  proto.pb.CreateGroupResp,
  /**
   * @param {!proto.pb.CreateGroupReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.CreateGroupResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.CreateGroupReq,
 *   !proto.pb.CreateGroupResp>}
 */
const methodInfo_LogicExt_CreateGroup = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.CreateGroupResp,
  /**
   * @param {!proto.pb.CreateGroupReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.CreateGroupResp.deserializeBinary
);


/**
 * @param {!proto.pb.CreateGroupReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.CreateGroupResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.CreateGroupResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.createGroup =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/CreateGroup',
      request,
      metadata || {},
      methodDescriptor_LogicExt_CreateGroup,
      callback);
};


/**
 * @param {!proto.pb.CreateGroupReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.CreateGroupResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.createGroup =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/CreateGroup',
      request,
      metadata || {},
      methodDescriptor_LogicExt_CreateGroup);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.UpdateGroupReq,
 *   !proto.pb.UpdateGroupResp>}
 */
const methodDescriptor_LogicExt_UpdateGroup = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/UpdateGroup',
  grpc.web.MethodType.UNARY,
  proto.pb.UpdateGroupReq,
  proto.pb.UpdateGroupResp,
  /**
   * @param {!proto.pb.UpdateGroupReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.UpdateGroupResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.UpdateGroupReq,
 *   !proto.pb.UpdateGroupResp>}
 */
const methodInfo_LogicExt_UpdateGroup = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.UpdateGroupResp,
  /**
   * @param {!proto.pb.UpdateGroupReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.UpdateGroupResp.deserializeBinary
);


/**
 * @param {!proto.pb.UpdateGroupReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.UpdateGroupResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.UpdateGroupResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.updateGroup =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/UpdateGroup',
      request,
      metadata || {},
      methodDescriptor_LogicExt_UpdateGroup,
      callback);
};


/**
 * @param {!proto.pb.UpdateGroupReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.UpdateGroupResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.updateGroup =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/UpdateGroup',
      request,
      metadata || {},
      methodDescriptor_LogicExt_UpdateGroup);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.GetGroupReq,
 *   !proto.pb.GetGroupResp>}
 */
const methodDescriptor_LogicExt_GetGroup = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/GetGroup',
  grpc.web.MethodType.UNARY,
  proto.pb.GetGroupReq,
  proto.pb.GetGroupResp,
  /**
   * @param {!proto.pb.GetGroupReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetGroupResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.GetGroupReq,
 *   !proto.pb.GetGroupResp>}
 */
const methodInfo_LogicExt_GetGroup = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GetGroupResp,
  /**
   * @param {!proto.pb.GetGroupReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetGroupResp.deserializeBinary
);


/**
 * @param {!proto.pb.GetGroupReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GetGroupResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GetGroupResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.getGroup =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/GetGroup',
      request,
      metadata || {},
      methodDescriptor_LogicExt_GetGroup,
      callback);
};


/**
 * @param {!proto.pb.GetGroupReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GetGroupResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.getGroup =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/GetGroup',
      request,
      metadata || {},
      methodDescriptor_LogicExt_GetGroup);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.GetGroupsReq,
 *   !proto.pb.GetGroupsResp>}
 */
const methodDescriptor_LogicExt_GetGroups = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/GetGroups',
  grpc.web.MethodType.UNARY,
  proto.pb.GetGroupsReq,
  proto.pb.GetGroupsResp,
  /**
   * @param {!proto.pb.GetGroupsReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetGroupsResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.GetGroupsReq,
 *   !proto.pb.GetGroupsResp>}
 */
const methodInfo_LogicExt_GetGroups = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GetGroupsResp,
  /**
   * @param {!proto.pb.GetGroupsReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetGroupsResp.deserializeBinary
);


/**
 * @param {!proto.pb.GetGroupsReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GetGroupsResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GetGroupsResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.getGroups =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/GetGroups',
      request,
      metadata || {},
      methodDescriptor_LogicExt_GetGroups,
      callback);
};


/**
 * @param {!proto.pb.GetGroupsReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GetGroupsResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.getGroups =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/GetGroups',
      request,
      metadata || {},
      methodDescriptor_LogicExt_GetGroups);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.AddGroupMembersReq,
 *   !proto.pb.AddGroupMembersResp>}
 */
const methodDescriptor_LogicExt_AddGroupMembers = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/AddGroupMembers',
  grpc.web.MethodType.UNARY,
  proto.pb.AddGroupMembersReq,
  proto.pb.AddGroupMembersResp,
  /**
   * @param {!proto.pb.AddGroupMembersReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.AddGroupMembersResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.AddGroupMembersReq,
 *   !proto.pb.AddGroupMembersResp>}
 */
const methodInfo_LogicExt_AddGroupMembers = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.AddGroupMembersResp,
  /**
   * @param {!proto.pb.AddGroupMembersReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.AddGroupMembersResp.deserializeBinary
);


/**
 * @param {!proto.pb.AddGroupMembersReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.AddGroupMembersResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.AddGroupMembersResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.addGroupMembers =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/AddGroupMembers',
      request,
      metadata || {},
      methodDescriptor_LogicExt_AddGroupMembers,
      callback);
};


/**
 * @param {!proto.pb.AddGroupMembersReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.AddGroupMembersResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.addGroupMembers =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/AddGroupMembers',
      request,
      metadata || {},
      methodDescriptor_LogicExt_AddGroupMembers);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.UpdateGroupMemberReq,
 *   !proto.pb.UpdateGroupMemberResp>}
 */
const methodDescriptor_LogicExt_UpdateGroupMember = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/UpdateGroupMember',
  grpc.web.MethodType.UNARY,
  proto.pb.UpdateGroupMemberReq,
  proto.pb.UpdateGroupMemberResp,
  /**
   * @param {!proto.pb.UpdateGroupMemberReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.UpdateGroupMemberResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.UpdateGroupMemberReq,
 *   !proto.pb.UpdateGroupMemberResp>}
 */
const methodInfo_LogicExt_UpdateGroupMember = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.UpdateGroupMemberResp,
  /**
   * @param {!proto.pb.UpdateGroupMemberReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.UpdateGroupMemberResp.deserializeBinary
);


/**
 * @param {!proto.pb.UpdateGroupMemberReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.UpdateGroupMemberResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.UpdateGroupMemberResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.updateGroupMember =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/UpdateGroupMember',
      request,
      metadata || {},
      methodDescriptor_LogicExt_UpdateGroupMember,
      callback);
};


/**
 * @param {!proto.pb.UpdateGroupMemberReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.UpdateGroupMemberResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.updateGroupMember =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/UpdateGroupMember',
      request,
      metadata || {},
      methodDescriptor_LogicExt_UpdateGroupMember);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.DeleteGroupMemberReq,
 *   !proto.pb.DeleteGroupMemberResp>}
 */
const methodDescriptor_LogicExt_DeleteGroupMember = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/DeleteGroupMember',
  grpc.web.MethodType.UNARY,
  proto.pb.DeleteGroupMemberReq,
  proto.pb.DeleteGroupMemberResp,
  /**
   * @param {!proto.pb.DeleteGroupMemberReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.DeleteGroupMemberResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.DeleteGroupMemberReq,
 *   !proto.pb.DeleteGroupMemberResp>}
 */
const methodInfo_LogicExt_DeleteGroupMember = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.DeleteGroupMemberResp,
  /**
   * @param {!proto.pb.DeleteGroupMemberReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.DeleteGroupMemberResp.deserializeBinary
);


/**
 * @param {!proto.pb.DeleteGroupMemberReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.DeleteGroupMemberResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.DeleteGroupMemberResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.deleteGroupMember =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/DeleteGroupMember',
      request,
      metadata || {},
      methodDescriptor_LogicExt_DeleteGroupMember,
      callback);
};


/**
 * @param {!proto.pb.DeleteGroupMemberReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.DeleteGroupMemberResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.deleteGroupMember =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/DeleteGroupMember',
      request,
      metadata || {},
      methodDescriptor_LogicExt_DeleteGroupMember);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.GetGroupMembersReq,
 *   !proto.pb.GetGroupMembersResp>}
 */
const methodDescriptor_LogicExt_GetGroupMembers = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/GetGroupMembers',
  grpc.web.MethodType.UNARY,
  proto.pb.GetGroupMembersReq,
  proto.pb.GetGroupMembersResp,
  /**
   * @param {!proto.pb.GetGroupMembersReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetGroupMembersResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.GetGroupMembersReq,
 *   !proto.pb.GetGroupMembersResp>}
 */
const methodInfo_LogicExt_GetGroupMembers = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GetGroupMembersResp,
  /**
   * @param {!proto.pb.GetGroupMembersReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetGroupMembersResp.deserializeBinary
);


/**
 * @param {!proto.pb.GetGroupMembersReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GetGroupMembersResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GetGroupMembersResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.getGroupMembers =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/GetGroupMembers',
      request,
      metadata || {},
      methodDescriptor_LogicExt_GetGroupMembers,
      callback);
};


/**
 * @param {!proto.pb.GetGroupMembersReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GetGroupMembersResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.getGroupMembers =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/GetGroupMembers',
      request,
      metadata || {},
      methodDescriptor_LogicExt_GetGroupMembers);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.SignInReq,
 *   !proto.pb.SignInResp>}
 */
const methodDescriptor_LogicExt_SignIn = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/SignIn',
  grpc.web.MethodType.UNARY,
  proto.pb.SignInReq,
  proto.pb.SignInResp,
  /**
   * @param {!proto.pb.SignInReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SignInResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.SignInReq,
 *   !proto.pb.SignInResp>}
 */
const methodInfo_LogicExt_SignIn = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.SignInResp,
  /**
   * @param {!proto.pb.SignInReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SignInResp.deserializeBinary
);


/**
 * @param {!proto.pb.SignInReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.SignInResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.SignInResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.signIn =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/SignIn',
      request,
      metadata || {},
      methodDescriptor_LogicExt_SignIn,
      callback);
};


/**
 * @param {!proto.pb.SignInReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.SignInResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.signIn =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/SignIn',
      request,
      metadata || {},
      methodDescriptor_LogicExt_SignIn);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.GetUserReq,
 *   !proto.pb.GetUserResp>}
 */
const methodDescriptor_LogicExt_GetUser = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/GetUser',
  grpc.web.MethodType.UNARY,
  proto.pb.GetUserReq,
  proto.pb.GetUserResp,
  /**
   * @param {!proto.pb.GetUserReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetUserResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.GetUserReq,
 *   !proto.pb.GetUserResp>}
 */
const methodInfo_LogicExt_GetUser = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GetUserResp,
  /**
   * @param {!proto.pb.GetUserReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetUserResp.deserializeBinary
);


/**
 * @param {!proto.pb.GetUserReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GetUserResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GetUserResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.getUser =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/GetUser',
      request,
      metadata || {},
      methodDescriptor_LogicExt_GetUser,
      callback);
};


/**
 * @param {!proto.pb.GetUserReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GetUserResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.getUser =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/GetUser',
      request,
      metadata || {},
      methodDescriptor_LogicExt_GetUser);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.UpdateUserReq,
 *   !proto.pb.UpdateUserResp>}
 */
const methodDescriptor_LogicExt_UpdateUser = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/UpdateUser',
  grpc.web.MethodType.UNARY,
  proto.pb.UpdateUserReq,
  proto.pb.UpdateUserResp,
  /**
   * @param {!proto.pb.UpdateUserReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.UpdateUserResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.UpdateUserReq,
 *   !proto.pb.UpdateUserResp>}
 */
const methodInfo_LogicExt_UpdateUser = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.UpdateUserResp,
  /**
   * @param {!proto.pb.UpdateUserReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.UpdateUserResp.deserializeBinary
);


/**
 * @param {!proto.pb.UpdateUserReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.UpdateUserResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.UpdateUserResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.updateUser =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/UpdateUser',
      request,
      metadata || {},
      methodDescriptor_LogicExt_UpdateUser,
      callback);
};


/**
 * @param {!proto.pb.UpdateUserReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.UpdateUserResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.updateUser =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/UpdateUser',
      request,
      metadata || {},
      methodDescriptor_LogicExt_UpdateUser);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.SearchUserReq,
 *   !proto.pb.SearchUserResp>}
 */
const methodDescriptor_LogicExt_SearchUser = new grpc.web.MethodDescriptor(
  '/pb.LogicExt/SearchUser',
  grpc.web.MethodType.UNARY,
  proto.pb.SearchUserReq,
  proto.pb.SearchUserResp,
  /**
   * @param {!proto.pb.SearchUserReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SearchUserResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.SearchUserReq,
 *   !proto.pb.SearchUserResp>}
 */
const methodInfo_LogicExt_SearchUser = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.SearchUserResp,
  /**
   * @param {!proto.pb.SearchUserReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SearchUserResp.deserializeBinary
);


/**
 * @param {!proto.pb.SearchUserReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.SearchUserResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.SearchUserResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicExtClient.prototype.searchUser =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicExt/SearchUser',
      request,
      metadata || {},
      methodDescriptor_LogicExt_SearchUser,
      callback);
};


/**
 * @param {!proto.pb.SearchUserReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.SearchUserResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicExtPromiseClient.prototype.searchUser =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicExt/SearchUser',
      request,
      metadata || {},
      methodDescriptor_LogicExt_SearchUser);
};


module.exports = proto.pb;

