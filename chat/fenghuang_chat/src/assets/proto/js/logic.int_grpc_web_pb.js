/**
 * @fileoverview gRPC-Web generated client stub for pb
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var connect_ext_pb = require('./connect.ext_pb.js')

var logic_ext_pb = require('./logic.ext_pb.js')
const proto = {};
proto.pb = require('./logic.int_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pb.LogicIntClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pb.LogicIntPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.ConnSignInReq,
 *   !proto.pb.ConnSignInResp>}
 */
const methodDescriptor_LogicInt_ConnSignIn = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/ConnSignIn',
  grpc.web.MethodType.UNARY,
  proto.pb.ConnSignInReq,
  proto.pb.ConnSignInResp,
  /**
   * @param {!proto.pb.ConnSignInReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.ConnSignInResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.ConnSignInReq,
 *   !proto.pb.ConnSignInResp>}
 */
const methodInfo_LogicInt_ConnSignIn = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.ConnSignInResp,
  /**
   * @param {!proto.pb.ConnSignInReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.ConnSignInResp.deserializeBinary
);


/**
 * @param {!proto.pb.ConnSignInReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.ConnSignInResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.ConnSignInResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.connSignIn =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/ConnSignIn',
      request,
      metadata || {},
      methodDescriptor_LogicInt_ConnSignIn,
      callback);
};


/**
 * @param {!proto.pb.ConnSignInReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.ConnSignInResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.connSignIn =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/ConnSignIn',
      request,
      metadata || {},
      methodDescriptor_LogicInt_ConnSignIn);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.SyncReq,
 *   !proto.pb.SyncResp>}
 */
const methodDescriptor_LogicInt_Sync = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/Sync',
  grpc.web.MethodType.UNARY,
  proto.pb.SyncReq,
  proto.pb.SyncResp,
  /**
   * @param {!proto.pb.SyncReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SyncResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.SyncReq,
 *   !proto.pb.SyncResp>}
 */
const methodInfo_LogicInt_Sync = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.SyncResp,
  /**
   * @param {!proto.pb.SyncReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SyncResp.deserializeBinary
);


/**
 * @param {!proto.pb.SyncReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.SyncResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.SyncResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.sync =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/Sync',
      request,
      metadata || {},
      methodDescriptor_LogicInt_Sync,
      callback);
};


/**
 * @param {!proto.pb.SyncReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.SyncResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.sync =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/Sync',
      request,
      metadata || {},
      methodDescriptor_LogicInt_Sync);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.MessageACKReq,
 *   !proto.pb.MessageACKResp>}
 */
const methodDescriptor_LogicInt_MessageACK = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/MessageACK',
  grpc.web.MethodType.UNARY,
  proto.pb.MessageACKReq,
  proto.pb.MessageACKResp,
  /**
   * @param {!proto.pb.MessageACKReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.MessageACKResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.MessageACKReq,
 *   !proto.pb.MessageACKResp>}
 */
const methodInfo_LogicInt_MessageACK = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.MessageACKResp,
  /**
   * @param {!proto.pb.MessageACKReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.MessageACKResp.deserializeBinary
);


/**
 * @param {!proto.pb.MessageACKReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.MessageACKResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.MessageACKResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.messageACK =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/MessageACK',
      request,
      metadata || {},
      methodDescriptor_LogicInt_MessageACK,
      callback);
};


/**
 * @param {!proto.pb.MessageACKReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.MessageACKResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.messageACK =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/MessageACK',
      request,
      metadata || {},
      methodDescriptor_LogicInt_MessageACK);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.OfflineReq,
 *   !proto.pb.OfflineResp>}
 */
const methodDescriptor_LogicInt_Offline = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/Offline',
  grpc.web.MethodType.UNARY,
  proto.pb.OfflineReq,
  proto.pb.OfflineResp,
  /**
   * @param {!proto.pb.OfflineReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.OfflineResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.OfflineReq,
 *   !proto.pb.OfflineResp>}
 */
const methodInfo_LogicInt_Offline = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.OfflineResp,
  /**
   * @param {!proto.pb.OfflineReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.OfflineResp.deserializeBinary
);


/**
 * @param {!proto.pb.OfflineReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.OfflineResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.OfflineResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.offline =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/Offline',
      request,
      metadata || {},
      methodDescriptor_LogicInt_Offline,
      callback);
};


/**
 * @param {!proto.pb.OfflineReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.OfflineResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.offline =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/Offline',
      request,
      metadata || {},
      methodDescriptor_LogicInt_Offline);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.SubscribeRoomReq,
 *   !proto.pb.SubscribeRoomResp>}
 */
const methodDescriptor_LogicInt_SubscribeRoom = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/SubscribeRoom',
  grpc.web.MethodType.UNARY,
  proto.pb.SubscribeRoomReq,
  proto.pb.SubscribeRoomResp,
  /**
   * @param {!proto.pb.SubscribeRoomReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SubscribeRoomResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.SubscribeRoomReq,
 *   !proto.pb.SubscribeRoomResp>}
 */
const methodInfo_LogicInt_SubscribeRoom = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.SubscribeRoomResp,
  /**
   * @param {!proto.pb.SubscribeRoomReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.SubscribeRoomResp.deserializeBinary
);


/**
 * @param {!proto.pb.SubscribeRoomReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.SubscribeRoomResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.SubscribeRoomResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.subscribeRoom =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/SubscribeRoom',
      request,
      metadata || {},
      methodDescriptor_LogicInt_SubscribeRoom,
      callback);
};


/**
 * @param {!proto.pb.SubscribeRoomReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.SubscribeRoomResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.subscribeRoom =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/SubscribeRoom',
      request,
      metadata || {},
      methodDescriptor_LogicInt_SubscribeRoom);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.SendMessageReq,
 *   !proto.pb.SendMessageResp>}
 */
const methodDescriptor_LogicInt_SendMessage = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/SendMessage',
  grpc.web.MethodType.UNARY,
  logic_ext_pb.SendMessageReq,
  logic_ext_pb.SendMessageResp,
  /**
   * @param {!proto.pb.SendMessageReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  logic_ext_pb.SendMessageResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.SendMessageReq,
 *   !proto.pb.SendMessageResp>}
 */
const methodInfo_LogicInt_SendMessage = new grpc.web.AbstractClientBase.MethodInfo(
  logic_ext_pb.SendMessageResp,
  /**
   * @param {!proto.pb.SendMessageReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  logic_ext_pb.SendMessageResp.deserializeBinary
);


/**
 * @param {!proto.pb.SendMessageReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.SendMessageResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.SendMessageResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.sendMessage =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/SendMessage',
      request,
      metadata || {},
      methodDescriptor_LogicInt_SendMessage,
      callback);
};


/**
 * @param {!proto.pb.SendMessageReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.SendMessageResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.sendMessage =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/SendMessage',
      request,
      metadata || {},
      methodDescriptor_LogicInt_SendMessage);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.PushRoomReq,
 *   !proto.pb.PushRoomResp>}
 */
const methodDescriptor_LogicInt_PushRoom = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/PushRoom',
  grpc.web.MethodType.UNARY,
  logic_ext_pb.PushRoomReq,
  logic_ext_pb.PushRoomResp,
  /**
   * @param {!proto.pb.PushRoomReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  logic_ext_pb.PushRoomResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.PushRoomReq,
 *   !proto.pb.PushRoomResp>}
 */
const methodInfo_LogicInt_PushRoom = new grpc.web.AbstractClientBase.MethodInfo(
  logic_ext_pb.PushRoomResp,
  /**
   * @param {!proto.pb.PushRoomReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  logic_ext_pb.PushRoomResp.deserializeBinary
);


/**
 * @param {!proto.pb.PushRoomReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.PushRoomResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.PushRoomResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.pushRoom =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/PushRoom',
      request,
      metadata || {},
      methodDescriptor_LogicInt_PushRoom,
      callback);
};


/**
 * @param {!proto.pb.PushRoomReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.PushRoomResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.pushRoom =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/PushRoom',
      request,
      metadata || {},
      methodDescriptor_LogicInt_PushRoom);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.PushAllReq,
 *   !proto.pb.PushAllResp>}
 */
const methodDescriptor_LogicInt_PushAll = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/PushAll',
  grpc.web.MethodType.UNARY,
  proto.pb.PushAllReq,
  proto.pb.PushAllResp,
  /**
   * @param {!proto.pb.PushAllReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.PushAllResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.PushAllReq,
 *   !proto.pb.PushAllResp>}
 */
const methodInfo_LogicInt_PushAll = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.PushAllResp,
  /**
   * @param {!proto.pb.PushAllReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.PushAllResp.deserializeBinary
);


/**
 * @param {!proto.pb.PushAllReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.PushAllResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.PushAllResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.pushAll =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/PushAll',
      request,
      metadata || {},
      methodDescriptor_LogicInt_PushAll,
      callback);
};


/**
 * @param {!proto.pb.PushAllReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.PushAllResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.pushAll =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/PushAll',
      request,
      metadata || {},
      methodDescriptor_LogicInt_PushAll);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.GetUserReq,
 *   !proto.pb.GetUserResp>}
 */
const methodDescriptor_LogicInt_GetUser = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/GetUser',
  grpc.web.MethodType.UNARY,
  logic_ext_pb.GetUserReq,
  logic_ext_pb.GetUserResp,
  /**
   * @param {!proto.pb.GetUserReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  logic_ext_pb.GetUserResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.GetUserReq,
 *   !proto.pb.GetUserResp>}
 */
const methodInfo_LogicInt_GetUser = new grpc.web.AbstractClientBase.MethodInfo(
  logic_ext_pb.GetUserResp,
  /**
   * @param {!proto.pb.GetUserReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  logic_ext_pb.GetUserResp.deserializeBinary
);


/**
 * @param {!proto.pb.GetUserReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GetUserResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GetUserResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.getUser =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/GetUser',
      request,
      metadata || {},
      methodDescriptor_LogicInt_GetUser,
      callback);
};


/**
 * @param {!proto.pb.GetUserReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GetUserResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.getUser =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/GetUser',
      request,
      metadata || {},
      methodDescriptor_LogicInt_GetUser);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.GetUsersReq,
 *   !proto.pb.GetUsersResp>}
 */
const methodDescriptor_LogicInt_GetUsers = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/GetUsers',
  grpc.web.MethodType.UNARY,
  proto.pb.GetUsersReq,
  proto.pb.GetUsersResp,
  /**
   * @param {!proto.pb.GetUsersReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetUsersResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.GetUsersReq,
 *   !proto.pb.GetUsersResp>}
 */
const methodInfo_LogicInt_GetUsers = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GetUsersResp,
  /**
   * @param {!proto.pb.GetUsersReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GetUsersResp.deserializeBinary
);


/**
 * @param {!proto.pb.GetUsersReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GetUsersResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GetUsersResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.getUsers =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/GetUsers',
      request,
      metadata || {},
      methodDescriptor_LogicInt_GetUsers,
      callback);
};


/**
 * @param {!proto.pb.GetUsersReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GetUsersResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.getUsers =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/GetUsers',
      request,
      metadata || {},
      methodDescriptor_LogicInt_GetUsers);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.ServerStopReq,
 *   !proto.pb.ServerStopResp>}
 */
const methodDescriptor_LogicInt_ServerStop = new grpc.web.MethodDescriptor(
  '/pb.LogicInt/ServerStop',
  grpc.web.MethodType.UNARY,
  proto.pb.ServerStopReq,
  proto.pb.ServerStopResp,
  /**
   * @param {!proto.pb.ServerStopReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.ServerStopResp.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.ServerStopReq,
 *   !proto.pb.ServerStopResp>}
 */
const methodInfo_LogicInt_ServerStop = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.ServerStopResp,
  /**
   * @param {!proto.pb.ServerStopReq} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.ServerStopResp.deserializeBinary
);


/**
 * @param {!proto.pb.ServerStopReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.ServerStopResp)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.ServerStopResp>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.LogicIntClient.prototype.serverStop =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.LogicInt/ServerStop',
      request,
      metadata || {},
      methodDescriptor_LogicInt_ServerStop,
      callback);
};


/**
 * @param {!proto.pb.ServerStopReq} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.ServerStopResp>}
 *     Promise that resolves to the response
 */
proto.pb.LogicIntPromiseClient.prototype.serverStop =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.LogicInt/ServerStop',
      request,
      metadata || {},
      methodDescriptor_LogicInt_ServerStop);
};


module.exports = proto.pb;

