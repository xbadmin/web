<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title>聊天室</title>
    <link rel="stylesheet" href="css/reset.css" />
    <link rel="stylesheet" href="layer/mobile/need/layer.css" />
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="css/emoji.css" />
    <link rel="stylesheet" type="text/css" href="css/contextMenu.css" />
    <link rel="stylesheet" href="css/wrapper.css" />
</head>
<body>
<!-- 换肤选择框 -->
<div id="huanfu-down">
    <div id="huanfu-content-m">
        <div class="box">
            <img src="images/bodybackimg1.jpg" alt="" />
            <div class="box-content"></div>
        </div>
    </div>
</div>
<!--私聊界面-->
<div id="private-chat">
    <div class="pleft" id="boxscrol4">
        <ul id="chatuserlist">

        </ul>
    </div>
    <div class="pright">
        <div class="prhead">
            <img id="prhead-img" src="" ><span id="prhead-name"></span>
            <div class="prOperate">
                <div id="prclose"><img src="images/closepr.png" alt=""></div>
                <div id="prsmall"><img src="images/small.png" alt=""></div>
            </div>
        </div>
        <div class="prchat">
        </div>
        <div class="premj">
            <form class="am-form am-form-horizontal" id="formChatImgPr">
                <div >
                    <i class="icon-picture" style="color: #a0a3a3;cursor: pointer;" id="chatImgStylePr"></i> <input id="chatImgPr" style="display: none" type="file"  name="file" />
                </div>
            </form>
        </div>
        <div class="predit"  id="boxscrol5" contenteditable="true" class="area"></div>
        <div class="prsend">
            <button>发送</button>
        </div>
    </div>
</div>
<div id="private-chat-small">
    <span id="point">新消息</span>
    <div class="prsOp">
        <div id="prsclose"><img src="images/close-fff.png" alt=""></div>
        <div id="prbig"><img src="images/big.png" alt=""></div>
    </div>
</div>
<div class="warp" style="background-size:100% 100%;">
    <!--<div class="header">-->
        <!--<div id="logo">-->
            <!--<img src="images/201709020913067017.png" />-->
        <!--</div>-->
        <!--<div id="nav">-->
            <!--<div class="nav-list" style="background: url('images/pic1.png') 8px 20px no-repeat;font-weight: 600;">-->
                <!--<span style="color: yellow;">保存到桌面</span>-->
            <!--</div>-->
            <!--<div class="nav-list" style="background: url('images/phone.png') 8px 20px no-repeat;font-weight: 600;">-->
                <!--<span>手机版</span>-->
            <!--</div>-->
            <!--<div class="nav-list" style="position: relative;" id="nav_kefu-">-->
                <!--<img src="images/onlineQQ.png" />--> 
                <!--<div id="nav_kefu_qq">-->
                    <!--<ul>-->
                        <!--<li><a href="">提现专员</a></li>-->
                        <!--<li><a href="">代理专员</a></li>-->
                        <!--<li><a href="">红包专员</a></li>-->
                        <!--<li><a href="">QQ客服（1）</a></li>-->
                        <!--<li><a href="">QQ客服（2）</a></li>-->
                        <!--<li><a href="">系统咨询客服 </a></li>-->
                    <!--</ul>-->
                <!--</div>-->
            <!--</div>-->
            <!--<div class="nav-list" style="background: url('images/201709202122354929.png') 8px 20px no-repeat;">-->
                <!--<span>开户/注册</span>-->
            <!--</div>-->
            <!--<div class="nav-list" style="background: url('images/201709202122492562.png') 8px 20px no-repeat;">-->
                <!--<span>开奖网</span>-->
            <!--</div>-->
            <!--<div class="nav-list" style="background: url('images/201709202123146793.png') 8px 20px no-repeat;">-->
                <!--<span>导航网</span>-->
            <!--</div>-->
            <!--<div class="nav-list" style="background: url('images/201709202123335660.png') 8px 20px no-repeat;">-->
                <!--<span>APP下载</span>-->
            <!--</div>-->
            <!--<div class="nav-list" style="background: url('images/201709202123527581.png') 8px 20px no-repeat;">-->
                <!--<span>代理QQ</span>-->
            <!--</div>-->
            <!--<div class="nav-list" style="background: url('images/201709202124195514.png') 8px 20px no-repeat;">-->
                <!--<span>在线客服</span>-->
            <!--</div>-->
        <!--</div>-->
        <!--<div id="user-operate">-->
            <!--<div class="huanfu-module">-->
                <!--<div class="user-" id="huanfu">-->
                    <!--换肤-->
                <!--</div>-->
            <!--</div>-->
            <!--<div class="personal_menu">-->
                <!--<div class="personal_menu_name">-->
                    <!--<img src="images/userImg.png" style="width: 25px;height: 25px;border-radius: 50%;margin-right: 10px;" />-->
                    <!--<span id="user_img_name_2" style="white-space: nowrap "></span>-->
                <!--</div>-->
                <!--<div class="personal_menu_list" id="personal_show_switch">-->
                    <!--<div class="personal_menu_list_update">-->
                        <!--个人资料-->
                    <!--</div>-->
                <!--</div>-->
            <!--</div>-->
        <!--</div>-->
       <!--  修改资料框 -->
        <div id="personal_show_module">
            <div id="personal_update_module">
                <form class="am-form am-form-horizontal" id="formfile">
                    <div style="height: 180px;justify-content: center;">
                        <img src="images/userImg.png" id="file_img" />
                    </div>
                    <input id="file" style="display: none" type="file" name="file" />
                    <br />
                </form>
                <div class="my_box" style="margin-bottom: 10px;">
                    <input type="button" value="更换头像" class="warp_btn" />
                </div>
                <div id="update_ok_btn" style="display: none;">
                    <input type="button" value="确认修改" class="warp_btn" style="background: orange" onclick="ok_update_head()" />
                </div>
                <div id="update_ok_btn_url" style="display: none;">
                    <input type="button" value="确认修改" class="warp_btn" style="background: orange" onclick="ok_update_head_url()" />
                </div>
                <div id="user_img_name" style="height: 40px;justify-content: center;"></div>
                <div id="user_level" style="height: 20px;justify-content: center;"></div>
                <div style="height: 40px;padding-left: 30px;">
                    昵称：
                    <span id="user_name_text"></span>
                    <input type="text" id="user_update_name" class="user_update_text" style="display: none" />
                    <i class="icon-pencil"></i>
                </div>
                <div style="height: 40px;padding-left: 30px;">
                    性别：
                    <span id="user_sex_text">女</span>
                    <select name="" id="user_update_sex" class="user_update_text" style="display: none"> <option value="女">女</option> <option value="男">男</option> </select>
                    <i class="icon-pencil"></i>
                </div>
                <div style="height: 40px;padding-left: 30px;">
                    年龄：
                    <span id="user_age_text"></span>
                    <input type="text" style="display: none" id="user_update_age" class="user_update_text" />
                    <i class="icon-pencil"></i>
                </div>
                <div style="height: 40px; margin-top: 30px;">
                    <input type="button" value="保存信息" style="display: none;" id="user_update_ok" class="warp_btn" />
                </div>
                <div class="personal_update_close">
                    <img src="images/close.png" style="width: 100%;height: 100%" />
                </div>
            </div>
        </div>
    <!--</div>-->
    <!--初始化姓名-->
    <div id="initial_module">
        <div style="width: 200px; height: 160px;background: #ffffff;text-align: center;border-radius: 10px;">
            <div style="width: 100%;float: left;color: red;font-size: 12px;margin-top: 20px;">
                请设置您的初始昵称
            </div>
            <div style="width: 100%;float: left;margin-top: 10px;">
                <input type="text" id="ininial_name" />
            </div>
            <div style="width: 100%;float: left;margin-top: 10px;">
                <input type="button" value="保存" class="warp_btn" style="margin: 0;" id="initial_ok" />
            </div>
        </div>
    </div>
    <div class="main" style="right:18px;">
    	<div id="logoPath"></div>
        <div class="gg-classify-btn" style="z-index:1">
            <button ify="chat" class="gg-classify-btn-pitch"><i class="icon-home"></i>在线聊天</button>
            <button ify="live" ><i class="icon-facetime-video"></i>美女直播</button>
            <!-- <button ify="open" ><i class=" icon-spinner"></i>开奖</button>
            <button ify="active" ><i class="icon-eye-open"></i>活动</button> -->
            <button ify="no" id="huanfu" ><i class="icon-eye-open"></i>换肤</button>
            <button ify="no" id="personModule" ><i class="icon-user"></i>会员中心</button>
        </div>
        <div class="module-ify" id="live" style="display: none;height: 80%;margin-top:40px;">
            <object type="application/x-shockwave-flash" name="webyy_client" data=" https://yy.com/s/54880976/54880976/yycomscene.swf  " width="100%" height="100%" id="webyy_client">
                <param name="movie" value=" https://yy.com/s/54880976/54880976/yycomscene.swf  ">
                <param name="quality" value="high">
                <param name="bgcolor" value="#000">
                <param name="allowScriptAccess" value="always">
                <param name="allowFullScreen" value="true">
                <param name="wmode" value="opaque">
                <param name="menu" value="false">
                <param name="flashvars" value="source=goldFin">
            </object>
        </div>
        <div class="main- module-ify" id="chat" style="height:100%;right:20px">
            <div class="clear-message" style="z-index:1;">
            	<!-- <input type="button" value="查看更多消息" onclick="mhistori()" style="margin-right:30px;" /> -->
            	<input type="button" value="滚动" id="messageRoll" data-value="1" style="background:url('images/check.png') 0px -14px no-repeat;padding-left:18px;transition:none;" />
                <input type="button" value="清屏" onclick="clearMessage()" />
            </div>
            <!--公告-->
            <!--<div id="notice">-->
                <!--<b>公告：</b>-->
                <!--<img src="images/laba.png" style="width: 13px; height: 14px" />-->
                <!--<marquee scrollamount="3" scrolldelay="150" direction="left" onmouseover="this.stop();" onmouseout="this.start();" style="cursor: pointer;color:yellow;width: 90%;"  id="noticeMessage"></marquee>-->
            <!--</div>-->
            <!--聊天内容区域-->
            <div class="chatting" id="boxscroll2" style="margin-top:5%;height:78%;display:inline-block;">
                <div id="message" style="height: 100%;position: absolute;width:100%;">
					
                </div>
            </div>
            <!--消息发送区域-->
            <div class="editor" style="margin-top:1.6%;height:10%">
                <div style="height: 25px;background: rgba(50, 50, 50, 0.7);margin-top: -25px;" id="liaotianModule">
                    <!-- <div id="emoji2" class="emoji-container" style=" position: absolute;z-index: 1;left:19px;">
                        <img alt="smile" src="emoji/unicode/1f604.png" class="emoji-tbtn" />
                        <input type="hidden" class="val" />
                        
                    </div> -->
                    <form class="am-form am-form-horizontal" id="formChatImg">
                        <div style=" position: absolute; z-index: 1; top: -25px;left: 11px;">
                            <i class="icon-picture" style="color: #FFFFFF;cursor: pointer;" id="chatImgStyle"></i> <input id="chatImg" style="display: none" type="file"  name="file" />
                        </div>
                    </form>
                    <div id="robotIs" style="position: absolute;z-index: 99999;left:100px;margin-top:5px;display:none;">
                        <b id="robotName" style="color:red;"></b>
                        <input type="button" value="关闭" class="manageChatRobot" onclick="noRobotSend()">
                    </div>
                </div>
                <textarea id="boxscroll3"  class="area" placeholder="文明聊天，共创优质的聊天环境，如有问题请咨询在线客服"></textarea>
                <input type="button" id="send" onclick="sendTest()" value="发送" />
                <input type="button" id="robotSend" style="display:none;" onclick="robotSend()"data-id="" data-name="" data-headimg="" value="发送" />
            </div>
        </div>
        <div class="main-left" style="background-size:100%;display:none">
            <div style="width:  100%;float: left;height: 20%;">
                <div style="width: 100%;height: 25%;line-height: 50px;color: #fff;font-size: 16px;">公告</div>
                <div style="width: 100%;height: 75%;">
                    <marquee id="notice2" style="color: #fff; width:90%; height: 90%;margin-top:5px; " direction=up  scrolldelay="10" scrollamount="1" loop="-1" behavior="scroll" hspace="20" vspace="10" onMouseOver="this.stop()"  onMouseOut="this.start()">
                        
                    </marquee>
                </div>
            </div>
            <div style="float: left;width:100%;text-align: center;" id="userListGif">
            	<img src="images/loading-1.gif" style="margin-top:100px;width:25px;" />
            </div>
           <ul style="float: left;display:none;" id="userListModule">
                <li> <input type="button" value="" class="down-btn" id="on-line-member" /> <input type="button" value="客服/管理" id="on-line-server" /> </li>
                <!-- <li> <input type="text" /> </li> -->
                <li>
                    <div class="user" id="boxscroll" style="overflow:auto">
                    <style>
                    	.kfjs{
                    		position:relative;
                    		float:left;
                    		margin-top:10px;
                    	}
                    	.kftitle{
                    		border-left: 3px solid #ed4101;padding: 0 8px;margin: 3px 10px;font-size: 16px;color:#fff;
                    	}
                    	.kfjs span{
                    		display:block;
                    		position:absolute;
                    		color:#fff;
                    		font-size:10px;
                    	}
                    	.kfjs img{
                    		width:30px;
                    		border-radius: 50px;
                    		margin-left:5px
                    	}
                    	.kfjs #zskf{
                    		margin-top:-34px;
                    		margin-left:40px;
                    	}
                    	.kfjs #zskfqq{
                    		margin-top:-20px;
                    		margin-left:41px;
                    	}
                    	.kfjs input[type="button"]{
                    		border:none;
                    		padding :4px 12px;
                    		padding-left:25px;
                    		margin-left:58px;
                    		cursor: pointer;
                    		transition:.5s;
                    	}
                    	.kfjs input[type="button"]:hover{
                    		border-radius: 5px;
                    	}
                    </style>
                    	<div id="zskfModule" style="display:none;">
                    		<div class="kfjs">
                            	<div class="kftitle">我的专属客服</div>
                            	<div style="margin:8px 0;"><img src="http://gallery.cache.wps.cn/gallery/files/mat_material/2011/11/23/renwu/808568c8_bfb4_4101_9330_4f6df2ddac52_preview.png" /><span id="zskf">客服专员</span><span id="zskfqq"></span></div>
                            	<div>
<!--                             	<input type="button" value="在线私聊" style="border:1px solid red;background:url('images/qq.png') no-repeat;background-position:7px 5px;color:red;" id="zaixiankfbtn"> -->                            		
									<input type="button" value="QQ交谈" style="border:1px solid #1296db;background:url('images/qq.png') no-repeat;background-position:7px 5px;color:#1296db; " id="qqkfbtn">
                            	</div>
                         </div>
                    	</div>
                       <div id="user-member" style="height:100%;overflow:auto"> 
                          <!--    <ul style="height: 30px; float: left">
                            <li><img src="http://xbtu111.com/chatserver/userImg.png" alt=""></li>
                            <li>测试用户</li>
                            <li><img src="./images/jinpai.png" alt=""></li>
                            </ul> -->
                            
                        </div>
                        <div id="user-server" style="display: none">
                            <ul style="height: 30px; float: left">
                            <!--  <li><img src="http://xbtu111.com/chatserver/userImg.png" alt=""></li>
                            <li>管理员</li>
                            <li><img src="./images/levelMax.png" alt=""></li>
                            </ul> -->
                        </div>
                    </div> </li>
            </ul>
        </div>
        <div id="open-list" style="right:0px">
            <div id="close-left" style="cursor: pointer;display: none;"><img src="images/right-open.png" style="width: 15px;float: left"></div>
            <div id="open-right" style="cursor: pointer;"><img src="images/left-open.png" style="width: 15px;float: left"></div>
        </div>
        
        <!--<div class="main-right">-->
            <!--<div class="gg-classify-btn">-->
                <!--<button class="gg-classify-btn-pitch"><i class="icon-facetime-video"></i>开奖直播</button>-->
                <!--<button><i class="icon-facetime-video"></i>文字开奖</button>-->
                <!--<button><i class=" icon-spinner"></i>在线投注</button>-->
                <!--<button><i class="icon-eye-open"></i>美女直播</button>-->
            <!--</div>-->
            <!--<div class="gg-classify-module">-->

            <!--</div>-->
        <!--</div>-->
    </div>
</div>
<input type="hidden" class="jide" />
<div class="pic_box" id="pic_box">
        <div class="header">
            <p>设置头像</p>
            <span class="close">x</span>
        </div>
        <span class="xiantiao"></span>
        <ul id="picUpHead">
           
        </ul>
        <div class="bt_box">
            <a class="gb" href="javascript:">关闭</a>
            <a class="queren" id="queren" href="javascript:">选择</a>
            <a class="queren" id="click_change_btn" href="#">自定义头像</a>
        </div>
    </div>
<script src="script/jquery-3.1.1.min.js"></script>
<script src="layer/layer.js"></script>
<script src="script/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="script/underscore-min.js"></script>
<script type="text/javascript" src="script/emojis.js"></script>
<script type="text/javascript" src="script/contextMenu.js"></script>
<script src="script/common.js"></script>
<script src="script/upHead.js"></script>
<script src="script/firing.js"></script>
<script src="script/index.js"></script>
<script src="script/jquery.lazyload.min.js"></script>
<script type="text/javascript">
	
    $(function(){
    	setPasteImg();
    	// 上传头像效果
    	var $box = document.getElementById('pic_box');
    	var $li = $box.getElementsByTagName('li');
    	var index = 0;
    	for(var i=0;i<$li.length;i++){
    	    $li[i].index=i;
    	    $li[i].onclick=function(){
    	        $li[index].style.borderRadius="15%";
    	        this.style.borderRadius="50%";
    	        index = this.index;
    	    }
    	}
    	$(".pic_box li img").click(function(){
    	    var src=$(this).attr("src");
    	    $(".jide").val(src);
    	})
        //头像上传—点击打开文件选择器
        $("#click_change_btn").on('click', function() {
            $('#file').click();
            $("#update_ok_btn_url").hide();
        });
        $('#file').on('change', function() {
            var format = ["bmp","jpg","gif","png"];
            var imgNameForm = $(this).val();
            if(imgNameForm == ""){
                return false;
            }
            imgNameForm = $(this).val().substring($(this).val().length-3)+"";
            var imagSize =  document.getElementById("file").files[0].size;
            if(imagSize > 204800){
            	alert("图片大小不能超过200KB")
            	return false
            } 
            if(jQuery.inArray(imgNameForm ,format) != -1){
                preImg('file','file_img');
                $("#update_ok_btn").show();
            }else{
                alert("头像只能上传bmp,jpg,gif,png格式");
                return false;
            }
            $(".pic_box").animate({
                'top':'-1000px'
            },500);
        });
        //聊天图片上传—点击打开文件选择器
        $("#chatImgStyle").on('click', function() {
        	if(!token){
        		alert('请先登录')
        		return false;
        	}
            $('#chatImg').click();
        });
        $("#chatImgStylePr").on('click', function() {
            $('#chatImgPr').click();
        });
        $('#boxscroll2').attr('style','margin-top:5%;height:78%;');
        $('#chatImg').on('change', function() {
            var format = ["bmp","jpg","gif","png"];
            var imgNameForm = $(this).val();
            var imgW,imgH;
            if(imgNameForm == ""){
                return false;
            }
            imgNameForm = $(this).val().substring($(this).val().length-3)+"";
            var imagSize =  document.getElementById("chatImg").files[0].size;
            var reader = new FileReader();
            reader.onload = function (e) {
	            var data = e.target.result;
	            if(imagSize > 204800){
	            	alert("图片大小不能超过200KB")
	            	return false
            	} 
	            //加载图片获取图片真实宽度和高度
		            var image = new Image();
		            image.onload=function(){
		            	imgW = image.width;
		            	imgH = image.height;
		                if(jQuery.inArray(imgNameForm ,format) != -1){
		                    upChatImg(imgW,imgH);
		                }else{
		                    alert("聊天图片只能上传bmp,jpg,gif,png格式");
		                    return false;
		                }
	             	};
	            image.src= data;
            };
            reader.readAsDataURL(document.getElementById("chatImg").files[0]);
            /* if(imagSize > 1145728){
            	alert("图片大小不能超过1MB")
            	return false
            } */
        });
        $('#chatImgPr').on('change', function() {
            var format = ["bmp","jpg","gif","png"];
            var imgNameForm = $(this).val();
            var imgW,imgH;
            if(imgNameForm == ""){
                return false;
            }
            imgNameForm = $(this).val().substring($(this).val().length-3)+"";
            var imagSize =  document.getElementById("chatImgPr").files[0].size;
            var reader = new FileReader();
            reader.onload = function (e) {
	            var data = e.target.result;
	            //加载图片获取图片真实宽度和高度
		            var image = new Image();
		            image.onload=function(){
		            	imgW = image.width;
		            	imgH = image.height;
		                if(jQuery.inArray(imgNameForm ,format) != -1){
		                    upChatImgPr(prname,imgW,imgH);
		                }else{
		                    alert("聊天图片只能上传bmp,jpg,gif,png格式");
		                    return false;
		                }
	             	};
	            image.src= data;
            };
            reader.readAsDataURL(document.getElementById("chatImgPr").files[0]);
            /* if(imagSize > 3145728){
            	alert("图片大小不能超过3MB")
            	return false
            } */

        });
    });
    //上传头像
    function ok_update_head() {
        //loading动画层
        var headLoading = layer.load(1, {
            shade: [0.3,'#fff'] //0.1透明度的白色背景
        });
        $.ajax({
            url: ip+'user/uploadHeadImg',
            type: 'POST',
            cache: false,
            data:new FormData($('#formfile')[0]),
            processData: false, //多文件上传
            contentType: false,
            success: function (data) {
                //接收后台数据后停止loading动画
                layer.close(headLoading);
                if(data.code == 1){
                    $("#file_img").attr('src',data.data.headImg);
                    $("#userHead"+data.data.id+"").attr('src',data.data.headImg);
                    $(".personal_menu_name img").attr('src',data.data.headImg);
                    $(".messageUserSelf").attr('src',data.data.headImg);
                    myHeadImg = data.data.headImg;
                    layer.msg("更换成功");
                    $("#update_ok_btn").hide();
                    $("#click_change_btn").show();
                }else{
                    layer.msg("上传头像失败,请重新选择头像")
                    $("#update_ok_btn").hide();
                }
            }
        })
    }
    function ok_update_head_url() {
        //loading动画层
        var headLoading = layer.load(1, {
            shade: [0.3,'#fff'] //0.1透明度的白色背景
        });
        $.ajax({
            url: ip+'user/uploadHeadImgUrl',
            type: 'POST',
            cache: false,
            data:{
            	url:$(".jide").val()
            },
            success: function (data) {
                //接收后台数据后停止loading动画
                layer.close(headLoading);
                if(data.code == 1){
                    $("#file_img").attr('src',data.data.headImg);
                    $("#userHead"+data.data.id+"").attr('src',data.data.headImg);
                    $(".personal_menu_name img").attr('src',data.data.headImg);
                    $(".messageUserSelf").attr('src',data.data.headImg);
                    myHeadImg = data.data.headImg;
                    layer.msg("更换成功");
                    $("#update_ok_btn_url").hide();
                    $("#click_change_btn").show();
                }else{
                    layer.msg("上传头像失败,请重新选择头像")
                    $("#update_ok_btn_url").hide();
                }
            }
        })
    }
    //上传聊天图片
    //公共
    function upChatImg(imgW,imgH) {
        //loading动画层
        var headLoading = layer.load(1, {
            shade: [0.3,'#fff'] //0.1透明度的白色背景
        });
        $.ajax({
            url: ip+'user/uploadPic',
            type: 'POST',
            cache: false,
            data: new FormData($('#formChatImg')[0]),
            processData: false, //多文件上传
            contentType: false,
            success: function (data) {
                //接收后台数据后停止loading动画
                if(data.code == 1){
                    layer.close(headLoading);
                    //发送图片消息
                    if(robotIsUser == 0){
                        sendTest(data.data,imgW,imgH);
                    }else{
                        robotSend(data.data,imgW,imgH);
                    }
                }else{
                	layer.close(headLoading);
                    layer.msg("发送图片失败,请重新发送")
                }
            }
        })
    }
    var inProcess = Date.parse(new Date()) - 9000;
    function upPasteImg(file,imgW,imgH) {
        inProcess = Date.parse(new Date())
        layer.msg('图片发送中...')
        let params = new FormData()
        params.append('file', file)
        var headLoading = layer.load(1, {
            shade: [0.3,'#fff'] //0.1透明度的白色背景
        });
        $.ajax({
            url: ip+'user/uploadPic',
            type: 'POST',
            cache: false,
            data: params,
            processData: false, //多文件上传
            contentType: false,
            success: function (data) {
                //接收后台数据后停止loading动画
                if(data.code == 1){
                    layer.close(headLoading);
                    //发送图片消息
                    if(robotIsUser == 0){
                        sendTest(data.data,imgW,imgH);
                    }else{
                        robotSend(data.data,imgW,imgH);
                    }
                }else{
                    layer.msg("发送图片失败,请重新发送")
                }
            }
        })
      };
    function setPasteImg() {
        document.addEventListener('paste', function (event) {
          if (event.clipboardData || event.originalEvent) {
            let clipboardData = (event.clipboardData || event.originalEvent.clipboardData)
            if (clipboardData.items) {
              var blob
              for (let i = 0; i < clipboardData.items.length; i++) {
                if (clipboardData.items[i].type.indexOf('image') !== -1) {
                  blob = clipboardData.items[i].getAsFile()
                }
              }
              var imgW,imgH;
              let render = new FileReader()
              render.onload = function (evt) {
                // 输出base64编码
                let base64 = evt.target.result
                const image = new Image()
                image.onload = function () {
                	imgW = image.width;
	            	imgH = image.height;
                }
                image.src = base64
              }
              if (blob) {
                render.readAsDataURL(blob)
                if (inProcess + 10000 >= Date.parse(new Date())) {
                	layer.msg('十秒内只能粘贴上传一次图片')
                } else {
                  upPasteImg(blob,imgW,imgH)
                }
              }
            }
          }
        })
      }
    //私聊
    function upChatImgPr(name,imgW,imgH) {
        //loading动画层
        var headLoading = layer.load(1, {
            shade: [0.3,'#fff'] //0.1透明度的白色背景
        });
        $.ajax({
            url: ip+'user/uploadPic',
            type: 'POST',
            cache: false,
            data: new FormData($('#formChatImgPr')[0]),
            processData: false, //多文件上传
            contentType: false,
            success: function (data) {
                //接收后台数据后停止loading动画
                if(data.code == 1){
                    layer.close(headLoading);
                    console.log(data.data);
                    //发送图片消息
                    prsend(name,data.data,imgW,imgH);
                }else{
                    layer.msg("上传图片失败,请重新上传")
                }
            }
        })
    }
    /**
     * 从 file 域获取 本地图片 url
     */
    function getFileUrl(sourceId) {
        var url;
        if (navigator.userAgent.indexOf("MSIE")>=1) { // IE
            url = document.getElementById(sourceId).value;
        } else if(navigator.userAgent.indexOf("Firefox")>0) { // Firefox
            url = window.URL.createObjectURL(document.getElementById(sourceId).files.item(0));
        } else if(navigator.userAgent.indexOf("Chrome")>0) { // Chrome
            url = window.URL.createObjectURL(document.getElementById(sourceId).files.item(0));
        }
        return url;
    }
    /**
     * 将本地图片 显示到浏览器上
     */
    function preImg(sourceId, targetId) {
        var url = getFileUrl(sourceId);
        var imgPre = document.getElementById(targetId);
        imgPre.src = url;
    }
    
    var loadImg = function(src,e){
    	$(e).attr('src', src)
    }
</script>
<script type="text/javascript">
$(function(){

})

</script>
<style>
.hide{
	display:none;
}
</style>
</body>
</html>