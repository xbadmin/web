var websocket,search,readyState,LoggedIn = 1;
var loading;
//判断当前浏览器是否支持WebSocket
function connect(){
	if ('WebSocket' in window) {
		var webSocketIp = window.location.host+window.location.pathname;
		ws = 'ws'
    	search = window.location.search;
    	ws = (location.protocol == 'https:') ? 'wss' : 'ws'
        websocket = new WebSocket(ws+"://"+webSocketIp+"websocket"+search);
    	
    	//连接发生错误的回调方法
    	websocket.onerror = function() {
    		layer.alert('连接发生错误，请刷页面后重新进入!', {
    			  icon: 2,
    		})
    	};
    	//连接成功建立的回调方法
    	websocket.onopen = function(event) {
    		 layer.msg("您已加入聊天室");
    		 layer.close(loading);
    	};
    	//连接关闭的回调方法
    	websocket.onclose = function() {
//    		layer.alert('聊天室链接已断开，请刷页面后重新进入!', {
//    			  icon: 2,
//    		})
    		loading = layer.load(1, {
    			  shade: [0.1,'#fff'] //0.1透明度的白色背景
			});
    	   reconnect()
    		
    	};
    	//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
    	window.onbeforeunload = function() {
    	    websocket.close();
    	};
    	
    	//接收到消息的回调方法
    	var num = 0;
    	websocket.onmessage = function(event) {
    		let allMessage = $('.user-message-list').not('.hide');
    		if(allMessage.length >30 && $("#messageRoll").attr('data-value') == 1){
    			for(let i in allMessage){
    				if((i-0)<(allMessage.length - 20)){
    					allMessage.eq(i).addClass('hide');
    				}
    			}
    		}
    	    // 解析收到的消息
    	    var message = JSON.parse(event.data);
    	    var messageCook = JSON.stringify(event.data)
    	    if (message.type === "message" || message.type === "img" || message.type === "redPaper") { //会话消息
    	    	message.chat.content = message.chat.content.replace(/&nbsp;/gi, "");
    	        chatNotes.push(message)
    	        messageContent(message,(chatNotes.length-1));
    	       /* if(message.chat.to == ''){
    	            if(chatIss == null){
    	            	var tmp='[';
    	                var estr=']';
    	                cacheHtml = cacheHtml.substring(1,cacheHtml.length-1)
    	                cacheHtml += tmp+messageCook+estr;
    	            	chatIss = 1;
    	            	localStorage.setItem("chatIss",1);
    	            }else{
    	            	var estr=']';
    	                cacheHtml = cacheHtml.substring(0,cacheHtml.length-2)
    	                cacheHtml += ','+messageCook.substring(1,messageCook.length) + estr
    	            }
    	            localStorage.setItem("chatListCache",cacheHtml);
    	        }*/
    	    }
    	    if (message.list !== null && message.list !== undefined) { //在线列表
    	    	if(num == 0){
    	    		userList(message.list);
    	    	};
    	    	num++;
    	    }
    	    if (message.type === 'notice') { //系统消息
    	    	if(message.name == myName && LoggedIn == 1 ){
    	    		LoggedIn++
    	    	}
//    	    	else if(message.name == myName && LoggedIn == 2){
//    	    		websocket.onclose()
//    	    		alert("您的账号已在其他地方登陆，请重新登录或修改密码")
//    	    	}
    	        if (myLevel == 1 || myLevel == 100) {
    	            layer.msg("系统消息：" + message.message)
    	        }
    	        var lis = message.list;
    	        var ros = message.robot;
    	        robotListHtml = '';
    	        robotListNum = 0;
    	        if(ros || lis){
//    	        	 for(var i=0;i<ros.length;i++){
//    	             	if(ros[i].nickname.length > 5){
//    	             		ros[i].nickname = ros[i].nickname.substring(0,5) + '...'
//    	     			} 
//    	     			if(ros[i].headImg == null){
//    	     				ros[i].headImg = 'https://xbtu111.com/chatserver/userImg.gif'
//    	     			}
//    	     			robotListNum ++
//    	             	robotListHtml += '<ul style="height: 30px; float: left" class=""><li><img  src="'+ros[i].headImg+'" style="width:25px;"></li><li><span>' + ros[i].nickname + '</span></li><li><input type="button" value="私聊" class="manageChat" ></li></ul>';
//    	             }
    	        	 $.each(lis, function(i) {
    	        		 if(lis[i].nickname.length > 5){
    	        			 lis[i].nickname = lis[i].nickname.substring(0,5) + '...'
    	      			} 
    	      			if(lis[i].headImg == null){
    	      				lis[i].headImg = 'https://tpxb.me/laots/userImg.png'
    	      			}    			
    	      			if(lis[i].level == '0'){
    	      				robotListNum ++
    	      				if(lis[i].readFlag!=null && lis[i].readFlag!=0 && (myLevel == 1 || myLevel == 100)){
    	      					robotListHtml += '<ul style="height: 30px; float: left" class=""><li class="alarm"><img  src="'+lis[i].headImg+'" style="width:25px;"><div class="divright">'+lis[i].readFlag+'</div></li><li><span>' + lis[i].nickname + '</span></li><li><input type="button" value="私聊" class="manageChat" id="chat' + lis[i].id + '"  data-level="' + lis[i].level + '" data-nickName="' +lis[i].nickname+ '" title="'+lis[i].nickname+'" data-name="' +lis[i].name+ '"  data-img="' + lis[i].headImg + '" ></li></ul>';
    	      				}else{
    	      					robotListHtml += '<ul style="height: 30px; float: left" class=""><li><img  src="'+lis[i].headImg+'" style="width:25px;"></li><li><span>' + lis[i].nickname + '</span></li><li><input type="button" value="私聊" class="manageChat" id="chat' + lis[i].id + '"  data-level="' + lis[i].level + '" data-nickName="' +lis[i].nickname+ '" title="'+lis[i].nickname+'" data-name="' +lis[i].name+ '"  data-img="' + lis[i].headImg + '" ></li></ul>';
    	      				}
    	      				
    	      			}
    	        	 });
    	        	 $.each(ros, function(i) {
    	        		 if(ros[i].nickname.length > 5){
    	              		ros[i].nickname = ros[i].nickname.substring(0,5) + '...'
    	      			} 
    	      			if(ros[i].headImg == null){
    	      				ros[i].headImg = 'https://tpxb.me/laots/userImg.png'
    	      			}
    	      			robotListNum ++
    	              	robotListHtml += '<ul style="height: 30px; float: left" class=""><li><img src="'+ros[i].headImg+'"  style="width:25px;" /></li><li><span>' + ros[i].nickname + '</span></li><li><input type="button" value="私聊" class="manageChat" ></li></ul>';
    	              
    	        	 });
    	        	 manageChat();
    	        }
    	       
    	    }
    	    if(message.type == 'delete'){
    	    	$("#message").html('');
    	    	getHistoricalList()
    	    }
    	    if (message.type === 'ban' & message.name == myName) { //禁言消息
    	    		layer.msg("没有发言权限");
    	    }
    	    if(message.type == 'banned' || message.type == 'filter' || message.type == 'black') {
    	    	layer.msg(message.message);
    			return false;
    		}
    	    /*if (message.type === "script") { //撤回消息
    	       var id = message.chat.content;
    	       $("."+id+"").remove()
    	       layer.msg('撤回成功')
    	    }*/
    	};

	} else {
		alert('Not support websocket')
	}
}
connect()

function reconnect(url) {
    //没连接上会一直重连，设置延迟避免请求过多
    var timer = setTimeout(function () {
        if (websocket.readyState == 2 || websocket.readyState == 3) {
        	connect();
        } else if (ws.readyState == 1) {
            clearTimeout(timer);
        }
    }, 1000);

}
// 创建聊天缓存
/*var chatListCache = localStorage.getItem("chatListCache"), cacheHtml = '', cacheLength ;
var chatIss = localStorage.getItem("chatIss")
if(chatListCache == null || chatListCache == [] ){
	localStorage.setItem("chatListCache",cacheHtml);
}else{
	cacheHtml = localStorage.getItem("chatListCache")
}
//  读取聊天缓存
if(chatListCache != null && chatListCache != ''){
    chatListCache = '['+JSON.parse(chatListCache)[0]+']'
    chatListCache = JSON.parse(chatListCache)
    cacheLength = chatListCache.length
}
if(cacheLength < 20){
	for(var i = 0; i < cacheLength; i++){
		messageContent(chatListCache[i],1)
	}
}else{
	cacheLength = cacheLength-20
	for(var i = cacheLength; i < cacheLength +20; i++){
		messageContent(chatListCache[i],1)
	}
}*/
// 加载更多记录
/*function lookCache(){
	if(cacheLength == 0){
		layer.msg('暂无更多记录')
	}else{
		if(cacheLength > 20){
			cacheLength = cacheLength-20
			for(var i = cacheLength; i < cacheLength +20; i++){
				messageContent(chatListCache[i],2)
			}
		}else{
			for(var i = cacheLength; i >= 0  ; i--){
				messageContent(chatListCache[i],2)
			}
			cacheLength = 0
		}
	}
}*/

