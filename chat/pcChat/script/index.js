//公共参数
var token = Request.token; //获取当前地址栏token
var myName = ''; //个人帐号
var myHeadImg = ''; //个人头像地址
var thisName = ''; //个人昵称
var myId = ''; //个人ID
var myLevel = ''; //个人等级
var ptId = Request.platId;
var gradeVip = '';
var chatNotes = new Array();
//var ip = 'localhost:8080/chat'; //后台ip
var robotIsUser = 0;
var adminTextColor = ''
var chatRecord = [];
var chatPage = 1;
//var start = 0;
//var end = 30;

// 清除聊天内容
function clearMessage() {
    document.getElementById('message').innerHTML = '';
    layer.msg('屏幕已清空');
    //start = 0;
    //end = 30;
    //dt = [];
    /*localStorage.removeItem("chatListCache");
    localStorage.removeItem("chatIss");
    cacheHtml = ''
    chatIss = null*/
}
// 加载聊天记录
/*function mhistori(){
	start += 30
	end += 30
	getHistoricalList();
	$('#message').children(":first").insertBefore()
}*/

function getHistoricalList(){
	var dt = [];
	postAjax(ip+'getPlatChatRecord', {
        token: token,
        platformId:ptId
        //start:start,
		//end:end
    },function(data){
    	chatRecord = data.data;
    	let d = data.data.splice(0,20);
    	$.each(d,function(index, item){
    		item = JSON.parse(item)
    		if(item.type =='message' || item.type =='img'|| item.type === "redPaper"){
    			dt.unshift(item)   			
    			//messageContent(item)
    		}
		});
    	//console.log('dt',dt)
    	chatNotes = dt
    	for (ret in dt){
    		//console.log('ret', ret)
    		messageContent(dt[ret],ret)
    	}
    
    });
}
//获取初始信息
function loadingUser() {
    if (token != null) {
        postAjax(ip+'getAuthentication', {
                token: token
            },
            function(data) {
            	if(data.code == 2001){
            		alert(data.msg)
            	}
            	ptId = data.data.platformId
                myName = data.data.name;  //保存个人帐号
                myHeadImg = data.data.headImg;  //保存个人头像
                myLevel = data.data.level; //保存用户等级
                myId = data.data.id; //保存用户id
                thisName = data.data.nickname
                gradeVip = data.data.grade
                $("#user_level").text(data.data.grade)
                if (typeof(data.data.headImg) == "undefined" || data.data.headImg == null) {   //判断用户是否有头像
                    myHeadImg = 'https://tpxb.me/laots/userImg.png';
                }
                $(".personal_menu_name img").attr('src', myHeadImg);   //右上角头像
                $("#file_img").attr('src', myHeadImg);  //个人资料框头像
                if (data.data.nickname == undefined) {  //判断用户初始昵称
                    $("#initial_module").css('display', 'flex');
                    return false;
                }
                if(data.data.ban == -1 || data.data.ban == 0){
                	$("#boxscroll3").attr("contenteditable","false")
                	$('#send').attr('disabled',"true")
                	$('<style>#boxscroll3:empty::before{content:"没有发言权限,请联系管理员"}</style>').appendTo('head');
                }
                // 加载websocket
            	jQuery.getScript("./script/webSocket.js",function(data, status, jqxhr) {});
            });
    }else{
    	jQuery.getScript("./script/webSocket.js",function(data, status, jqxhr) {});
    	// 游客身份
    	$("#boxscroll3").attr("contenteditable","false")
    	$('#send').attr('disabled',"true")
    	$('<style>#boxscroll3:empty::before{content:"游客无发言权限,请登录后发言"}</style>').appendTo('head');
        locaMobile(0);
        // 加载websocket
    }
}
function updateUser(postData) {
    if ($("#user_update_age").val() < 0 || $("#user_update_age").val() > 150) {
        layer.msg("请输入合法的年龄");
        return false;
    }
    if ($("#user_update_name").val().length < 1 || $("#user_update_name").val().length > 6) {
        layer.msg("请输入1~6位的昵称");
        return false;
    }
    if (postData.age == null || postData.age == "") {
        postData.age = 20;
    }
    if (postData.sex == null || postData.sex == "") {
        postData.sex = "男";
    }
    postAjax(ip+'user/updateUser', postData,
        function(data) {
            var a = data.data;
            $("#user_age_text").text(a.age);
            $("#user_update_age").val(a.age);
            $("#user_sex_text").text(a.sex);
            $("#user_update_sex").val(a.sex);
            $("#user_name_text").text(a.nickname);
            $("#user_img_name").text("帐号：" + a.name);
            $("#user_update_name").val(a.nickname);
            $("#user_img_name_2").text(a.nickname);
            $("#name" + a.id + "").text(a.nickname);
            thisName = a.nickname;
            layer.msg("修改成功");
            $(this).hide()
        });
    $("#user_update_ok").hide();
    $(".user_update_text").hide();
    $(".user_update_text").siblings('span').show();
}
//修改初始昵称
function loadingName(postData) {
    postAjax( ip + 'user/addNickname', postData,
        function(data) {
	    	if(data.code == 1){
	    	     layer.msg("设置初始昵称成功");
	             loadingUser();
	             $("#initial_module").hide();
	    	}else{
	    		layer.msg("改昵称已被使用");
	    	}  
        });
}
//拉黑
function defirend(name) {
    postAjax( ip + 'user/banBlack', {
            name: name
        },
        function(data) {
            if (data.code == 1) {
                layer.msg("拉黑成功")
            }else{
            	layer.msg(data.msg)
            }
            $(".ul-context-menu").hide()
        })
}
//获取公告
function noticeMessage() {
    var html = '';
    getAjax( ip + 'notice/getListByOpen', {},
        function(data) {
        if(data.data !== []){
            for (var i = 0 ; i< data.data.length; i++){
                if(data.data[i].open === true){
                    html += data.data[i].title +'：'+ data.data[i].content + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                }
            }
            $("#noticeMessage").html(html)
        } else {
            $("#noticeMessage").html("暂无公告")
        }
    })
}
//发送消息
function sendTest(img,imgW,imgH) {
    var text = $("#boxscroll3").val();
    var messageType = 1;
    if(typeof (img) != "undefined"){
        text = img
        messageType = 2;
    }
    if (text == null || text == "" || text == '\n' || text == '<div><br></div><div><br></div>') {
        layer.msg("请输入要发送的消息哦~");
        $("#boxscroll3").remove();
        $("#liaotianModule").after('<textarea id="boxscroll3"  class="area" placeholder="文明聊天，共创优质的聊天环境，如有问题请咨询在线客服"></textarea>');
        return false;
    }
    //判断是否存在换行，如果存在删除
    if (text.substring(text.length - 15) == '<div><br></div>') {
        text = text.substring(0, text.length - 15);
    }
    
  let param = {
		msg:JSON.stringify({
			chat: {
	            content: text,
	            form: thisName,
	            to: "",
	            //接收人,如果没有则置空,如果有多个接收人则用,分隔
	            time: getDateFull(),
	            headImg: myHeadImg,
	            id: myId,
	            name: myName,
	            level:myLevel,
	            imgWidth:imgW,
	            imgHeight:imgH,
	            msgType:messageType,
	            grade:gradeVip,
	            adminTextColor:adminTextColor
        },
        type: "message",
	        platformId: ptId,
		}),
		platformId: ptId,
		token: token,
	}

  $.ajax({
	url: ip+'sendMsg',
	data: param,
	dataType: 'json',
	type: 'POST',
	// cache:isCache,
	async : false, 
	beforeSend: function () {
		
	},
	error: function (data) {
		console.log(data)
	},
	success: function (data) {

	}
});
    
//    let param = JSON.stringify({
//        chat: {
//            content: text,
//            form: thisName,
//            to: "",
//            //接收人,如果没有则置空,如果有多个接收人则用,分隔
//            time: getDateFull(),
//            headImg: myHeadImg,
//            id: myId,
//            name: myName,
//            level:myLevel,
//            imgWidth:imgW,
//            imgHeight:imgH,
//            msgType:messageType,
//            grade:gradeVip,
//            adminTextColor:adminTextColor
//        },
//        type: "message"
//    });
//  websocket.send(param);

    //发送消息后清空输入框
    $("#boxscroll3").remove();
    $("#liaotianModule").after('<textarea id="boxscroll3"  class="area" placeholder="文明聊天，共创优质的聊天环境，如有问题请咨询在线客服"></textarea>');
    $("#boxscroll3").focus(function(){
    	$(this).css('border','1px solid rgba(102, 175, 233)')
    })
    $("#boxscroll3").blur(function(){
    	$(this).css('border','1px solid rgba(230, 230, 230, .3)')
    })
}
// 私聊
function prsend(name,img,imgW,imgH) {
	webHeart();
    var text = $("#boxscrol5").html();
    var messageType = 1;
    if(typeof (img) != "undefined"){
        text = img
        messageType = 2;
    }
    if (text == null || text == "" || text == '\n' || text == '<div><br></div><div><br></div>') {
        $("#boxscrol5").remove();
        $(".premj").after('<div class="predit"  id="boxscrol5" contenteditable="true" class="area"></div>');
        layer.msg("请输入要发送的消息哦~");
        return false;
    }
    //判断是否存在换行，如果存在删除
    if (text.substring(text.length - 15) == '<div><br></div>') {
        text = text.substring(0, text.length - 15);
      //去除首尾空格
    }
    text = text.replace(/&nbsp;/gi, "");
    var msg = JSON.stringify({
        chat: {
            content: text,
            form: thisName,
            to: name,
            //接收人,如果没有则置空,如果有多个接收人则用,分隔
            time: getDateFull(),
            headImg: myHeadImg,
            id: myId,
            name: myName,
            level:myLevel,
            imgWidth:imgW,
            imgHeight:imgH,
            msgType:messageType,
            grade:gradeVip,
            adminTextColor:adminTextColor
        },
        type: "message",
        platformId: ptId
    });
    
    postAjax(ip+'sendMsg', {
        token: token,
        msg:msg,
        platformId:ptId
        //start:start,
		//end:end
    },function(data){
    	
    });
//    var msg = JSON.stringify({
//        chat: {
//        	 content: text,
//             form: thisName,
//             to: name,
//             //接收人,如果没有则置空,如果有多个接收人则用,分隔
//             time: getDateFull(),
//             headImg: myHeadImg,
//             id: myId,
//             name: myName,
//             level:myLevel,
//             imgWidth:imgW,
//             imgHeight:imgH,
//             msgType:messageType,
//             grade:gradeVip,
//             adminTextColor:adminTextColor
//        },
//        type: "message",
//        platformId: ptId
//    });
//    
//    websocket.send(JSON.stringify({
//        chat: {
//            content: text,
//            form: thisName,
//            to: name,
//            //接收人,如果没有则置空,如果有多个接收人则用,分隔
//            time: getDateFull(),
//            headImg: myHeadImg,
//            id: myId,
//            name: myName,
//            level:myLevel,
//            imgWidth:imgW,
//            imgHeight:imgH,
//            msgType:messageType,
//            grade:gradeVip,
//            adminTextColor:adminTextColor
//        },
//        type: "message"
//    }));
    //发送消息后清空输入框
    $("#boxscrol5").remove();
    $(".premj").after('<div class="predit"  id="boxscrol5" contenteditable="true" class="area"></div>');
}
//返回用户列表html
function userListHtml(id, name, level, headImg, deName,chat,readFlag) {
	var title = name;
	if(name.length > 7){
		name = name.substring(0,5) + '...'
	}
	if(chat == 1){
		chat = '<li><input type="button" value="私聊" class="manageChat" id="chat' + id + '"  data-level="' + level + '" data-nickName="' +name+ '" title="'+title+'" data-name="' +deName+ '"  data-img="' + headImg + '" ></li>'
	}else{
		chat = ''
	}
    //普通用户没有title
    if (level == 0) {
    	if(readFlag!=null && readFlag!=0 && (myLevel == 1 || myLevel == 100)){
    		return '<ul title="'+title+'" onclick=openUpUser(&quot;' + deName + '&quot;) style="height: 30px; float: left" id="contextMenu' + id + '" data-name="' + deName + '">' + '  <li  class="alarm"><img id="userHead' + id + '" src="' + headImg + '" style="width:25px;"><div class="divright">'+readFlag+'</div></li>' + '  <li><span id="name' + id + '">' + name + '</span></li>' +chat+'  </ul>'
    	}else{
    		return '<ul title="'+title+'" onclick=openUpUser(&quot;' + deName + '&quot;) style="height: 30px; float: left" id="contextMenu' + id + '" data-name="' + deName + '">' + '  <li><img id="userHead' + id + '" src="' + headImg + '" style="width:25px;"></li>' + '  <li><span id="name' + id + '">' + name + '</span></li>' +chat+'  </ul>'
    	}
    }else if(level == 101){
    	return '<ul title="'+title+'" onclick=openUpUser(&quot;' + deName + '&quot;) style="height: 30px; float: left"  id="contextMenu' + id + '"  data-name="' + deName + '" class="adminIstrator"> \n' + '  <li><img id="userHead' + id + '" src="' + headImg + '" alt="" style="width:25px;"></li>\n' + '  <li><span style="color: yellow;font-weight: 600;">【系统】</span><span id="name' + id + '">' + name + '</span></li>' +chat+'</ul>'
    }else{
        //管理有金色title
        return '<ul title="'+title+'" onclick=openUpUser(&quot;' + deName + '&quot;) style="height: 30px; float: left"  id="contextMenu' + id + '"  data-name="' + deName + '"> \n' + '  <li><img id="userHead' + id + '" src="' + headImg + '" alt="" style="width:25px;"></li>\n' + '  <li><span style="color: yellow;font-weight: 600;">【管理】</span><span id="name' + id + '">' + name + '</span></li>' +chat+'</ul>'
    }
}
var robotListHtml = '' ,robotListNum = 0;
//用户列表
function userList(list) {
    var commonUser = ''; //普通列表
    var superUser = ''; //管理员列表
    var oneself = '';  //自己
    var commonNum = 0; //用户人数
    var ServerNum = 0; //管理员人数
    var noUser = ''; //游客列表
    var contextMenuIdArray = []; //右键菜单用户列表id数组
    //遍历用户列表
    $.each(list,
        function(index, item) {
            contextMenuIdArray.unshift(item.id);
            //默认值
            if (typeof(item.headImg) == "undefined") {
                item.headImg = 'https://tpxb.me/laots/userImg.png';
            }
            if (typeof(item.age) == "undefined") {
                item.age = 18;
            }
            if (typeof(item.sex) == "undefined") {
                item.sex = '男';
            }
            //如果不是自己
            if (item.name != myName) {
            	console.log(item.level == 0)
                if (item.level == 0) {
                    commonNum++;
                    commonUser += userListHtml(item.id, item.nickname, item.level, item.headImg, item.name,1,item.readFlag)
                    /*if(item.nickname.substring(0,2) == '游客'){
                    	noUser += userListHtml(item.id, item.nickname, item.level, item.headImg, item.name,1)
                    }else{
                        commonUser += userListHtml(item.id, item.nickname, item.level, item.headImg, item.name,1)
                    }*/
                } else {
                    ServerNum++;
                    superUser += userListHtml(item.id, item.nickname, item.level, item.headImg, item.name,1);
                }
            } else {
                if (item.level == 0) {
                    commonNum++;
                    oneself += userListHtml(item.id, item.nickname, item.level, myHeadImg, item.name,0);
                    thisName = item.nickname;
                } else {
                    ServerNum++;
                    oneself += userListHtml(item.id, item.nickname, item.level, myHeadImg, item.name,0);
                    // thisName = '<span style="color: yellow;font-weight: 600;">【管理】</span>' + item.nickname;
                    thisName = item.nickname
                }
            //普通用户
            // if (item.level == 0) {
            //         commonNum++;
            //         commonUser += userListHtml(item.id, item.nickname, 0, myHeadImg, item.name);
            //         thisName = item.nickname;
            //     } else {
            //     //管理员用户
            //         ServerNum++;
            //         superUser += userListHtml(item.id, item.nickname, 1, myHeadImg, item.name);
            //         thisName = '<span style="color: yellow;font-weight: 600;">【管理】</span>' + item.nickname;
            //     //放置初始数据
                $("#user_age_text").text(item.age);
                $("#user_update_age").val(item.age);
                $("#user_sex_text").text(item.sex);
                $("#user_update_sex").val(item.sex);
                $("#user_name_text").text(item.nickname);
                $("#user_img_name").text("帐号：" + item.name);
                $("#user_update_name").val(item.nickname);
                $("#user_img_name_2").text(item.nickname);
            }
        });
    //为了优化
    $.ajax({
		url: ip+'admin/robot/getRobotLoginList',
		data: {},
		dataType: 'json',
		// cache:isCache,
		async : false, 
		beforeSend: function () {
			
		},
		error: function (data) {
			console.log(data)
		},
		success: function (data) {
			var d = data.data;
			if(robotListHtml == ''){
				for(let i in d){
					if(d[i].nickname.length > 5){
						d[i].nickname = d[i].nickname.substring(0,5) + '...'
					} 
					if(d[i].headImg == null){
						d[i].headImg = 'https://tpxb.me/laots/userImg.png'
					}
					robotListNum ++
					robotListHtml +='<ul style="height: 30px; float: left"><li><img  src="'+d[i].headImg+'" style="width:25px;"></li><li><span>' + d[i].nickname + '</span></li><li><input type="button" value="私聊" class="manageChat" ></li></ul>'
	
				}
			}
		}
	});
   $("#user-member").html(robotListHtml); //用户列表
   $("#on-line-member").val('在线会员（' + robotListNum + '）'); //用户数量
    $("#user-server").html(superUser);//管理员列表
    $("#on-line-server").val('客服/管理（' + ServerNum + '）'); //管理员数量
    $("#userListGif").hide()
    $("#userListModule").show()
    if (myLevel == 0) {
        $("#user-member").prepend(oneself)
        $('.adminIstrator').hide();
        
    } else {
        $("#user-server").prepend(oneself)
    }
    $("#user-member").append(noUser)
    //给管理员分配右键菜单
    if (myLevel == 1 || myLevel == 100) {
        contextMeun(contextMenuIdArray);
    }
    //游客用户无私聊权限
    if(token == undefined){
    	$(".manageChat").hide()
    }
    logoPath();
//    robotSendList();
    manageChat();
}
//接收消息
function messageContent(e,deleteId, loadMore) {
	var throwCss = '';
	if(e.chat.msgType == 2){
		e.chat.content = "<img onclick=\"loadImg('"+ e.chat.content +"',this)\" src='"+e.chat.content+"' style='width:100%; height: 100%;' alt='点击重新加载'>"
	}
	if(e.chat.msgType == 3){
		if(e.chat.textColor){
			throwCss = 'color:' + e.chat.textColor
		}
	}
	if(e.chat.msgType == 4){
		e.chat.content = '[ 收到红包，请使用手机登录领取 ]'
	}
	if(e.chat.msgType == 5 ||e.chat.msgType == 7){
		e = thorwSplit(e)
		e.chat.content = '[ 收到玩家发起的跟投，请使用手机下注 ]'
	}
	if(e.chat.msgType == 6){
		e = thorwSplit(e)
		e.chat.content = '今日战绩<br>投注：'+e.chat.bet+' 奖金：'+e.chat.win+' 盈亏: '+e.chat.bunko
		throwCss = 'background:#f86704;color:#fff;'
	}
	if(e.chat.level === 1 || e.chat.level === 100){
		if(e.chat.adminTextColor){
			throwCss = 'color:' + e.chat.adminTextColor
		}
	}
	if(e.chat.headImg == null){
		e.chat.headImg = 'https://tpxb.me/laots/userImg.png'
	}
    var htmlSL = '<li id="list'+e.chat.name+'" class="pactive"><img  src="'+e.chat.headImg+'" ><b style="display: none;">'+e.chat.name+'</b><span>'+e.chat.form+'</span><img src="images/close.png" class="pclose" id="pclose'+e.chat.name+'"></li>'
    if(e.chat.to != ""){
        if($("#boxscrol"+e.chat.name+"").html() == undefined && e.chat.name != myName){	
            if($.inArray(e.chat.name, nameSlList)  == -1) {
                $(".prlistchat").hide();
                $(".prchat").append('<div class="prlistchat" id="boxscrol' + e.chat.name + '"></div>')
                nameSlList.unshift(e.chat.name);
                $(".pleft li").removeClass("pactive");
                $("#chatuserlist").append(htmlSL);
                privateClick(e.chat.name);
                userChatOperate(e.chat.name)
                userChatBox()
                $("#prhead-img").attr("src", e.chat.headImg)
                $("#prhead-name").text(e.chat.form)
                $(".prsend").html('<button id="prbutton">发送</button>');
                prname = e.chat.name
                $("#prbutton").click(function () {
                    prsend(prname)
                });
                $("#boxscrol" + e.chat.name + "").niceScroll({
                    cursorborder: "",
                    cursorcolor: "#a0a3a3",
                    boxzoom: false,
                    autohidemode: false
                });
                getPrivateRecord(e.chat.name)
            }
        }
        if($("#private-chat").css("display") == "none"){
        	clearInterval(timer)
            $("#private-chat-small").show()
            timer = setInterval("point()",500);
        }
        $("#prbig").attr('data-name',e.chat.name)
    }
    var appendHtml = '';
    var privateChat = '';
    //如果是自己发送的显示到右边
    if (e.chat.id == myId) {
        if (e.chat.level === 1 || e.chat.level === 100) {
            e.chat.form = '<span style="color: yellow;font-weight: 600;">【管理】</span>' + e.chat.form;
        } else if (e.chat.level === 0) {
        	if(e.chat.grade){
        		e.chat.form = '<span style="color: yellow;font-weight: 600;">【'+e.chat.grade+'】</span>' + e.chat.form;
        	}
        }
        appendHtml = ' <div class="user-message-list messageUserSelf' + e.chat.id + '" ><img data-original="' + myHeadImg + '" class="lazy1 user_message_head messageUserSelf' + e.chat.id + '" style="float: right;margin-right: 30px;"  data-name="' + e.chat.name + '" data-id="'+ e.chat.id +'" data-did="'+deleteId+'">' +
            '<div class="user-message-list-name" style="right: 80px; text-align: right"><div style="float: right;margin-left: 20px;margin-top: -2px;">' + e.chat.form + '</div>' +
            '<div class="user-message-list-date" >&nbsp;&nbsp;&nbsp;' + e.chat.time + '&nbsp;&nbsp;&nbsp;</div></div><div class="user-message-list-info" style="white-space: pre-line;float: right;'+throwCss+'">' + e.chat.content + '</div></div>';
        privateChat = ' <div class="user-message-list messageUserSelf' + e.chat.id + '" ><img src="' + myHeadImg + '" class="lazy1 user_message_head messageUserSelf' + e.chat.id + '" style="float: right;margin-right: 30px;"  data-name="' + e.chat.name + '" data-id="'+ e.chat.id +'" data-did="'+deleteId+'">' +
        '<div class="user-message-list-name" style="right: 80px; text-align: right"><div style="float: right;margin-left: 20px;margin-top: -2px;">' + e.chat.form + '</div>' +
        '<div class="user-message-list-date" >&nbsp;&nbsp;&nbsp;' + e.chat.time + '&nbsp;&nbsp;&nbsp;</div></div><div class="user-message-list-info" style="white-space: pre-line;float: right;'+throwCss+'">' + e.chat.content + '</div></div>';
    } else {
    	var levelName;
        if (e.chat.level == 1 || e.chat.level == 100) {
        	levelName = e.chat.form + '<span style="color: yellow;font-weight: 600;">【管理】</span>' ;
        } else if (e.chat.msgType == 3) {
        	levelName = e.chat.form + '<span style="color: yellow;font-weight: 600;">【计划员】</span>';
        } else if (e.chat.level == 0) {
        	if(e.chat.grade){
            	levelName = e.chat.form + '<span style="color: yellow;font-weight: 600;">【'+e.chat.grade+'】</span>';
        	} else {
        		levelName = e.chat.form
        	}
        } else {
        	levelName = e.chat.form
        }
        appendHtml = '<div class="user-message-list  messageUserSelf' + e.chat.id + '" style="padding-left:10px;" data-name="' + e.chat.name + '"><img onclick="eidtName(&quot;'+e.chat.form+'&quot;)" data-original="'+e.chat.headImg+'"   class="lazy1 user_message_head messageUserSelf' + e.chat.id + '" data-name="' + e.chat.name + '" data-id="'+ e.chat.id +'" data-did="'+deleteId+'">' +
            '<div class="user-message-list-name">' + levelName + '<div class="user-message-list-date">&nbsp;&nbsp;&nbsp;' + e.chat.time + '&nbsp;&nbsp;&nbsp;</div></div>' +
            '<div class="user-message-list-info"  style="white-space: pre-line;'+throwCss+'">' + e.chat.content + '</div></div>';
        privateChat  = '<div class="user-message-list  messageUserSelf' + e.chat.id + '" style="padding-left:10px;" data-name="' + e.chat.name + '"><img src="' + e.chat.headImg + '"  class="lazy1 user_message_head messageUserSelf' + e.chat.id + '" data-name="' + e.chat.name + '" data-id="'+ e.chat.id +'" data-did="'+deleteId+'">' +
        '<div class="user-message-list-name">' + levelName + '<div class="user-message-list-date">&nbsp;&nbsp;&nbsp;' + e.chat.time + '&nbsp;&nbsp;&nbsp;</div></div>' +
        '<div class="user-message-list-info"  style="white-space: pre-line;'+throwCss+'">' + e.chat.content + '</div></div>';//私聊	
        $(".messageUserSelf" + e.chat.id + "").attr("src", e.chat.headImg);
        $("#userHead" + e.chat.id + "").attr("src", e.chat.headImg);
    }
	if(e.chat.to != "" && e.chat.name == myName){  // 私聊接收
        $("#boxscrol"+e.chat.to+"").append(privateChat)
        $("#boxscrol5").focus();
        $("#boxscrol"+e.chat.to+"").scrollTop($("#boxscrol"+e.chat.to+"")[0].scrollHeight);
    }else if(e.chat.to != "" && e.chat.to == myName){
        $("#boxscrol"+e.chat.name+"").append(privateChat)
        $("#boxscrol5").focus();
        $("#boxscrol"+e.chat.name+"").scrollTop($("#boxscrol"+e.chat.name+"")[0].scrollHeight);
    }else{
    	if(loadMore){
        	$("#message").prepend(appendHtml);
    	}else{
        	$("#message").append(appendHtml);
    	}
        $("#boxscroll3").focus();
    }
    //让聊天区始终滚动到最下面
    var chat = $("#boxscroll2");
    if($("#messageRoll").attr('data-value') == 1 && !loadMore){
        chat.scrollTop(chat[0].scrollHeight);
    }else if(loadMore && $("#messageRoll").attr('data-value') == 1){
    	 chat.scrollTop($('#boxscroll2 .user-message-list')[20].offsetTop);
    }
    if (myLevel == 1 || myLevel == 100) {
        contextMeunImg()
    }
    dragFunc('private-chat');
    
  	 $("#message img.lazy1").lazyload({
		 container: $('.chatting'),   // 对指定标签对象内的图片实现效果，表示在此标签中滚动时，触发加载（注意：如果容器未设置height和overfload:auto，那么会默认加载所有图片）
		 placeholder : "images/userImg.png", //用图片提前占位
		 effect: "fadeIn",
//		 threshold: 1000,                    //当图片顶部距离显示区域还有100像素时，就开始加载
	 });
}
// 监听滚动条获取聊天记录
var getChatPage = 1;
$("#boxscroll2").scroll(function(){
	//获取滚动条滚动的位置
	var getChatTop = $("#boxscroll2").scrollTop();
	if(getChatTop  == 0){
		setTimeout(function(){
		
		},500)
		let dt = []
		if(chatPage*20 > chatRecord.length){
			layer.msg('没有更多聊天记录');
			return
		}
		layer.msg('加载聊天记录', {time:1500});
//		let allMessage = $('.user-message-list.hide');
//		let noPush = false;
//		if(allMessage.length){
//			if(allMessage.length > 30){
//				noPush = true;
//				for(let i in allMessage){
//					if((i-0)<(allMessage.length - 20)){
//						allMessage.eq(i).removeClass('hide');
//					}
//				}
//			}else{
//				allMessage.removeClass('hide');
//			}
//		}
		if(noPush){
			return false;
		}
		chatPage+=1;
		let start = (chatPage-1)*20;
		let end = chatPage*20;
    	let d = chatRecord.splice(start,end);
    	$.each(d,function(index, item){
    		item = JSON.parse(item)
    		if(item.type =='message' || item.type =='img'|| item.type === "redPaper"){
    			dt.unshift(item)   			
    			//messageContent(item)
    		}
		});
    	
    	chatNotes = dt
    	for (ret in dt){
    		//console.log('ret', ret)
    		messageContent(dt[ret],ret, 'prepend')
    	}
	}
})
//可拖动私聊界面
function dragFunc(id) {
    var Drag = document.getElementById(id);
    Drag.onmousedown = function(event) {
        var ev = event || window.event;
        event.stopPropagation();
        var disX = ev.clientX - Drag.offsetLeft;
        var disY = ev.clientY - Drag.offsetTop;
        document.onmousemove = function(event) {
            var ev = event || window.event;
            Drag.style.left = ev.clientX - disX + "px";
            Drag.style.top = ev.clientY - disY + "px";
            Drag.style.cursor = "move";
        };
    };
    Drag.onmouseup = function() {
        document.onmousemove = null;
        this.style.cursor = "default";
    };
};

function noChat(name){
	postAjax( ip + 'user/banJY', {
        name: name,
        token:token
    },
    function(data) {
        if (data.code == 1) {
        	layer.msg('操作成功');
        }else{
        	layer.msg(data.msg);
        }
        $(".ul-context-menu").hide()
    })
}
//遍历获得用户列表右键菜单
var contextUserName = ''; //用户帐号
function contextMeun(e) {
    $.each(e,
        function(index, id) {
            $("#contextMenu" + id + "").contextMenu({
                width: 110,
                // width
                itemHeight: 30,
                // 菜单项height
                bgColor: "#333",
                // 背景颜色
                color: "#fff",
                // 字体颜色
                fontSize: 12,
                // 字体大小
                hoverBgColor: "#99CC66",
                // hover背景颜色
                target: function(ele) { // 当前元素
                    // contextUserId = ele[0].id.substring(contextUserId.length-1); //获取当前用户ID
                    // console.log(ele.attr("data-name")); //获取当前用户名称
                    contextUserName = ele.attr("data-name");
                },
                menu: [{ // 菜单项
                    text: "禁言/解除",
                    icon: "images/add.png",
                    callback: function() {
                        noChat(contextUserName)
                    }
                },
                    {
                        text: "关闭",
                        icon: "images/copy.png",
                        callback: function() {
                        	$(".ul-context-menu").hide()
                        }
                    }

                ]
            });
        })
}
//遍历获得聊天区域右键菜单
function contextMeunImg() {
    $(".user_message_head").contextMenu({
        width: 110,
        // width
        itemHeight: 30,
        // 菜单项height
        bgColor: "#333",
        // 背景颜色
        color: "#fff",
        // 字体颜色
        fontSize: 12,
        // 字体大小
        hoverBgColor: "#99CC66",
        // hover背景颜色
        target: function(ele) { // 当前元素
            contextUserId = ele.attr("data-id");//获取当前用户ID
            // console.log(ele.attr("data-name")); //获取当前用户名称
            contextUserName = ele.attr("data-name");
            contextDeleteId = ele.attr("data-did");
        },
        menu: [{ // 菜单项
            text: "禁言/解除",
            icon: "images/add.png",
            callback: function() {
                noChat(contextUserName)
            }
        },
            {
                text: "撤回此消息",
                icon: "images/add.png",
                callback: function() {
                	if(chatNotes[contextDeleteId].chat.msgType == 2){
                		var strs = chatNotes[contextDeleteId].chat.content;
                		var astr = strs.split('\'');
                		chatNotes[contextDeleteId].chat.content = astr[1];
                	}
                	postAjax(ip+'delMemberRecord', {
                        token: token,
                        platformId:ptId,
                        message:JSON.stringify(chatNotes[contextDeleteId])
                    },function(data){
                    	console.log('data',data)
                    	if(data.code == 1){
                    		layer.msg('撤回成功')
                    	}
                    });
                }
            }
        /*,{
            text: "撤回此用户消息",
            icon: "images/add.png",
            callback: function() {
            	websocket.send(JSON.stringify({
                    chat: {
                        content: 'messageUserSelf' + contextUserId,
                    },
                    type: "script"
                }));
            }
        }*/
        ,
        {
            text: "关闭",
            icon: "images/copy.png",
            callback: function() {
            	$(".ul-context-menu").hide()
            }
        }

        ]
    });
}
//关闭websocket连接
function closeWebSocket() {
    websocket.close();
    layer.msg('聊天功能已关闭')
}
//补0函数
function appendZero(s) {
    return ("00" + s).substr((s + "").length);
}
//获取当前时间
function getDateFull() {
    var date = new Date();
    // date.getFullYear() + "-" + appendZero(date.getMonth() + 1) + "-" + appendZero(date.getDate()) + " " + appendZero(date.getHours()) + ":" + appendZero(date.getMinutes()) + ":" + appendZero(date.getSeconds());
    var currentdate = date.getFullYear()+ '-' + appendZero(date.getMonth()+1)+ '-'+ appendZero(date.getDate()) + ' ' + appendZero(date.getHours()) + ":" + appendZero(date.getMinutes()) + ':' + appendZero(date.getSeconds());
    return currentdate;
}
//获取ID和私聊方等级
var nameSlList = [];
var prname='';
function getPrivateRecord(nameSL){
	postAjax(ip+'getPrivateChatRecord', {
        token: token,
        start:'0',
        end:'-1',
        toName:nameSL
    },
    function(data) {
    	$("#boxscrol"+nameSL+"").html('')
    	var e = {}
    	for(var i = data.data.length-1; i >= 0;i--){
    		data.data[i] = JSON.parse(data.data[i])
    		/*if(data.data[i].chat.name == myName && data.data[i].chat.to == nameSL){
        		messageContent(data.data[i])
    		}*/
    		messageContent(data.data[i])
    	}
    });
}
//私聊按钮点击
function manageChat() {
	webHeart();
    $(".manageChat").click(function() {
        //获取id
        var id = $(this).attr("id");
        // 机器人没有id
        if(id == undefined){
        	layer.msg("没有私聊权限",function() {
        	})
        	return false;
        }
        id = id.substring(4,id.length);
        //获取私聊人的名称帐号
        var nameSL = $(this).attr("data-name");
        var nickNameSl = $(this).attr("data-nickName");
        var title = $(this).attr("title");
        //获取对方等级
        var heLevel = $(this).attr("data-level");
        //获取对方头像
//        var imgSL = $(this).parent('ul').children('img').attr("data-img");
        var imgSL = $(this).parent().parent().find('img').attr('src');
        //私聊用户列表
        var htmlSL = '<li id="list'+nameSL+'" class="pactive"><img  src="'+imgSL+'" ><b style="display: none;">'+nameSL+'</b><span>'+nickNameSl+'</span><img src="images/close.png" class="pclose" id="pclose'+nameSL+'"></li>'
        var onButton = '<button id="prbutton">发送</button>'
        var prmodule = '<div class="prlistchat" id="boxscrol'+nameSL+'"></div>';
        //普通用户
        //判断是否是自己
        if(id == myId){
            layer.msg("请不要私聊自己",function () {

            });
            return false;
        }
        //普通用户
        if (myLevel == 0) {
            //私聊管理员
            if (heLevel == 1 || heLevel == 100) {
                //判断数组里面是否已经存在 存在0 不存在-1
                if($.inArray(nameSL, nameSlList)  == -1){
                    prname = nameSL
                    nameSlList.unshift(nameSL);
                    $(".pleft li").removeClass("pactive");
                    $("#chatuserlist").append(htmlSL);
                    privateClick(nameSL);
                    userChatOperate(nameSL)
                    userChatBox()
                    if( $("#private-chat").css("display") == "none"){
                        $("#private-chat").fadeIn(100)
                    }
                    $("#prhead-img").attr("src",imgSL)
                    $("#prhead-name").text(title)
                    $(".prsend").html(onButton);
                    $(".prlistchat").hide();
                    $(".prchat").append(prmodule)
                    $("#prbutton").click(function () {
                        prsend(prname)
                    });
                    $("#boxscrol"+nameSL+"").niceScroll({
                        cursorborder: "",
                        cursorcolor: "#a0a3a3",
                        boxzoom: false,
                        autohidemode: false
                    });
                    $('#boxscrol5').focus('keyup',
                        function(event) {
                            document.onkeydown = function(event) {
                                var e = event || window.event || arguments.callee.caller.arguments[0];
                                if (e && e.keyCode == 13) { // enter 键
                                    //要做的事情
                                    prsend(prname);
                                }
                            };
                        });
                    $("#boxscrol5").focus();
                }else{
                    prname = nameSL;
                    $(".pleft li").removeClass("pactive");
                    $("#list"+nameSL+"").addClass("pactive")
                    $("#prhead-img").attr("src",imgSL)
                    $("#prhead-name").text(title)
                    $(".prsend").html(onButton);
                    $(".prlistchat").hide();
                    $("#boxscrol"+nameSL+"").show()
                    $("#prbutton").click(function () {
                        prsend(prname)
                    })
                    $("#boxscrol5").focus();
                }
            } else {
                //私聊普通用户
                layer.msg("没有私聊权限",
                    function() {

                    })
            }
            //管理员用户
        } else {
            //判断数组里面是否已经存在 存在0 不存在-1
            if($.inArray(nameSL, nameSlList)  == -1){
                prname = nameSL
                nameSlList.unshift(nameSL);
                $(".pleft li").removeClass("pactive");
                $("#chatuserlist").append(htmlSL);
                    privateClick(nameSL);
                    userChatOperate(nameSL)
                    userChatBox()
                if( $("#private-chat").css("display") == "none"){
                    $("#private-chat").fadeIn(100)
                }
                $("#prhead-img").attr("src",imgSL)
                $("#prhead-name").text(title)
                $(".prsend").html(onButton);
                $(".prlistchat").hide();
                $(".prchat").append(prmodule)
                $("#prbutton").click(function () {
                    prsend(prname)
                })
                $("#boxscrol"+nameSL+"").niceScroll({
                    cursorborder: "",
                    cursorcolor: "#a0a3a3",
                    boxzoom: false,
                    autohidemode: false
                });
                $('#boxscrol5').focus('keyup',
                    function(event) {
                        document.onkeydown = function(event) {
                            var e = event || window.event || arguments.callee.caller.arguments[0];
                            if (e && e.keyCode == 13) { // enter 键
                                //要做的事情
                                prsend(prname);
                            }
                        };
                    });
                $("#boxscrol5").focus();
            }else{
                prname = nameSL;
                $(".pleft li").removeClass("pactive");
                $("#list"+nameSL+"").addClass("pactive")
                $("#prhead-img").attr("src",imgSL)
                $("#prhead-name").text(title)
                $(".prsend").html(onButton);
                $(".prlistchat").hide();
                $("#boxscrol"+nameSL+"").show()
                $("#prbutton").click(function () {
                    prsend(prname)
                })
                $("#boxscrol5").focus();
            }
            //私聊已读
            readPrivateMsg(nameSL);
            
        }
        getPrivateRecord(nameSL)
    });
}
//删除私聊事件
function privateClick(name) {
	webHeart();
    $("#pclose"+name+"").click(function () {
        nameSlList.splice($.inArray(name,nameSlList),1);
        $(this).parent("li").remove();
        userChatBox();
        $("#prhead-img").attr("src",$("#chatuserlist").find("li:eq(0)").find("img").attr("src"))
        $("#prhead-name").text($("#chatuserlist").find("li:eq(0)").find("span").text())
        $("#chatuserlist").find("li:eq(0)").addClass("pactive")
        prname = $("#chatuserlist").find("li:eq(0)").find("b").text()
        $("#boxscrol"+prname+"").show()
        $("#boxscrol"+name+"").remove()
    });
}
function logoPath(){
	$.ajax({
		url: ip+"admin/agent/getPlatformConfig",
		data: {},
		dataType: 'json',
		// cache:isCache,
		beforeSend: function () {
			
		},
		error: function (data) {
			layer.msg('网络错误', {icon: 2});
		},
		success: function (d) {
			if(d.code == 1){
				// logo
				if(d.data.logo != undefined){
					if(d.data.logo.length > 5){
						$("#logoPath").html('<img src="'+d.data.logo+'" />');
						$("#logoPath").css("display","block");
						$(".gg-classify-btn").css("margin-left","170px")
					}
				}
				//  标题
				if(d.data.kjUrl != undefined){
					if(d.data.kjUrl.length > 2){
						$("title").html(d.data.kjUrl);
					}
				}
				// 公告
				if(d.data.navigationUrl != undefined){
					if(d.data.navigationUrl.length > 2){
						$("#notice2").html(d.data.navigationUrl);
					}
				}
				// QQ客服
				if(d.data.appUrl != null){
					var html = $("#zskfModule").html()
					$("#zskfModule").remove()
					$("#user-server").append(html)
					$("#zskfqq").text('QQ:'+ d.data.appUrl)
					$("#qqkfbtn").click(function(){
						var url = 'tencent://message/?uin='+d.data.appUrl+'&amp;Site=web&amp;Menu=yes'
						var a = $("<a href='"+url+"' target='_blank' style='display:none;'></a>").get(0);  
			            var e = document.createEvent('MouseEvents');  
			            e.initEvent('click', true, true);  
			            a.dispatchEvent(e); 
					})
				}
				if(d.data.adminTextColor){
					adminTextColor = d.data.adminTextColor
				}
			}else{
				layer.msg(d.data, {icon: 2});
			}
		}
	});
}
function locaMobile(){
	var browser={
	        versions:function(){
	            var u = navigator.userAgent, app = navigator.appVersion;
	            return {//移动终端浏览器版本信息
	                trident: u.indexOf('Trident') > -1, //IE内核
	                presto: u.indexOf('Presto') > -1, //opera内核
	                webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
	                gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
	                mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
	                ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
	                android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
	                iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
	                iPad: u.indexOf('iPad') > -1, //是否iPad
	                webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
	                weixin: u.indexOf('MicroMessenger') > -1, //是否微信
	                qq: u.match(/\sQQ/i) == " qq" //是否QQ
	            };
	        }(),
	        language:(navigator.browserLanguage || navigator.language).toLowerCase()
	    };
	    if(browser.versions.mobile || browser.versions.ios || browser.versions.android ||
	        browser.versions.iPhone || browser.versions.iPad){
	        //移动
		    window.location.href =  window.location.protocol + '//' +window.location.host + window.location.pathname + "iphoneChat/index.html?token=" + token
	    }else{
	        //PC
	    }
}
function robotSendList(){
	var html = '<div id="robotSendModule">';
	if(myLevel != 0){
		$.ajax({
			url: ip+'admin/robot/getRobotLoginList',
			data: {},
			dataType: 'json',
			// cache:isCache,
			async : false, 
			beforeSend: function () {
				
			},
			error: function (data) {
				console.log(data)
			},
			success: function (data) {
				var d = data.data
				if(d.length > 20){
					d.length = 20
				}
				for(var i = 0; i< d.length; i++){
					if(d[i].nickname.length > 5){
						d[i].nickname = d[i].nickname.substring(0,5) + '...'
					} 
					if(d[i].headImg == null){
						d[i].headImg = 'https://tpxb.me/laots/userImg.png'
					}
//					html +='<ul class="robotIn" style="height: 30px; float: left"><li><img  src="'+d[i].headImg+'" style="width:25px;"></li><li><span>' + d[i].nickname + '</span></li><li><input type="button" value="发言" class="manageChat"'
//					+' onclick="robotSendBtn(&quot;'+ d[i].id+ '&quot;,&quot;'+ d[i].nickname+ '&quot;,&quot;'+ d[i].headImg+ '&quot;)" ></li></ul>'
				}
				html += '</div>'
				$("#user-server").append(html)
				//  机器人排序
				$(".robotIn").click(function(){
					var index = $(this).index()
					var $one_li = $("#robotSendModule ul:eq(0)");    //  获取<ul>节点中第二个<li>元素节点
			        var $two_li = $("#robotSendModule ul:eq("+index+")");    //  获取<ul>节点中第三个<li>元素节点
			        $two_li.insertBefore($one_li);    //移动节点
				})
			}
		});
	}
}
// 按钮展示
function robotSendBtn(id, name,headImg){
	$("#robotSend").attr('data-id',id);
	$("#robotSend").attr('data-name',name);
	$("#robotSend").attr('data-headimg',headImg);
	$("#robotSend").show();$("#send").hide();
	robotIsUser = 1;
	$("#robotIs").show();
	$("#robotName").text(name)
}
function noRobotSend(){
	robotIsUser = 0;
	$("#robotIs").hide();
	$("#robotSend").hide();$("#send").show();
}
//  机器人发言
function robotSend(img){
	var text = $("#boxscroll3").val(), id = $("#robotSend").attr('data-id'), name = $("#robotSend").attr('data-name'), headImg = $("#robotSend").attr('data-headimg');
	var messageType = 1;
    if(typeof (img) != "undefined"){
        text = img
        messageType = 2;
    }
	$.ajax({
		url : ip + "/admin/robot/robotMessage",
		data : {
			id : id,
			nickname : name,
			headImg : headImg,
			content : text,
			msgType : messageType,
		},
		dataType : 'json',
		// cache:isCache,
		beforeSend : function() {

		},
		error : function(data) {
			layer.msg('网络错误', {
				icon : 2
			});
		},
		success : function(data) {
			//发送消息后清空输入框
		    $("#boxscroll3").remove();
		    $("#liaotianModule").after('<textarea id="boxscroll3"  class="area" placeholder="文明聊天，共创优质的聊天环境，如有问题请咨询在线客服"></textarea>');
		    $("#boxscroll3").focus(function(){
		    	$(this).css('border','1px solid rgba(102, 175, 233)')
		    })
		    $("#boxscroll3").blur(function(){
		    	$(this).css('border','1px solid rgba(230, 230, 230, .3)')
		    })
		    $("#boxscroll3").focus();
		}
	});
}
	function webHeart(){
		websocket.send(JSON.stringify({
	        chat: {
	            content: '重连',
	            form: thisName,
	            to: "",
	            //接收人,如果没有则置空,如果有多个接收人则用,分隔
	            time: getDateFull(),
	            headImg: myHeadImg,
	            id: myId,
	            name: myName,
	            level:myLevel
	        },
	        type: "heart"
	    }));
	} 
	function thorwSplit(data){
		if(data.chat.msgType == "5"){
			data = data.chat
			data1 = JSON.parse(data.content);
			data.chat = {
				msgType : '5',
				form : data.form,
				headImg : data.headImg,
				level : data.level,
				name : data.name,
				time : data.time,
				id : data.id,
				lotCode : data1.lotCode,
				playCode : data1.data[0].playCode,
				min : data1.data[0].min,
				money : data1.data[0].money,
				oddsId : data1.data[0].oddsId,
				max : data1.data[0].max,
				name : data1.data[0].name,
				qiHao : data1.qiHao,
				playName : data1.playName,
				version : data1.version,
				lotType : data1.lotType,
				lotName : data1.lotName,
				groupCode : data1.groupCode,
				to:''
			}
			data.type = "message"
    	} 
		if(data.chat.msgType == "6"){
			data = data.chat
			data1 = JSON.parse(data.content);
			data.chat = {
				msgType : '6',
				form : data.form,
				headImg : data.headImg,
				level : data.level,
				name : data.name,
				time : data.time,
				id : data.id,
				type : data.type,
				bet:data1.bet,
				bunko:data1.bunko,
				win:data1.win,
				to:''
			}
			data.type = "message"
    	} 
		return data;
	}
	function eidtName(name){
		name = $("#boxscroll3").val() + '@' + name +' '
		$(".area").val(name);
	}
	
	function readPrivateMsg(nameSL){
		postAjax(ip+'readPrivateMsg', {
	        token: token,
	        start:'0',
	        end:'-1',
	        toName:nameSL
	    },
	    function(data) {
	    	if(data.code==1){
	    		$(".main-left .divright").removeClass("divright");
	    	}
	    });
	}
	