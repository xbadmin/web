var ip ='';
var robot;
//随页面启动
$(function () {
    //给聊天框赋值高度
    var chatHei =   $(".main-").height() - ($(".editor").height()+ 101 );
    $(".chatting").height(chatHei)
    locaMobile()
    ip=window.location.protocol+"//"+window.location.host+window.location.pathname;
    $("#boxscroll3").focus(function(){
        $("#ascrail2001").css("display","none");
    });
    $("#private-chat").css('left',(document.body.clientWidth-700)/2+'px')
    //初始化
     loadingUser();
 	//  定时请求防止断开
 	setInterval("webHeart()",200000);
 	getHistoricalList()
//     loadingTerrace()
//     loadingUserToken()
//绑定事件
//用户管理员切换
$("#on-line-member").click(function() {
    $(this).addClass("down-btn").siblings().removeClass("down-btn");
    $("#user-member").show();
    $("#user-server").hide();
});
$("#on-line-server").click(function() {
    $(this).addClass("down-btn").siblings().removeClass("down-btn");
    $("#user-member").hide();
    $("#user-server").show();
});
// 自定义滚动条

$("#boxscroll").niceScroll({
    cursorborder: "",
    cursorcolor: "#fff",
    boxzoom: false,
    autohidemode: false
});
 //输入框text
    $("#boxscrol4").niceScroll({
        cursorborder: "",
        cursorcolor: "#a0a3a3",
        boxzoom: false,
        autohidemode: false
    });
    $("#boxscrol5").niceScroll({
        cursorborder: "",
        cursorcolor: "#a0a3a3",
        boxzoom: false,
        autohidemode: false
    });

//页面加载聊天区域滚动最下
var chat = $("#boxscroll2");
chat.scrollTop(chat[0].scrollHeight);
$("#messageRoll").click(function(){
	if($(this).attr('data-value') == 1){
		$(this).attr('data-value',0)
		$(this).css('background-position','0 5px');
		$('.user-message-list').removeClass('hide');
	}else{
		$(this).attr('data-value',1)
		$(this).css('background-position','0 -14px');
		let allMessage = $('.user-message-list');
		if(allMessage.length > 30){
			for(let i in allMessage){
				if((i-0)<(allMessage.length - 20)){
					allMessage.eq(i).addClass('hide');
				}
			}
		}

	}
})
//网页换肤
$("#huanfu").click(function() {
    $("#huanfu-down").css("z-index", "999").css("opacity", "1");
    $('#huanfu-content-m').append('<div class="box"><img src="images/bodybackimg2.jpg" alt="" /><div class="box-content"></div></div><div class="box"><img src="images/bodybackimg3.jpg" alt="" />'+
            '<div class="box-content"></div></div><div class="box"><img src="images/bodybackimg4.jpg" alt="" /><div class="box-content"></div>'+
        '</div><div class="box"><img src="images/bodybackimg5.jpg" alt="" /><div class="box-content"></div></div><div class="box"><img src="images/bodybackimg6.jpg" alt="" /><div class="box-content"></div></div>')
});
$(".box").click(function() {
    var backImgUrl = $(this).find('img').attr('src');
    localStorage.setItem("sessionBack",backImgUrl);
    $(".warp").css("background-image", 'url(' + backImgUrl + ')')
});
if(localStorage.getItem("sessionBack")  != null){
    $(".warp").css("background-image", 'url(' + localStorage.getItem("sessionBack") + ')')
}
$("#huanfu-down").click(function() {
    $(this).css("z-index", "-1").css("opacity", "0");
});
//多功能按钮切换样式
$(".gg-classify-btn button").click(function() {
    $(this).addClass('gg-classify-btn-pitch').siblings().removeClass('gg-classify-btn-pitch');
    var ifyId = $(this).attr("ify");
    if(ifyId != "no"){
        $(".module-ify").hide()
        $("#"+ifyId+"").show()
    }
});
//个人信息修改区域
$(".personal_update_close").click(function() {
    $("#personal_show_module").css('display', 'none');
});
//修改个人信息小铅笔显示修改框
$(".icon-pencil").click(function() {
    $(this).siblings('input').show();
    $(this).siblings('select').show();
    $(this).siblings('span').hide();
    $("#user_update_ok").show()
});
//确认修改按钮
$("#user_update_ok").click(function() {
    var postData = {
        age: $("#user_update_age").val(),
        nickname: $("#user_update_name").val(),
        sex: $("#user_update_sex").val()
    };
    updateUser(postData);
});
//初始化姓名按钮
$("#initial_ok").click(function() {
    var postData = {
        nickname: $("#ininial_name").val()
    };
    loadingName(postData);
});
//回车发送消息
$('#boxscroll3').focus('keyup',
    function(event) {
        document.onkeydown = function(event) {
            var e = event || window.event || arguments.callee.caller.arguments[0];
            if (e && e.keyCode == 13) {
            	if(robotIsUser == 0){
            		sendTest();
                }else{
                	robotSend().focus();
                }
            }
        };
    });
$("#boxscroll3").focus(function(){
	$(this).css('border','1px solid rgba(102, 175, 233)')
})
$("#boxscroll3").blur(function(){
	$(this).css('border','1px solid rgba(230, 230, 230, .3)')
})
$("#open-right").click(function () {
    $(this).hide()
    $("#close-left").show()
    $(".main-left").show()
    $(".module-ify").css("right","250px")
    $("#open-list").css("right","220px")
    $(".warp .main .main- .chatting").css('height','78%')
    
    var commonUser = ''; //普通列表
    var superUser = ''; //管理列表
    var commonNum = 0;
    var superNum = 0;
    $.ajax({
		url: ip+'user/getOnlineList',
		type:'POST',
		data: {
			token: token,
			platFormId: ptId,
		},
		dataType: 'json',
		// cache:isCache,
		async : false, 
		beforeSend: function () {
			
		},
		error: function (data) {
			console.log(data)
		},
		success: function (data) {
			robotListHtml = '';
			robotListNum = 0;	
			if(data.msg === 'success'){
				var d = data.data.slice(1)
				if(robotListHtml == ''){
				for(var i = 0; i< d.length; i++){
					if(d[i].nickname.length > 5){
						d[i].nickname = d[i].nickname.substring(0,5) + '...'
					} 
					if(d[i].headImg == null){
						d[i].headImg = 'https://tpxb.me/laots/userImg.png'
					}
					if(d[i].level !== 0){
						continue
					}
						robotListNum ++
						robotListHtml +='<ul style="height: 30px; float: left"><li><img class="lazy" data-original="'+d[i].headImg+'" style="width:25px;"></li><li><span>' + d[i].nickname + '</span></li><li><input type="button" value="私聊"  id="chat' + d[i].id + '"  data-level="' + d[i].level + '" data-nickName="' +d[i].nickname+ '" title="'+d[i].nickname+'" data-name="' +d[i].name+ '" class="manageChat" ></li></ul>'
					}
				}
				commonUser +=robotListHtml
				commonNum +=robotListNum
			}
		    $("#user-member").html(commonUser); //用户列表
		    $("#on-line-member").val('在线会员（' + commonNum + '）'); //用户数量
		    manageChat()
	      	 $("img.lazy").lazyload({
	  		 container: $('#user-member'),   // 对指定标签对象内的图片实现效果，表示在此标签中滚动时，触发加载（注意：如果容器未设置height和overfload:auto，那么会默认加载所有图片）
	  		 placeholder : "images/userImg.png", //用图片提前占位
	  		 effect: "fadeIn",
	  		 threshold: 10,                    //当图片顶部距离显示区域还有100像素时，就开始加载
			 });
		}
	});
    
    $.ajax({
		url: ip+'user/getOnlineAdminList',
		type:'POST',
		data: {
			token: token,
			platFormId: ptId,
		},
		dataType: 'json',
		// cache:isCache,
		async : false, 
		beforeSend: function () {
			
		},
		error: function (data) {
			console.log(data)
		},
		success: function (data) {
			robotListHtml = '';
			robotListNum = 0;
			if(data.msg === 'success'){
				var d = data.data
				if(robotListHtml == ''){
				for(var i = 0; i< d.length; i++){
					if(d[i].nickname.length > 5){
						d[i].nickname = d[i].nickname.substring(0,5) + '...'
					} 
					if(d[i].headImg == null){
						d[i].headImg = 'https://tpxb.me/laots/userImg.png'
					}
						robotListNum ++
						robotListHtml +='<ul style="height: 30px; float: left"><li><img src="'+d[i].headImg+'" style="width:25px;"></li><li><span>' + d[i].nickname + '</span></li><li><input type="button" value="私聊" id="chat' + d[i].id + '"  data-level="' + d[i].level + '" data-nickName="' +d[i].nickname+ '" title="'+d[i].nickname+'" data-name="' +d[i].name+ '" class="manageChat" ></li></ul>'
					}
				}
				superUser +=robotListHtml
				superNum +=robotListNum
			}
		    $("#user-server").html(superUser); //用户列表
		    $("#on-line-server").val('客服/管理（' + superNum + '）'); //用户数量
		    manageChat()
       	
		}
	});

})
    $("#close-left").click(function () {
        $(this).hide()
        $("#open-right").show()
        $(".main-left").hide()
        $(".module-ify").css("right","20px")
        $("#open-list").css("right","0px")
        $(".warp .main .main- .chatting").css('height','76%')
    })
    

});
// 会员中心
$("#personModule").click(function(){
	openUpUser(myName)
})
//私聊框点击样式
// $(".pleft li").on({
//     mouseover : function(){
//         $(this).find(".pclose").css("opacity","0.8")
//     } ,
//     mouseout : function(){
//         $(this).find(".pclose").css("opacity","0")
//     }
// }) ;
$("#prclose").click(function () {
    $("#private-chat").fadeOut(100)
    $("#chatuserlist").html("");
    $(".prchat").html("")
    nameSlList = []
})
$("#prsmall").click(function () {
    $("#private-chat").fadeOut(100)
    $("#private-chat-small").fadeIn(100)
})
$("#prsclose").click(function () {
    $("#private-chat-small").fadeOut(100)
    $("#chatuserlist").html("");
    $(".prchat").html("")
    nameSlList = []
})
$("#prbig").click(function () {
    $("#private-chat-small").fadeOut(100);
    $("#private-chat").fadeIn(100);
    clearInterval(timer);
    $("#point").hide();
    var name = $(this).attr('data-name');
    //已读私聊
    readPrivateMsg(name);
    getPrivateRecord(name);
})
var timer;
function point() {
    $("#point").toggle();
}
function userChatOperate(name){
    $("#list"+name+"").click(function () {
        prname = name;
        $(this).addClass("pactive").siblings().removeClass("pactive");
        $("#prhead-img").attr("src",$(this).find("img").attr("src"));
        $("#prhead-name").text($(this).find("span").text())
        $(".prlistchat").hide();
        $("#boxscrol"+name+"").show()
    });
}
function userChatBox() {
    if($("#chatuserlist").find("li").length == 1){
        /*$(".pleft").css("opacity","0")*/
    }else{
        $(".pleft").css("opacity","1")
    }
}
function openUpUser(name){
	if(name == myName){
		$("#personal_show_module").css('display', 'flex');
	}
}