//ajax
function postAjax(url,postData,fun) {
    $.ajax({
        type: "post",
        url: url,
        data: postData,
        async:false,
        dataType: 'json',
        // cache:isCache,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function () {

        },
        error: function (data) {
            layer.msg("连接发生错误")
        },
        success: function (data) {
            success(data,fun)
        }
    });
}
function getAjax(url,getData,fun) {
    $.ajax({
        type: "get",
        url: url,
        data:getData,
        async:false,
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        // cache:isCache,
        beforeSend: function () {

        },
        error: function (data) {
            layer.msg("连接发生错误")
        },
        success: function (data) {
            success(data, fun);
        }
    });
}
function success(data,fun) {
    var msg = "OK";
    if(msg == "OK"){
        fun(data);
    }else{
        alert(msg);
    }
}
//获取地址栏参数
function UrlSearch() {
    var name, value;
    var str = location.href; //取得整个地址栏
    var num = str.indexOf("?");
    str = str.substr(num + 1); //取得所有参数   stringvar.substr(start [, length ]
    var arr = str.split("&"); //各个参数放到数组里
    for (var i = 0; i < arr.length; i++) {
        num = arr[i].indexOf("=");
        if (num > 0) {
            name = arr[i].substring(0, num);
            value = arr[i].substr(num + 1);
            this[name] = value;
        }
    }
}
var Request = new UrlSearch(); //实例化
// var token = Request.token;
function loadingTerrace() {
    var getData = {
        platformId: "136532",
        name: "tyc"
    };
    postAjax(ip + 'admin/agent/add', getData,
        function(data) {
        console.log(data)
    });
}
function loadingUserToken() {
    var getData = {
        token: "15dacb78-fe9c-4f5f-b7d6-daf4a199fcea",
        platformId: "136532",
        level: 0,
        name: "xiaohai"
    };
    getAjax(ip + 'getToken', getData,
        function(data) {
            console.log(data)
        });
}