import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store/store'
import game from '@/components/game' //游戏
import chat from '@/components/chat' //聊天
import myList from '@/components/myList' //我的
import recharge from '@/components/recharge' //充值
import addressList from '@/components/addressList' //通讯录
import scrollFreshen from "@/components/include/scrollFreshen";
import login from "@/components/login"; //登录
import register  from "@/components/register";
import lottery from "@/components/lottery/lottery";
import setCenter from "@/components/myList/setCenter"; //设置
import bank from "@/components/recharge/bank";
import fast from "@/components/recharge/fast";
import online from "@/components/recharge/online";
import virtual from "@/components/recharge/virtual";

Vue.use(Router)

const router = new Router({
  routes: [
    {path: '/game', name: 'game', component: game,meta:{cartoon:2}},
    {path: '/', name: 'chat', component: chat,meta:{cartoon:0}},
    {path: '/myList', name: 'myList', component: myList,meta:{cartoon:4}},
    {path: '/recharge', name: 'recharge', component: recharge,meta:{cartoon:3}},
    {path: '/addressList', name: 'addressList', component: addressList,meta:{cartoon:1}},
    {path: '/scrollFreshen', name: 'scrollFreshen', component: scrollFreshen},
    //lotter
    {path:'/lottery',name:"lottery",component:lottery},
    //login
    {path: '/login', name: 'login', component: login},
    {path: '/register', name: 'register', component: register},
    //myList
    {path:"/setCenter", name:"setCenter", component:setCenter},
    //recharge
    {path:'/bank',name:'bank',component:bank},
    {path:'/fast',name:'fast',component:bank},
    {path:'/online',name:'online',component:bank},
    {path:'/virtual',name:'virtual',component:virtual},
  ]
})
router.beforeEach(function (to, from, next) {
    // console.log(to)
  // console.log(from)
  // store.commit('updateLoadingStatus', {isLoading: true})
  next()
})

router.afterEach(function (to) {
  // let time = setInterval(()=>{
  //   store.commit('updateLoadingStatus', {isLoading: false})
  //   // console.log("ssss")
  //   clearInterval(time)
  // },1000)

})
export default router
