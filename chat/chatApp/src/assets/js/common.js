import store from '@/store/store';
import {entrys} from '@/assets/js/api/module.js'
import router from '@/router/index'
import {Toast} from 'vant'

const common={
  windowVerlap:store.state.windowVerlap,
  handlWindowetack(type){
    switch (type) {
      case 1:
        this.windowVerlap.headerLotter.caipiaoClassify = false;
        this.windowVerlap.headerLotter.militaryExploits = false;
        this.windowVerlap.headerLotter.mainLotterPlay =!this.windowVerlap.headerLotter.mainLotterPlay;
        this.windowVerlap.DrawLssue.headerOpenNumber = false;
        this.windowVerlap.DrawLssue.headerOpenRule =false;
      break;
      case 2:
        this.windowVerlap.headerLotter.caipiaoClassify = !this.windowVerlap.headerLotter.caipiaoClassify;
        this.windowVerlap.headerLotter.militaryExploits = false;
        this.windowVerlap.headerLotter.mainLotterPlay =false;
        this.windowVerlap.DrawLssue.headerOpenNumber = false;
        this.windowVerlap.DrawLssue.headerOpenRule =false;
      break;
    }
  },
  getMeminfo:function () {
    entrys.getMeminfo().then(res=>{
      if (res.data.success){
        store.commit("access",res.data.content)
      }
    })
  },
  permutationAndCombination:function(source = [], selectedLimit, isPermutation = true) {
    if (!Array.isArray(source)) return source

    source = [...new Set(source)]
    selectedLimit = selectedLimit || source.length

    const result = []
    const sourceLen = source.length

    selectedLimit = selectedLimit > sourceLen ? sourceLen : selectedLimit

    const innerLoop = (prefix = [], done = [], index = 0) => {
      const prefixLen = prefix.length

      for (let i = isPermutation ? 0 : index; i < sourceLen; i++) {

        if (prefixLen > selectedLimit - 1) break
        if (done.includes(i)) continue

        const item = source[i]
        const newItem = [...prefix, item]

        if (prefixLen === selectedLimit - 1) {
          result.push(newItem)
        }

        if (prefixLen < selectedLimit - 1) {
          innerLoop(newItem, [...done, i], index++)
        }

      }
    }

    if (source.length) {
      if (!isPermutation && selectedLimit === sourceLen) {
        return source
      }

      innerLoop()
    }

    return result
  },
  //getaccountInfo 获取账户信息
  getaccountInfo:function () {
    entrys.getaccountInfo().then(res=>{
      if (res.data.success) {
        store.commit("accountInfo", res.data.content)
        store.commit('isLogin',true)
      }else {
        Toast({
          message:res.data.msg,
          duration:1000,
          forbidClick:true
        })
        store.commit('isLogin',false)
        router.replace({
          path:'/login',
          query:{
            redirect:router.currentRoute.fullPath
          }
        })


      }
    })
  },
  //自动第三方转出
  //第三方额度自动转出
  getcheckLastThirdBalance:function () {
    entrys.getcheckLastThirdBalance().then(res=>{
      if (res.data.success){
        store.commit('getautoTransIsChangeOver',false)
      }
    })
  }



}

export default common
