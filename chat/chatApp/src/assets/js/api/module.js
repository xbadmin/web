import base from './api';
import axios from '@/assets/js/api/common';
import Qs from 'qs'

// 允许携带cookie
axios.defaults.withCredentials=true

var that =this
const home ={
  getStationInfo(){
    return axios.post(`${base.getStationInfo}`, '')
  },
  lotteryDate(params){
    return axios.post(`${base.lotteryGroup}`,Qs.stringify(params))
  },
  getLoctterys(params){
    return axios.get(`${base.getLoctterys}`,{params:params})
  },
  getDatasBesidesList(params){
    return axios.get(`${base.getDatasBesidesList}`,{params:params})
  },
  getQpGameItems(params){
    return axios.get(`${base.getQpGameItems}`,{params:params})
  },
  getDzGameItem(params){
    return axios.get(`${base.getDzGameItem}`,{params:params})
  },
  getDatasBesidesLotterys(params){
    return axios.post(`${base.getDatasBesidesLotterys}`,Qs.stringify(params))
  },
  getfastThirdRealTransMoney(params){
    return axios.post(`${base.fastThirdRealTransMoney}`,Qs.stringify(params))
  },
  getforwardBbin(params){
    return axios.post(`${base.forwardBbin}`,Qs.stringify(params))
  },
  getforwardEbet(params){
    return axios.post(`${base.forwardEbet}`,Qs.stringify(params))
  },
  getforwardBg(params){
    return axios.post(`${base.forwardBg}`,Qs.stringify(params))
  },
  getforwardMg(params){
    return axios.post(`${base.forwardMg}`,Qs.stringify(params))
  },
  getforwardAg(params){
    return axios.post(`${base.forwardAg}`,Qs.stringify(params))
  },
  getforwardPt(params){
    return axios.post(`${base.forwardPt}`,Qs.stringify(params))
  },
  getforwardQt(params){
    return axios.post(`${base.forwardQt}`,Qs.stringify(params))
  },
  getforwardJdb(params){
    return axios.post(`${base.forwardJdb}`,Qs.stringify(params))
  },
  getforwardIsb(params){
    return axios.post(`${base.forwardIsb}`,Qs.stringify(params))
  },
  getforwardMw(params){
    return axios.post(`${base.forwardMw}`,Qs.stringify(params))
  },
  getforwardTh(params){
    return axios.post(`${base.forwardTh}`,Qs.stringify(params))
  },
  getforwardKy(params){
    return axios.post(`${base.forwardKy}`,Qs.stringify(params))
  },
  getStationSwitch(params){
    return axios.post(`${base.getStationSwitch}`,Qs.stringify(params))
  },
  logout(params){
    return axios.post(`${base.logout}`,Qs.stringify(params))
  }
}
const caipiao ={
  getLotGamePlays(params){
    return axios.post(`${base.getLotGamePlays}`,Qs.stringify(params))
  },
  getCountDown(params){
    return axios.post(`${base.getCountDown}`,Qs.stringify(params))
  },
  getopen_result_detail(params){
    return axios.post(`${base.getopen_result_detail}`,Qs.stringify(params))
  },
  getDolotBet(params){
    return axios.post(`${base.getDolotBet}`,Qs.stringify(params))
  },
  getforwardReal(params){
    return axios.post(`${base.getforwardReal}`,Qs.stringify(params))
  }
}

const entrys ={
  getVerifyCodeSwitch(params){
    return axios.post(`${base.getVerifyCodeSwitch}`,Qs.stringify(params))
  },
  getGuestSwitch(){
    return axios.get(`${base.getGuestSwitch}`,{params:''})
  },
  getLogin(params){
    return axios.post(`${base.getLogin}`,Qs.stringify(params));
  },
  getaccountInfo(){
    return axios.get(`${base.getaccountInfo}`,{params:''})
  },
  getMeminfo(){
    return axios.get(`${base.getMeminfo}`,{params:''})
  },
  getcheckLastThirdBalance(params){
    return axios.post(`${base.getcheckLastThirdBalance}`,Qs.stringify(params))
  },
  getfastThirdRealTransMoney(params){
    return axios.post(`${base.getfastThirdRealTransMoney}`,Qs.stringify(params))
  }
}

const recharge ={
    getLunBo(){
      return axios.get(`${base.getLunBo}`,{params:''})
    },
    payAisleMenu(){
      return axios.get(`${base.payAisleMenu}`,{params:''})
    },
    payWayList(params){
      return axios.get(`${base.payWayList}`,{params:params})
    }
}
export {
  home,caipiao,entrys,recharge
}
