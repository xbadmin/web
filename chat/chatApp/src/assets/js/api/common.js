import axios from 'axios'
import store from '@/store/store'
import router from '@/router/index'
import {Toast} from 'vant'

var instance = axios.create({    timeout: 1000 * 12});
const  errorHandler =(status,other,response)=>{
  console.log(status)
  if(response.message.includes('timeout')){
    Toast({
      message:'请求超时',
      dutation:1500,
      forbidClick:true
    })
   return
  }
  switch (status) {
    case 401:
      router.replace({
        path:'/login',
        query:{
          redirect:router.currentRoute.fullPath
        }
      })
      break;
    case 403:
      Toast({
        message:"登录过期，请重新登录",
        duration:1000,
        forbidClick:true
      })
      // localStorage.removeItem("token");
      store.commit("isLogin",false);
      setInterval(()=>{
        router.replace({
          path:'/login',
          query: {
            redirect: router.currentRoute.fullPath
          }
        })
      })
      break;
    case 404:
      Toast({
        message:"网络请求不存在",
        duration:1500,
        forbidClick:true
      })
      break;
    default:
      Toast({
        //error.response.data.msg,
        message:'未知请求错误！请再次尝试',
        dutation:1500,
        forbidClick:true
      })
  }
}


if (process.env.NODE_ENV == 'development'){
  instance.defaults.baseURL = '/api/';
}else if (process.env.NODE_ENV == 'debug'){
  instance.defaults.baseURL = '/api/'
}else if (process.env.NODE_ENV == 'production'){
  instance.defaults.baseURL = store.state.apiUrl
}

instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
instance.defaults.withCredentials=true;
instance.interceptors.request.use(
  config =>{
    // accessToken
    // const token =store.state.token;
    // token && (config.headers.Authorization = token);
    return config;
  },
  error => {
    return Promise.error(error);
  },
)
instance.interceptors.response.use(
  response => {
    if (response.status == 200 ){
      if (response.data.hasOwnProperty('isLogin')){
        store.state.isLogin  = response.request.response.isLogin
        if (!response.data.isLogin){
          Toast({
            message:response.data.msg,
            duration:1000,
            forbidClick:true
          })
          store.commit("isLogin",false);
          setTimeout(()=>{
            router.replace({
              path:'/login',
              query: {
                redirect: router.currentRoute.fullPath
              }
            })
          },1000)
        }
      }
      return Promise.resolve(response)
    }else {
      return Promise.reject(response)
    }
  },
  error => {
    const { response} = error;
    if (response){
      errorHandler(response.status, response.data.msg,response);
      return Promise.reject(response);
    }else {
      if (!window.navigator.onLine) {
        store.commit('updateLoadingStatus', {isLoading:true});
        Toast({
          message:"请检测网络是否通畅！",
          duration:1000,
          forbidClick:true
        })
        return Promise.reject(error);
      }else {
        //404
        // Toast({
        //   message:"请求失败！",
        //   duration:1000,
        //   forbidClick:true
        // })
        // return Promise.reject(error);
      }
    }
  }
)

export default instance;
// axios.interceptors.response.use(
//   response =>{
//     if (response.status == 200){
//       return Promise.resolve(response);
//     }else{
//       return Promise.reject(response);
//     }
//   },
//   error=>{
//     if (error.response.status){
//       switch (error.response.status) {
//         case 401:
//           router.replace({
//             path:'/login',
//             query:{
//               redirect:router.currentRoute.fullPath
//             }
//           })
//           break;
//         case 403:
//           this.$toast({
//             message:"登录过期，请重新登录",
//             duration:1000,
//             forbidClick:true
//           })
//           localStorage.removeItem("token");
//           store.commit('loginSuccess',null);
//           setInterval(()=>{
//             router.replace({
//               path:'/login',
//               query: {
//                 redirect: router.currentRoute.fullPath
//               }
//             })
//           })
//           break;
//         case 404:
//           this.$toast({
//             message:"网络请求不存在",
//             duration:1500,
//             forbidClick:true
//           })
//           break;
//         default:
//           this.$toast({
//             message:error.response.data.msg,
//             dutation:1500,
//             forbidClick:true
//           })
//       }
//     }
//     return Promise.reject(error.response);
//   }
// )
