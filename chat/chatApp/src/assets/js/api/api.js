const native = 'native/'
const base ={
  //网站基础信息
  getStationInfo:native + 'getStationInfo' +'.do',
  //彩种分类信息
  lotteryGroup : native + 'lotteryGroup' +'.do',
  //获取彩种全部信息
  getLoctterys : native + 'getLoctterys'+ '.do',
  //获取游戏大类信息
  getDatasBesidesList:native + 'getDatasBesidesList' + '.do',
  //获取棋牌小游戏
  getQpGameItems:native + 'getQpGameItems' + '.do',
  //获取棋牌小游戏
  getDzGameItem:native + 'getDzGameItem' + '.do',
  //获取游戏小分类信息
  getDatasBesidesLotterys: native +'getDatasBesidesLotterys'+'.do',
  //自动转换额度
  fastThirdRealTransMoney:native +'fastThirdRealTransMoney'+'.do',
  //bbin
  forwardBbin:'forwardBbin'+'.do',
  //ebetDz
  forwardEbet:'forwardEbet'+'.do',
  //bg
  forwardBg:'forwardBg'+'.do',
  //mg
  forwardMg:'forwardMg' + '.do',
  //ag电子
  forwardAg: 'forwardAg' + '.do',
  //JDB电子
  forwardJdb:'forwardJdb' + '.do',
  //cq9电子
  forwardCq9:'forwardCq9' +'.do',
  //isb电子
  forwardIsb:'forwardIsb' + '.do',
  //MW电子
  forwardMw:'forwardMw' +'.do',
  //toQT电子
  forwardQt:'forwardQt' +'.do',
  //Ttg电子
  forwardTtg:'forwardTtg' +'.do',
  //pt电子
  forwardPt:'forwardPt' +'.do',
  //天豪棋牌
  forwardTh:'forwardTh' + '.do',
  //开元棋牌
  forwardKy:'forwardKy'+'.do',
  //自动转换额度等开关
  getStationSwitch:native + 'getStationSwitch' + '.do',
  //登出
  logout:native + 'logout' + '.do',

  //彩票
  getLotGamePlays:native +'getLotGamePlays' + '.do',
  //获取下一期数据时间期号等
  getCountDown:native + 'getCountDown' + '.do',
  //获取开奖号码post
  getopen_result_detail:native + 'open_result_detail' + '.do',
  //投注数据
  getDolotBet:native + 'dolotBet.do',
  //z这条接口是跳转第三的接口，任何第三方都可以跳转
  getforwardReal:native + 'forwardReal.do',

  //入口
  //验证码
  getVerifyCodeSwitch:native +"getVerifyCodeSwitch.do",
  //试玩开关
  getGuestSwitch:native + "getGuestSwitch.do",
  //登录
  getLogin:native + "login.do",
  //获取用户信息
  getaccountInfo:native + "accountInfo.do",
  //获取用户金额
  getMeminfo:native + "meminfo.do",
  //默认从第三方转出金额
  getcheckLastThirdBalance:native +'checkLastThirdBalance.do',
  //自动转入额度到第三方
  getfastThirdRealTransMoney:native +'fastThirdRealTransMoney.do',

  //充值页面
  //轮播图
  getLunBo:native + "getLunBo.do",
  payAisleMenu:native + 'payAisleMenu.do',
  payWayList:native + 'payWayList.do',
}

export default base;
