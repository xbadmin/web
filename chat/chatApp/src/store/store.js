import  Vue from 'vue';
import  Vuex from 'vuex';
import VueCookies from 'vue-cookies';
Vue.use(Vuex);

const store= new Vuex.Store({
  state:{
    isLoading:true,
    isLogin:false,
    access:{},
    accountInfo:{},
    windowVerlap:{},//避免多个弹窗重叠在一起
    apiUrl:'',
    StationInfo:{},
    version:5,//彩票版本
    stationSwitch:[],//开关
    autoTransIs:{switchIs:false,changeOver:false},//自动转换额度
  },
  mutations:{
    isLogin(state,val){
      state.isLogin = val;
      localStorage.setItem("isLogin",val)
    },
    updateLoadingStatus(state,load){
      state.isLoading = load.isLoading
      localStorage.setItem("isLoading",load.isLoading)
    },
    updataStationInfo(state,value){
      state.StationInfo = value
      // state.apiUrl = 'https://x002.xb336.com'
      state.apiUrl = location.protocol  + '//' + value.content.host
      localStorage.setItem("StationInfo",JSON.stringify(value))
    },
    handlWindowetack(state,val){
      state.windowVerlap.headerLotter = val
    },
    handlWindowetack2(state,vale){
      state.windowVerlap.DrawLssue = vale
    },
    handlWindowetack3(state,vale){
      state.windowVerlap.footerLotter =vale
    },
    handlWindowetack4(state,vale){
      state.windowVerlap.DrawLssue.headerOpenRule = vale
    },
    accountInfo(state,val){
      state.accountInfo =val
    },
    access(state,val){
      state.access =val
      localStorage.setItem("access",JSON.stringify(val))
    },
    getStationSwitch(state,val){
      state.stationSwitch = val
      localStorage.setItem("stationSwitch",JSON.stringify(val))
    },
    //自动转入转出开关
    getautoTransIs(state,val){
      state.autoTransIs.switchIs = Boolean(val)
      localStorage.setItem("autoTransIsSwitchIs",JSON.stringify(val))
    },
    //自动转入
    getautoTransIsChangeOver(state,val){
      state.autoTransIs.changeOver = Boolean(val)
      localStorage.setItem("autoTransIsChangeOver",JSON.stringify(val))
    }
  },
  getters:{
    updataStationInfo:(state,getter)=>{
        return
    }
  },
  actions:{

  },
})
export default store

