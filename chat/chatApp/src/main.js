// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from '@/store/store'
// const store = require('./store/store')
import Vant from 'vant' //vant全局引入
import {home,caipiao,entrys,recharge} from './assets/js/api/module.js'
import VueAwesomeSwiper from 'vue-awesome-swiper' //映入全局swipper
import VueLazyload from "vue-lazyload";
import {Header,Button,Field } from 'mint-ui'
import common from '@/assets/js/common'
//import VueBetterScroll from 'vue2-better-scroll'
// var VueBetterScroll = require('vue2-better-scroll')
// import cookie from 'vue-cookie'
// Vue.prototype.$cookie = cookie;
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

Vue.use(Vant)
Vue.use(VueLazyload,{
  preLoad:1.3,
  error:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAACaUlEQVRoQ+2ZTU7jQBCF30sEW8INgjTJFo4AJ4BZj4jjE8ANyNwgcwInQayBEzA3mMw2RiI3IOsIU6gte+QxtmPs9h/Eq8Rtt9/XVV1dXU00/GLD9ePzAPSnYghwSeKozlYRwZzAeGFwqnS6FujNxCIwrLPwsDYBJvaAJr9dy3FL8NAk8b7WV+KE/ZncATj1bt6vHQyXJld1BOpa0tltYxLUqwDEF7t2cLA0uayjeF9T15LubhtP/v//ABYDNiIqBQd9C1CFu20tEJxQ6nfZEz+3BfrXciavsEh0FIAIVmzBXJxTheTCr1wASjwEt5Eqie9lQOQC6M1kRWAvCkBZwja4X7QJMgOkSTvU8v54zt9FQnxdAC8XeU4a3bWD/aJzqcwWUML7UxmBuIqEEPxcGBwV6T6uhkD+limV6E1lTOIiKFQEv2yDl1nF92YyFKDzOOB4Ux+5AdxN0I0cwfF2b23M7R+cb/pwXLsST8By1xTAtAdUKXPspQUgq9jwe0HxftsmiNoAJC2KSRC1AHBd8AUPfjoSuTDGuFPlAGnEJ7mTNgAlRJzotCJ2jgg6BCZJI//u3VCOpQUgavLpmtjv+gmtL7kB0uREWmF0AnzEf7VB6AJwc6EW/oDoahOXpiMdAEr8TssNe+XXTnUAVFo/zQsQlbylsby2Z/IAlBou44izApQeLnUCVBIudQFUFi51AWibfAV1lDuVKEhX6m5jAZp6wBE8YrpbOzCLLoukHurQg15ZR+2dz7ym++Yf8imSylfaDCbxyzj/zsS8DbYqSh1m6K/MV/6CGPlV8EYc6iWNzhagTN+J+tYbCy3hhrOcXsAAAAAASUVORK5CYII=',
  loading:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAACaUlEQVRoQ+2ZTU7jQBCF30sEW8INgjTJFo4AJ4BZj4jjE8ANyNwgcwInQayBEzA3mMw2RiI3IOsIU6gte+QxtmPs9h/Eq8Rtt9/XVV1dXU00/GLD9ePzAPSnYghwSeKozlYRwZzAeGFwqnS6FujNxCIwrLPwsDYBJvaAJr9dy3FL8NAk8b7WV+KE/ZncATj1bt6vHQyXJld1BOpa0tltYxLUqwDEF7t2cLA0uayjeF9T15LubhtP/v//ABYDNiIqBQd9C1CFu20tEJxQ6nfZEz+3BfrXciavsEh0FIAIVmzBXJxTheTCr1wASjwEt5Eqie9lQOQC6M1kRWAvCkBZwja4X7QJMgOkSTvU8v54zt9FQnxdAC8XeU4a3bWD/aJzqcwWUML7UxmBuIqEEPxcGBwV6T6uhkD+limV6E1lTOIiKFQEv2yDl1nF92YyFKDzOOB4Ux+5AdxN0I0cwfF2b23M7R+cb/pwXLsST8By1xTAtAdUKXPspQUgq9jwe0HxftsmiNoAJC2KSRC1AHBd8AUPfjoSuTDGuFPlAGnEJ7mTNgAlRJzotCJ2jgg6BCZJI//u3VCOpQUgavLpmtjv+gmtL7kB0uREWmF0AnzEf7VB6AJwc6EW/oDoahOXpiMdAEr8TssNe+XXTnUAVFo/zQsQlbylsby2Z/IAlBou44izApQeLnUCVBIudQFUFi51AWibfAV1lDuVKEhX6m5jAZp6wBE8YrpbOzCLLoukHurQg15ZR+2dz7ym++Yf8imSylfaDCbxyzj/zsS8DbYqSh1m6K/MV/6CGPlV8EYc6iWNzhagTN+J+tYbCy3hhrOcXsAAAAAASUVORK5CYII=',
  attempt:1
})
Vue.config.productionTip = false;
Vue.prototype.$http = {home,caipiao,entrys,recharge};
Vue.prototype.$common =  common;
Vue.use(VueAwesomeSwiper);
Vue.component(Header.name,Header);
Vue.component(Button.name,Button );
Vue.component(Field.name, Field );
// Vue.use(VueBetterScroll)x
// 焦点事件
Vue.directive('focus', {
  // 当被绑定的元素插入到 DOM 中时……
  inserted: function (el) {
    // 聚焦元素
    el.focus()
  }
})
/* eslint-disable no-new */
import 'vant/lib/index.css';
import '@/assets/css/icon.scss';
import '@/assets/css/reset.scss';
import '@/assets/css/border.scss';
import '@/assets/css/variblesAndMixin.scss';
import '@/assets/css/common.scss';
import 'swiper/css/swiper.css' //映入全局swipper css
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
})
