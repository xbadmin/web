
const webpack = require('webpack')
module.exports = {
    productionSourceMap:false,
    css: {
        loaderOptions: {
            sass: {
                data: `@import "@/scss/var.scss";`
            }
        }
    },
    configureWebpack:{
        plugins:[
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                'root.jQuery': 'jquery'
            })
        ]
    }
}