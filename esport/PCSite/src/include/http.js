import Vue from 'vue'
import axios from 'axios'
import Store from '@/store'
import Router from '@/router'
import Config from './config'
import $helper from './helper'

const http = axios.create({
    baseURL:Config.serviceAPI,
    headers:{
        /* 当前平台类型 */
        application:'pc',
        /* 当前用户token */
        token:'',
    }
})

/* 请求拦截器 */
http.interceptors.request.use(request => {

    let {data = {}} = request

    /* #######  多数接口强制要求userid这个值。判断后使用vuex补全  ######*/
    if(data && !data.userID && Store.state.USERINFO && Store.state.USERINFO.userID){
        data.userID = Store.state.USERINFO.userID
    }

    /* #######  加密处理  ######*/
    request.data = $helper.encryptionData(data)
    return request
})

/* 响应拦截器 */
http.interceptors.response.use(response => {
    const {resultCode,message} = response.data
    if(resultCode != 1){
        /* 提示服务端报错信息 */
        switch(resultCode){
            /* 赔率发生变化 */
            case 9:
                break
            default:
                // Vue.prototype.$message({message,type:'error'})
                break
        }

        /* 根据服务端code后置操作 */
        switch(resultCode){
            /* token过期 */
            case 401:
                /* 注销操作 */
                $helper.goLogout()
                /* 跳转至首页 */
                // Router.push({name:'homeIndex'})
                // Store.commit('setInterActive','login')
                // Vue.prototype.$message({message,type:'发生未知错误，请刷新页面'})
                Vue.prototype.$message.closeAll()
                Vue.prototype.$message('登录状态失效，请刷新页面或重新登录')
                break
            default:
                break
        }

    }
    return response
})

Vue.prototype.$http = http
