/* API地址 */
let ApiGroup = {}
switch (process.env.NODE_ENV) {
    case 'development':
        ApiGroup = {
            serviceAPI:'https://webapi.xinbocloud.com/',
        }
        break
    case 'production':
        ApiGroup = {
            serviceAPI:'https://webapi.xinbocloud.com/',
        }
        break
}
export default {
     ...ApiGroup,
     oddsHubUrl:'/MatchHub',                                    /* 赔率变化sokect hub地址 */
     logLevel:2,                                                /* 日志显示等级 */
}