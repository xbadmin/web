import Vue from 'vue'
import Store from '@/store'
import dayjs from 'dayjs'
import md5 from 'js-md5'
import $config from './config'

const $helper = function(){

    /* 提示用户进行登录 */
    this.goLogin = () => {
        /* 登录提示 */
        Vue.prototype.$message.warning({
            message:'请登录',
            type:'warning'
        })
        /* 打开登录层 */
        Store.commit('setInterActive','login')
    }

    /* 写入用户登录信息 */
    /**
     * @user        需要写入的用户信息
     * @remember    是否需要记住账号
     */
    this.setUserLogin = (user,remember=false) => {
        /* 缓存用户token及account */
        if(remember){
            /* 一、记住账号 */
            localStorage.setItem('userinfo',JSON.stringify(user))
            sessionStorage.setItem('userinfo',JSON.stringify(user))
            localStorage.setItem('rememberUserName',user.account)
        }else{
            /* 二、不需记住账号 */
            sessionStorage.setItem('userinfo',JSON.stringify(user))
        }
        /* 设置到store */
        Store.commit('setUserBasicInfo',user)
        /* 刷新一次用户信息 */
        Store.dispatch('refreshUserInfo')
    }

    /* 用户进行注销 */
    this.goLogout = () => {
    /* 清除缓存 */
        localStorage.removeItem('userinfo')
        sessionStorage.removeItem('userinfo')
        /* 将token从头部信息排除 */  
        Vue.prototype.$http.defaults.headers.token = ''
        /* 还原store状态 */
        Store.commit('resetSystem',Vue.prototype._protoStoreState)
        /* 重新初始化应用 */
        Store.dispatch('initialWeb')
        /* 重新拉取在线人数 */
        if(Vue.prototype.$oddsSocket){
            Vue.prototype.$oddsSocket.invoke('GetOnlineCount')
        }
    }

    /* 返回当期日期 */
    this.getNowDate = () => {
        return new dayjs().format('YYYY-MM-DD')
    }

    /* 返回默认以往日期 */
    this.getPastDate = () => {
        return new dayjs().subtract(15,'day').format('YYYY-MM-DD')
    }

    /* 返回当前月第一天 */
    this.getMonthFirst = () => {
        return new dayjs().startOf('month').format('YYYY-MM-DD')
    }

    /* 返回当前月最后一天 */
    this.getMonthLast = () => {
        return new dayjs().endOf('month').format('YYYY-MM-DD')
    }

    /* 快速复制文本信息 */
    this.copyHandle = (char='',remind=false) => {
        /* 过滤文本内容 */
        if(typeof char == 'string'){
            char = char.replace(/<[^>]+>/g,'')
        }
        /* 复制文本内容 */
        Vue.prototype.$copyText(char)
        /* 打印提示 */
        remind = remind === null ? '复制成功' : remind
        if(remind && typeof remind == 'string'){
            Vue.prototype.$message({message:remind,type:'success'})
        }
    }

    /* 打印日志 */
    this.log = (obj,remind=null,level=1) => {
        if(level >= $config.logLevel){
            /* 如果有提示信息 */
            if(remind){
                console.warn(`${new dayjs().format('hh:mm:ss')}: ${remind}`)
            }
            if(obj){
                console.dir(obj)
            }
        }
    }

    /* 格式化投注数据对象 */
    this.getEsportsOddsObject = item => {
        
        let result = {
            mid:'',                         /* 赛事id */
            edoid:'',                       /* 投注id */
            odds:'',                        /* 当前投注赔率(str)    */
            newOdds:0,                      /* 当期投注赔率(num)    */
            oldOdds:0,                      /* 上期投注赔率(num)    */
            oddsBetState:1,                 /* 当前投注状态 */
            result:0,                       /* 当前投注（玩法）是否有结果 */
        }

        /* 赛事id */
        let mid = item.mid || false
        if(mid) result.mid = mid

        /* 投注id */
        let edoid = item.edoid || false
        if(edoid)   result.edoid = edoid

        /* 当前投注赔率 */
        let odds = item.odds || item.newOdds || false
        if(odds){
            result.odds = odds
            result.newOdds = parseFloat(odds)
        }

        /* 上期投注赔率 */
        let oldOdds = item.oldOdds || false
        if(oldOdds){
            result.oldOdds = parseFloat(oldOdds)
        }

        /* 当前投注状态 */
        if(item.oddsBetState){
            result.oddsBetState = parseInt(item.oddsBetState)
        }

        /* 当前投注（玩法）是否有结果 */
        if(item.result){
            result.result = parseInt(item.result)
        }

        return result
    }

    /* 对请求参数加密  原参数 、时间戳、 随机字串、秘钥*/
    this.encryptionData = (data,unixTimestamp=false,random=false,secretKey=false) => {

        /* 一、原参数转为升序对象 */
        data = this.jsonSort(data)

        /* 二、获取当前时间戳 */
        unixTimestamp = unixTimestamp ? unixTimestamp : new dayjs().unix()
        
        /* 三、随机数 */
        random = random ? random : 'aaa' + Math.random()

        /* 四、秘钥 */
        secretKey = secretKey ? secretKey : `d4rHhEcEyYiPng3o47IFiyrfjpkSI3NpuzPH84SfuAA7fNiAqny2e6B0uCqy6OASklYPAQ68J1lTqL3WTSf2gSIknbDnzGYvGeTO4ApiajY0tgnPJ3D1HIq6aAPK3v07`
        
        /* 五、使用&连接 */
        const signature = md5(`${JSON.stringify(data)}&${unixTimestamp}&${random}&${secretKey}`)

        data.timestamp = unixTimestamp
        data.random = random
        data.signature = signature

        return data
    }

    /* 对json升序排序 */
    this.jsonSort = jsonObj => {
        let arr=[]
        for(var key in jsonObj){
            arr.push(key)
        }
        arr.sort()
        let result={}
        for(var i in arr){
            const jsonItem = jsonObj[arr[i]] === null ? '' : jsonObj[arr[i]]
            result[arr[i]] = jsonItem
        }
        return result
    }

}

export default new $helper()