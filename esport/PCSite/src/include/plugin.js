import Vue from 'vue'
/* element-ui */
import { Carousel,CarouselItem, DatePicker,Select,Option,Pagination,Loading,Message,MessageBox,Scrollbar,Input} from 'element-ui'
import lang from 'element-ui/lib/locale/lang/zh-CN'
import locale from 'element-ui/lib/locale'
locale.use(lang)

Vue.component('el-carousel',Carousel)
Vue.component('el-carousel-item',CarouselItem)
Vue.component('el-date-picker',DatePicker)
Vue.component('el-select',Select)
Vue.component('el-option',Option)
Vue.component('el-pagination',Pagination)
Vue.component('el-scrollbar',Scrollbar)
Vue.component('el-input',Input)

Vue.prototype.$loading = Loading
Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$alert = MessageBox.alert

/* material */
import {MdField,MdButton,MdCheckbox,MdRadio,MdList,MdMenu,MdContent} from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
Vue.use(MdField)
Vue.use(MdButton)
Vue.use(MdCheckbox)
Vue.use(MdRadio)
Vue.use(MdList)
Vue.use(MdMenu)
Vue.use(MdContent)

/* 剪贴板插件 */
import VueClipBoard from 'vue-clipboard2'
Vue.use(VueClipBoard)

/* 全局配置挂载到window对象 */
import Config from './config'
window.$config = Config

/* 图片懒加载 */
import VueLazyLoad from 'vue-lazyload'
Vue.use(VueLazyLoad,{
    loading:require('@/assets/img/lazy-load.png'),
    preLoad:2
})