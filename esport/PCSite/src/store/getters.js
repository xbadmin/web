import BigNumber from 'bignumber.js'
export default {
    /* 网站名称 */
    WEB_NAME:state => {
        let result = ''
        if(state.WEB_INFO){
            result = state.WEB_INFO.siteName || ''
        }
        return result
    },
    /* 网站状态 */
    SITE_STATE:state => {
        let result = false
        if(state.WEB_INFO){
            result = state.WEB_INFO.state ? parseInt(state.WEB_INFO.state) : 1
        }
        return result
    },
    /* 网站运营商ID */
    SITE_SID:state => {
        let result = ''
        if(state.WEB_INFO){
            result = state.WEB_INFO.sid || ''
        }
        return result
    },
    /* 网站LOGO */
    SITE_LOGO:state => {
        let result = ''
        if(state.WEB_INFO){
            result = state.WEB_INFO.logo || ''
        }
        return result
    },
    /* 网站联系电话 */
    SITE_PHONE:state => {
        let result = ''
        if(state.WEB_INFO){
            result = state.WEB_INFO.phone || ''
        }
        return result
    },
    /* 网站联系QQ */
    SITE_QQ:state => {
        let result = ''
        if(state.WEB_INFO){
            result = state.WEB_INFO.qq || ''
        }
        return result
    },
    /* 网站微信二维码 */
    SITE_WX_QRCODE:state => {
        let result = ''
        if(state.WEB_INFO){
            result = state.WEB_INFO.wxUrl || ''
        }
        return result
    },
    
    /* 用户是否为代理商 */
    USER_IS_AGENT:state => {
        let result = false
        if(state.USERINFO){
            result = state.USERINFO.isUion || false
        }
        return result 
    },
    
    /* 用户冻结金额 */
    USER_MONEY_FREEZE:state => {
        let result = false
        if(state.USERINFO){
            result = state.USERINFO.freeze ? parseInt(state.USERINFO.freeze) : 0
        }
        return result
    },
    /* 优惠活动列表 进行中优惠 */
    PROMOTION_LIST_STATE_0:state => {
        let result = []
        if(Array.isArray(state.PROMOTION_LIST) && state.PROMOTION_LIST.length){
            result = state.PROMOTION_LIST.filter(item => item.state == '0')
        }
        return result
    },
    /* 优惠活动列表 以往优惠 */
    PROMOTION_LIST_STATE_1:state => {
        let result = []
        if(Array.isArray(state.PROMOTION_LIST) && state.PROMOTION_LIST.length){
            result = state.PROMOTION_LIST.filter(item => item.state == '1')
        }
        return result
    },
    /* 资讯类型列表 */
    NEWS_TYPE_LIST:() => {
        let result = new Map()
        result.set('xw',{id:'7b261dc2-1cdd-49f0-b477-00280c873a1c',name:'新闻'})
        result.set('tj',{id:'aed6eda2-c663-4d91-886c-01574e9034aa',name:'图集'})
        result.set('sp',{id:'328f1bac-bfdf-42b5-a637-012707a8a6b8',name:'视频'})
        result.set('zt',{id:'1a96dbfb-c18c-426c-a7da-00152574b0e0',name:'专题'})
        return result
    },
    /* 首页用 专题列表 */
    NEWS_SPECIAL_LIST1:state => {
        let result = false
        if(Array.isArray(state.NEWS_SPECIAL_LIST)){
            result = state.NEWS_SPECIAL_LIST.slice(0,6)
        }
        return result
    },
    /* 资讯模块用 专题列表 */
    NEWS_SPECIAL_LIST2:state => {
        let result = false
        if(Array.isArray(state.NEWS_SPECIAL_LIST)){
            result = state.NEWS_SPECIAL_LIST.slice(0,3)
        }
        return result
    },

    /* MAP 玩家投注列表（单关） 键： 投注ID*/
    MAP_USER_ODDS_LIST:state => {
        let result = false
        if(Array.isArray(state.USER_ODDS_LIST) && state.USER_ODDS_LIST.length){
            result = new Map()
            state.USER_ODDS_LIST.map((item,index) => {
                /* 设置（在state中）数组的下标 */
                item.arrayIndex = index
                /* 计算可赢取金额 */
                item.winMoney = new BigNumber(item.odds).multipliedBy(item.money).toFixed(2,1)
                /* push */
                result.set(item.edoid,item)
            })
        }
        return result
    },
    /* MAP 玩家投注列表（串关） 键：投注ID */
    MAP_USER_ODDS_LIST2:(state,getters) => {
        let result = false
        if(getters.MAP_USER_ODDS_LIST.size){
            result = new Map()
            getters.MAP_USER_ODDS_LIST.forEach(item => {
                if(item.canCombine){
                    /* 每赛事只允许一种玩法进入串关列表 */
                    result.set(item.mid,item)
                }
            })
        }
        return result
    },
    /* 玩家投注列表（串关） 数组便于组件调用 */
    USER_ODDS_LIST2:(state,getters) => {
        let result = []
        if(getters.MAP_USER_ODDS_LIST2.size){
            getters.MAP_USER_ODDS_LIST2.forEach(item => {
                if(item.canCombine){
                    result.push(item)
                }
            })
        }
        return result
    },
}