import Vue from 'vue'
import VueRouter from '@/router'
export default {
    /* 设置交互应用层 */
    setInterActive:(state,component) => {
        state.IA_COMPONENT = component
    },
    /* 设置网站基本信息 */
    setWebBasicInfo:(state,datas) => {
        const {siteName} = datas
        document.title = siteName
        /* 维护中跳转 */
        const siteState = datas.state ? parseInt(datas.state) : 0
        if(siteState === 2){
            VueRouter.push({name:'defend'})
        }
        state.WEB_INFO = datas
    },
    /* 设置用户基本信息（账户名及token) */
    setUserBasicInfo:(state,basicInfo) => {
        const {account=undefined,token=undefined} = basicInfo
        if(account) state.USER_ACCOUNT = account
        if(token){
            // state.USER_TOKEN = token
            let url = location.search; //获取url中"?"符后的字串
            let theRequest = new Object();
            if (url.indexOf("?") != -1) {
                let str = url.substr(1);
                let strs = str.split("&");
                for(let i = 0; i < strs.length; i ++) {
                    theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
                }
            }
            /* 更新至http请求工具请求头部信息 */
            Vue.prototype.$http.defaults.headers.token = theRequest.token
        }
    },
    /* 设置用户业务信息 */
    setUserInfo:(state,userinfo) => {
        state.USERINFO = userinfo
    },
    /* 设置用户connnectID */
    setUserConnectionID:(state,connectionID) => {
        state.CONNECTION_ID = connectionID
    },
    /* 设置推荐信息列表 */
    setPromotionList:(state,list) => {
        if(Array.isArray(list) && list.length){
            state.PROMOTION_LIST = list
        }
    },
    /* 设置短信验证码倒计时 */
    setSmsCodeCountDown:(state,countdown) => {
        state.SMS_COUNT_DOWN = countdown
    },
    /* 设置邀请码 */
    setRecommendCode:(state,code) => {
        state.RECOMMEND_CODE = code
    },
    /* 记录更新过投注详情列表的赛事id */
    setEsportsPullHistory:(state,mid) => {
        state.ESPORTS_PULL_HISTORY.add(mid)
    },
    /* 设置竞猜游戏列表  时间类型 */
    setEsportsSortId:(state,id) => {
        state.ESPORTS_SORT_ID = id
    },
    /* 设置竞猜游戏筛选类型（游戏id） */
    setEsportsGameId:(state,egid) => {
        state.ESPORTS_GAME_ID = egid
    },
    /* 设置竞猜游戏列表 */
    setEsportsGameList:(state,list) => {
        state.ESPORTS_GAME_LIST = list
    },
    /* 资讯 设置资讯类型 */
    setNewsTypeList:(state,list) => {
        if(Array.isArray(list) && list.length){
            state.NEWS_TYPE_LIST = list
        }
    },
    /* 资讯 设置资讯种类*/
    setNewsSortList:(state,list) => {
        if(Array.isArray(list) && list.length){
            state.NEWS_SORT_LIST = list
        }
    },
    /* 资讯 设置资讯专题*/
    setNewsSpecialList:(state,list) => {
        if(Array.isArray(list) && list.length){
            state.NEWS_SPECIAL_LIST = list
        }
    },
    /* 设置投注模式 */
    setEsportsOddsMode:(state,status) => {
        state.ESPORTS_ODDS_MODE = status
    },
    /* 新增用户投注 */
    pushUserOddsList:(state,data) => {
        state.USER_ODDS_LIST.unshift(data)
    },
    /* 更新用户投注 */
    updateUserOddsItem:(state,config) => {
        const {arrayIndex,newObject} = config
        if(arrayIndex !== false && newObject){
            state.USER_ODDS_LIST[arrayIndex] = Object.assign(state.USER_ODDS_LIST[arrayIndex],newObject)
        }
    },
    /* 删除用户投注 */
    removeUserOddsItem:(state,index) => {
        state.USER_ODDS_LIST.splice(index,1)
    },
    /* 重置用户投注状态 */
    reUserOddsListBetResult:state => {
        state.USER_ODDS_LIST.map((item,index) => {
            item.betResult = 0
            state.USER_ODDS_LIST[index] = item
        })
    },
    /* 设置串关投注数据 */
    setEsportsOddsList2:(state,data) => {
        state.USER_ODDS_LIST2 = Object.assign(state.USER_ODDS_LIST2,data)
    },
    /* 清空投注实体列表 */
    clearUserOddsList:state => {
        state.USER_ODDS_LIST = []
        state.USER_ODDS_LIST2.money = ''
    },
    /* 切换是否接收赔率变化 */
    setAcceptOddsChange:(state,status) => {
        state.ACCEPT_ODDS_CHANGE = status
    },
    /* 设置网站在线人数 */
    setOnlineCount:(state,count) => {
        state.ONLINE_COUNT = count
    },
    /* 注销+重置 */
    resetSystem:(state,newState) => {
        if(newState){
            newState = JSON.parse(newState)
            Object.keys(state).map(key => state[key] = newState[key])
        }
    },
}
