import Vue from 'vue'
import dayjs from 'dayjs'
import md5 from 'js-md5'
import Config from '../include/config'
export default {
    /* 根据用户名获得用户个人信息 */
    refreshUserInfo:function({commit,state}){
        let url = location.search; //获取url中"?"符后的字串
        let theRequest = new Object();
        if (url.indexOf("?") != -1) {
            let str = url.substr(1);
            let strs = str.split("&");
            for(let i = 0; i < strs.length; i ++) {
                theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
            }
        }
        return new Promise(reslove => {
            Vue.prototype.$http.post('/api/Member/GetUserBaseInfo',{
                userID:theRequest.userName,
                connectionID:state.CONNECTION_ID
            }).then(response => {
                const {resultCode,resultData} = response.data
                if(resultCode == 1){
                    commit('setUserInfo',resultData)
                }
                reslove(response.data)
            })
            .catch(()=>{
                /* 捕获到错误 按token失效处理 */
                reslove({resultCode:401})
            })
        })
    },
    /* 用户注销     */
    userLogOut:function(){
        const config = {}
        if(sessionStorage.getItem('userinfo')){
            const userinfo = JSON.parse(sessionStorage.getItem('userinfo'))
            config.userID = userinfo.account || ''
        }
        return new Promise(reslove => {
            Vue.prototype.$http.post('/api/Member/Logout',config).then(()=>{
                reslove()
            })
        })
    },
    /* 初始化业务数据 */
    initialWeb:function({commit,dispatch}){

        Vue.prototype.$helper.log(null,'初始化应用数据')

        /* 请求运营商（网站基本）数据 */
        Vue.prototype.$http.post('api/BaseInfo/GetSiteOperator').then(response => {
            const {resultCode,resultData} = response.data
            /* 请求成功 */
            if(resultCode == 1){
                commit('setWebBasicInfo',resultData)
                /* 从运营商数据中获取socket地址 */
                const {signalUrl} = resultData
                if(signalUrl && !Vue.prototype.$oddsSocket){
                    /* 初始化socket */
                    dispatch('initialWebsocket',signalUrl)
                }
            }
        })

        /* 拉取竞猜游戏列表 */
        dispatch('getEsportsGameList')
        /* 拉取优惠活动列表 */
        dispatch('getPromotionList')
        /* 拉取资讯种类 */
        dispatch('getNewsSortList')
        /* 拉取资讯专题 */
        dispatch('getNewsSpecialList')
    },
    /* 初始化socket */
    initialWebsocket:function({dispatch,commit,state},signalUrl){
        /* ########    投注变化    ########## */
        const connection = new window.signalR.HubConnectionBuilder().withUrl(signalUrl).build()
        Vue.prototype.$oddsSocket = connection
        /* 监听赔率变化 */
        dispatch('listenOddsChange')
        /* 监听赛事变化 */
        dispatch('listenMatchChange')
        /* 监听玩法变化 */
        dispatch('listenPlayChangedList')
        /* 监听投注结果变化 */
        dispatch('listenBetChangedList')
        /* 监听在线人数变化 */
        dispatch('listenOnlineCount')
        /* 监听登录状态变更 */
        dispatch('listenKickedUserList')

        /* 初始化socket链接 */
        const connectAction = () => {
            /* 状态提示 */
            connection.start().then(() => {
                Vue.prototype.$helper.log(null,'socket 链接完成',2)
                const Timestamp = new dayjs().unix()
                const Random = 'bbb' + Math.random()
                const Key = 'd4rHhEcEyYiPng3o47IFiyrfjpkSI3NpuzPH84SfuAA7fNiAqny2e6B0uCqy6OASklYPAQ68J1lTqL3WTSf2gSIknbDnzGYvGeTO4ApiajY0tgnPJ3D1HIq6aAPK3v07'
                const Signature = md5(`{}&${Timestamp}&${Random}&${Key}`)

                /* 验证socket签名 */
                connection.invoke('VerifyConnection',Timestamp,Random,Signature).then(response => {
                    const {resultData} = response
                    /* 设置ConnectionID */
                    if(resultData)  commit('setUserConnectionID',resultData)
                    if(state.USERINFO){
                        /* 重新拉取用户信息（通知服务端ConnectionID） */
                        dispatch('refreshUserInfo')
                    }
                })

                /* 拉取在线人数 */
                connection.invoke('GetOnlineCount')

            }).catch(error => {
                Vue.prototype.$helper.log(error,'socket 断线  5秒后重新链接',2)
                setTimeout(connectAction,5000)
            })

            /* 断线重连 */
            connection.connectionClosed = () => {
                setTimeout(connectAction,5000)
            }
        }
        connectAction()
    },
    /* (socket)监听赔率变化 */
    listenOddsChange:function({dispatch}){
        Vue.prototype.$oddsSocket.on('GetOddsChangedList',socketList => {
            if(socketList != '{}'){
                socketList = JSON.parse(socketList)
                /* 转为map结构并增加到emit参数中 */
                const socketListMap = new Map()
                for(const mid in socketList){
                    const item = socketList[mid]
                    socketListMap.set(mid,item)
                }
                /* Vue.prototype.$helper.log(socketList,'赔率发生了一次变化') */

                /* 分发数据 */
                window.eventBus.$emit('listenOddsList',{socketList,socketListMap})
                dispatch('updateEsportsOddsList',{socketList,socketListMap})
            }
        })
    },
    /* (socket)监听赛事变化 */
    listenMatchChange:function({dispatch}){
        Vue.prototype.$oddsSocket.on('GetMatchChangedList',socketList => {
            if(socketList != '{}'){
                socketList = JSON.parse(socketList)
                /* 转为map结构并增加到emit参数中 */
                const socketListMap = new Map()
                for(const mid in socketList){
                    const item = socketList[mid]
                    socketListMap.set(mid,item)
                }
                /* Vue.prototype.$helper.log(socketList,'赛事发生了一次变化') */

                /* 分发数据 */
                window.eventBus.$emit('listenMatchList',{socketList,socketListMap})
                dispatch('updateEsportsMatchList',{socketList,socketListMap})
            }
        })
    },
    /* (socket)监听玩法变化 */
    listenPlayChangedList:function({dispatch}){
        Vue.prototype.$oddsSocket.on('GetPlayChangedList',socketList => {
            if(socketList != '{}'){
                socketList = JSON.parse(socketList)
                const socketListMap = new Map()
                for(const mid in socketList){
                    const item = socketList[mid]
                    socketListMap.set(mid,item)
                }
                /* Vue.prototype.$helper.log(socketList,'玩法发生了一次变化') */

                /* 分发数据 */
                window.eventBus.$emit('listenPlayList',{socketList,socketListMap})
                dispatch('updateEsportsPlayList',{socketList,socketListMap})
            }
        })
    },
    /* (socket)监听投注结果变化 */
    listenBetChangedList:function(){
        Vue.prototype.$oddsSocket.on('GetBetChangedList',socketList => {
            if(socketList != '{}'){
                socketList = JSON.parse(socketList)
                const socketListMap = new Map()
                for(const mid in socketList){
                    const item = socketList[mid]
                    socketListMap.set(mid,item)
                }
                /* Vue.prototype.$helper.log(socketList,'玩法发生了一次变化') */

                /* 分发数据 */
                window.eventBus.$emit('listenBetChange',{socketList,socketListMap})
            }
        })
    },
    /* (socket)监听在线人数变化 */
    listenOnlineCount:function({commit}){
        Vue.prototype.$oddsSocket.on('GetOnlineCount',result => {
            if(result != '{}'){
                result = JSON.parse(result)
                const {count = 1} = result
                commit('setOnlineCount',count)
            }
        })
    },
    /* (socket)监听登录状态变更 */
    listenKickedUserList:function({state}){
        Vue.prototype.$oddsSocket.on('GetKickedUserList',result => {
            if(result != '[]'){
                result = JSON.parse(result)
                if(Array.isArray(result) && result.length){
                    result.map(userData => {
                        /* 被踢下线 */
                        if(userData.accessToken == state.USER_TOKEN){
                            /* 继续验证操作客户端，userId，前台/后台 */
                            if(userData.userType == 1 && userData.application == 'pc'){
                                Vue.prototype.$message({message:'此用户已在其它地方登录,您已被强制下线',type:'warning'})
                                Vue.prototypeadministrator.$helper.goLogout()
                            }
                        }
                    })
                }
            }
        })
    },
    /* 赔率变化更新到用户投注列表 */
    updateEsportsOddsList:function({getters,commit},socketData){
        const {socketListMap} = socketData
        if(getters.MAP_USER_ODDS_LIST2.size && socketListMap.size){
            getters.MAP_USER_ODDS_LIST2.forEach(item => {
                /* 寻找赛事交集 */
                if(socketListMap.has(item.mid)){
                    /* 得到赛事下赔率集合 */
                    const socketOddsList = socketListMap.get(item.mid)
                    socketOddsList.map(item2 => {
                        /* 寻找与投注列表的交集 */
                        if(getters.MAP_USER_ODDS_LIST.has(item2.edoid)){

                            Vue.prototype.$helper.log(item2.edoid,'投注控制器中：投注状态发生变化')

                            /* state投注实体 */
                            const stateItem = getters.MAP_USER_ODDS_LIST.get(item2.edoid)
                            /* socket更新数据 */
                            const newDetail = item2.newDetail || {}
                            /* 操作对象 */
                            const newObject = {}

                            try{
                                /* 最新赔率 */
                                if(newDetail.odds){
                                    newObject.odds = newDetail.odds
                                }
                                /* 投注允许状态 */
                                newObject.betState = parseInt(newDetail.betState) || 0

                                /* 赔率变化状态 */
                                if(item2.oddsState){
                                    newObject.oddsState = parseInt(item2.oddsState)
                                }
                            }catch{
                            }

                            /* 更新到state列表 */
                            commit('updateUserOddsItem',{
                                arrayIndex:stateItem.arrayIndex,
                                newObject
                            })
                        }
                    })
                }
            })
        }
    },
    /* 玩法变化更新到用户投注列表 */
    updateEsportsPlayList:function({getters,commit},socketData){
        const {socketListMap} = socketData
        if(getters.MAP_USER_ODDS_LIST2.size && socketListMap.size){
            getters.MAP_USER_ODDS_LIST2.forEach(item => {
                /* 寻找赛事交集 */
                if(socketListMap.has(item.mid)){

                    Vue.prototype.$helper.log(item.edoid,'投注控制器中：玩法状态发生变化')

                    /* 得到赛事下玩法集合 */
                    const socketOddsList = socketListMap.get(item.mid)
                    /* 匹配玩法更新 */
                    if(socketOddsList[item.edid]){
                        /* state投注实体 */
                        const stateItem = getters.MAP_USER_ODDS_LIST.get(item.edoid)

                        /* 得到玩法对象 */
                        const playDetail = socketOddsList[item.edid].newDetail || {}
                        const {betState="0",canCombine=true} = playDetail

                        /* 更新到state列表 */
                        commit('updateUserOddsItem',{
                            arrayIndex:stateItem.arrayIndex,
                            newObject:{betState,canCombine}
                        })
                    }
                }
            })
        }
    },
    /* 赛事变化更新到用户投注列表 */
    updateEsportsMatchList:function({state,getters,commit},socketData){
        const {socketListMap} = socketData
        if(getters.MAP_USER_ODDS_LIST2.size && socketListMap.size){
            getters.MAP_USER_ODDS_LIST2.forEach(item => {
                /* 寻找赛事交集 */
                if(socketListMap.has(item.mid)){

                    Vue.prototype.$helper.log(item.mid,'投注控制器中：赛事状态发生变化')
                    const newDetail = socketListMap.get(item.mid).newDetail
                    /* 遍历到投注赛事下的所有投注 */
                    const oddsResult = state.USER_ODDS_LIST.filter(item2 => item.mid == item2.mid)
                    oddsResult.map(item3 => {
                        const newObject = getters.MAP_USER_ODDS_LIST.get(item3.edoid)
                        /* 更新具体选项 */
                        try{
                            /* 投注状态 */
                            newObject.betState = parseInt(newDetail.betState) || 0
                            /* 可串关状态 */
                            if(item3.canCombine){
                                newObject.canCombine = newDetail.canCombine
                            }
                        }catch{
                        }

                        /* 更新到state列表 */
                        commit('updateUserOddsItem',{
                            arrayIndex:newObject.arrayIndex,
                            newObject
                        })

                    })

                }
            })

        }
    },
    /* 拉取图形验证码 */
    getCaptchaInfo:function(){
        return new Promise(reslove => {
            Vue.prototype.$http.post('api/Common/Captcha/GetCaptchaInfo',{
                width: 85,              /* 图片宽度 */
                height: 40,             /* 图片高度 */
                charCount: 4            /* 验证码字符个数 */
            }).then(response => {
                const {resultCode,resultData} = response.data
                if(resultCode == 1){
                    reslove(resultData)
                }
            })
        })
    },
    /* 获取短信验证码 */
    getSmsCode:function(){
        return new Promise(reslove => {
            /* 模拟短信响应 */
            setTimeout(()=>{
                reslove({countdown:60})
            },1000)
        })
    },
    /* 设置短信验证码倒计时 */
    setSmsCodeCountDown:function({commit,state},config){
        /* 多任务防抖 */
        if(state.SMS_COUNT_DOWN)    return false
        /* 倒计时时间 */
        const {countdown} = config
        commit('setSmsCodeCountDown',countdown)
        /* 递归 */
        const countDownAction = () => {
            if(state.SMS_COUNT_DOWN && typeof state.SMS_COUNT_DOWN == 'number'){
                setTimeout(()=>{
                    localStorage.setItem('smsCountDown',state.SMS_COUNT_DOWN - 1)
                    commit('setSmsCodeCountDown',state.SMS_COUNT_DOWN - 1)
                    countDownAction()
                },1000)
            /* 倒计时结束 */
            }else{
                commit('setSmsCodeCountDown',false)
                localStorage.removeItem('smsCountDown')
            }
        }
        countDownAction()
    },
    /* 拉取竞猜游戏列表 */
    getEsportsGameList:function({commit}){
        Vue.prototype.$http.post('api/Bet/GetBaseEgameList').then(response => {
            const {resultCode,resultData} = response.data
            /* 请求成功 */
            if(resultCode == 1){
                const {baseEgameList} = resultData
                commit('setEsportsGameList',baseEgameList)
            }
        })
    },
    /* 拉取优惠活动列表 */
    getPromotionList:function({commit}){
        Vue.prototype.$http.post('api/BaseInfo/GetPPromotionList',{
            state:'',
            pageIndex:0,
            pageSize:0
        }).then(response => {
            const {resultCode,resultData} = response.data
            /* 请求成功 */
            if(resultCode == 1){
                commit('setPromotionList',resultData.pPromotionList || [])
            }
        })
    },
    /* 资讯 拉取资讯类型 */
    getNewsTypeList:function({commit}){
        Vue.prototype.$http.post('api/News/GetNewsTypeList').then(response => {
            const {resultCode,resultData} = response.data
            /* 请求成功 */
            if(resultCode == 1){
                const newsTypeList = resultData.newsTypeList || []
                commit('setNewsTypeList',newsTypeList)
            }
        })
    },
    /* 资讯 拉取资讯种类 */
    getNewsSortList:function({commit}){
        Vue.prototype.$http.post('api/News/GetNewsSortList').then(response => {
            const {resultCode,resultData} = response.data
            /* 请求成功 */
            if(resultCode == 1){
                const newsSortList = resultData.newsSortList || []
                commit('setNewsSortList',newsSortList)
            }
        })
    },
    /* 资讯 拉取资讯专题 */
    getNewsSpecialList:function({commit,getters}){
        const itid = getters.NEWS_TYPE_LIST.get('zt').id
        Vue.prototype.$http.post('api/News/GetNewsContentList',{
            itid,
            sid:'',
            nsid:[],
            keyword:'',
            pageIndex:1,
            pageSize:6
        }).then(response => {
            const {resultCode,resultData} = response.data
            /* 请求成功 */
            if(resultCode == 1){
                const newsContentList = resultData.newsContentList || []
                commit('setNewsSpecialList',newsContentList)
            }
        })
    },
    /* 用户下注行为 */
    setUserOdds:function({commit,getters,state},item){
        /* 取消下注 */
        if(getters.MAP_USER_ODDS_LIST && getters.MAP_USER_ODDS_LIST.has(item.edoid)){
            const esportsForMap = getters.MAP_USER_ODDS_LIST.get(item.edoid)
            const {arrayIndex} = esportsForMap
            state.USER_ODDS_LIST.splice(arrayIndex,1)
        }else{
            /* 下注 */
            commit('pushUserOddsList',item)
            /* 指定控制器tabKey */
            window.eventBus.$emit('setBetControlerView',1)
        }
    },
    /* 提交单关投注 */
    betOdds1:function({state,dispatch},userBetSingleList){
        return new Promise((reslove,reject) => {
            Vue.prototype.$http.post('api/Bet/UserBetSingle',{
                userBetSingleList,
                acceptOddsChange:state.ACCEPT_ODDS_CHANGE
            }).then(response => {
                dispatch('beforeBetOdds',response).then(response => reslove(response))
            }).catch(reject)
        })
    },
    /* 提交串关投注 */
    betOdds2:function({state,dispatch},userBetMultipleExtend){

        /* 检查串关数量 */
        if(userBetMultipleExtend.betType > 8){
            Vue.prototype.$message({message:`串关投注最多投注八关`,type:'error',showClose:true})
            return
        }

        /* 对json进行排序 */
        userBetMultipleExtend = Vue.prototype.$helper.jsonSort(userBetMultipleExtend)
        return new Promise((reslove,reject) => {
            Vue.prototype.$http.post('api/Bet/UserBetMultiple',{
                userBetMultipleExtend,
                acceptOddsChange:state.ACCEPT_ODDS_CHANGE
            }).then(response => {
                dispatch('beforeBetOdds',response).then(response => reslove(response))
            }).catch(reject)
        })
    },
    /* （单关/串关）投注后 处理投注结果 */
    beforeBetOdds:function({getters,commit},response){
        return new Promise(reslove => {
            let {resultCode,resultData} = response.data
            switch(resultCode){
                /* 投注成功 */
                case 1:
                    return reslove(response)
                /* 超出投注最大或最小金额 */
                case 12:
                /* 游戏已停售 */
                case 13:
                /* 比赛已结束或取消 */
                case 14:
                /* 不支持此投注（比赛开/封盘） */
                case 15:
                /* 不支持此投注（赔率状态，可/否投注） */
                case 16:
                /* 比赛不支持串关投注 */
                case 17:
                /* 滚盘不支持串关投注 */
                case 21:
                /* 投注选项有无法投注比赛，请重新确定！ */
                case 22:
                /* 玩法已停售 */
                case 23:
                /* 比赛已经开始 --没有滚盘的比赛投注 */
                case 24:
                    /* 更新投注结果状态 */
                    commit('reUserOddsListBetResult')
                    const failBetList = resultData && resultData.failBetList ? resultData.failBetList || false : false
                    if(failBetList){
                        failBetList.map(item => {
                            if(getters.MAP_USER_ODDS_LIST.has(item.edoid)){
                                const item2 = getters.MAP_USER_ODDS_LIST.get(item.edoid)
                                commit('updateUserOddsItem',{
                                    arrayIndex:item2.arrayIndex,
                                    newObject:{betResult:resultCode}
                                })
                            }
                        })
                    }
                    return reslove(response)
                default:
                    return reslove(response)
            }
        })
    },
    /* 投注后 重新拉取用户信息 、 清零投注数据 */
    afterBetOdds:function({dispatch,commit}){
        return new Promise(reslove => {
            dispatch('refreshUserInfo').then(()=>{
                commit('clearUserOddsList')
                reslove()
            })
        })
    },
}
