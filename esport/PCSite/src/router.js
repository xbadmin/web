import Vue from 'vue'
import Store from './store'
import Router from 'vue-router'
/* 主路由 */
import HomePage from './views/home'
import IndexPage from './views/index'
import RulesPage from './views/rules/index'
import promotionPage from './views/promotion'
import AppDownPage from './views/appDown'
import Agent from './views/Agent/index'
import logOutPage from './views/logout'
import DefendPage from './views/defend'

/* 个人中心 */
import UsPage from './views/usPage'
import UsIndexPage from './views/us/index'
import UsDepositPage from './views/us/deposit'
import UsWithdrawPage from './views/us/withdraw'
import UsRecordPage from './views/us/record'
import UsOrderPage from './views/us/order'
import UsRecommendPage from './views/us/recommend'
import UsMessagePage from './views/us/message'
/* 个人中心 - 代理商中心 */
import UsAgentPage from './views/us/agent'
import UsAgentIndexPage from './components/us/agent/index'
import UsAgentMemberListPage from './components/us/agent/memberList'
import UsAgentOrderPage from './components/us/agent/order'
import UsAgentBonusList from './components/us/agent/bonusList'
/* 竞猜 */
import EsportsPage from './views/esports/index'
/* 资讯 */
import NewsPage from './views/news/index'
import NewsListPage from './views/news/list'
import NewsArticleDetailPage from './views/news/article/detail'
import NewsPhotoDetailPage from './views/news/photo/detail'
import NewsMovieDetailPage from './views/news/movie/detail'
import NewsZtDetailPage from './views/news/zhuanti/detail'

Vue.use(Router)

const theRouter = new Router({
  routes: [
    /* 主路由 */
    {
      path: '',
      component: HomePage,
      children:[
        /* 首页 */
        {
          path:'index ',
          name:'homeIndex',
          component:IndexPage
        },
        /* 首页（通过推荐码进入） */
        {
          path:'/recommend/:code',
          name:'homeIndexRecommend',
          component:IndexPage
        },
        /* 条款和协议 */
        {
          path:'Rules/:dom?',
          name:'homeRules',
          component:RulesPage,
        },
        /* 竞猜 */
        {
          path:'',
          name:'homeEsports',
          component:EsportsPage,
        },
        /* 竞猜详情 */
        {
          path:'/:mid',
          name:'homeEsportsDetail',
          component:EsportsPage,
        },
        /* 资讯 */
        {
          path:'news',
          component:NewsPage,
          children:[
            /* 资讯 新闻 列表页 默认页 */
            {
              path:'list/:mode?',
              name:'homeNews',
              component:NewsListPage,
            },
            /* 资讯 新闻 详情页 */
            {
              path:'article/:id',
              name:'homeNewsArticleDetail',
              component:NewsArticleDetailPage,
            },
            /* 资讯 图片 详情页 */
            {
              path:'photo/:id',
              name:'homeNewsPhotoDetail',
              component:NewsPhotoDetailPage,
            },
            /* 资讯 视频 详情页 */
            {
              path:'movie/:id',
              name:'homeNewsMovieDetail',
              component:NewsMovieDetailPage,
            },
            /* 资讯 专题 详情页 */
            {
              path:'zhuanti/:id',
              name:'homeNewsZtDetail',
              component:NewsZtDetailPage
            }
          ]
        },
        /* 优惠活动 */
        {
          path:'Promotion/:pid?',
          name:'homePromotion',
          component:promotionPage
        },
        /* App下载 */
        {
          path:'App',
          name:'homeApp',
          component:AppDownPage,
        },
        /* 合营伙伴 */
        {
          path:'Agent',
          name:'AgentPage',
          component:Agent,
        },
      ]
    },
    /* 个人中心 */
    {
      path:'/us',
      component:UsPage,
      beforeEnter:(to,from,next)=>{
        /* 如果用户未登录 */
        if(!Store.state.USERINFO){
          next({name:'homeIndex'})
          Vue.prototype.$helper.goLogin()
        }else{
          next()
        }
      },
      children:[
        /* 首页 */
        {
          path:'',
          name:'usIndex',
          component:UsIndexPage,
        },
        /* 充值/存款 */
        {
          path:'deposit',
          name:'usDeposit',
          component:UsDepositPage,
        },
        /* 提现/提款 */
        {
          path:'withdraw',
          name:'usWithdraw',
          component:UsWithdrawPage,
        },
        /* 交易记录 */
        {
          path:'record/:type',
          name:'usRecord',
          component:UsRecordPage,
        },
        /* 投注记录 */
        {
          path:'order',
          name:'usOrder',
          component:UsOrderPage
        },
        /* 我的推荐 */
        {
          path:'recommend',
          name:'usRecommend',
          component:UsRecommendPage,
        },
        /* 我的消息 */
        {
          path:'message',
          name:'usMessage',
          component:UsMessagePage,
        },
        /* 代理商中心 */
        {
          path:'agent',
          component:UsAgentPage,
          beforeEnter:(to,from,next)=>{
            /* 判断用户是否为代理商 */
            const isUion = Store.state.USERINFO.isUion || false
            isUion ? next() : next({name:'usIndex'})
          },
          children:[
            /* 开下级会员 */
            {
              path:'',
              name:'usAgentIndex',
              component:UsAgentIndexPage,
            },
            /* 会员中心 */
            {
              path:'memberlist',
              name:'usAgentMemberList',
              component:UsAgentMemberListPage,
            },
            /* 投注记录 */
            {
              path:'order',
              name:'usAgentOrder',
              component:UsAgentOrderPage,
            },
            /* 佣金列表 */
            {
              path:'unionBonusList',
              name:'usUnionBonusList',
              component:UsAgentBonusList,
            },
          ]
        },
      ]
    },
    /* 注销 */
    {
      path:'/logout/:remind?',
      name:'logout',
      component:logOutPage,
    },
    /* 站点维护中 */
    {
      path:'/Defend',
      name:'defend',
      component:DefendPage,
    }
  ]
})

/* 全局路由守卫 */
theRouter.beforeEach((to,from,next) => {

  /* 站点维护中 */
  if(to.name != 'defend' && Store.getters.SITE_STATE === 2){
    next({name:'defend'})
    return 
  }

  /* 豁免守卫路由： */
  if(to.name == 'logout'){
    next()
  }else{
    /* 身份验证 */
    if(!Store.state.USERINFO){

      let userinfo = null
      let loading = null
  
      /* 验证用户登录状态 ，用户刷新时重新将用户信息挂载至store */
      const setStore = user => {
        loading = Vue.prototype.$loading.service(({lock: true,background: 'rgba(0, 0, 0, 0.7)'}))
  
        /* 设置到store */
        Store.commit('setUserBasicInfo',user)
        /* 刷新一次用户信息 */
        Store.dispatch('refreshUserInfo').then(result => {
          loading.close()

          const {resultCode} = result
          /* token正常 */
          if(resultCode == 1){
            next()
          /* token异常失效 */
          }else if(resultCode == 401){
            next(false)
          }else{
            /* 身份信息已失效 进行注销 */
            next({name:'logout'})
          }

        })
      }
  
      /* 长效缓存（已经退出页面） */
      if(localStorage.getItem('userinfo')){
        userinfo = localStorage.getItem('userinfo')
        sessionStorage.setItem('userinfo',userinfo)
        setStore(JSON.parse(userinfo))
      /* 短期缓存（未退出刷新） */
      }else if(sessionStorage.getItem('userinfo')){
        userinfo = sessionStorage.getItem('userinfo')
        setStore(JSON.parse(userinfo))
      }else{
        next()
      }
      
    }else{
      next()
    }

  }

})

export default theRouter