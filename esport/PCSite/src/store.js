import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './store/mutations'
import actions from './store/actions'
import getters from './store/getters'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    USER_ACCOUNT:false,                     /* 用户个人用户名 */
    USER_TOKEN:false,                       /* 用户个人token */
    CONNECTION_ID:false,                    /* 用户个人CONNECTION ID */
    USERINFO:false,                         /* 用户个人信息 */
    WEB_INFO:false,                         /* 网站基本信息 */
    ONLINE_COUNT:1,                         /* 在线人数 */
    PROMOTION_LIST:false,                   /* 优惠活动列表 */
    IA_COMPONENT:false,                     /* 交互层名称 */
    SMS_COUNT_DOWN:false,                   /* 短信验证码倒计时 */
    RECOMMEND_CODE:false,                   /* (可能出现的、通过链接获得的他人)邀请码 */
    ESPORTS_PULL_HISTORY:new Set(),         /* 记录更新过投注详情列表的赛事id列表 */
    ESPORTS_SORT_ID:1,                      /* 竞猜游戏列表  时间类型    1今日    2滚盘   3赛前   4赛果 */
    ESPORTS_GAME_ID:'',                     /* 竞猜游戏筛选类型（游戏id）   当前游戏分类id   默认 '' = 全部 */
    ESPORTS_GAME_LIST:false,                /* 竞猜游戏列表 */
    ESPORTS_MATCH_LIST:new Map(),           /* 竞猜赛事列表 */
    ESPORTS_ODDS_MODE:1,                    /* 投注模式  1：单关 2：串关 */
    USER_ODDS_LIST:[],                      /* 玩家投注列表（单关） */
    USER_ODDS_LIST2:{                       /* 玩家投注（串关）(串关的数据列表并不在这里而是在 getters 中 根据 state.USER_ODDS_LIST 派生，这里的数据只是结合getters.USER_ODDS_LIST2 在组件内做逻辑实现。如串关投注金额字段) */
      money:'',
    },
    ACCEPT_ODDS_CHANGE:true,                /* 接收赔率变化 */
    NEWS_SORT_LIST:[],                      /* 资讯  资讯种类 */
    NEWS_SPECIAL_LIST:[],                   /* 资讯  资讯专题 */
  },
  mutations,
  actions,
  getters,
})
