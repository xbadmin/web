import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueVideoPlayer from './views/esports/video-player'

// require videojs style
import 'video.js/dist/video-js.css'
import './views/esports/video-player/custom-theme.css'

/* store 初始状态 */
Vue.prototype._protoStoreState = JSON.stringify(store.state)
Vue.use(VueVideoPlayer, /* {
  options: global default options,
  events: global videojs events
} */)
/* http请求工具 */
import '@/include/http'
/* animate.css */
import 'animate.css'
/* 全局样式 */
import '@/scss/app.scss';
/* 插件 */
import '@/include/plugin'

/* 辅助函数 */
import $helper from '@/include/helper'
Vue.prototype.$helper = $helper

/* eventBus */
window.eventBus = new Vue()

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
