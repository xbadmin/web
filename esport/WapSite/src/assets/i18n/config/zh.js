const zh = {
  label : {
    now: '今日',
    match:'滚盘',
    before:'赛前',
    result:'赛果',
    filter:'筛选',
  },
  gameFilter:'游戏筛选',
  all:'全部',
  highLoad:'上拉加载',
  downRenovate:'下拉刷新',
  Release:'释放后刷新数据',
  deleteAll:'清空全部',
  matchStatus:{
    noStart:'未开始',
    beInMatch:'即将滚盘',
    inLive:'直播中',
    inMatch:'滚盘中',
  },
  gateType:{
    alone:'单关',
    bunch:'串关',
  },
  inputMoney:'请输入金额',
  oddsChange:{
    yes:'接受赔率变化',
    no:'不接受赔率变化',
  },
  maxOrder:'最大投注',
  type:'类型',
  allOdds:'总赔率',
  selectOrder:'已选注单',
  oddsOrOrderChange:'您所选注单的盘口、赔率或有效性已产生变化',
  money:{
    win:'可赢金额',
    max:'最高可赢',
    out:'下注金额',
  },
  // 提示
  popup:{
    bunchMax8:'串关投注最多选择8个投注项目',
    bunchMin2:'串关投注最少选择2个投注项目',
    lowestPut:'最低投注',
    putSuccess:'投注成功',
  },
  // 按钮
  button:{
    confirm:'确定',
    develop:'展开',
    fold:'折叠',
    nowPut:'立即下注',
    confirmPut:'确认下注',
  },
  // 个人中心
  personal:{
    title:'个人中心',
    balance:'余额',
    giftBalance:'彩金',
    freeze:'冻结款',
    BettingRules:'投注规则',
    BettingRecord:'投注记录',
    message:'消息',
    languageType:'中文版'
  },
  // 消息
  message:{
    title:'消息',
  },
  // 投注记录
  BettingRecord:{
    title:'投注记录',
    Unliquidated:'未结算',
    Closed:'已结算',
    GoPlay:'去玩两把',
  },
  // 投注规则
  BettingRules:{
    title:'投注规则',
    oneTheTitle:'投注',
    oneTheContentOne:'您必须仅将服务用于合法的博彩目的，不得也不可试图恶意或以对服务或我们的公正产生不利影响的方式操纵任何市场或任何服务环节。',
    oneTheContentTwo:'我们有权要求客户提供身份证明（护照复印件、驾驶执照或国家身份证）以及存款证明。我们保留在收到所需信息之前对任何账户暂停下注或限制账户选项的权利。',
    oneTheContentThree:'您知道并接受所有赔率及盘口都是不断变化的，只有在投注瞬间才会确定。',
    oneTheContentFour:'保留取消任何投注的全部或部分的单方面绝对酌情权。所有投注的风险均由您自行承担。 保留在任何时间取消任何投注的权利。',
    oneTheContentFive:'您须对所有经由您账户所产生的交易和行为承担责任，无论该交易是否由您本人作出。',
    oneTheContentSix:'您有责任确保投注准确无误。一旦完成投注，不得将其取消。',
    oneTheContentSeven:'一旦注单被确认，该注单不得取消，撤回或更改，并且该注单会视为您投注的有效证据。',
    oneTheContentEight:'当下列情况发生时，保留扣留资金及宣布某个赛事投注无效的权利：',
    oneTheContentNine:'保留在任何时间暂停盘口和/或取消任何投注的权利。一旦盘口被暂停，系统将拒绝接收该盘口的所有投注。保留在不提前通知客户的情况下，停止任何一场比赛开盘的权利。',
    oneTheContentTen:'网站上提供的“最高可返”计算仅供参考，且所有投注均将依据所接受的赔率和投注额结算。如果复式投注中有选项被取消，则“返还”数额将相应减少。',
    oneTheContentEleven:'如有足够证据证明包含相同选项的一连串投注为同一个或同一组人所进行的，有权根据后序调查结果来取消投注或扣留返还资金。',
    oneTheContentTwelve:'如有 异常投注，保留以下权利，无需事先通知。',
    oneTheContentThirteen:'拒绝或暂停任何被疑存有诈骗行为，或攻击、操纵网站及破坏正常投注运作的用户（包括网站）。任何“异常投注”将在不事先通知的情况下被作废。网站严禁使用人工智能软体或“机械软体”。将采取相关的防范与侦测措施。对于任何意图或利用人工智能来进行欺骗、攻击、操纵或破坏投注运作活动的用户，{{WEB_NAME}}有权暂停，冻结 其户口或取消其投注。',
    oneTheContentFourteen:'如本公司工作人员在受注时出错（如投注单有明显打印错误、赛事赔率和投注赔率不匹配等），或在受注时有证据表明存在不公平、违反现行规则的情况，或有其他证据表明投注不正确，本博彩公司保留宣布此等投注无效的权利。此时投注派彩按赔率为“1”的情况实施。',
    oneTheContentFifteen:'如果电子竞技游戏出现技术故障、不完整的广播等情况，只有投注在未发生/未计算的赛事以等于“1”的赔率结算。',
    oneTheContentSixteen:'如果比赛出现不符合体育道德的行为，本公司保留权力在国际组织定论之前限制体育赛事的投注，若确定为非体育道德行为的比赛，注单视为无效。这些投注按赔率“1”计算。管理部门无需向客户提供证据及结论。',
    oneTheContentSeventeen :'接受客户投注时，互联网连接失败不构成取消投注的理由。',
    oneTheContentEighteen:'所有的投注记录将被存入交易数据库，并作为任何时间任何交易的有效依据。',
    oneTheContentNineteen:'所有在账户交易记录显示确认信息的注单，即视为有效注单。',
    oneTheContentTwenty:'如客户对本博彩公司存在欺诈行为（在网站注册多个账号，使用软件程序自动投注，游戏出现仲裁情况，若游戏账户并非用来投注，且滥用忠实客户项目）本博彩公司保留采用以下方式终止此等欺诈行为的权利：取消投注；关闭客户的博彩帐户并退回已存款资金。',
    oneTheContentTwenty_one :'保留限制特殊比赛项目的最高投注金额，以及在不作出任何通知和说明理由的情况下限制或增加某个客户最大赔率的权利。',
    oneTheContentTwenty_Two :'所有已结算得注单于72小时后不得更改，且在该时间段后不再接受任何投诉。公司仅可在赛果公布后72小时内，重置/更正因人为失误、系统错误或因参考赛果来源而造成错误的赛果。',
  },
}
export default zh;
