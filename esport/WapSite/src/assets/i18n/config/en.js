const en = {
  // 导航菜单
  label : {
    now: 'now',
    match:'in match',
    before:'before',
    result:'result',
    filter:'filter',
  },
  gameFilter:'gameFilter',
  all:'all',
  highLoad:'high pull load',
  downRenovate:'drop down renovate',
  Release:'Release renovate data',
  deleteAll:'delete all',
  matchStatus:{
    noStart:'no start',
    beInMatch:'be in match',
    inLive:'in live',
    inMatch:'in match',
  },
  gateType:{
    alone:'alone',
    bunch:'bunch',
  },
  inputMoney:'Please enter the amount',
  oddsChange:{
    yes:'Change in acceptance odds',
    no:'no Change in acceptance odds',
  },
  maxOrder:'max order',
  type:'type',
  allOdds:'all odds',
  selectOrder:'select order',
  oddsOrOrderChange:'odds or validity of the note you selected have changed',
  money:{
    win:'Win money',
    max:'max win',
    put:'put money',
  },
  // 提示
  popup:{
    bunchMax8:'Select up to 8 betting items',
    bunchMin2:'Select down to 2 betting items',
    lowestPut:' lowest put the ',
    putSuccess:'put Success!',
  },
  // 按钮
  button:{
    confirm:'confirm',
    develop:'develop',
    fold:'fold',
    nowPut:'now put',
    confirmPut:'confirm put',
  },
  // 个人中心
  personal:{
    title:'personal centrality',
    balance:'balance',
    giftBalance:'giftBalance',
    freeze:'freeze',
    BettingRules:'Betting rules',
    BettingRecord:'Betting record',
    message:'message',
    languageType:'english'
  },
  // 消息
  message:{
    title:'message',
  },
  // 投注记录
  BettingRecord:{
    title:'Betting record',
    Unliquidated:'Unliquidated',
    Closed:'Closed',
    GoPlay:'Go play',
  },
  // 投注规则
  BettingRules:{
    title:'Betting rules',
    oneTheTitle:'Betting',
    oneTheContentOne:'You must only use the service for legitimate gambling purposes and must not and must not attempt to manipulate any market or any service link maliciously or in a manner that adversely affects the service or our impartiality.',
    oneTheContentTwo:' We have the right to require the customer to provide identification (copy of passport, driver\'s license or national identity card) and certificate of deposit. We reserve the right to suspend bets or limit account options on any account before receiving the required information.',
    oneTheContentThree:' You know and accept that all odds and outlets are constantly changing and will only be determined at the moment of betting.',
    oneTheContentFour:'reserves the unilateral absolute discretion to cancel all or part of any bet. All bets are at your own risk. reserves the right to cancel any bet at any time.',
    oneTheContentFive:' You are liable for all transactions and actions arising from your account, whether or not the transaction is made by you.',
    oneTheContentSix:' It is your responsibility to ensure that the bets are accurate. Once the bet is completed, it shall not be cancelled.',
    oneTheContentSeven:' Once the note is confirmed, the note shall not be cancelled, withdrawn or changed, and the note shall be regarded as valid evidence of your bet.',
    oneTheContentEight:' The right to withhold funds and declare an event\'s bet null and void is reserved when:',
    oneTheContentNine:'reserves the right to suspend the opening and/or cancel any betting at any time. Once the disk port is suspended, the system will refuse to receive all bets on the disk port. reserves the right to stop any game opening without prior notice to the customer.',
    oneTheContentTen:'The "maximum return" calculation provided on the website is for reference only and all bets will be settled on the basis of the accepted odds and bets. if an option in the duplex bet is cancelled, the "returned" amount will be reduced accordingly.',
    oneTheContentEleven:' If there is sufficient evidence that a series of bets containing the same option is carried out by the same or the same group of people, the right to cancel the bet or withhold the return of funds is based on the results of the subsequent investigation.',
    oneTheContentTwelve:' In case of abnormal betting, reserve the following rights without prior notice.',
    oneTheContentThirteen:'Reject or suspend any users suspected of fraud, or attack, manipulate, and disrupt normal betting operations (including websites). Any "abnormal betting" will be invalidated without prior notice. The website forbids the use of artificial intelligence software or "mechanical software ". Relevant preventive and detection measures will be taken. For any user who intends or uses artificial intelligence to cheat, attack, manipulate, or disrupt betting operations , has the right to suspend, freeze accounts or cancel their bets.',
    oneTheContentFourteen:' The betting company reserves the right to declare such bets null and void if the staff member of the company makes an error in the bet (e.g. there is a clear print error in the bet sheet, the match odds and the bet odds do not match, etc.), or if there is evidence at the time of the bet that there is an injustice, a violation of the existing rules, or other evidence that the bet is incorrect. At this time betting lottery according to the odds of "1" implementation.',
    oneTheContentFifteen:'If e-sports games have technical failures, incomplete broadcasting, etc., only bets are settled at an unoccurring / uncounted event equal to "1" odds.',
    oneTheContentSixteen:'If the competition does not conform to sports ethics, the company reserves the right to limit the betting of sports events before the conclusion of international organizations. If the competition is determined to be non-sports moral behavior, the note is deemed invalid. These bets are calculated at odds 1. Management does not need to provide evidence and conclusions to customers.',
    oneTheContentSeventeen :' When accepting customer bets, failure of Internet connection does not constitute a reason to cancel bets.',
    oneTheContentEighteen:' All betting records will be deposited in the transaction database and used as a valid basis for any transaction at any time.',
    oneTheContentNineteen:' All notes showing confirmation information in the account transaction record shall be regarded as valid notes.',
    oneTheContentTwenty:' If the customer has fraud against the gambling company (registration of multiple accounts on the website, automatic betting using software programs, arbitration of the game, if the game account is not used for betting, And abuse of loyal customer projects) the betting company reserves the right to terminate such fraud by canceling bets, closing the client\'s betting account and returning the deposited funds.',
    oneTheContentTwenty_one :' reserves the right to limit the maximum bet amount for special events and to limit or increase a customer\'s maximum odds without any notice and justification.',
    oneTheContentTwenty_Two :' All settled notes shall not be changed after 72 hours and no complaints will be accepted after that period. The company can only reset / correct the result of human error, system error or reference result within 72 hours after the announcement.',
  },
}
export default en;
