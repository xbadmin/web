//对state通常会做的映射
export const userInfo = state => state.userInfo
export const oddsChange = state => state.oddsChange
export const gameChange = state => state.gameChange
export const betmodule = state => state.betmodule
export const playChange = state => state.playChange
export const gameTitle = state => state.gameTitle
export const bettingModuleL = state => state.bettingModuleL
export const newsSearch = state => state.newsSearch
export const sortListCount = state => state.sortListCount
export const gameSortList = state => state.gameSortList
export const agentInfo = state => state.agentInfo