import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/index")
const indexD = ()=>import("@/components/tabs/index/index_default")
const login = ()=>import("@/components/login")
const register = ()=>import("@/components/register")

//
const interact = ()=>import("@/components/interact")
const personal = ()=>import("@/components/personal")
const personalD = ()=>import("@/components/tabs/personal/personal_default")
const personalW = ()=>import("@/components/tabs/personal/personal_white")
const withdrawDeposit = ()=>import("@/components/center/withdrawDeposit")
const bindCard = ()=>import("@/components/center/bindCard")
const lottery_record = ()=>import('@/components/center/lottery_record')
const website_message = ()=>import('@/components/center/website_message')
const account_details = ()=>import('@/components/center/account_details')
const upd_password = ()=>import('@/components/center/upd_password')
const usdt_address = ()=>import('@/components/center/usdt_address')
const mnyrecord = ()=>import('@/components/center/mnyrecord')
const pay_select_way = ()=>import("@/components/center/pay_select_way")
const service = ()=>import("@/components/service")
const active = ()=>import("@/components/active")
const serviceCode = ()=>import("@/components/serviceCode")
const set = ()=>import("@/components/setting")
const address = ()=>import("@/components/address")
const lobby = ()=>import("@/components/lobby")
const throwinto= ()=>import("@/components/lobby/index")

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: false,requireAuth:false},}, // 首页
        {path: '/indexD', name:'indexD', component:indexD, meta: {keepAlive: false,requireAuth:false},}, // 默认首页

        {path: '/login', name:'login', component:login, meta: {keepAlive: false,requireAuth:false},}, // 登录页
        {path: '/register', name:'register', component:register, meta: {keepAlive: false,requireAuth:false},}, // 注册页
        {path: '/interact', name:'interact', component:interact, meta: {keepAlive: true,requireAuth:false},}, // 互动中心

        {path: '/active', name:'active', component:active, meta: {keepAlive: true,requireAuth:false},}, // 优惠活动
        {path: '/service', name:'service', component:service, meta: {keepAlive: true,requireAuth:false},}, // 在线客服
        {path: '/serviceCode', name:'serviceCode', component:serviceCode, meta: {keepAlive: true,requireAuth:false},}, // 客服二维码

        // 个人中心
        {path: '/personal', name:'personal', component:personal, meta: {keepAlive: false,requireAuth:true},}, // 个人中心
        {path: '/personalD', name:'personalD', component:personalD, meta: {keepAlive: false,requireAuth:true},}, // 个人中心
        {path: '/personalW', name:'personalW', component:personalW, meta: {keepAlive: false,requireAuth:true},}, // 个人中心
        {path: '/bindCard', name:'bindCard', component:bindCard, meta: {keepAlive: false,requireAuth:true},}, // 银行卡信息
        {path: '/withdrawDeposit',name:'withdrawDeposit',component:withdrawDeposit, meta: {keepAlive: false,requireAuth:true},},//提款
        {path:'/lottery_record',name:'lottery_record',component:lottery_record,meta: {keepAlive: false,requireAuth:true},},//彩票
        {path:'/website_message',name:'website_message',component:website_message,meta: {keepAlive: false,requireAuth:true},},//我的消息
        {path:'/account_details',name:'account_details',component:account_details,meta: {keepAlive: false,requireAuth:true},},//账户明细
        {path:'/upd_password',name:'upd_password',component:upd_password,meta: {keepAlive: false,requireAuth:true},},//修改提款，账户密码
        {path: '/mnyrecord',name:'mnyrecord',component:mnyrecord,meta: {keepAlive: false,requireAuth:true},},//用户账变记录
        {path: '/pay_select_way',name:'pay_select_way',component:pay_select_way, meta: {keepAlive: false,requireAuth:true},},//充值
        {path: '/usdt_address',name:'usdt_address',component:usdt_address, meta: {keepAlive: false,requireAuth:true},},//充值


        //设置
        {path:'/set',name:'set',component:set,meta:{keepAlive: false,requireAuth:false}},
        {path:'/address',name:'address',component:address,meta:{keepAlive: false,requireAuth:false}},
        {path:'/lobby',name:'lobby',component:lobby,meta:{keepAlive: false,requireAuth:false}},
        // 彩票页面
        {path:'/throwinto',name:'throwinto',component:throwinto,meta:{keepAlive: false,requireAuth:true}},
    ]
})

