import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/index")
const login = ()=>import("@/components/login")
const register = ()=>import("@/components/register")
const personal = ()=>import("@/components/personal")
const draw_notice = ()=>import("@/components/draw_notice")
const draw_notice_details = ()=>import("@/components/draw_notice_details")
const help = ()=>import("@/components/help")
const service = ()=>import("@/components/service")
const appDownload = ()=>import("@/components/appDownload")

const lottery = ()=>import("@/components/lottery/lottery")

const lottery_record = ()=>import("@/components/center/lottery_record")
const website_message = ()=>import("@/components/center/website_message")
const upd_password = ()=>import("@/components/center/upd_password")
const mnyrecord = ()=>import("@/components/center/mnyrecord")
const czForm = ()=>import("@/components/center/czForm")
const withdrawDeposit = ()=>import("@/components/center/withdrawDeposit")
const bindCard = ()=>import("@/components/center/bindCard")
const moneyShift = ()=>import("@/components/center/moneyShift")
const language = ()=>import("@/components/center/language")
const share = ()=>import("@/components/center/share")
const team = ()=>import("@/components/center/team")
const teamIndex = ()=>import("@/components/center/teamIndex")
const czUrl = ()=>import("@/components/center/czUrl")
const usdt_address = ()=>import("@/components/center/usdt_address")
const czQrcode = ()=>import("@/components/center/czQrcode")

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: false,requireAuth:false},}, // 首页
        {path: '/login', name:'login', component:login, meta: {keepAlive: false,requireAuth:false},}, // 登录页
        {path: '/register', name:'register', component:register, meta: {keepAlive: false,requireAuth:false},}, // 注册页
        {path: '/draw_notice', name:'draw_notice', component:draw_notice, meta: {keepAlive: false,requireAuth:false},}, // 开奖公告
        {path: '/draw_notice_details', name:'draw_notice_details', component:draw_notice_details, meta: {keepAlive: false,requireAuth:false},}, // 开奖详情
        {path: '/service', name:'service', component:service, meta: {keepAlive: true,requireAuth:false},}, // 在线客服
        {path: '/appDownload', name:'appDownload', component:appDownload, meta: {keepAlive: true,requireAuth:false},}, // APP下载
        {path: '/help', name:'help', component:help, meta: {keepAlive: true,requireAuth:false},}, // 帮助

        //彩种
        {path: '/lottery', name:'lottery', component:lottery, meta: {keepAlive: true,requireAuth:true},}, // 彩种

        // 个人中心
        {path: '/personal', name:'personal', component:personal, meta: {keepAlive: false,requireAuth:true},}, // 个人中心
        {path:'/lottery_record',name:'lottery_record',component:lottery_record,meta: {keepAlive: false,requireAuth:true},},//彩票
        {path:'/website_message',name:'website_message',component:website_message,meta: {keepAlive: false,requireAuth:true},},//我的消息
        {path:'/upd_password',name:'upd_password',component:upd_password,meta: {keepAlive: false,requireAuth:true},},//修改密码
        {path:'/mnyrecord',name:'mnyrecord',component:mnyrecord,meta: {keepAlive: false,requireAuth:true},},//账变记录
        {path:'/czForm',name:'czForm',component:czForm,meta: {keepAlive: false,requireAuth:true},},//充值
        {path:'/withdrawDeposit',name:'withdrawDeposit',component:withdrawDeposit,meta: {keepAlive: false,requireAuth:true},},//提现
        {path:'/bindCard',name:'bindCard',component:bindCard,meta: {keepAlive: false,requireAuth:true},},//提现地址
        {path:'/moneyShift',name:'moneyShift',component:moneyShift,meta: {keepAlive: false,requireAuth:true},},//转移
        {path:'/language',name:'language',component:language,meta: {keepAlive: false,requireAuth:false},},//语言设置
        {path:'/share',name:'share',component:share,meta: {keepAlive: false,requireAuth:true},},//分享
        {path:'/team',name:'team',component:team,meta: {keepAlive: false,requireAuth:true},},//团队
        {path:'/teamIndex',name:'teamIndex',component:teamIndex,meta: {keepAlive: false,requireAuth:true},},//团队下级
        {path:'/czUrl',name:'czUrl',component:czUrl,meta: {keepAlive: false,requireAuth:true},},//充值页面
        {path:'/usdt_address',name:'usdt_address',component:usdt_address,meta: {keepAlive: false,requireAuth:true},},//钱包地址
        {path:'/czQrcode',name:'czQrcode',component:czQrcode,meta: {keepAlive: false,requireAuth:true},},//二维码地址
    ]
})

