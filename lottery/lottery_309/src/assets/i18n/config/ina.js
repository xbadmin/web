const ina = {
    language: [
        {label: 'Beralih bahasa berhasil', num: 0},//Language switch successful
        {label: 'Pengaturan bahasa', num: 1},//language settings
    ],
    index: [
        {label: 'Panduan pemula', num: 0},//Beginner's Guide
        {label: 'Rekor taruhan', num: 1},//betting record
        {label: 'Perdagangan maya', num: 2},//The virtual trade
        {label: 'Beralih ke akun demo', num: 3},//have switched to an analog account
        {label: '', num: 4},
    ],
    bindCard: [
        {label: 'Tetapkan alamat penarikan', num: 0},//Set up withdrawal address
        {label: 'Nama', num: 1},//name
        {label: 'Alamat penarikan', num: 2},//withdrawal address
        {label: 'Kata sandi penarikan', num: 3},//withdrawals password
        {label: 'Kirimkan', num: 4},//submit
        {label: 'Untuk mengubah alamat, silakan hubungi layanan pelanggan online', num: 5},//please contact online customer service to change the address
        {label: 'Pengikatan berhasil！', num: 6},//binding successful


        {label: 'Kembali ke rumah', num: 7},//back homepage
        {label: 'Tarik sekarang', num: 8},//immediately withdraw funds
        {label: '', num: 9},
    ],
    cz: [
        {label: 'Isi ulang', num: 0},//recharge
        {label: 'USDT jumlah', num: 1},//USDT amount
        {label:'jenis pembayaran',num:2 },
        {label: '', num: 2},
    ],
    lotteryRecord: [
        {label: 'Bertaruh', num: 0},//bet
        {label: 'Memenangkan lotere', num: 1},//win a prize in a lottery
        {label: 'Laba', num: 2},//profit
        {label: 'Tidak lagi', num: 3},//No more
        {label: 'Warna-warni', num: 4},//lottery types
        {label: 'Jumlah taruhan', num: 5},//betting amount
        {label: 'Yuan', num: 6},//yuan
        {label: 'Nomor', num: 7},//number
        {label: 'Waktu', num: 8},//time
        {label: 'Telah memenangkan', num: 9},//Has the winning
        {label: 'Tidak ditarik', num: 10},//No lottery has been drawn yet
        {label: 'Isu', num: 11},//issue
        {label: 'Belum ada catatan taruhan', num: 12},//No betting record yet

        {label: 'Detail pesanan', num: 13},//order details
        {label: 'Penutup', num: 14},//close
        {label: 'Jumlah order', num: 15},//order number
        {label: 'Akun', num: 16},//account number
        {label: 'Jumlah taruhan tunggal', num: 17},//single bet amount
        {label: 'Waktu taruhan', num: 18},//betting time
        {label: 'Jumlah taruhan', num: 19},//number of bets
        {label: 'Banyak', num: 20},//multiple
        {label: 'Taruhan total', num: 21},//Total bet
        {label: 'Cara bermain', num: 22},//how to play
        {label: 'Bonus', num: 23},//bonus
        {label: 'Nomor pemenang', num: 24},//Winning numbers
        {label: 'Taruhan menengah', num: 25},//Intermediate bet
        {label: 'Status lotere', num: 26},//lottery status
        {label: 'Jumlah kemenangan', num: 27},// winning amount
        {label: 'Laba rugi', num: 28},//profit and loss
        {label: 'Nomor taruhan', num: 29},//Betting number
        {label: '', num: 30},
        {label: '', num: 31},
    ],
    mnyrecord: [
        {label: 'Tipe', num: 0},//type
        {label: 'Jumlah order', num: 1},//Order number
        {label: 'Jumlah sebelum perubahan', num: 2},//Amount before change
        {label: 'Jumlah yang diubah', num: 3},//Changed amount
        {label: 'Jumlah setelah perubahan', num: 4},//Amount after change
        {label: 'Waktu memproses', num: 5},//processing time
        {label: 'Komentar', num: 6},//remark
        {label: 'Belum ada perubahan akun', num: 7},//No account changes yet
        {label: '', num: 8},
    ],
    moneyShift: [
        {label: 'Transfer', num: 0},//transfer
        {label: 'Identitas pengguna', num: 1},//User's identity
        {label: '', num: 2},
    ],
    share: [
        {label: 'Mengundang', num: 0},//invite
        {label: 'Klik untuk menyalin tautan', num: 1},//Click on the copy link
        {
            label: '1、Untuk mempromosikan pasar, SG telah membentuk mekanisme pengembalian komisi tiga tingkat. Dia secara pribadi adalah anggota tim lv1, lv2, lv3. Semua anggota tim lv1 bisa mendapatkan 1% cashback untuk setiap transaksi nyata. Anda bisa mendapatkan cashback 0,5% pada setiap transaksi nyata dari semua anggota tim lv2. Anda bisa mendapatkan cashback 0,3% untuk setiap transaksi nyata dari semua anggota tim lv3.',
            num: 2
        },
        //1.To promote the market, SG has established a three-level commission refund mechanism.He is personally a member of the LV1, LV2, and LV3 teams.All team LV1 members will receive 1% cash back on each genuine transaction.You get 0.5% cash back on every genuine transaction from all members of the team LV2  .You will receive 0.3% cash back on each genuine transaction from all members of the lV3 team.
        {
            label: '2、Mengambil pelanggan ABCD sebagai contoh, pelanggan A mengirimkan tautan undangan langsung ke B, Pelanggan B berhasil mendaftar sebagai anggota lv1, Klien B mengirimkan tautan undangan langsung ke C, Pelanggan B dan Pelanggan C berhasil terdaftar sebagai anggota lv1 B, Anggota lv2 A, Klien C mengirimkan tautan undangan langsung ke D, Klien D berhasil mendaftar, Menjadi anggota lv1 C; lv2 anggota B; Anggota tingkat 3.',
            num: 3
        },
        //2、Take Client ABCD as an example, Client A sends A direct invitation link to B, and Client B successfully registers as an LV1 member of A, Client B sends A direct invitation link to C, and Client C sends A direct invitation link to D, and Client D successfully registers.  Become an LV1 member of C;  Lv2 member of B;  Level 3 member of A.
        {label: '3、Klik tim saya, Klik ikon lv1, lv2, lv3 untuk menelusuri detail anggota tim.', num: 4},
        //3. Click my team and click the ICONS of LV1, LV2 and LV3 to browse the details of team members.
        {
            label: '4、Kegiatan dapat berubah, Pengumuman terbaru pada platform akan berlaku. SG berhak atas interpretasi akhir.',
            num: 5
        },
        //4. If there is any change in the activity, the latest announcement of the platform shall prevail.  SG reserves the right of final interpretation.
        {label: '', num: 6},
    ],
    team: [
        {label: 'Tim', num: 0},//team
        {label: 'Komisi', num: 1},//commission
        {label: 'Nilai', num: 2},//grade
        {label: 'Jumlah komisi', num: 3},//commission amount
        {label: 'Menyegarkan', num: 4},//refresh
        {label: 'Orang tim', num: 5},//team member
        {label: 'Nama Panggilan Pengguna', num: 6},//user nickname
        {label: 'Keseimbangan', num: 7},//balance
        {label: 'Tim tingkat pertama', num: 8},//first level team
        {label: 'Tim sekunder', num: 9},//secondary team
        {label: 'Tim tersier', num: 10},//tertiary team
        {label: '', num: 11},
    ],
    updPass: [
        {label: 'Password lama', num: 0},//old password
        {label: 'Kata sandi baru', num: 1},//new password
        {label: 'Ulangi kata sandi baru', num: 2},//Repeat new password
        {label: 'Ubah kata sandi masuk', num: 3},//Modify login password
        {label: 'Ubah kata sandi penarikan', num: 4},//Modify withdrawal password
        {label: 'Kata sandi lama tidak boleh kosong！', num: 5},//old password cannot be empty
        {label: 'Kata sandi baru tidak boleh kosong！', num: 6},//new password cannot be empty
        {label: 'Ulangi kata sandi tidak boleh kosong', num: 7},//Repeat password cannot be empty
        {label: 'Kata sandi baru yang berulang dan kata sandi baru tidak konsisten！', num: 8},//Repeat the new password and the new password is inconsistent
        {label: 'Penyetelan ulang kata sandi selesai', num: 9},//Password reset complete
        {label: 'Kembali ke rumah', num: 10},//back homepage
        {label: '', num: 11},
    ],
    message: [
        {label: 'Surat stasiun', num: 0},//Station message
        {label: 'Edit', num: 1},//edit
        {label: 'Detail informasi situs', num: 2},//Site information details
        {label: 'Tidak ada informasi untuk saat ini', num: 3},//No information at the moment
        {label: 'Telah membaca', num: 4},//Have read
        {label: 'Pilih Semua', num: 5},//select all
        {label: 'Membatalkan', num: 6},//cancel
        {label: 'Menghapus', num: 7},//delete
        {label: 'Berhasil membaca', num: 8},//Read success
        {label: 'Berhasil dihapus', num: 9},//delete success
        {label: 'Konfirmasi untuk menandai dipilih sebagai telah dibaca？', num: 10},//Confirm to mark checked as read
        {label: 'OK untuk menghapus opsi yang dipilih？', num: 11},//Confirm to delete the selected option
        {label: '', num: 12},
    ],
    withdrawDeposit: [
        {label: 'menarik', num: 0},//withdraw
        {label:'kartu bank',num:1 },
        {label:'mata uang',num:2 },
        {label:'protokol',num:3 },
        {label:'dompet',num:4 },
        {label:'nama sebenarnya',num:5 },
        {label:'Saldo rekening',num:6 },
        {label:'Jumlah penarikan',num:7 },
        {label:'Silakan masukkan kata sandi penarikan',num:8 },
        {label:'',num:9 },
        {label:'',num:10 },
        {label:'',num:11 },
        {label:'',num:12 },

    ],
    notice: [
        {label: 'Pengumuman platform', num: 0},//Platform announcement
        {label: '24 jam tidak lagi muncul', num: 1},//24 hours no longer show
        {label: 'Jangan tampilkan popup ini lagi selama 24 jam', num: 2},//Don't show this popup again within 24 hours
        {label: '', num: 3},
    ],
    footer: [
        {label: 'Rumah', num: 0},//homepage
        {label: 'Catatan lotere', num: 1},//lottery record
        {label: 'Berdagang', num: 2},//deal
        {label: 'Milikku', num: 3},//my
        {label: '', num: 4},
    ],
    lottery: [
        {label: 'Hasil undian', num: 0},//lottery result
        {label: 'Catatan lotere', num: 1},//lottery record
        {label: 'Aturan', num: 2},//rule
        {label: 'Istilah terakhir', num: 3},//last issue
        {label: 'Mengharapkan', num: 4},//issue
        {label: 'Tenggat waktu', num: 5},//cut-off
        {label: 'Keseimbangan', num: 6},//balance
        {label: 'Terpilih', num: 7},//selected
        {label: 'Jumlah', num: 8},//money
        {label: 'Jumlah total', num: 9},//total
        {label: 'Silakan masukkan jumlahnya', num: 10},//Please enter the amount
        {label: 'Membersihkan', num: 11},//clear
        {label: 'Konfirmasi', num: 12},//confirm
        {label: 'Sebagai penutup', num: 13},//In closing
        {label: 'Belum buka', num: 14},//Has not been opened
        {label: 'Ditutup, harap perhatikan nomor edisi saat ini untuk taruhan', num: 15},//Closed, please pay attention to the current issue number for betting
        {label: 'Harap isi jumlah yang benar', num: 16},//Please fill in the correct amount
        {label: 'Jumlah taruhan minimum：0.01', num: 17},//Minimum bet amount
        {label: 'Taruhan berhasil', num: 18},//successful bet
        {label: 'Beralih akun resmi', num: 19},//Switched official account
        {label: '', num: 20},
    ],
    app: [
        {label: 'APP Unduh', num: 0},//APP download
        {
            label: 'Pengingat: Disarankan untuk menggunakan Alipay atau browser untuk memindai kode QR untuk mengunduh',
            num: 1
        },//Reminder: It is recommended to use Alipay or browser to scan the QR code to download
        {label: 'Seluruh hak cipta', num: 2},//all rights reserved
        {label: '', num: 3},
    ],
    drawNotice: [
        {label: 'Hasil undian', num: 0},//lottery result
        {label: 'Undian lotere', num: 1},//lottery draw
        {label: 'Klik untuk memuat lebih banyak', num: 2},//Click to load more
        {label: '', num: 3},
    ],
    help: [
        {label: 'Memandu', num: 0},//guide
        {label: '', num: 1},
    ],
    login: [
        {label: 'Nama belakang', num: 0},//user name
        {label: 'Kata sandi', num: 1},//password
        {label: 'Kode verifikasi', num: 2},//verification code
        {label: 'Ingat kata Sandi', num: 3},//remember password
        {label: 'Gabung', num: 4},//login
        {label: 'Daftar', num: 5},//register
        {label: 'Layanan online', num: 6},//online customer service
        {label: 'Mendarat dengan sukses', num: 7},//login successfully
        {label: 'Akun pengguna', num: 8},//user account
        {label: 'Nama pengguna harus 5-11 huruf atau angka bahasa Inggris', num: 9},//Username should be 5-11 English letters or numbers
        {label: 'Kata sandi masuk', num: 10},//login password
        {label: 'Kata sandinya adalah 6-16 huruf atau angka bahasa Inggris', num: 11},//The password is 6-16 English letters or numbers
        {label: 'Masukkan kata kunci kembali', num: 12},//Repeat the password
        {label: 'Silakan masukkan kata sandi lagi', num: 13},//Please enter your password again
        {label: 'Kedua kata sandi tidak cocok', num: 14},//The two passwords do not match
        {label: 'Silahkan masukkan kode verifikasi', num: 15},//Please enter the verification code
        {label:'Kirim kode verifikasi',num:16 },
        {label:'Kode promosi',num:17 },
        {label: '', num: 16},
    ],
    personal: [
        {label: 'Berhenti', num: 0},//quit
        {label: 'Level saat ini', num: 1},//The current level
        {label: 'Tingkat selanjutnya', num: 2},//next level
        {label: 'Nilai pertumbuhan', num: 3},//growth value
        {label: 'Diperlukan untuk peningkatan', num: 4},//Required for upgrade
        {label: 'Kelompok ku', num: 5},//my team
        {label: 'Catatan perubahan akun', num: 6},//account change record
        {label: 'Modifikasi kata sandi masuk', num: 7},//Login password modification
        {label: 'Penarikan perubahan kata sandi', num: 8},//Withdrawal password change
        {label: 'Kode undangan', num: 9},//Invite code
        {label: 'APP Unduh', num: 10},//APP download
        {label: 'Ganti bahasa', num: 11},//switch the language
        {label: 'Setel kata sandi penarikan', num: 12},//Set withdrawal password
        {label: 'Kata sandi penarikan', num: 13},// withdrawal password
        {label: 'Silakan masukkan kata sandi penarikan', num: 14},//Please enter your withdrawal password
        {label: 'Masuk kembali', num: 15},//enter again
        {label: 'Silakan masukkan kata sandi penarikan lagi', num: 16},//Please enter withdrawal password again
        {label: 'Nilai', num: 17},//grade
        {label: 'Nilai pertumbuhan', num: 18},//growth value
        {label: 'Hadiah promosi (yuan)', num: 19},//Promotion reward (yuan)
        {label: 'Peringatan keselamatan', num: 20},//safety tips
        {label: 'Apakah akan keluar dari akun saat ini', num: 21},//Whether to log out of the current account
        {label: 'Setel kata sandi penarikan', num: 22},//Set withdrawal password
        {label: 'Anda belum mengatur kata sandi penarikan, apakah Anda ingin mengaturnya sekarang？', num: 23},//You have not set a withdrawal password, do you want to set it now?
        {label: 'Tidak', num: 24},//no
        {label: 'Ya', num: 25},//yes
        {label: 'Tetapkan alamat penarikan', num: 26},//Set the withdrawal address
        {label: ' Anda belum mengatur alamat penarikan, apakah Anda ingin mengaturnya sekarang？', num: 27},//You have not set the withdrawal address, do you want to set it now?
        {label: 'Atur berhasil！', num: 28},//set successfully
        {label: 'Lanjutkan pengaturan', num: 29},//continue setting
        {label: 'Penarikan', num: 30},//withdrawal
        {label:'alamat dompet',num:31 },
        {label:'Setel alamat dompet',num:32 },
        {label:'',num:33 },
    ],
    personal_header: [
        {label: 'Kembali', num: 0},//return

        {label: 'Saring', num: 1},//filter
        {label: 'Mulailah', num: 2},//start
        {label: 'Tenggat waktu', num: 3},//deadline
        {label: 'Waktu mulai', num: 4},//start time
        {label: 'Akhir waktu', num: 5},//end time
        {label: 'Negara', num: 6},//status
        {label: 'Warna-warni', num: 7},//lottery type
        {label: 'Klik untuk memilih warna', num: 8},//Click to select lottery type
        {label: 'Status pemesanan', num: 9},//order status
        {label: 'Jenis perubahan akun', num: 10},//Account change type

        {label: 'Semua', num: 11},//all
        {label: 'Jenis rekaman', num: 12},//record type
        {label: 'Tipe transaksi', num: 13},//Transaction Type
        {label: 'Status pemrosesan', num: 14},//Processing status
        {label: 'Semua catatan', num: 15},//all records
        {label: 'Menanyakan', num: 16},//Inquire
        {label: 'Sejarah taruhan lotere', num: 17},//Lottery betting record
        {label: 'Semua warna', num: 18},//All lottery type
        {label: 'Semua status', num: 19},//all status
        {label: 'Isi ulang catatan', num: 20},//recharge record
        {label: 'Semua produk', num: 21},//all products
        {label: 'Produk baru', num: 22},//new product
        {label: 'Barang dagangan acara', num: 23},//activities of goods
        {label: 'Perintah masuk', num: 24},//login prompt
        {label: 'Jenis platform', num: 25},//platform type
        {label: '', num: 26},
    ],
    status: [
        {label: 'Tidak ditarik', num: 0},//Not drawn
        {label: 'Lotere telah ditarik', num: 1},//lottery has been drawn
        {label: 'Tidak menang', num: 2},//did not win
        {label: 'Pembatalan', num: 3},//Cancellation
        {label: 'Pengolahan', num: 4},//Processing
        {label: 'Berhasil diproses', num: 5},//Processed successfully
        {label: 'Pemrosesan gagal', num: 6},//Processing failed

        {label: 'Tidak terbiasa', num: 7},//unaccalimed
        {label: 'Diterima', num: 8},//received
        {label: 'Tidak diproses', num: 9},//not processed
        {label: 'Diproses', num: 10},//processed
        {label: 'Pemrosesan gagal', num: 11},//Processing failed

        {label: 'semua jenis ', num: 12},//all types
        {label: 'Dibatalkan', num: 13},//Cancelled
        {label: 'Kedaluwarsa', num: 14},//expired
        {label: 'Terbalik', num: 15},//Commission returned

        {label: 'Tertunda', num: 16},//pending
        {label: 'Menerima', num: 17},//receive
        {label: 'Diterima dengan sukses', num: 18},//Received successfully
        {label: 'Memenangkan lotere', num: 19},//winning the lottery
        {label: 'Mengikat', num: 20},//Tie
        {label: 'Dibatalkan', num: 21},//Cancelled order
        {label: 'Dikonfirmasi', num: 22},//confirmed
        {label: 'Pembatalan sistem', num: 23},//System cancel
        {label: 'Perlombaan dipotong setengah', num: 24},//The race is die
        {label: 'Pengembalian hadiah berhasil', num: 25},//Reward rollback successful
        {label: 'Pengecualian kembalikan', num: 26},//rollback exception
        {label: 'Menggambar pengecualian', num: 27},//Draw exception
        {label: 'Batalkan taruhan', num: 28},//Cancel bet order
        {label: 'Untuk di konfirmasi', num: 29},//to be confirmed

        {label: 'Keresahan', num: 30},//Unsettlement
        {label: 'Mapan', num: 31},//Settled
        {label: 'Penyelesaian gagal', num: 32},//Settlement failed

        {label: 'Kirimkan', num: 33},//submit
        {label: 'Telah memenangkan', num: 34},//has won
        {label: 'Menunggu undian', num: 35},//waiting for the draw

        {label: '', num: 36},
    ],
    money_type: [
        {label: 'Pembayaran manual', num: 0},//Manual payment

        {label: 'Pengurangan manual', num: 1},//Manual deduction

        {label: 'Penarikan online gagal', num: 2},//Online withdrawal failed
        {label: 'Tarik uang secara online', num: 3},//Withdraw money online
        {label: 'Pembayaran online', num: 4},//Online payments

        {label: 'Setoran cepat', num: 5},//Quick deposit
        {label: 'Setoran umum', num: 6},//general deposit
        {label: 'Tambah uang', num: 7},//Anti-commission plus money
        {label: 'Anti air rollback', num: 8},//Anti-commission rollback
        {label: 'Rabat agen plus uang', num: 9},//Agent rebate plus money
        {label: 'Pengembalian rabat agen', num: 10},//Agent rebate rollback

        {label: 'Rabat agen multi-level plus uang', num: 11},//Multi-level agent rebate plus money
        {label: 'Kembalikan titik pengembalian proxy multi-level', num: 12},//Multi-level proxy return point rollback
        {label: 'Akun agen untuk lebih banyak uang', num: 13},//The agent accounts for the money
        {label: 'Kuota tiga pihak ditransfer ke kuota sistem', num: 14},// three-party quota is transferred to the system quota
        {label: 'Kuota sistem dialihkan ke kuota tripartit', num: 15},// system quota is transferred to the tripartite quota
        {label: 'Transfer kuota di tempat', num: 16},//In-site quota transfer
        {label: 'Transfer kuota keluar stasiun', num: 17},//In-station quota transfer out
        {label: 'Pembatalan Deposit', num: 18},//Deposit Cancellation

        {label: 'Pembatalan Penarikan', num: 19},//Withdrawal Cancellation
        {label: 'Hadiah promosi', num: 20},//promotion gift
        {label: 'Daftar untuk diberikan', num: 21},//Register gift
        {label: 'Bonus setoran', num: 22},//deposit bonus
        {label: 'Judi olahraga', num: 23},//sports betting
        {label: 'Penghargaan Pai Olahraga', num: 24},//Sports Pie Award
        {label: 'Pembatalan Olahraga', num: 25},//Sports Cancellation

        {label: 'Penghargaan pai olahraga kembali lagi', num: 26},//Sports pie awards roll back
        {label: 'Taruhan lotere', num: 27},//lottery betting
        {label: 'Hadiah lotere', num: 28},//Lottery sent award
        {label: 'Pembatalan lotere', num: 29},//lottery cancellation

        {label: ' Pengembalian hadiah lotere', num: 30},//lottery sent award rollback

        {label: 'Berpartisipasi dalam sindikat lotere', num: 31},//Participate in lottery syndicates
        {label: 'Lotrenya penuh', num: 32},//Participate in lottery syndicates is full
        {label: 'Kombinasi lotere tidak valid', num: 33},//lottery combination is invalid

        {label: 'Pembatalan pembelian gabungan lotere', num: 34},//lottery combined buy cancel order
        {label: 'Sindikat lotere', num: 35},//Lottery combined pie prizes
        {label: 'Tenggat Penggabungan Lotere', num: 36},//Lottery combined deadline
        {label: 'Tandai Enam Taruhan', num: 37},//Mark Six Betting
        {label: 'Tandai Enam Pembayaran', num: 38},//Mark Six Payouts

        {label: 'Tandai Enam Pembayaran Kembalikan', num: 39},//Mark Six Payouts Roll Back
        {label: 'Tandai Enam Pembatalan', num: 40},//Mark Six Cancellation
        {label: 'Pemenang acara', num: 41},//Activities of winning
        {label: 'Uang Tunai untuk Poin', num: 42},//Cash redemption points
        {label: 'Tukarkan poin dengan uang tunai', num: 43},//Redeeming points for cash
        {label: 'Transfer tiga arah gagal', num: 44},//Tripartite transfer failure
        {label: 'Lotre tripartit meningkat', num: 45},//Tripartite lottery increase
        {label: 'Pengurangan lotere tripartit', num: 46},//Tripartite lottery reduction
        {label: 'Ambil amplop merah', num: 47},//snatch red envelope
        {label: 'Amplop merah', num: 48},//give red envelopes
        {label: 'Jumlah amplop merah yang dikembalikan', num: 49},//Red envelope amount returned
        {label: 'Pengaya antarmuka sistem', num: 50},//System interface add-money
        {label: 'Pengaya antarmuka sistem', num: 51},//Withdrawal records

        {label: 'Setoran online', num: 52},//online deposit
        {label: 'Setoran umum', num: 53},//general deposit
        {label: 'Setoran mata uang virtual', num: 55},//virtual currency deposit
        {label: '', num: 54},
    ],
    replace: [
        {label: 'menit', num: 0},
        {label: 'Jam', num: 1},
        {label: 'Besar', num: 2},
        {label: 'Kecil', num: 3},
        {label: 'satu', num: 4},
        {label: 'pasangan', num: 5},
        {label: 'Besar satu', num: 6},
        {label: 'Besar pasangan', num: 7},
        {label: 'Kecil satu', num: 8},
        {label: 'Kecil pasangan', num: 9},
        {label: 'Mengintegrasikan', num: 10},
        {label: '', num: 11},
        {label: '', num: 12},
        {label: '', num: 13},
        {label: '', num: 14},
        {label: '', num: 15},
    ],
    tips:[
        {label:'akun tidak ada',num:0 },
        {label:'Silakan masukkan konten',num:1 },
        {label:'Telah dikirim',num:2 },
        {label:'Berhasil dikirim',num:3 },
        {label:'Kode area',num:4 },
        {label:'Please select an area code',num:5 },
        {label:'nomor telepon',num:6 },
        {label:'Silakan masukkan nomor telepon',num:7 },
        {label:'taruhan gagal',num:8 },
        {label:'Login gagal, silakan berkonsultasi dengan layanan pelanggan online untuk detailnya',num:9 },
        {label:'Pendaftaran gagal, silakan berkonsultasi dengan layanan pelanggan online untuk detailnya',num:10 },
        {label:'Jumlah isi ulang tidak boleh kurang dari',num:11 },
        {label:'Jumlah isi ulang tidak boleh lebih dari',num:12 },
        {label:'Terjadi kesalahan saat menyimpan pesanan',num:13 },
        {label:'jenis pembayaran yang salah',num:14 },
        {label:'permintaan ilegal',num:15 },
        {label:'Masukkan karakter ilegal!',num:16 },
        {label:'Isi ulang ditutup',num:17 },
        {label:'Isi ulang gagal, silakan hubungi layanan pelanggan online',num:18 },
        {label:'Kode promosi tidak ada',num:19 },
        {label:'Kesalahan kode verifikasi',num:20 },
        {label:'Pendaftaran gagal, silakan hubungi layanan pelanggan online',num:21 },
        {label:'registrasi berhasil',num:22 },
        {label:'Silahkan hubungi layanan pelanggan online untuk memodifikasi',num:23 },
        {label:'Silakan masukkan persetujuan',num:24 },
        {label:'Silakan masukkan alamat',num:25 },
        {label:'Silakan masukkan kata sandi',num:26 },
        {label:'Penarikan berhasil',num:27 },
        {label:'Jumlah penarikan harus bilangan bulat',num:28 },
        {label:'Kesalahan format jumlah penarikan',num:29 },
        {label:'Jumlah penarikan tidak boleh lebih besar dari jumlah maksimum',num:30 },
        {label:'Jumlah penarikan tidak boleh kurang dari jumlah minimum',num:31 },
        {label:'Tidak ada izin penarikan',num:32 },
        {label:'Hanya dapat ditarik sekali',num:33 },
        {label:'Kata sandi penarikan kosong',num:34 },
        {label:'Ditarik hari ini',num:35 },
        {label:'Kata sandi penarikan kosong',num:36 },
        {label:'Penarikan gagal, silakan hubungi layanan pelanggan online',num:37 },
        {label:'Pengguna belum menetapkan kata sandi penarikan',num:38 },
        {label:'Kata sandi penarikan salah',num:39 },
        {label:'Nama pemegang kartu tidak cocok',num:40 },
        {label:'sudah digunakan! Silakan ganti dan coba lagi!',num:41 },
        {label:'Berisi karakter ilegal',num:42 },
        {label:'Pengikatan gagal, silakan hubungi layanan pelanggan online',num:43 },
        {label:'Pembayaran Online',num:44 },
        {label:'Isi ulang dompet',num:45 },
        {label:'Isi ulang kode QR',num:46 },
        {label:'Isi ulang jumlah',num:47 },
        {label:'Isi ulang terlalu sering, coba lagi nanti',num:48 },
        {label:'Aplikasi penarikan gagal, dan volume taruhan tidak memenuhi standar!',num:49 },
        {label:'Jumlah penarikan maksimum telah tercapai hari ini',num:50 },
        {label:'Waktu penarikan terlampaui',num:51 },
        {label:'',num:52 },
    ],
    tip: [
        {label: '', num: 0},
        {label: '', num: 1},
        {label: '', num: 2},
        {label: '', num: 3},
        {label: '', num: 4},
        {label: '', num: 5},
        {label: '', num: 6},
        {label: '', num: 7},
        {label: '', num: 8},
        {label: '', num: 9},
        {label: '', num: 10},
        {label: '', num: 11},
        {label: '', num: 12},
        {label: '', num: 13},
        {label: '', num: 14},
        {label: '', num: 15},
    ],
}
export default ina;