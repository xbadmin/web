# lottery_v3

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration

### 62uuu310  uuu309
See [Configuration Reference](https://cli.vuejs.org/config/).
