import Vue from 'vue'
import Router from 'vue-router'
const index = ()=>import ('@/components/index')
const index1 = ()=>import ('@/components/index/index1')
const index2 = ()=>import ('@/components/index/index2')
const index3 = ()=>import ('@/components/index/index3')
const index4 = ()=>import ('@/components/index/index4')
const activeDetails = ()=>import ('@/components/index/activeDetails')
const recharge = ()=>import ('@/components/my/recharge')
const tixian = ()=>import ('@/components/my/tixian')
const jiaoyi = ()=>import ('@/components/my/jiaoyi')
const touzhu = ()=>import ('@/components/my/touzhu')
const details = ()=>import ('@/components/my/details')
const information = ()=>import ('@/components/my/information/information')
const bindCard = ()=>import ('@/components/my/bindCard')
const anquan = ()=>import ('@/components/my/anquan')
const daili = ()=>import ('@/components/my/daili')
const integral = ()=>import ('@/components/my/integral')
const dlExplain = ()=>import ('@/components/my/daili/dlExplain')
const teamTj = ()=>import ('@/components/my/daili/teamTj')
const vipAdmin = ()=>import ('@/components/my/daili/vipAdmin')
const xjRegister = ()=>import ('@/components/my/daili/xjRegister')
const edit = ()=>import ('@/components/my/daili/edit')
const yjtc = ()=>import ('@/components/my/daili/yjtc')
const yingkui = ()=>import ('@/components/my/yingkui')
const msg = ()=>import ('@/components/my/msg')
const setLoginPwd = ()=>import ('@/components/my/anquan/setLoginPwd')
const download = ()=>import ('@/components/index/download')
const sycz = ()=>import ('@/components/cz/sycz')
const ssc = ()=>import ('@/components/cz/ssc')
const dpc = ()=>import ('@/components/cz/dpc')
const pk10 = ()=>import ('@/components/cz/pk10')
const syxw = ()=>import ('@/components/cz/syxw')
const xgc = ()=>import ('@/components/cz/xgc')
const ks = ()=>import ('@/components/cz/ks')
const klsf = ()=>import ('@/components/cz/klsf')
const pcdd = ()=>import ('@/components/cz/pcdd')
const moreSsc = ()=>import ('@/components/cz/more/ssc/ssc')
const moreKs = ()=>import ('@/components/cz/more/kuaisan/ssc')
const login = ()=>import ('@/components/login/login')
const letterDetails = ()=>import ('@/components/my/msg/letterDetails')
const noticeDetails = ()=>import ('@/components/my/msg/noticeDetails')
const kefu = ()=>import ('@/components/my/kefu')
const agentRegister = ()=>import ('@/components/login/agentRegister')
const realTouZHu = ()=>import ('@/components/my/realTouZHu')
const helpCentre = ()=>import ('@/components/my/helpCentre')
const realGame = ()=>import ('@/components/my/realGame')
const games = ()=>import ('@/components/index/games')
const xbSport = ()=>import ('@/components/sport/xbSport')
const match = ()=>import ('@/components/sport/match')
const chat = ()=>import ('@/components/index/chat/chat')
const userInfo = ()=>import ('@/components/index/chat/userInfo')
const privateChat = ()=>import ('@/components/index/chat/privateChat')
const adminList = ()=>import ('@/components/index/chat/adminList')
const basketBall = ()=>import ('@/components/sport/basketBall')
const allTouzhu = ()=>import ('@/components/my/allTouzhu')
const sanfangLottery = ()=>import ('@/components/my/sanLotteryTouZhu')
const sportTouzhu = ()=>import ('@/components/my/sportTouzhu')
const sportDetail = ()=>import ('@/components/my/betting_record_sport')
const chessTouzhu = ()=>import ('@/components/my/chessTouZhu')
const esportTouZhuDetails = ()=>import ('@/components/my/esportTouZhuDetails')
const esportTouZhu = ()=> import('@/components/my/esportTouZhu')
const egameTouzhu = ()=>import ('@/components/my/egameTouZhu')
const activityCenter = ()=>import ('@/components/index/activityCenter')
const zhangbian = ()=>import ('@/components/my/zhangbian')
const agentRebates = ()=>import ('@/components/my/daili/agentRebates')
const helpDetails = ()=>import ('@/components/my/msg/helpDetails')

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/', component: index, children: [//首页
			{ path: '/', component: index1 },
			{ path: '/index1',meta:{ keepAlive: true } , component: index1 },
			{ path: '/index2', component: index2 },
			{ path: '/index3', component: index3 },
			{ path: '/index4', component: index4 },
		]
    },
    { path: '/activeDetails',component: activeDetails},//活动详情
    { path: '/login',component: login},//登陆
    { path: '/download',component: download},//APP下载
    { path: '/recharge',component: recharge},//充值
    { path: '/tixian',component: tixian},//提现
    { path: '/jiaoyi',component: jiaoyi},//交易记录
    { path: '/touzhu',component: touzhu},//投注记录
    { path: '/details',component: details},//注单详情
    { path: '/information',component: information},//个人信息
    { path: '/bindCard',component: bindCard},//绑定银行卡
    { path: '/anquan',component: anquan},//安全中心
    { path: '/daili',component: daili},//代理中心
    { path: '/dlExplain',component: dlExplain},//代理说明
    { path: '/agentRebates',component: agentRebates},//代理返点
    { path: '/teamTj',component: teamTj},//团队统计
    { path: '/vipAdmin',component: vipAdmin},//会员管理
    { path: '/xjRegister',component: xjRegister},//下级开户
    { path: '/edit',component: edit},//修改推广链接
    { path: '/yjtc',component: yjtc},//业绩提成
    { path: '/yingkui',component: yingkui},//今日盈亏
    { path: '/msg',component: msg},//我的消息
    { path: '/letterDetails',component: letterDetails},//站内信详情
    { path: '/noticeDetails',component: noticeDetails},//站内信详情
    { path: '/setLoginPwd',component: setLoginPwd},//修改登陆密码/取款密码
    { path: '/sycz',component: sycz},//所有彩种
    { path: '/ssc',component: ssc},//时时彩
    { path: '/dpc',component: dpc},//低频彩
    { path: '/pk10',component: pk10},//pk10
    { path: '/syxw',component: syxw},//11选5
    { path: '/xgc',component: xgc},//香港彩
    { path: '/ks',component: ks},//快三
    { path: '/klsf',component: klsf},//快乐十分
    { path: '/pcdd',component: pcdd},//pc蛋蛋
    { path: '/moreSsc',component: moreSsc},//时时彩更多历史记录
    { path: '/moreKs',component: moreKs},//时时彩更多历史记录
    { path: '/kefu',component: kefu},//客服
    { path: '/agentRegister',component: agentRegister},//代理注册
    { path: '/realTouZhu',component: realTouZHu},//代理注册
    { path: '/realGame',component: realGame},// 真人额度转换
    { path: '/games',component: games},// 真人额度转换
    { path: '/xbSport',component: xbSport},// 体育界面
    { path: '/basketBall',component: basketBall},// 信博篮球
    { path: '/match',component: match},// 体育赛事详情
    { path: '/chat',component: chat},// 聊天室
    { path: '/privateChat',component: privateChat},// 私聊
    { path: '/adminList',component: adminList},// 私聊管理员
    { path: '/allTouzhu',component: allTouzhu},// 投注记录列表
    { path: '/sanfangLottery',component: sanfangLottery},// 三方彩票投注记录
    { path: '/sportTouzhu',component: sportTouzhu},// 体育投注记录
    { path: '/sportDetail',component: sportDetail},// 体育投注记录详细
    { path: '/chessTouzhu',component: chessTouzhu},// 棋牌游戏投注记录
    { path: '/esportTouZhu',component: esportTouZhu},// 电竞游戏投注记录
    { path: '/esportTouZhuDetails',component: esportTouZhuDetails},// 电竞游戏投注记录详情
    { path: '/helpCentre',component: helpCentre},// 帮助中心
    { path: '/helpDetails',component: helpDetails},// 帮助中心
    { path: '/egameTouzhu',component: egameTouzhu},// 电子游戏投注记录
    { path: '/activityCenter',component: activityCenter},// 活动中心
    { path: '/chat/userInfo',component: userInfo},// 聊天室用户信息
    { path: '/zhangbian',component: zhangbian},// 帐变记录
    { path: '/integral',component: integral},// 积分兑换
  ]
})
