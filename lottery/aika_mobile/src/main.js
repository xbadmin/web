import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import Qs from 'qs'
import Vant from 'vant';
import { Lazyload } from 'vant';
import 'vant/lib/index.css';
import fastclick from 'fastclick'
import VueResource from 'vue-resource'
import commoncss from '../static/common.css'
import commonjs from '../static/common.js'
import $ from 'jquery'
import store from './store'
import VueClipboard from 'vue-clipboard2'
import VueCookies from 'vue-cookies'


Vue.use(VueCookies)
Vue.use(VueResource);
Vue.use(VueClipboard)
Vue.use(Vant);
Vue.use(Lazyload, {
  // loading: '../static/img/index/pic.png'
  error: '../static/img/index/pic.png'
});


// Vue.config.productionTip = false;
window.axios = axios;

window.Qs = Qs;
fastclick.attach(document.body)
/* eslint-disable no-new */
new Vue({
  data: function(){
    return {
      //  全局变量
      lotteryBetModel: 111,  //圆角分
    }
  },
  mounted(){
    if(localStorage.lotteryBetModel) this.$root.lotteryBetModel = localStorage.getItem("lotteryBetModel")
    let balanceIs = sessionStorage.getItem("autoTrans");
    if(balanceIs == 'true' || balanceIs == null){
      $.ajax({
        url:'/native/checkLastThirdBalance.do',
        type:'post',
        xhrFields : {
          withCredentials : true
        },
        success:function (res) {
          if(res.success){
            sessionStorage.setItem("autoTrans", "false");
          }
        }
      })
    }
  },
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
