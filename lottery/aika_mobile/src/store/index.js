import Vue from 'vue' // 引入vue
import Vuex from 'vuex' // 引入vuex
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    chatSwitch:window.sessionStorage.getItem('chatSwitch'), // 聊天室开关
    newChatMsg: '进入聊天大厅',// 聊天大厅的最新信息
    privateChatList:[],
    privateChat:[], // 私聊列表
    socket: null, //聊天室socket
    nickname: '',
    GuestSwitch: true,
    showRegister:window.sessionStorage.getItem('showRegister'),
    domainUrl:window.sessionStorage.getItem('domainUrl'),
    qhbSwitchKey:false, //红包开关
    b22102Show:false,// b22102判断
    b00201Show:false,// b00201判断z
    stationInfo:{}, //网站开关
    webSiteInfo:{}, //网站基础信息
    floder:'' //站点编号
  },
  getters:{
    getPrivateChat: (state) => state.privateChat,

  },
  plugins: [createPersistedState({storage: window.sessionStorage})],
  actions:{
    clearPrivateChat({commit}) {
      this.state.privateChat = []
    },
    setChatMsg({commit}, newmsg) {
      this.state.newChatMsg = newmsg
    },
    addPrivateChat({commit}, chat){
      if(this.state.privateChatList.indexOf(chat.chat.form) === -1){
        this.state.privateChatList.push(chat.chat.form);
        this.state.privateChat.push({
          name: chat.chat.name,
          content:[chat]
        })
      }else {
        this.state.privateChat.forEach(function (item) {
          if(item.name === chat.chat.form){
            item.content.push(chat)
          }
        });
      }
    }
  },
  mutations:{
    setChatSwitch: (state, data) => {
      state.chatSwitch = data;
      window.sessionStorage.setItem('chatSwitch', data);
    },
    setShowRegister: (state, data) => {
      state.showRegister = data;
      window.sessionStorage.setItem('showRegister', data);
    },
    changeQhbSwitchKey:(state , data)=>{
      state.qhbSwitchKey = data;
  },
    getLocation22102Show:(state,data)=>{
        state.b22102Show = data
    },
    getLocationb00201Show:(state,data)=>{
        state.b00201Show = data
    },
    getStationInfo:(state,data)=>{
      state.stationInfo = data
    },
    getFloder:(state,data)=>{
      state.floder = data.content.station.floder
      state.webSiteInfo = data
    },
  }

});

export default store // 导出store
