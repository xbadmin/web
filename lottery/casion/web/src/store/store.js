import Vue from 'vue'//引入vue
import Vuex from 'vuex'//引入vuex
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        apiUrl: location.hostname == 'localhost' ? '/api/' : location.protocol + '//' + location.hostname + '/',
        isLogin: false,
        userInfo: '',
        vipData: '',
        stationInfo:'', //  网站基础信息
        beforeLotteryData:[], // 投注页面彩种切换
        stationSwitch:'', //  网站开关
    },
    plugins: [createPersistedState({storage: window.sessionStorage})],
    mutations: {
        pushIsLogin(state,e){
            state.isLogin = e
        },
        pushUserInfo(state,e){
            state.userInfo = e
        },
        pushStationInfo(state,e){
            state.stationInfo = e
        },
        pushStationSwitch(state,e){
            state.stationSwitch = e
        },
        pushVipData(state,e){
            state.vipData = e
        },
        pushBeforeLotteryData(state,e){
            state.beforeLotteryData.push(e)
        },
    }
});


export default store
