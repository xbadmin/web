import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/tabs/index")
const active = ()=>import("@/components/tabs/active")
const wallet = ()=>import("@/components/tabs/wallet")
const service = ()=>import("@/components/tabs/service")

const login = ()=>import("@/components/login")
const register = ()=>import("@/components/register")

const account = ()=>import("@/components/account")
const funds = ()=>import("@/components/funds")
const record = ()=>import("@/components/record")
const vip = ()=>import("@/components/vip")
const language = ()=>import("@/components/language")
const help = ()=>import("@/components/help")

const gameClassify = ()=>import("@/components/gameClassify")
const lottery = ()=>import("@/components/lottery/lottery")

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: false,requireAuth:false},}, // 首页
        {path: '/active', name:'active', component:active, meta: {keepAlive: true,requireAuth:false},}, // 优惠活动
        {path: '/wallet', name:'wallet', component:wallet, meta: {keepAlive: true,requireAuth:true},}, // 钱包
        {path: '/service', name:'service', component:service, meta: {keepAlive: false,requireAuth:false},}, // 在线客服

        // 登录注册
        {path: '/login', name:'login', component:login, meta: {keepAlive: true,requireAuth:false},}, // 登录
        {path: '/register', name:'register', component:register, meta: {keepAlive: true,requireAuth:false},}, // 注册

        // 二级页面
        {path: '/account', name:'account', component:account, meta: {keepAlive: false,requireAuth:false},}, // 账户
        {path: '/funds', name:'funds', component:funds, meta: {keepAlive: false,requireAuth:false},}, // 记录
        {path: '/record', name:'record', component:record, meta: {keepAlive: false,requireAuth:false},}, // 投注
        {path: '/vip', name:'vip', component:vip, meta: {keepAlive: false,requireAuth:false},}, // 会员等级
        {path: '/language', name:'language', component:language, meta: {keepAlive: false,requireAuth:false},}, // 语言设置
        {path: '/help', name:'help', component:help, meta: {keepAlive: false,requireAuth:false},}, // 用户协助

        //游戏
        {path: '/gameClassify', name:'gameClassify', component:gameClassify, meta: {keepAlive: false,requireAuth:true},}, // 游戏分类页面
        {path: '/lottery', name:'lottery', component:lottery, meta: {keepAlive: false,requireAuth:true},}, // 彩票游戏

    ]
})

