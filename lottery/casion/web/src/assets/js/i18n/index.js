import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n)

// 引入语言配置文件
import zh from './config/zh';
import en from './config/en';
import vn from './config/vn';

// 创建实例
const i18n = new VueI18n({
  // 设置默认语言
  locale: localStorage.getItem('locale') || 'zh',
  // 添加多语言
  messages:{
    zh: zh,
    en: en,
    vn: vn,
  }
})
// 暴露i18n
export default  i18n;
