import $ from  'jquery'
import { Toast } from 'vant';
// import store from '../../store/store.js'
// import router from '../../router/router'

// 验证返回结果
let validateIs = true
export default{
    loading:function (onOff) {
        if(onOff){
            Toast.loading({
                message: '加载中...',
                forbidClick: true,
            });
        } else {
            Toast.clear()
        }
    },
    validateCodex:function(data){
        // 用于跳出循环
        let circleIs = true
        let xss = /xss/i , script = /script/i , blank = /\s/
        if(xss.test(data) || script.test(data)){
            alert('包含敏感字符，请重新输入!');
            validateIs = false
            circleIs = false
        } else if(blank.test(data)){
            alert('请勿包含空格等!');
            validateIs = false
            circleIs = false
        } else if(!data){
            alert('请输入内容!');
            validateIs = false
            circleIs = false
        } else {
            validateIs = true
        }
        return circleIs
    },
    // 前端非空,xss,特殊字符验证
    dataSafeValidate:function (type,data) {
        let that = this
        switch (type) {
            case 1:
                //字符串类型
                if(!that.validateCodex(data)) return false
                break;
            case 2:
                // 数组类型
                $.each(data,function (index,item) {
                    if(!that.validateCodex(item)) return false
                });
                break;
            default:
                console.log('暂无可执行语句')
        }
        return validateIs
    },
    addFavicon:function (floder){
        let favicon = ''
        switch (floder) {
            case 't006':
                favicon = 'https://tpxb.me/x002104/1111.png'
                break;
        }
        var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
        link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = favicon;
        document.getElementsByTagName('head')[0].appendChild(link);
    },
    // 查询日期
    dataSearch:function (num) {
        let year = new Date().getYear()+1900, month = new Date().getMonth() + 1, day = new Date().getDate() , lastMonthDay = new Date(year, month -1, 0).getDate();
        let result = {
            startTime:'',
            endTime:''
        }
        let lastDay = day - parseInt(num)
        if(lastDay < 1){
            if(month - 1 != 0){
                result.startTime = year + '-' + (month - 1) +'-' + (lastMonthDay + lastDay) + " 00:00:00"
                result.endTime = year + '-' + month +'-' + day + " 23:59:59"
            } else {
                result.startTime = (year - 1) + '-' + 12 +'-' + (lastMonthDay + lastDay) + " 00:00:00"
                result.endTime = year + '-' + month +'-' + day + " 23:59:59"
            }
        } else {
            result.startTime = year + '-' + month +'-' + lastDay+ " 00:00:00"
            result.endTime = year + '-' + month +'-' + day+ " 23:59:59"
        }
        return result
    },
    //解析毫秒1
    milliDate:function (e){
        let date = new Date(e)
        return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDay()
    },
    //解析毫秒2
    getMyDate:function (str) {
        let oDate = new Date(str),
            oYear = oDate.getFullYear(),
            oMonth = oDate.getMonth()+1,
            oDay = oDate.getDate(),
            oHour = oDate.getHours(),
            oMin = oDate.getMinutes(),
            oSen = oDate.getSeconds(),
            oTime = oYear +'-'+ this.getzf(oMonth) +'-'+ this.getzf(oDay) +' '+ this.getzf(oHour) +':'+ this.getzf(oMin) +':'+this.getzf(oSen);//最后拼接时间
        return oTime ;
    },
    getzf:function (num) {
        if(parseInt(num) < 10){
            num = '0'+num;
        }
        return num;
    },
    filterPayType:function (type){
        switch (type){
            case '3':
                return 'WEIXIN'
            case '4':
                return 'ALIPAY'
            case '5':
                return 'QQPAY'
            case '6':
                return 'JDPAY'
            case '7':
                return 'BAIDU'
            case '8':
                return 'UNION'
            case '9':
                return 'UNIONPAY'
            case '10':
                return 'QQPAYBARCODE'
            case '11':
                return 'WEIXINH5BARCODE'
            case '12':
                return 'ALIH5BARCODE'
            case '13':
                return 'ALIPDD'
        }
    },
}
