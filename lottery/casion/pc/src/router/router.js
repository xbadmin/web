import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/index")
const phone = ()=>import("@/components/phone")
const sport = ()=>import("@/components/sport")
const real = ()=>import("@/components/real")
const egame = ()=>import("@/components/egame")
const games = ()=>import("@/components/games")
const chess = ()=>import("@/components/chess")
const active = ()=>import("@/components/active")
const help = ()=>import("@/components/help")
const register = ()=>import("@/components/register")
const message = ()=>import("@/components/message")

const personal = ()=>import("@/components/center/personal")
const recharge = ()=>import("@/components/center/recharge")
const drawMoney = ()=>import("@/components/center/drawMoney")
const integral = ()=>import("@/components/center/integral")
const businessDetail = ()=>import("@/components/center/businessDetail")
const yuebao = ()=>import("@/components/center/yuebao")
const lotteryRecords = ()=>import("@/components/center/lotteryRecords")
const sportRecords = ()=>import("@/components/center/sportRecords")
const gameRecords = ()=>import("@/components/center/gameRecords")
const realRecords = ()=>import("@/components/center/realRecords")
const chessRecords = ()=>import("@/components/center/chessRecords")

const pay = ()=>import("@/components/center/pay/pay")
const payBank = ()=>import("@/components/center/pay/payBank")



Vue.use(Router)

export default new Router({
    routes: [
        //keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: true,requireAuth:false},}, // 首页
        {path: '/phone', name:'phone', component:phone, meta: {keepAlive: true,requireAuth:false},}, // APP下载
        {path: '/sport', name:'sport', component:sport, meta: {keepAlive: true,requireAuth:false},}, // 体育运动
        {path: '/real', name:'real', component:real, meta: {keepAlive: true,requireAuth:false},}, // 真人
        {path: '/egame', name:'egame', component:egame, meta: {keepAlive: true,requireAuth:false},}, // 电子
        {path: '/games', name:'games', component:games, meta: {keepAlive: true,requireAuth:false},}, // 彩票
        {path: '/chess', name:'chess', component:chess, meta: {keepAlive: true,requireAuth:false},}, // 棋牌
        {path: '/active', name:'active', component:active, meta: {keepAlive: true,requireAuth:false},}, // 优惠活动
        {path: '/help', name:'help', component:help, meta: {keepAlive: true,requireAuth:false},}, // 帮助
        {path: '/register', name:'register', component:register, meta: {keepAlive: true,requireAuth:false},}, // 注册
        {path: '/message', name:'message', component:message, meta: {keepAlive: true,requireAuth:true},}, // 站内信

        // 个人中心
        {path: '/personal', name:'personal', component:personal, meta: {keepAlive: false,requireAuth:true},}, // 个人中心
        {path: '/recharge', name:'recharge', component:recharge, meta: {keepAlive: false,requireAuth:true},}, // 充值
        {path: '/drawMoney', name:'drawMoney', component:drawMoney, meta: {keepAlive: false,requireAuth:true},}, // 在线提款
        {path: '/integral', name:'integral', component:integral, meta: {keepAlive: false,requireAuth:true},}, // 积分兑换
        {path: '/businessDetail', name:'businessDetail', component:businessDetail, meta: {keepAlive: false,requireAuth:true},}, // 明细查询
        {path: '/yuebao', name:'yuebao', component:yuebao, meta: {keepAlive: false,requireAuth:true},}, // 余额宝
        {path: '/lotteryRecords', name:'lotteryRecords', component:lotteryRecords, meta: {keepAlive: false,requireAuth:true},}, // 彩票投注记录
        {path: '/sportRecords', name:'sportRecords', component:sportRecords, meta: {keepAlive: false,requireAuth:true},}, // 体育投注记录
        {path: '/gameRecords', name:'gameRecords', component:gameRecords, meta: {keepAlive: false,requireAuth:true},}, // 电子投注记录
        {path: '/realRecords', name:'realRecords', component:realRecords, meta: {keepAlive: false,requireAuth:true},}, // 真人投注记录
        {path: '/chessRecords', name:'chessRecords', component:chessRecords, meta: {keepAlive: false,requireAuth:true},}, // 棋牌投注记录

        //充值页面
        {path: '/pay', name:'pay', component:pay, meta: {keepAlive: false,requireAuth:true},}, // 扫码支付
        {path: '/payBank', name:'payBank', component:payBank, meta: {keepAlive: false,requireAuth:true},}, // 银行支付

    ]
})

