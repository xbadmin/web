import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store/store.js'
import i18n from './assets/i18n/'
import common from './assets/js/common.js'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import {post,fetch,patch,put} from '@/assets/js/http'
import VueCookies from 'vue-cookies'

Vue.config.productionTip = false
Vue.prototype.$post = post;
Vue.prototype.$fetch = fetch;
Vue.prototype.$patch = patch;
Vue.prototype.$put = put;

Vue.use(VueCookies)
// vant html组件
import { Swipe, SwipeItem } from 'vant';

Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(ElementUI);

router.beforeEach((to, from, next) => {
  if(to.meta.requireAuth){
    if(store.state.isLogin){
      next()
    } else {
      next({path: '/', query:{ redirect: to.fullPath}})
    }
  } else {
    next()
  }
})

new Vue({
  data: function(){
    return {
      //  全局变量  刷新后初始化
      common: common, // common方法
    }
  },
  render: h => h(App),
  router,
  store,
  i18n,
}).$mount('#app')
