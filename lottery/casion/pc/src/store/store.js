import Vue from 'vue'//引入vue
import Vuex from 'vuex'//引入vuex
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        apiUrl: location.hostname == 'localhost' ? '/api/' : location.protocol + '//' + location.hostname + '/',
        isLogin: false,
        userInfo: '',
        stationInfo:'', //  网站基础信息
        stationSwitch:'', //  网站开关
        money:'',
        notice:'',
        serviceUrl:'',
        languageList:[],
    },
    plugins: [createPersistedState({storage: window.sessionStorage})],
    mutations: {
        pushIsLogin(state,e){
            state.isLogin = e
        },
        pushUserInfo(state,e){
            state.userInfo = e
        },
        pushStationInfo(state,e){
            state.stationInfo = e
        },
        pushStationSwitch(state,e){
            state.stationSwitch = e
        },
        pushMoney(state,e){
            state.money = parseFloat(e).toFixed(2)
        },
        pushNotice(state,e){
            state.notice = e
        },
        pushServiceUrl(state,e){
            state.serviceUrl = e
        },
        pushLanguageList(state,e){
            state.languageList = e
        },
    }
});


export default store
