import Vue from 'vue'
import common from '@/assets/js/common.js'
import store from '../../store/store.js'
let vue = new Vue()

export function http_ajax (method, url, params,loading, dom) {
    url = store.state.apiUrl + url
    if(loading)common.loading(loading,dom)
    return new Promise((resolve, reject) => {
        vue.$http({
            url: url,
            params: params,
            headers:{
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            credentials: true,
            method: method
        }).then(response => {
            if(loading)common.loading(false,dom)
            if(response.body.msg == '请先登陆' && window.location.href.indexOf('gister') == -1){
                common.isLoginSession(false)
            } else {
                resolve(response.body)
            }
        }, response => {
            if(loading)common.loading(false,dom)
            reject(response)
        })
    })
}
