import Vue from 'vue'
import Router from 'vue-router'
const index = ()=>import("@/components/index")
const login = ()=>import ("@/components/login");
const register = ()=>import ("@/components/register");
const agentRegister = ()=>import ("@/components/agentRegister");
const lobby = ()=>import ("@/components/lobby");
const active = ()=>import ("@/components/active");
const help = ()=>import ("@/components/help");
const phone = ()=>import ("@/components/phone");
const memberInfo = ()=>import ("@/components/center/memberInfo");
const securityCenter = ()=>import ("@/components/center/securityCenter");
const manageBankcard = ()=>import ("@/components/center/manageBankcard");
const lotteryRecords = ()=>import ("@/components/center/lotteryRecords");
const chessRecords = ()=>import ("@/components/center/chessRecords");
const sportRecords = ()=>import ("@/components/center/sportRecords");
const realRecords = ()=>import ("@/components/center/realRecords");
const gameRecords = ()=>import ("@/components/center/gameRecords");
const accountDetail = ()=>import ("@/components/center/accountDetail");
const yuEBao = ()=>import ("@/components/center/yuEBao");
const agentInfo = ()=>import ("@/components/center/agentInfo");
const teamChange = ()=>import ("@/components/center/teamChange");
const memberSupervise = ()=>import ("@/components/center/memberSupervise");
const cathecticMinute = ()=>import ("@/components/center/cathecticMinute");
const levelAccount = ()=>import ("@/components/center/levelAccount");
const achievement = ()=>import ("@/components/center/achievement");
const businessDetail = ()=>import ("@/components/center/businessDetail");
const recharge = ()=>import ("@/components/center/recharge");
const rechargeIframe = ()=>import ("@/components/center/rechargeIframe");
const drawMoney = ()=>import ("@/components/center/drawMoney");
const change = ()=>import ("@/components/center/change");
const integral = ()=>import ("@/components/center/integral");
const message = ()=>import ("@/components/center/message");
const notice = ()=>import ("@/components/center/notice");
const setBankcard = ()=>import ("@/components/center/child/setBankcard");
const updateUserPass = ()=>import ("@/components/center/child/updateUserPass");
const lottery = ()=>import ("@/components/lottery/lottery");
const sport = ()=>import ("@/components/sport");
const real = ()=>import ("@/components/real");
const chess = ()=>import ("@/components/chess");
const esport = ()=>import ("@/components/esport");
const chat =()=>import("@/components/chat");
const egame = ()=>import ("@/components/egame");
const ie = ()=>import ("@/components/ie");

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: true,requireAuth:false},}, // 首页
        {path: '/login', name:'login', component:login, meta: {keepAlive: false,requireAuth:false},}, // 登录页
        {path: '/register', name:'register', component:register, meta: {keepAlive: false,requireAuth:false},}, // 注册页
        {path: '/agentRegister', name:'agentRegister', component:agentRegister, meta: {keepAlive: false,requireAuth:false},}, // 注册页
        {path: '/lobby', name:'lobby', component:lobby, meta: {keepAlive: true,requireAuth:false},}, // 大厅
        {path: '/active', name:'active', component:active, meta: {keepAlive: true,requireAuth:false},}, // 互动中心
        {path: '/help', name:'help',component:help,meta: {keepAlive: true,requireAuth:false},},          //帮助指南
        {path: '/phone', name:'phone',component:phone,meta: {keepAlive: true,requireAuth: false},},        //手机购彩
        {path: '/memberInfo', name:'memberInfo',component:memberInfo,meta: {keepAlive: true,requireAuth: true},},        //个人信息
        {path: '/securityCenter', name:'securityCenter',component:securityCenter,meta: {keepAlive: true,requireAuth: true},},        //安全中心
        {path: '/manageBankcard', name:'manageBankcard',component:manageBankcard,meta: {keepAlive: true,requireAuth: true},},        //银行卡管理
        {path: '/lotteryRecords', name:'lotteryRecords',component:lotteryRecords,meta: {keepAlive: false,requireAuth: true},},        //彩票投注记录
        {path: '/chessRecords', name:'chessRecords',component:chessRecords,meta: {keepAlive: false,requireAuth: true},},        //棋牌投注记录
        {path: '/sportRecords', name:'sportRecords',component:sportRecords,meta: {keepAlive: false,requireAuth: true},},        //体育投注记录
        {path: '/realRecords', name:'realRecords',component:realRecords,meta: {keepAlive: false,requireAuth: true},},        //真人投注记录
        {path: '/gameRecords', name:'gameRecords',component:gameRecords,meta: {keepAlive: false,requireAuth: true},},        //电子投注记录
        {path: '/accountDetail', name:'accountDetail',component:accountDetail,meta: {keepAlive: false,requireAuth: true},},        //账变明细
        {path: '/yuEBao', name:'yuEBao',component:yuEBao,meta: {keepAlive: false,requireAuth: true},},        //余额宝
        {path: '/businessDetail', name:'businessDetail',component:businessDetail,meta: {keepAlive: false,requireAuth: true},},        //交易记录
        {path: '/recharge', name:'recharge',component:recharge,meta: {keepAlive: true,requireAuth: true},},        //充值
        {path: '/rechargeIframe', name:'rechargeIframe',component:rechargeIframe,meta: {keepAlive: true,requireAuth: true},},        //充值
        {path: '/drawMoney', name:'drawMoney',component:drawMoney,meta: {keepAlive: true,requireAuth: true},},        //提款
        {path: '/change', name:'change',component:change,meta: {keepAlive: true,requireAuth: true},},        //额度转换
        {path: '/integral', name:'integral',component:integral,meta: {keepAlive: true,requireAuth: true},},        //积分兑换
        {path: '/message', name:'message',component:message,meta: {keepAlive: true,requireAuth: true},},        //消息中心
        {path: '/notice', name:'notice',component:notice,meta: {keepAlive: true,requireAuth: true},},        //消息中心
        {path: '/setBankcard', name:'setBankcard',component:setBankcard,meta: {keepAlive: true,requireAuth: true},},        //添加银行卡
        {path: '/updateUserPass', name:'updateUserPass',component:updateUserPass,meta: {keepAlive: false,requireAuth: true},},        //修改用户密码
        {path: '/agentInfo', name:'agentInfo',component:agentInfo,meta: {keepAlive: true,requireAuth: true},},        //代理说明
        {path: '/teamChange', name:'teamChange',component:teamChange,meta: {keepAlive: false,requireAuth: false},},        //团队统计
        {path: '/memberSupervise', name:'memberSupervise',component:memberSupervise,meta: {keepAlive: false,requireAuth: true},},        //会员管理
        {path: '/cathecticMinute', name:'cathecticMinute',component:cathecticMinute,meta: {keepAlive: false,requireAuth: true},},        //投注明细
        {path: '/levelAccount', name:'levelAccount',component:levelAccount,meta: {keepAlive: false,requireAuth: true},},        //下级开户
        {path: '/achievement', name:'achievement',component:achievement,meta: {keepAlive: false,requireAuth: true},},        //业绩提成
        // 彩票页面
        {path: '/lottery', name:'lottery',component:lottery,meta: {keepAlive: false,requireAuth: true},},        //投注页面
        // 三方游戏页面
        {path: '/sport', name:'sport',component:sport,meta: {keepAlive: false,requireAuth: false},},        //体育赛事
        {path: '/real', name:'real',component:real,meta: {keepAlive: false,requireAuth: false},},        //真人
        {path: '/chess', name:'chess',component:chess,meta: {keepAlive: false,requireAuth: false},},        //棋牌
        {path: '/esport', name:'esport',component:esport,meta: {keepAlive: false,requireAuth: false},},        //电竞
        {path: '/egame', name:'egame',component:egame,meta: {keepAlive: false,requireAuth: false},},        //电子游戏
        {path: '/chat', name:'chat',component:chat,meta: {keepAlive: false,requireAuth: false},},        //电子游戏

        // other
        {path: '/ie', name:'ie',component:ie,meta: {keepAlive: false,requireAuth: false},},        //ie
    ]
})
