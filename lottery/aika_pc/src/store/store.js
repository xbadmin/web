import Vue from 'vue'//引入vue
import Vuex from 'vuex'//引入vuex
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        apiUrl: location.hostname == 'localhost' ? '/api/' : location.protocol + '//' + location.hostname + '/',
        isLogin: false,
        userInfo: '',
        money:'',
        rollDraw:'',
        version:5, //彩票版本
        buyLotteryModel:false,
        switch:'', // 开关列表
        navList:'', // 导航菜单列表
        stationInfo:'', // 网站信息
        indexLotteryList:'', // 首页彩种列表
        beforeLotteryData:[], // 投注页面彩种切换
        balanceGemSwitch:false, // 余额宝开关
        serviceUrl:'', // 在线客服
        serviceUrlTime:{end:"23:59",start:"00:00"},
        exchange:false,//积分兑换
        chatData:'',
        escapeList:'',
        moneyTypePc:['1','2','3','4','5','6','大','小','单','双'],
        moneyTypeDefault:['1','2','3','4','5','6','大','小','单','双'],
        moneyTypeInfoPc:[{name:'大',showName:'大'},{name:'小',showName:'小'},{name:'单',showName:'单'},{name:'双',showName:'双'},],
    },
    // plugins: [createPersistedState()],
    plugins: [createPersistedState({storage: window.sessionStorage})],
    mutations: {
        loginOn: state => state.isLogin = true,
        loginOff: state => state.isLogin = false,
        Balance: state => state.balanceGemSwitch = true,
        pushSwitch(state,e){
            state.switch = e
        },
        pushNavList(state,e){
            state.navList = e
        },
        pushStationInfo(state,e){
            state.stationInfo = e
        },
        pushIndexLotteryList(state,e){
            state.indexLotteryList = e
        },
        pushBeforeLotteryData(state,e){
            state.beforeLotteryData.push(e)
        },
        getExchangeDate(state,e){
            state.exchange =e
        },
        pushPlatChat(state,e){
            state.chatData = e
        },
        pushMoneyTypePc(state,e){
            state.moneyTypePc = e
        },
        pushMoneyTypeDefault(state,e){
            state.moneyTypeDefault = e
        },
        pushMoneyTypeInfoPc(state,e){
            state.moneyTypeInfoPc = e
        },
        pushEscapeList(state,e){
            state.escapeList = e
        },
    }
});

export default  store
