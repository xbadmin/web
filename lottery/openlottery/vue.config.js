const webpack = require('webpack')
module.exports = {
    publicPath :process.env.NODE_ENV === 'production' ?'/common/template/lottery/open/':'/',
    // 打包去掉map文件
    productionSourceMap:false,
}
