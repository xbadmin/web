import Vue from 'vue'
import Router from 'vue-router'
import histroy from "@/components/histroy";
import index from "@/components/index"

Vue.use(Router)

export default new Router({
    routes:[
        {path:"/",name:"index",component:index},
        {path:"/histroy",name:"histroy",component:histroy},
    ]
})