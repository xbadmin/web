import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router/index'
import { CountDown } from 'vant';
/*引入资源请求插件*/
import VueResource from 'vue-resource'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(CountDown)
Vue.use(VueResource)

global.url = (location.host.indexOf('localhost')) !== -1 ? 'https:x002.xb336.com' : location.protocol + '//' + location.host

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
