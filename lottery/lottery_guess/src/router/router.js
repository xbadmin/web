import Vue from 'vue'
import Router from 'vue-router'

const index = ()=>import("@/components/index")
const login = ()=>import("@/components/login")
const register = ()=>import("@/components/register")
const draw_notice = ()=>import("@/components/draw_notice")
const draw_notice_details = ()=>import("@/components/draw_notice_details")
const lotteryHall = ()=>import("@/components/lotteryHall")

//
const interact = ()=>import("@/components/interact")
const personal = ()=>import("@/components/personal")
const withdrawDeposit = ()=>import("@/components/center/withdrawDeposit")
const bindCard = ()=>import("@/components/center/bindCard")
const lottery_record = ()=>import('@/components/center/lottery_record')
const sixmark_record = ()=>import('@/components/center/sixmark_record')
const website_message = ()=>import('@/components/center/website_message')
const account_details = ()=>import('@/components/center/account_details')
const upd_password = ()=>import('@/components/center/upd_password')
const mnyrecord = ()=>import('@/components/center/mnyrecord')
const backwater = ()=>import('@/components/center/backwater')
const awardRecord = ()=>import('@/components/center/awardRecord')
const scoreChange = ()=>import('@/components/center/scoreChange')
const record_chess = ()=>import('@/components/center/record_chess')
const live_record = ()=>import('@/components/center/live_record')
const egame_record = ()=>import('@/components/center/egame_record')
const sport_record = ()=>import('@/components/center/sport_record')
const lottery_third_record = ()=>import('@/components/center/lottery_third_record')
const pay_select_way = ()=>import("@/components/center/pay_select_way")
const usdt_address = ()=>import("@/components/center/usdt_address")

const service = ()=>import("@/components/service")
const active = ()=>import("@/components/active")
const appDownload = ()=>import("@/components/appDownload")
const serviceCode = ()=>import("@/components/serviceCode")
const lottery = ()=>import("@/components/lottery/lottery")

Vue.use(Router)

export default new Router({
    routes: [
        //主页面 keepAlive 页面缓存 requireAuth登录状态
        {path: '/', name:'index', component:index, meta: {keepAlive: false,requireAuth:false},}, // 首页
        {path: '/login', name:'login', component:login, meta: {keepAlive: false,requireAuth:false},}, // 登录页
        {path: '/register', name:'register', component:register, meta: {keepAlive: false,requireAuth:false},}, // 注册页
        {path: '/draw_notice', name:'draw_notice', component:draw_notice, meta: {keepAlive: false,requireAuth:false},}, // 开奖公告
        {path: '/draw_notice_details', name:'draw_notice_details', component:draw_notice_details, meta: {keepAlive: false,requireAuth:false},}, // 开奖详情
        {path: '/interact', name:'interact', component:interact, meta: {keepAlive: true,requireAuth:false},}, // 互动中心
        {path: '/lotteryHall', name:'lotteryHall', component:lotteryHall, meta: {keepAlive: true,requireAuth:false},}, // 竞猜大厅

        {path: '/active', name:'active', component:active, meta: {keepAlive: true,requireAuth:false},}, // 优惠活动
        {path: '/service', name:'service', component:service, meta: {keepAlive: true,requireAuth:false},}, // 在线客服
        {path: '/appDownload', name:'appDownload', component:appDownload, meta: {keepAlive: true,requireAuth:false},}, // APP下载
        {path: '/serviceCode', name:'serviceCode', component:serviceCode, meta: {keepAlive: true,requireAuth:false},}, // 客服二维码
        // 彩票页面
        {path: '/lottery', name:'lottery', component:lottery, meta: {keepAlive: false,requireAuth:true},},
        // 个人中心
        {path: '/personal', name:'personal', component:personal, meta: {keepAlive: false,requireAuth:true},}, // 个人中心
        {path: '/bindCard', name:'bindCard', component:bindCard, meta: {keepAlive: false,requireAuth:true},}, // 银行卡信息
        {path: '/withdrawDeposit',name:'withdrawDeposit',component:withdrawDeposit, meta: {keepAlive: false,requireAuth:true},},//提款
        {path:'/lottery_record',name:'lottery_record',component:lottery_record,meta: {keepAlive: false,requireAuth:true},},//彩票
        {path:'/sixmark_record',name:'sixmark_record',component:sixmark_record,meta: {keepAlive: false,requireAuth:true},},//六合彩投注记录
        {path:'/website_message',name:'website_message',component:website_message,meta: {keepAlive: false,requireAuth:true},},//我的消息
        {path:'/account_details',name:'account_details',component:account_details,meta: {keepAlive: false,requireAuth:true},},//账户明细
        {path:'/upd_password',name:'upd_password',component:upd_password,meta: {keepAlive: false,requireAuth:true},},//修改提款，账户密码
        {path: '/mnyrecord',name:'mnyrecord',component:mnyrecord,meta: {keepAlive: false,requireAuth:true},},//用户账变记录
        {path: '/backwater',name:'backwater',component:backwater,meta: {keepAlive: false,requireAuth:true},},//反水记录
        {path: '/awardRecord',name:'awardRecord',component:awardRecord,meta: {keepAlive: false,requireAuth:true},},//活动中奖记录
        {path: '/scoreChange',name:'scoreChange',component:scoreChange,meta: {keepAlive: false,requireAuth:true},},//积分兑换
        {path: '/record_chess',name:'record_chess',component:record_chess,meta: {keepAlive: false,requireAuth:true},},//棋牌投注
        {path: '/live_record',name:'live_record',component:live_record,meta: {keepAlive: false,requireAuth:true},},//真人投注
        {path: '/egame_record',name:'egame_record',component:egame_record,meta: {keepAlive: false,requireAuth:true},},//电子投注记录
        {path: '/sport_record',name:'sport_record',component:sport_record,meta: {keepAlive: false,requireAuth:true},},//体育投注记录
        {path: '/lottery_third_record',name:'lottery_third_record',component:lottery_third_record,meta: {keepAlive: false,requireAuth:true},},//三方投注记录
        {path: '/pay_select_way',name:'pay_select_way',component:pay_select_way, meta: {keepAlive: false,requireAuth:true},},//充值
        {path: '/usdt_address',name:'usdt_address',component:usdt_address, meta: {keepAlive: false,requireAuth:true},},//钱包地址

    ]
})

